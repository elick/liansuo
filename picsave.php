<?php
/**
 * Note:for octet-stream upload
 * 这个是流式上传PHP文件
 * Please be amended accordingly based on the actual situation
 */
define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');

$user_id = empty($_GET['user_id'])? 0 : intval($_GET['user_id']);
$post_input = 'php://input';
$save_path = dirname( __FILE__ );
$postdata = file_get_contents( $post_input );

if ( isset( $postdata ) && strlen( $postdata ) > 0 ) {
	$portrait_url = 'data/users_portrait/' . uniqid() . '.jpg';
	$filename = $save_path .'/'. $portrait_url ;
	$handle = fopen( $filename, 'w+' );
	fwrite( $handle, $postdata );
	fclose( $handle );
	 
	if ( is_file( $filename ) ) {
		 $sql = 'UPDATE ' . $ecs->table('users') . " SET `portrait`='$portrait_url' WHERE `user_id`='" . $user_id . "'";
         $db->query($sql);
		
		echo "Image data save successed,file:";
		
		exit ();
	}else {
		die ( 'Image upload error!' );
	}
}else {
	die ( 'Image data not detected!' );
}
