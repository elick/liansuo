<?php
define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');


/*------------------------------------------------------ */
//-- 用户银行卡号列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'card_list')
{   admin_priv('users_manage');
 
	$card_list = bankcard_list();
    $smarty->assign('dbcards',      $card_list['card_list']);
    $smarty->assign('filter',       $card_list['filter']);
    $smarty->assign('record_count', $card_list['record_count']);
    $smarty->assign('page_count',   $card_list['page_count']);
    $smarty->assign('full_page',    1);
    $smarty->assign('sort_card_id', '<img src="images/sort_desc.gif">');

    assign_query_info();
	$smarty->display('bankcard_list.htm');

   
}

/*------------------------------------------------------ */
//-- ajax返回用户列表
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    $card_list = bankcard_list();
    $smarty->assign('dbcards',      $card_list['card_list']);
    $smarty->assign('filter',       $card_list['filter']);
    $smarty->assign('record_count', $card_list['record_count']);
    $smarty->assign('page_count',   $card_list['page_count']);

    $sort_flag  = sort_flag($card_list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('bankcard_list.htm'), '', array('filter' => $card_list['filter'], 'page_count' => $card_list['page_count']));
}
/*------------------------------------------------------ */
//-- 审核会银行卡
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'confirm_card')
{
    /* 检查权限 */
    admin_priv('surplus_manage');

    /* 初始化 */
    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;

    /* 如果参数不合法，返回 */
    if ($id == 0)
    {
        ecs_header("Location: users.php?act=card_list\n");
        exit;
    }

    /* 查询当前用户银行卡的信息 */
    $card_info = array();
    $card_info = $db->getRow("SELECT * FROM " .$ecs->table('users_card'). " WHERE card_id = '$id'");
    $card_info['add_time'] = local_date($_CFG['time_format'], $card_info['add_time']);

 
    /* 模板赋值 */
  
    $smarty->assign('card_info',      $card_info);
  

    /* 页面显示 */
    assign_query_info();
    $smarty->display('user_card_confirm.htm');
}

/*------------------------------------------------------ */
//-- 审核会银行卡
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'action_card')
{
    /* 检查权限 */
    admin_priv('surplus_manage');

    /* 初始化 */
    $id = isset($_POST['id']) ? intval($_POST['id']) : 0;
	$confirm = isset($_POST['confirm']) ? intval($_POST['confirm']) : 0;
	$admin_note = isset($_POST['admin_note']) ? trim($_POST['admin_note']) : 0;
    $time = time();

	if($confirm == 0){
	  $temp = 2 ;
	  }
	else $temp = 1;
	

     $sql = "UPDATE " .$ecs->table('users_card'). " SET ".
                   "admin_user    = '$_SESSION[admin_name]', ".
                   "admin_note    = '$admin_note', ".
				   "edit_time    = '$time', ".
                   "validate      = '$temp' WHERE card_id = '$id'";
     $db->query($sql);
	  /* 提示信息 */
      $link[0]['text'] = $_LANG['back_list'];
      $link[0]['href'] = 'bank_card.php?act=card_list&' . list_link_postfix();

      sys_msg($_LANG['attradd_succed'], 0, $link);
}


/*------------------------------------------------------ */
//-- 编辑银行卡页面
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_card')
{
    admin_priv('surplus_manage'); //权限判断
    $id = isset($_GET['id'])? intval($_GET['id']): 0;
	$sql = "SELECT * FROM ".$ecs->table('users_card')."WHERE card_id='$id'";
	$dbcard = array();
	$dbcard = $db->getRow($sql);
	
    $smarty->assign('dbcard',        $dbcard);
	
	$href = 'bank_card.php?act=card_list&' . list_link_postfix();
    $smarty->assign('action_link', array('href' => $href, 'text' => '收款帐户管理'));
    assign_query_info();
    $smarty->display('edit_card_info.htm');
}

/*------------------------------------------------------ */
//-- 更新银行卡页面
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'card_update')
{
    admin_priv('surplus_manage'); //权限判断
    $id = isset($_POST['id'])? intval($_POST['id']): 0;
	
     $owner_name = isset($_POST['owner_name']) ? trim($_POST['owner_name']) : '';
	 $card_number = isset($_POST['card_number']) ? trim($_POST['card_number']) : '';
	 $open_bank = isset($_POST['open_bank']) ? trim($_POST['open_bank']) : '';
	 $bank_type = isset($_POST['bank_type']) ? trim($_POST['bank_type']) : '';
	 $validate = isset($_POST['confirm']) ? intval($_POST['confirm']) : 0;
	 $admin_note = isset($_POST['admin_note']) ? trim($_POST['admin_note']) : '';
	 $edit_time = time();
   
	$sql = "UPDATE ".$ecs->table('users_card'). " SET " .
	               "admin_user    = '$_SESSION[admin_name]', ".
                   "admin_note    = '$admin_note', ".
				   "card_number   = '$card_number', ".
				   "owner_name    = '$owner_name', ".
                   "open_bank     = '$open_bank', ".
				   "bank_type     = '$bank_type', ".
				   "validate      = '$validate', ".
                   "edit_time     = '$edit_time' WHERE card_id = '$id'";
	
    $db->query($sql);
    /* 提示信息 */
      $link[0]['text'] = $_LANG['back_list'];
      $link[0]['href'] = 'bank_card.php?act=card_list&' . list_link_postfix();

      sys_msg($_LANG['attradd_succed'], 0, $link);
}

/*------------------------------------------------------ */
//-- 删除用户帐户
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'remove')
{
    /* 检查权限 */
    admin_priv('users_drop');

    $sql = "DELETE FROM " . $ecs->table('users_card') . " WHERE card_id = '" . $_GET['id'] . "'";
    $m = $db->query($sql);
    /* 通过插件来删除用户 */


    /* 提示信息 */
    $link[] = array('text' => $_LANG['go_back'], 'href'=>'bank_card.php?act=card_list');
    sys_msg(sprintf('该帐户删除成功'), 0, $link);
}


/*------------------------------------------------------ */
//-- 批量删除用户帐户
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'batch_remove')
{
    /* 检查权限 */
    admin_priv('users_drop');

    if (isset($_POST['checkboxes']))
    {
        $sql = "SELECT owner_name FROM " . $ecs->table('users_card') . " WHERE card_id " . db_create_in($_POST['checkboxes']);
        $col = $db->getCol($sql);
        $owner = implode(',',addslashes_deep($col));
        $count = count($col);
        $sql = "DELETE  FROM " . $ecs->table('users_card') . " WHERE card_id " . db_create_in($_POST['checkboxes']);
		$m = $db->query($sql);
		if($m>0){
        admin_log($owner, 'batch_remove', 'users');
        $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'bank_card.php?act=card_list');
        sys_msg(sprintf('已经成功删除了 %d 个用户帐号。', $count), 0, $lnk);
		}
    }
    else
    {
        $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'bank_card.php?act=card_list');
        sys_msg("没有该帐户信息", 0, $lnk);
    }
}

/*------------------------------------------------------ */
//-- 获取银行卡信息
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'get_card_info')
{
    
    $cid = isset($_REQUEST['id'])?intval($_REQUEST['id']):0;
    if (empty($order_id))
    {
        make_json_response('', 1, $_LANG['error_get_goods_info']);
    }
    $card_info = array();
    $sql = "SELECT c.*, u.mobile_phone as phone " .
            "FROM " . $ecs->table('users_card') . " AS c ".
            "LEFT JOIN " . $ecs->table('users') . " AS u ON c.user_id = u.user_id " .
            "WHERE o.order_id = '{$order_id}' ";
    $card_info = $db->getRow($sql);
	     
    $smarty->assign('card_info', $card_info);
    $str = $smarty->fetch('bank_card_info.htm');
    $cards[] = array('card_id' => $id, 'str' => $str);
    make_json_result($cards);
}

/**
 *  返回银行卡列表数据
 *
 * @access  public
 * @param
 *
 * @return void
 */
 
function bankcard_list()
{
    $result = get_filter();
    if ($result === false)
    {
        /* 过滤条件 */
        $filter['keywords'] = empty($_REQUEST['keywords']) ? '' : trim($_REQUEST['keywords']);
        if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1)
        {
            $filter['keywords'] = json_str_iconv($filter['keywords']);
        }
        $filter['process_type'] = empty($_REQUEST['process_type']) ? -1 : intval($_REQUEST['process_type']);
        $filter['start_date'] = empty($_REQUEST['start_date']) ? 0 : local_strtotime($_REQUEST['start_date']);
        $filter['end_date'] = empty($_REQUEST['end_date']) ? 0 : local_strtotime($_REQUEST['end_date']);

        $filter['sort_by']    = empty($_REQUEST['sort_by'])    ? 'card_id' : trim($_REQUEST['sort_by']);
        $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC'     : trim($_REQUEST['sort_order']);
         
        $ex_where = ' WHERE 1 ';
        if ($filter['keywords'])
        {
            $ex_where .= " AND owner_name LIKE '%" . mysql_like_quote($filter['keywords']) ."%'";
        }
        if ($filter['process_type']==1)
        {
           $ex_where .= "AND card_type ='zfb'";
        }
		elseif ($filter['process_type']==2)
        {
           $ex_where .= "AND card_type = 'bank_card'";
        }
		
        if ($filter['start_date'])
        {
             $ex_where .=" AND edit_time >= '$filter[start_date]' ";
        }
        if ($filter['end_date'])
        {
            $ex_where .=" AND edit_time < '$filter[end_date]' ";
        }

        $filter['record_count'] = $GLOBALS['db']->getOne("SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('users_card') . $ex_where);

        /* 分页大小 */
        $filter = page_and_size($filter);
        $sql = "SELECT c.*, u.user_name ". " FROM " . $GLOBALS['ecs']->table('users_card') . " c LEFT JOIN " . 
		$GLOBALS['ecs']->table('users') ." u on c.user_id = u.user_id " . $ex_where ." ORDER by " . $filter['sort_by'] . ' ' 
		. $filter['sort_order'] ." LIMIT " . $filter['start'] . ',' . $filter['page_size'];

        $filter['keywords'] = stripslashes($filter['keywords']);
        set_filter($filter, $sql);
    }
    else
    {
        $sql    = $result['sql'];
        $filter = $result['filter'];
    }

    $bankcard_list = $GLOBALS['db']->getAll($sql);

    $count = count($bankcard_list);
    for ($i=0; $i<$count; $i++)
    {   $bankcard_list[$i]['upload'] = "data/usercardimg/".$bankcard_list[$i]['upload'];
	    $bankcard_list[$i]['ac_type'] =  $bankcard_list[$i]['card_type']=='zfb'?"支付宝":"银行卡";
        $bankcard_list[$i]['edit_time'] = local_date($GLOBALS['_CFG']['date_format'], $bankcard_list[$i]['edit_time']);
    }

    $arr = array('card_list' => $bankcard_list, 'filter' => $filter,
        'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}
?>
