<?php


define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
$smarty->assign('lang', $_LANG);
admin_priv('affiliate');
$config = get_affiliate();


    /* 检查权限 */
    admin_priv('users_manage');
    $smarty->assign('ur_here',      $_LANG['03_users_list']);
    $auid = empty($_GET['auid'])?intval($_REQUEST['id']):intval($_GET['auid']);
    $user_list['user_list'] = array();
	$users_parent = array();
	$sql = "SELECT user_name as name FROM " . $ecs->table('users') . " WHERE user_id = '$auid'";
	$users_parent['name'] = $GLOBALS['db']->getOne($sql);
	$users_parent['id'] = $auid;
	
	if($auid){
		$_SESSION['auid'] = $auid;
		}
	if($_SESSION['auid']){
		$auid = $_SESSION['auid'];
		 }

    $affiliate = unserialize($GLOBALS['_CFG']['affiliate']);
    $smarty->assign('affiliate',    $affiliate);
	$smarty->assign('users_parent', $users_parent);

    empty($affiliate) && $affiliate = array();

    $num = count($affiliate['item']);
    $up_uid = "'$auid'";
	$all_uid = "'$auid'";
    $all_count = 0;
     for ($i = 1 ; $i <=$num ;$i++)
            {
                $count = 0;
                if ($up_uid)
                {
                    $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE recmded_id IN($up_uid)";
                    $query = $db->query($sql);
                    $up_uid = '';
                    while ($rt = $db->fetch_array($query))
                    {
                        $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
						$affdb[$i]['rank_id'][] = $rt['user_id'];
                        if($i < $num)
                        {
                            $all_uid .= ", '$rt[user_id]'";
							
                        }
                        $count++;
                    }
                }
				
                $affdb[$i]['num'] = $count;
				$all_count +=$count;
                $affdb[$i]['point'] = $affiliate['item'][$i-1]['level_point'];
                $affdb[$i]['money'] = $affiliate['item'][$i-1]['level_money'];
            }	  

if($_REQUEST['act'] == 'member_down')
  {
    $user_list = filters_list($all_uid, $all_count, $affdb, $num);
	
	$smarty->assign('users_parent', $users_parent);
    $smarty->assign('user_list',    $user_list['user_list']);
	$smarty->assign('filter',       $user_list['filter']);
    $smarty->assign('record_count', $user_list['record_count']);
	$smarty->assign('page_count',   $user_list['page_count']);
    $smarty->assign('full_page',    1);
    $smarty->assign('action_link',  array('text' => $_LANG['back_note'], 'href'=>"users.php?act=edit&id=$auid"));

    assign_query_info();
    $smarty->display('affiliate_downlist.htm');
   }

elseif($_REQUEST['act'] == 'query')
 {  
    $user_list = filters_list($all_uid, $all_count, $affdb, $num);
    
	
    $smarty->assign('user_list',    $user_list['user_list']);
    $smarty->assign('filter',       $user_list['filter']);
    $smarty->assign('record_count', $user_list['record_count']);
    $smarty->assign('page_count',   $user_list['page_count']);

    $sort_flag  = sort_flag($user_list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('affiliate_downlist.htm'), '', array('filter' => $user_list['filter'], 'page_count' => $user_list['page_count']));
 }


function get_affiliate()
{
    $config = unserialize($GLOBALS['_CFG']['affiliate']);
    empty($config) && $config = array();

    return $config;
}

function put_affiliate($config)
{
    $temp = serialize($config);
    $sql = "UPDATE " . $GLOBALS['ecs']->table('shop_config') .
           "SET  value = '$temp'" .
           "WHERE code = 'affiliate'";
    $GLOBALS['db']->query($sql);
    clear_all_files();
}

function filters_list($all_uid ,$count, $affdb, $num)
{       
      $result = get_filter();
    if ($result === false)
    {
        /* 过滤条件 */
        $filter['keywords'] = empty($_REQUEST['keywords']) ? '' : trim($_REQUEST['keywords']);
        if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1)
        {
            $filter['keywords'] = json_str_iconv($filter['keywords']);
        }
        $filter['aff_rank'] = empty($_REQUEST['aff_rank']) ? 0 : intval($_REQUEST['aff_rank']);
        $filter['pay_points_gt'] = empty($_REQUEST['pay_points_gt']) ? 0 : intval($_REQUEST['pay_points_gt']);
        $filter['pay_points_lt'] = empty($_REQUEST['pay_points_lt']) ? 0 : intval($_REQUEST['pay_points_lt']);

        $filter['sort_by']    = empty($_REQUEST['sort_by'])    ? 'user_id' : trim($_REQUEST['sort_by']);
        $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC'     : trim($_REQUEST['sort_order']);
          
        $ex_where = " WHERE recmded_id IN ($all_uid)";
		
        if ($filter['keywords'])
        {
            $ex_where .= " AND user_name LIKE '%" . mysql_like_quote($filter['keywords']) ."%'";
        }
        if ($filter['pay_points_gt'])
        {
             $ex_where .=" AND pay_points >= '$filter[pay_points_gt]' ";
        }
        if ($filter['pay_points_lt'])
        {
            $ex_where .=" AND pay_points < '$filter[pay_points_lt]' ";
        }

        $filter['record_count'] = $count;

        /* 分页大小 */
        $filter = page_and_size($filter);
        $sql = "SELECT user_id, user_name, email, is_validated, user_money, frozen_money, rank_points, pay_points, reg_time ".
                " FROM " . $GLOBALS['ecs']->table('users') . $ex_where .
                " ORDER by " . $filter['sort_by'] . ' ' . $filter['sort_order'];
                

        $filter['keywords'] = stripslashes($filter['keywords']);
        set_filter($filter, $sql);
    }
    else
    {
        $sql    = $result['sql'];
        $filter = $result['filter'];
    }

    $user_list = $GLOBALS['db']->getAll($sql);

    $count = count($user_list);
    for ($i=0; $i<$count; $i++)
    {  
	    for($j=1; $j<=$num; $j++){
		   if(@in_array($user_list[$i]['user_id'],$affdb[$j]['rank_id'])){
			     $user_list[$i]['level']=$j;
				 
			   }
		 }
        $user_list[$i]['reg_time'] = local_date($GLOBALS['_CFG']['date_format'], $user_list[$i]['reg_time']);
    }
	 $temp1_list = array();
    if($filter['aff_rank']){
	  for ($i=0; $i<$count; $i++){
	     if($filter['aff_rank']==$user_list[$i]['level'])
	     $temp1_list[]=$user_list[$i];
	    }
	  }
	else{
	 $temp1_list = $user_list;
	 }
	//" LIMIT " . $filter['start'] . ',' . $filter['page_size'];
	$i = $filter['start'];
	$m = $filter['page_size']+$filter['start'];
	$temp2_list=array();
	for($i;$i<=$m;$i++){
	   if($temp1_list[$i]['user_id']){
	   $temp2_list[]=$temp1_list[$i];
	  }
	 } 
	$filter['record_count'] = count($temp1_list);
	
    $arr = array('user_list' => $temp2_list, 'filter' => $filter,
        'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}

?>