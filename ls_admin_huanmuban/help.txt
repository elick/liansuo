Array
(
    [47] => Array
        (
            [cat_id] => article_cat.php?id=47
            [cat_name] => 新手上路
            [article] => Array
                (
                    [0] => Array
                        (
                            [article_id] => 42
                            [title] => 注册用户
                            [short_title] => 注册用户
                            [url] => article.php?id=42
                        )

                    [1] => Array
                        (
                            [article_id] => 43
                            [title] => 积分规则
                            [short_title] => 积分规则
                            [url] => article.php?id=43
                        )

                    [2] => Array
                        (
                            [article_id] => 44
                            [title] => 网站购物流程
                            [short_title] => 网站购物流程
                            [url] => article.php?id=44
                        )

                    [3] => Array
                        (
                            [article_id] => 45
                            [title] => 搜索商品
                            [short_title] => 搜索商品
                            [url] => article.php?id=45
                        )

                )

        )

    [50] => Array
        (
            [cat_id] => article_cat.php?id=50
            [cat_name] => 售后服务
            [article] => Array
                (
                    [4] => Array
                        (
                            [article_id] => 46
                            [title] => 退换货流程
                            [short_title] => 退换货流程
                            [url] => article.php?id=46
                        )

                    [5] => Array
                        (
                            [article_id] => 47
                            [title] => 售后服务
                            [short_title] => 售后服务
                            [url] => article.php?id=47
                        )

                    [6] => Array
                        (
                            [article_id] => 48
                            [title] => 联系我们
                            [short_title] => 联系我们
                            [url] => article.php?id=48
                        )

                    [7] => Array
                        (
                            [article_id] => 49
                            [title] => 退款申请
                            [short_title] => 退款申请
                            [url] => article.php?id=49
                        )

                )

        )

    [48] => Array
        (
            [cat_id] => article_cat.php?id=48
            [cat_name] => 配送指南
            [article] => Array
                (
                    [8] => Array
                        (
                            [article_id] => 50
                            [title] => 配送公告
                            [short_title] => 配送公告
                            [url] => article.php?id=50
                        )

                    [9] => Array
                        (
                            [article_id] => 51
                            [title] => 免运费优惠
                            [short_title] => 免运费优惠
                            [url] => article.php?id=51
                        )

                    [10] => Array
                        (
                            [article_id] => 52
                            [title] => 配送时间费用
                            [short_title] => 配送时间费用
                            [url] => article.php?id=52
                        )

                    [11] => Array
                        (
                            [article_id] => 53
                            [title] => 配送范围
                            [short_title] => 配送范围
                            [url] => article.php?id=53
                        )

                )

        )

    [51] => Array
        (
            [cat_id] => article_cat.php?id=51
            [cat_name] => 客服中心
            [article] => Array
                (
                    [12] => Array
                        (
                            [article_id] => 54
                            [title] => 积分的使用
                            [short_title] => 积分的使用
                            [url] => article.php?id=54
                        )

                    [13] => Array
                        (
                            [article_id] => 55
                            [title] => 会员权益
                            [short_title] => 会员权益
                            [url] => article.php?id=55
                        )

                    [14] => Array
                        (
                            [article_id] => 56
                            [title] => 投诉建议
                            [short_title] => 投诉建议
                            [url] => article.php?id=56
                        )

                    [15] => Array
                        (
                            [article_id] => 57
                            [title] => 加盟合作
                            [short_title] => 加盟合作
                            [url] => article.php?id=57
                        )

                )

        )

    [49] => Array
        (
            [cat_id] => article_cat.php?id=49
            [cat_name] => 如何付款
            [article] => Array
                (
                    [16] => Array
                        (
                            [article_id] => 58
                            [title] => 修订订单
                            [short_title] => 修订订单
                            [url] => article.php?id=58
                        )

                    [17] => Array
                        (
                            [article_id] => 59
                            [title] => 货到付款
                            [short_title] => 货到付款
                            [url] => article.php?id=59
                        )

                    [18] => Array
                        (
                            [article_id] => 60
                            [title] => 支付问题
                            [short_title] => 支付问题
                            [url] => article.php?id=60
                        )

                    [19] => Array
                        (
                            [article_id] => 61
                            [title] => 财富通支付
                            [short_title] => 财富通支付
                            [url] => article.php?id=61
                        )

                )

        )

)
