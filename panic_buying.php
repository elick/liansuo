<?php


define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
   
   
   /* 初始化分页信息 */
   $page = isset($_REQUEST['page'])   && intval($_REQUEST['page'])  > 0 ? intval($_REQUEST['page'])  : 1;
   $size = isset($_CFG['page_size'])  && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 12;
   
   $default_sort_order_method = $_CFG['sort_order_method'] == '0' ? ' DESC' : 'ASC';
   $default_sort_order_type   = $_CFG['sort_order_type'] == '0' ? 'goods_id' : ($_CFG['sort_order_type'] == '1' ? 'shop_price' : 'last_update');
   
   $sort  = (isset($_REQUEST['sort'])  && in_array(trim(strtolower($_REQUEST['sort'])), array('goods_id', 'shop_price', 'last_update','sales_volume_base', 'commentnum'))) ? 
      trim($_REQUEST['sort'])  : $default_sort_order_type;
   $order = (isset($_REQUEST['order']) && in_array(trim(strtoupper($_REQUEST['order'])), array('ASC', 'DESC')))? trim($_REQUEST['order']) : $default_sort_order_method;
   
   if($sort=='sales_volume_base'){
	   $cur=2;
	   }
	elseif($sort=='last_update'){
	   $cur=3;
	  }
	elseif($sort=='shop_price'){
	  $cur=4;
	 }
	else{$cur=1;}
   
    assign_template();
	$goods_list = get_panic_goods($page, $size, $sort, $order);
	$time = gmtime();
	$sql = " SELECT count(*) FROM ". $GLOBALS['ecs']->table('goods') ." WHERE is_on_sale = 1 AND is_alone_sale = 1 AND is_delete = 0 " .
            " AND is_promote = 1 AND promote_start_date <= '$time' AND promote_end_date >= '$time' AND promote_price = 10.00 ";
    $count = $db->getOne($sql);
	$max_page = ($count> 0) ? ceil($count / $size) : 1;
    if ($page > $max_page)
    {
        $page = $max_page;
    }
	
	$pager['search'] = array(
        'sort'       => $sort,
        'order'      => $order
    );

	$pager = get_pager('panic_buying.php', $pager['search'], $count, $page, $size);
		
	
    $smarty->assign('goods_list',      $goods_list);
	$smarty->assign('cur',             $cur);
	$smarty->assign('max_page',        $max_page);
	$smarty->assign('page',            $page);
	$smarty->assign('count',           $count);
	$smarty->assign('pager',           $pager);
    $smarty->assign('categories',      get_categories_tree()); // 分类树
    $smarty->assign('helps',           get_shop_help());       // 网店
	
	$smarty->display('panic_buying.dwt');
	
	
	/**
 * 获得促销商品
 *
 * @access  public
 * @return  array
 */
function get_panic_goods($page, $pagesize, $sort, $order)
{
    $time = gmtime();
    $order_type = $GLOBALS['_CFG']['recommend_order'];

    /* 取得促销lbi的数量限制 */
    $num = ($page-1)*$pagesize;
    $sql = 'SELECT g.goods_id, g.goods_name, g.goods_name_style, g.sales_volume_base, g.promote_end_date, g.market_price, g.shop_price AS org_price, g.promote_price, ' .
                "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, ".
                "promote_start_date, promote_end_date, g.goods_brief, g.goods_thumb, goods_img, b.brand_name, " .
                "g.is_best, g.is_new, g.is_hot, g.is_promote, RAND() AS rnd " .
            'FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('brand') . ' AS b ON b.brand_id = g.brand_id ' .
            "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                "ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ".
            'WHERE g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 ' .
            " AND g.is_promote = 1 AND promote_start_date <= '$time' AND promote_end_date >= '$time' AND promote_price = 10.00 ";
    $sql .= "order by ".$sort." ".$order;
    $sql .= " LIMIT $num , $pagesize";
    $result = $GLOBALS['db']->getAll($sql);

    $goods = array();
    foreach ($result AS $idx => $row)
    {
        if ($row['promote_price'] > 0)
        {
            $promote_price = bargain_price($row['promote_price'], $row['promote_start_date'], $row['promote_end_date']);
            $goods[$idx]['promote_price'] = $promote_price > 0 ? price_format($promote_price) : '';
			$goods[$idx]['zhekou_price'] = price_format($row['promote_price']/$row['market_price'])*10;
        }
        else
        {
            $goods[$idx]['promote_price'] = '';
        }

        $goods[$idx]['id']           = $row['goods_id'];
        $goods[$idx]['name']         = $row['goods_name'];
		$goods[$idx]['salesnum']     = $row['sales_volume_base'];
		$goods[$idx]['end_date']     = date('Y-m-d-H-i-s', $row['promote_end_date']);
        $goods[$idx]['brief']        = $row['goods_brief'];
        $goods[$idx]['brand_name']   = $row['brand_name'];
        $goods[$idx]['goods_style_name']   = add_style($row['goods_name'],$row['goods_name_style']);
        $goods[$idx]['short_name']   = $GLOBALS['_CFG']['goods_name_length'] > 0 ? sub_str($row['goods_name'], $GLOBALS['_CFG']['goods_name_length']) : $row['goods_name'];
        $goods[$idx]['short_style_name']   = add_style($goods[$idx]['short_name'],$row['goods_name_style']);
        $goods[$idx]['market_price'] = price_format($row['market_price']);
        $goods[$idx]['shop_price']   = price_format($row['shop_price']);
        $goods[$idx]['thumb']        = get_image_path($row['goods_id'], $row['goods_thumb'], true);
        $goods[$idx]['goods_img']    = get_image_path($row['goods_id'], $row['goods_img']);
        $goods[$idx]['url']          = build_uri('goods', array('gid' => $row['goods_id']), $row['goods_name']);
    }

    return $goods;
}



?>

