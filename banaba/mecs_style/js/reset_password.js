$.fn.passwordStrength = function(options){
return this.each(function(){
var that = this;that.opts = {};
that.opts = $.extend({}, $.fn.passwordStrength.defaults, options);
that.div = $(that.opts.targetDiv);
that.defaultClass = that.div.attr('class');
that.percents = (that.opts.classes.length) ? 100 / that.opts.classes.length : 100;
v = $(this).keyup(function(){
if( typeof el == "undefined" )
this.el = $(this);
var s = getPasswordStrength (this.value);
var p = this.percents;
var t = Math.floor( s / p );  
if( 100 <= s ) t = this.opts.classes.length - 1; 
this.div.removeAttr('class').addClass( this.defaultClass ).addClass( this.opts.classes[ t ]);
})
});
function getPasswordStrength(H){
var D=(H.length);
if(D>7){
  D=7
}
var F=H.replace(/[0-9]/g,"");
var G=(H.length-F.length);
if(G>3){G=3}
var A=H.replace(/\W/g,"");
var C=(H.length-A.length);
if(C>3){C=3}
var B=H.replace(/[A-Z]/g,"");
var I=(H.length-B.length);
if(I>3){I=3}
var E=((D*10)-40)+(C*15)+(I*10);
if(E<0){E=0}
if(E>100){E=100}
return E
}
};
$.fn.passwordStrength.defaults = {
classes : Array('is10','is20','is30','is40','is50','is60','is70','is80','is90','is100'),
targetDiv : '#passwordStrengthDiv2',
cache : {}
}
$.passwordStrength = {};
$.passwordStrength.getRandomPassword = function(size){
var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
var size = size || 8;
var i = 1;
var ret = ""
while ( i <= size ) {
$max = chars.length-1;
$num = Math.floor(Math.random()*$max);
$temp = chars.substr($num, 1);
ret += $temp;
i++;
}
return ret;
}

$(function(){
$('#password_reg3').passwordStrength();
}); 
function submitPwd(){ 
	    var frm = document.forms['getPassword2']; 
	    var new_password = frm.elements['new_password'].value;
	    var confirm_password = frm.elements['confirm_password'].value;
		var msg = $('#reset_ul').find('li.msg em');
	    if(new_password == '' || confirm_password =='' ){text =(new_password == '') ? '密码不能为空':'确认密码不能为空！';
	       msg.text(text).show().delay(2000).fadeOut(); return false}
		if(new_password.length<6){
			text ='登录密码不能少于 6 个字符！';
	        msg.text(text).show().delay(2000).fadeOut();return false
			 }
	    if(new_password != confirm_password ){text ='两次输入的密码不一致！';
	      msg.text(text).show().delay(2000).fadeOut();return false}
         return true;
	}
	
function send_ma_code(){
	 var frm = document.forms['getPassword3']; 
	 var mobile = frm.elements['mobile'].value;
	$("#sendMobileCode3").addClass("btn-disabled").html('请稍等<img src="banaba/mecs_style/images/loading2.gif" />');
	$.post('user.php?act=send_mobile_code&flag=1',{mobile:mobile},function(response){var res = $.evalJSON(response);
    $("#sendMobileCode3").removeClass("btn-disabled").html("发送验证码");
	if(!res.error){$(".ftx-03").text(120);$("#countDown_label3").show();
	setTimeout(countDown3,1000);$("#sendMobileCode3").addClass("btn-disabled")}
	else{new Msg.warn(res.message)}},'text');return}
	
function countDown3(){
	var time = parseInt($(".ftx-03").text());
	$(".ftx-03").text(time - 1);
	if(time <= 1){$("#countDown_label3").hide();
	$("#mobileCode_error").hide();
	$("#sendMobileCode3").removeClass("btn-disabled")}
	else{setTimeout(countDown3,1000)}}
	
function getMobilePwd(){ 
	    var frm = document.forms['getPassword3']; 
	    var _mobile = frm.elements['mobile'].value;
	    var _mobile_code = frm.elements['mobile_code'].value;
		var _back_act = frm.elements['back_act'].value;
		var msg = $('#mobile_psd').find('li.msg em');
	    if(_mobile == '' || _mobile_code =='' ){text =(_mobile == '') ? '手机号码不能为空':'验证码不能为空！';
	       msg.text(text).show().delay(2000).fadeOut(); return false}
		if(!lyecs.check.isMobile(_mobile)){
			text ='请输入正确的手机号码！';
	        msg.text(text).show().delay(2000).fadeOut();
			return false;
			 }
			 
	  $.post('user.php?act=check_pwd_sms',{mobile:_mobile,mobile_code:_mobile_code,back_act:_back_act},
			 function(response){var res = $.evalJSON(response); 
			                    
			 if(res.error>0){ _tag=res.error;  msg.text(res.content).show().delay(2000).fadeOut(); }
			 else{location.href = res.url}},'text');
          
	
		  return  false;
		 
	}