/************************************注：勿直接修改此样式文件******************/
$(".lazy").lazyload({
	 skip_invisible : true,
	 effect: "fadeIn",
	 failure_limit : 10
});
var loader = '<div class="loader">&nbsp;</div>', result = '<div class="result"></div>', status = '<div class="status"></div>', page = 'undefined', action = 'undefined';
$(document).ready(function() {
    $(".count_box").click(function (event) {
        if (event.target.className == "spinner-arrow-up") {
            countNum(1);
        } else if(event.target.className == "spinner-arrow-down") {
            countNum(-1);
        }else{
			return;
		}
    });
	$(".count_box #number").blur(function () {	
		countNum(0);
	});
	lyecs.common.priceForm();
	$('.tab-title li').hover(function(){
		var tab_li = $(this);
		tab_litimeoutid = setTimeout(function(){
			tab_data=tab_li.attr("tab-data");
			tab_li.parent().find("li").removeClass("cur");
			tab_li.parent().next().find('.tab-box').hide();		
			tab_li.addClass("cur");
			tab_li.parent().next().find('.tab-box-'+tab_data).fadeIn("fast");
		},300);
	}, function(){
		 clearTimeout(tab_litimeoutid);
	});
	$('.tab-title2 .item').hover(function(){
		var tab_item = $(this);
		tab_data=tab_item.attr("tab-data");
		tab_item.parent().find(".item").removeClass("cur");
		tab_item.parents('.tab-title2').next().find('.tab-box').hide();		
		tab_item.addClass("cur");
		tab_item.parents('.tab-title2').next().find('.tab-box-'+tab_data).show();
	});
	$('.proIntro').mouseenter(function(){
		var old_height=$(this).attr('_top');
		var to_height=parseInt(old_height)-$(this).height()+30;
		$(this).stop().animate({'top':to_height},'fast').addClass('hover');
	});
	$('.proIntro').mouseleave(function(){
		var old_height=$(this).attr('_top');
		$(this).stop().animate({'top':old_height},'fast').removeClass('hover');
	});
	$('.tab-click-title li').click(function(){
		var tab_li = $(this);
		tab_data=tab_li.attr("tab-data");
		tab_li.parent().find("li").removeClass("cur");
		tab_li.parent().next().find('.tab-box').hide();		
		tab_li.addClass("cur");
		tab_li.parent().next().find('.tab-box-'+tab_data).fadeIn("fast");
	});
	$('.select-box').bind("click",function(){
		$(this).find('ul').slideToggle('fast');
	});
	$('.select-box').mouseleave(function(){
		$(this).find('ul').hide();
	});
	$('.nav li,#funcTab li, .category li, .all_category li').hover(function(){
		if ($(this).hasClass('parent')) {
			$(this).addClass('parent_hover');
		} else {
			$(this).addClass('hover');
		}
		if ($(this).hasClass('current')) {
			$(this).addClass('current_hover');
		}
	}, function(){
		if ($(this).hasClass('parent_hover')) {
			$(this).removeClass('parent_hover');
		} else {
			$(this).removeClass('hover');
		}
		if ($(this).hasClass('current_hover')) {
			$(this).removeClass('current_hover');
		}
	});
	$('.goods-list-c li').hover(function(){
		$(this).addClass('hover');
	}, function(){
		$(this).removeClass('hover');
	});
	$('.thumb li a').click(function(){
		$(this).parents('.thumb').find('li').removeClass('cur');
		$(this).parent().addClass('cur');
	});
	$('.all_cat .list,.indexBrand li').hover(function(){
		$(this).addClass('hover');
	}, function(){
		$(this).removeClass('hover');
	});
	$('.top-small-nav').hover(function(){
		$(this).addClass('small-nav-hover');
	}, function(){		
		$(this).removeClass('small-nav-hover');
	});
	$('#categorysBox').hover(function(){
		$(".categorys-main").show();
	}, function(){		
		$(".categorys-main").hide();
	});
	$('.category .sub_cat_lv1').each(function(){
		var sub = $(this);
		var parent = sub.parent();
		parent.find('p.level_1 .icon').click(function(){
			if (parent.hasClass('hide_sub')) {
				parent.removeClass('hide_sub');
			} else {
				parent.addClass('hide_sub');
			}
		});
	});
	$('.nav li.level_1').each(function(){
		if ($(this).find('.current').length > 0) {
			$(this).addClass('current');
		}
	});
	$('.category li.level_2').each(function(){
		if ($(this).find('.current').length > 0 || $(this).hasClass('current')) {
			$(this).find('.sub_cat_lv2').show();
			$(this).find('.icon2').addClass('show_sub');
		}
	});
	$('.category li.level_1').each(function(){
		if ($(this).hasClass('current') || $(this).find('.current').length > 0) {
			$(this).removeClass('hide_sub');
		}
	});
	$('.quick_login').live('click',function(){
		openQuickLogin();
		return false;
	});	
    lyecs.common.toolBar();//加载右侧浮动栏（购物车等）
	loadCart();
	$('.tip').tipsy({gravity: 's',fade: true,html: true});
	$('.fittings_list li[title!=""]').tipsy({gravity: 's',fade: true});
	$('.back_to_top').click(function(){
		$body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
		$body.animate({scrollTop: $('body').offset().top}, 800);
		return false;
	});
	$('.rank_star').rating({
		focus: function(value, link){
			var tip = $('#star_tip');
			tip[0].data = tip[0].data || tip.html();
			tip.html(link.title || 'value: '+value);
		},
		blur: function(value, link){
			var tip = $('#star_tip');
			$('#star_tip').html(tip[0].data || '');
		}
	});
	$('.end_time[title!=""]').each(function(){
		var countdown = $(this);
		countdown.parent().find('.label').text(lang.remaining_time + lang.colon);
		var end_date = countdown.attr('title').split('-');
		countdown.removeAttr('title');
		var date = new Date(end_date[0],end_date[1]-1,end_date[2],end_date[3],end_date[4],end_date[5]);
		countdown.countdown({
			until: date,
			layout: '<em>{dn}</em>{dl}<em>{hn}</em>{hl}<em>{mn}</em>{ml}<em>{sn}</em>{sl}'
		});
	});
	$('.lyecs_main .goods_list li,.lyecs_main .goods_li_list li').hover(function(){
		$(this).addClass('hover');
	}, function(){
		$(this).removeClass('hover');
	});
	$('.show-tip').focus(function() {
		org_value=$(this).attr('org-value');
		if($(this).val()==org_value){
			$(this).val('');
			$(this).css({color:'#333'});
		}
	});
	$('.show-tip').blur(function() {
		org_value=$(this).attr('org-value');
		if($(this).val()==''){
			$(this).val(org_value);
			$(this).css({color:'#999'});
		}
	});
	$('.inner_lv1 .icon2').click(function() {
		if($(this).hasClass('show_sub')){
			$(this).parent().find('.sub_cat_lv2').hide();
			$(this).removeClass('show_sub');
		}else{
			$(this).parent().find('.sub_cat_lv2').show();
			$(this).addClass('show_sub');
			
		}
	});
	var subscription_email = $('#subscription input[name="email"]');
	$('#subscription').append(loader).append(result);
	subscription_email.tipsy({
		trigger:'manual',gravity:'s',fade: true
	}).focusout(function() {
		$(this).tipsy('hide');
	}).keypress(function() {
		$(this).tipsy('hide');
	});
	//延迟执行
	$(".more-menu").hoverIntent(function(){$(this).addClass('hover');},function(){$(this).removeClass('hover');});
	$('.slider-box').each(function(){
		var show_num=parseInt($(this).attr("shownum")),
			real_num=$(this).find("li").length,
			right_btn= $(this).find(".slider-btn-r"),
			left_btn= $(this).find(".slider-btn-l");
		if(show_num<real_num){
			$(this).find(".goods-list").marquee({
				auto: false,
				speed: 1000,
				showNum: show_num,
				stepLen: show_num,
				prevElement: right_btn,
				nextElement: left_btn
			});
		}else{
			right_btn.hide();
			left_btn.hide();
		}
	});
	$('.filter_box dl').each(function(){
		var filter_dd=$(this).find("dd");
		if(filter_dd.height()>70){
			$(this).find(".attrMore").show();
			filter_dd.height("58");
		}
		if($(this).find("a.current").length>0){
			filter_dd.height("auto");	
			$(this).find(".attrMore").addClass("attrMoreUp");			
		}
	});
	$('.filter_box dl .attrMore').click(function() {
		if($(this).hasClass("attrMoreUp")){
			$(this).parent().height("58");
			$(this).removeClass("attrMoreUp");
		}else{
			$(this).parent().height("auto");	
			$(this).addClass("attrMoreUp");	
		}
	});
});
