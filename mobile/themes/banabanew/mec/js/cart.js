var NALA=NALA||{};
var lizi_postage_fee=postage_fee||99;
NALA.myCart={promotionBuy:true,common:{$box:null,$cartFooter:null},

init:function(){var that=this,objCom=that.common;
 if($('#cart-box').length==0){return;}
objCom.$box=$('#cart-box');
objCom.$cartFooter=$('#cart-footer');
that.updateCart();that.listEvent();
objCom.$cartFooter.on('click','a.btn',function(){var _btn=$(this),$list=null,lineid=[];
if(_btn.find('img').length>0||_btn.hasClass('btn-off')){return false;}
if(!that.promotionBuy){ NALA.dialog.tip('商品金额小于'+lizi_postage_fee+'元，不能购买购物车促销商品！');
_btn.addClass('btn-off').html('去结算');return false;}
_btn.html('<img src="images/loading.gif" /> 提交中');
objCom.$box.find('li').each(function(){var $li=$(this),_id='';
if($li.hasClass('disabled')||$li.find('a.check_no').length>0){return;}
_id=$li.find('input[name=lineid]').val();
lineid.push(_id);});
$.ajax({url:'flow.php?step=sureBuyNumber',type:'post',data:{'idList':lineid.join(',')},
success:function(result){location.href='flow.php?step=checkout';},
error:function(xhr){_btn.html('去结算');
NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");}});});},

listEvent:function(){var that=this,$list=that.common.$box.find('li'),
timeJson=NALA.timeJson,setTime=null,isEdit=false,numReg=/^[1-9]\d*$/;
$('#cart_edit').on('click',function(){var _this=$(this);
if(isEdit){$list.removeClass('edit_li');
_this.html('编辑');isEdit=false;}
else{$list.addClass('edit_li');_this.html('完成');isEdit=true;}});
$list.each(function(){var _input=$(this).find('input[type=number]'),
_val=_input.val(),$time=$(this).find('em.time');
_input.data('oval',_val);timeDown($time);});

$list.on('click','a.del-btn',function(){var $del=$(this),
 $li=$del.parents('li'),
 id=$li.find('input[name=lineid]').val();
 var cb=function(){that.deleteGoods(id,$li);}
NALA.dialog.confirm('您确定要删除此商品吗？',cb);}).on('click','a.check',
function(){var $a=$(this);if($a.hasClass('check_no')){$a.removeClass('check_no');}
else{$a.addClass('check_no');}
that.updateCart();}).on('click','span',function(){var _this=$(this),_input=_this.siblings('input'),
_limit=parseInt(_input.attr('max')),num=_input.val();if(_this.hasClass('disabled')){return false;}
clearTimeout(setTime);if(!numReg.test(num)){_input.val(_input.data('oval'));return false;}
num=parseInt(num);if(_this.hasClass('add')){num++;}else{num--;}
if(num>_limit){num=_limit;_this.addClass('disabled');
  NALA.dialog.tip('此商品最多购买'+_limit+'件。');}
else if(num<=1){num=1;_this.addClass('disabled');}
_this.siblings('span').removeClass('disabled');
_input.val(num).data('oval',num);
setTime=setTimeout(function(){var id=_this.parents('li').find('input[name=lineid]').val();
that.updateGoodsNum(id,num);that.updateCart();},500);}).on('keyup','input[type=number]',
function(){var _input=$(this),oval=_input.data('oval'),_limit=parseInt(_input.attr('max')),val=_input.val();
clearTimeout(setTime);
if(val==0){setTime=setTimeout(function(){_input.val(oval);},500);}
else{setTime=setTimeout(function(){if(numReg.test(val)){if(val>_limit){val=_limit;_input.val(val);}
_input.siblings('span').removeClass('disabled');
_input.data('oval',val).trigger('blur');
var id=_input.parents('li').find('input[name=lineid]').val();
that.updateGoodsNum(id,val);that.updateCart();}
else{_input.val(oval);}},500);}}).on('paste','input[type=number]',function(){return false;});
function timeDown($time){var gotime=null,TimeDown=null,_time=parseInt($time.data('time')/1000);
TimeDown=function(){var _TIME=timeJson(_time);if(_time<0){clearInterval(gotime);$time.parents('li').addClass('disabled');
$time.replaceWith('<span class="error">此商品已过期</span>');that.updateCart();}
else{$time.html(_TIME.hours+'时'+_TIME.mins+'分'+_TIME.secs+'秒');_time--;}};
TimeDown();gotime=setInterval(TimeDown,1000);}},

updateCart:function(){var that=this,objCom=that.common,$group=objCom.$box.find('.goods-list'),
_hasPromotion=false,_jianInfo='',
_totalItems=_totalPrice=_totalJian=_tobuyLines=0;
if($group.length==0){location.reload();return;}
$group.each(function(){var _gthis=$(this),
fullSale=_gthis.data('fullsale'),
_groupPrice=0;
_gthis.find('li').each(function(){var $li=$(this),$input,$one,n=p=np=item_num=0;
if($li.hasClass('disabled')||$li.find('a.check_no').length>0){return;}
_tobuyLines++;if($li.find('.isPromotion').length>0){_hasPromotion=true;}
$input=$li.find('input[type=number]');
$one=$li.find('em.p-one');item_num=$li.find('h3').length;
if($input.length>0){n=parseInt($input.val());}
else if($li.find('cite').length>0){n=parseInt($li.find('cite').data('num'));}
else{n=1;}
p=parseFloat($one.text());np=n*p;_totalItems+=item_num*n;_totalPrice+=np; 
if(fullSale!=undefined){_groupPrice+=np;}});
if(fullSale!=undefined){var _fullSaledata=fullSale.data;
canFull=false,_fullIndex=_jian=0, 
_manInfo=_nextInfo='',
_zengData=_fullSaledata[0], 
$fullInfo=_gthis.find('.list-hd'); 
$.each(_fullSaledata,function(i,v){if(_groupPrice>=parseFloat(v[0])
){_zengData=v; _jian=parseFloat(v[1]);_fullIndex=parseInt(i)+1;canFull=true;}}); 
if(fullSale.type==="jian"){if(_fullIndex<2)
{_nextInfo='还差'+(_fullSaledata[_fullIndex][0]-_groupPrice).toFixed(2)+'元，可累计减'+_fullSaledata[_fullIndex][1]+'元'; }
if(canFull){_nextInfo='<i>已减'+_jian+'元</i>'+_nextInfo;_manInfo='&nbsp;满'+_fullSaledata[_fullIndex-1][0]+'减'+_fullSaledata[_fullIndex-1][1];}
else{_manInfo='&nbsp;满'+_fullSaledata[0][0]+'减'+_fullSaledata[0][1];}
$fullInfo.html('<span class="man-title"><em>满减</em><b>'+fullSale.acname+_manInfo+'！</b></span><span class="man-info">'+_nextInfo+'</span>');_totalJian=_totalJian+_jian;}
else if(fullSale.type==="zeng"){
 if(canFull){_nextInfo='已获赠品：<i>'+_zengData[2]+'</i>';}
else{_nextInfo='还差'+(_zengData[0]-_groupPrice).toFixed(2)+'元，可获得：<i>'+_zengData[2]+'</i>';}
$fullInfo.html('<span class="man-title man-zeng"><em>满赠</em><b>'+fullSale.acname+'</b></span><span class="man-info">'+_nextInfo+'</span>');}}});_totalPrice=_totalPrice-_totalJian;
if(_hasPromotion&&_totalPrice<lizi_postage_fee){that.promotionBuy=false;}
  else{that.promotionBuy=true;}
if(_totalJian>0){_jianInfo='，&nbsp;&nbsp;已减'+_totalJian+'元';}
if(_tobuyLines==0){objCom.$cartFooter.html('<div class="l">共0件商品，总额：<p>￥<strong>0.00</strong></p></div><div class="r"><a class="btn btn-off" href="javascript:;">去结算</a></div>');}
else{objCom.$cartFooter.html('<div class="l">共'+_totalItems+'件商品，总额：<p>￥<strong>'+_totalPrice.toFixed(2)+'</strong>'+_jianInfo+'</p></div><div class="r"><a class="btn" href="javascript:;">去结算</a></div>');}},

updateGoodsNum:function(id,num){var param={lineItemId:id,lineCount:num};
$.post('flow.php?step=ajax_update_cart',param);},

deleteGoods:function(id,$li){var that=this;$.ajax({url:'flow.php?step=drop_goods',type:'post',data:{'idList':id},
success:function(result){NALA.dialog.close();
setTimeout(function(){$li.addClass('on_remove');},100);
if($li.siblings('li').length==0){
	setTimeout(function(){$li.parents('.goods-list').remove();
    that.updateCart();},350);}
else{setTimeout(function(){$li.remove();that.updateCart();},350);}},
error:function(xhr){NALA.dialog.tip("删除失败，请稍后再试。("+xhr.status+")");}});}};
$(function(){NALA.myCart.init();});