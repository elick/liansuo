var NALA=NALA||{};
NALA.myUsers={

init:function(){var that=this, $collection = $("#collection-list"),$timeUp=$("#timeUp"), $to_sub=$("#to_sub"),
$add_bonus=$("#add_bonus"), $bonus_sn=$("#bonus_sn"),$com_sub = $("#com_sub"),$clear_btn = $("#clear_btn"),
$msg_form=$("#msg_form"),$msg_submit=$('#msg_submit'),$msg_type=$msg_form.find('input[name=msg_type]'),
$msg_title=$msg_form.find('input[name=msg_title]'),$msg_content=$msg_form.find('textarea[name=msg_content]');
$timeUp.each(function(){ var _this=$(this), timeTo=_this.data("time");  that.timeUp(_this, timeTo);});
 
 $msg_submit.click(function(){var _type = _title=_content=_txt='';
  if($msg_submit.find('img').length>0){return false;}
  _type=$.trim($msg_type.val()); _title=$.trim($msg_title.val()); _content=$.trim($msg_content.val());
 _title==""? _txt="标题不能为空!":_content==""?_txt="留言内容不能为空!":_txt="";
 if(_txt!=""){NALA.dialog.tip(_txt);return false;}
 if(_title.length<3){NALA.dialog.tip("留言标题不能少于3个字!");return false;}
 if(_content.length<10){NALA.dialog.tip("留言标题不能少于10个字!");return false;}
$msg_submit.html('<img src="images/loading.gif" /> 提交中');
$.ajax({url:'user.php?act=act_add_message',type:'post',dataType:'json',data:{'msg_type':_type,'msg_title':_title, 'msg_content':_content},
success:function(result){
if(result.status==1){NALA.dialog.tip('留言成功！');setTimeout(function(){location.href=result.url;},1000);}
else{NALA.dialog.tip('留言提交失败！');}},
error:function(xhr){NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");}});return false;});

$com_sub.click(function(){var _rec_id=_goods_id=_rank =_content=$rank='' ,$eval_list=$("#eval_list"),
 $goods_id=$eval_list.find('input[name=goods_id]'),$rec_id=$eval_list.find('input[name=rec_id]'),
 $content=$eval_list.find('textarea[name=content]'), $rank_ul=$('#eval_rank'); 
  if($com_sub.find('img').length>0){return false;}
  _rec_id=$.trim($rec_id.val()); _goods_id=$.trim($goods_id.val()); _content=$.trim($content.val());
  $rank = $rank_ul.find("li.selected");_rank=$.trim($rank.data('rank'));
 if(_content==""){NALA.dialog.tip("请输入您评论内容");return false;}
 if(_content.length<5){NALA.dialog.tip("评论内容不能少于5个字!");return false;}
 if(_content.length>300){NALA.dialog.tip("留言标题多余300个字!");return false;}
$msg_submit.html('<img src="images/loading.gif" /> 提交中');
$.ajax({url:'ma_com.php?act=act_addcom',type:'post',dataType:'json',data:{'rec_id':_rec_id,'goods_id':_goods_id, 'rank':_rank,'content':_content},
success:function(result){
if(result.status==1){NALA.dialog.tip('评论成功！');setTimeout(function(){location.href=result.url;},1000);}
else{NALA.dialog.tip('评论提交失败！');}},
error:function(xhr){NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");}});return false;});


$add_bonus.click(function(){var _bonus_sn = '';
  _bonus_sn=$.trim($bonus_sn.val()),Reg_code=/^[a-z0-9A-Z]+$/;
 if(_bonus_sn==""){NALA.dialog.tip("请输入优惠劵号");return false;}
 if(!Reg_code.test(_bonus_sn)){NALA.dialog.tip('请输入正确的优惠券号码。');return false;}
$.ajax({url:'user.php?act=act_add_bonus',type:'post',dataType:'json',data:{'bonus_sn':_bonus_sn},
success:function(result){
if(result.status==1){NALA.dialog.tip('添加成功！');setTimeout(function(){location.href=result.url;},1000);}
else{NALA.dialog.tip("添加失败！");}},
error:function(xhr){NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");}});return false;});

$to_sub.click(function(){  $('html, body, .page').animate({scrollTop:$("#footer").offset().top - 50 + "px"}, 300); return false; }); 
$clear_btn.click(function(){ var cb=function(){that.clear_history();}
 NALA.dialog.confirm('您确定要清除历史记录',cb); });

 if($collection.length==0){return;}
 $list=$collection.find('li'), 
 $list.on('click','a.del-btn',function(){var $del=$(this),
 $li=$del.parents('li'),
 id=$li.find('input[name=lineid]').val();
 var cb=function(){that.delCollection(id,$li);}
 NALA.dialog.confirm('您确定要删除此商品吗？',cb);}).on('click','a.to_cart',
function(){var num=1, $a=$(this), _id=$a.data('id'); 
 that.addTo_cart(_id,num);});

 
 },

delCollection:function(id,$li){var that=this;$.ajax({url:'user.php?act=delete_collection',type:'post',data:{'col_id':id},
success:function(result){NALA.dialog.close();
setTimeout(function(){$li.addClass('on_remove');}, 100);
if($li.siblings('li').length==0){setTimeout(function(){$li.parents('.collection-list').remove();}, 350);}
else{setTimeout(function(){$li.remove()}, 350);}},
error:function(xhr){NALA.dialog.tip("删除失败，请稍后再试。("+xhr.status+")");}});},

addTo_cart:function(_id,num){var that=this; $.ajax({url:'flow.php?step=add_to_cart',type:'post',data:{'goods_id':_id,'itemNumber':num}, dataType:"json",
success:function(result){ 
          if(result.status==0){NALA.dialog.tip(result.text);}
		  else if(result.status==2){NALA.dialog.tip('请先登录！');}
	      else if(result.status==1){ 
			  NALA.userInfo.cart=result.count; that.addCart_success_dialog();}},
error:function(xhr){$btn.removeClass('clicked');NALA.dialog.tip('服务器忙，请稍后再试。('+xhr.status+')');}});},

clear_history:function(){var that=this;$.ajax({url:'user.php?act=clear_history',type:'post',data:{'_id':1}, dataType:"json",
success:function(data){NALA.dialog.close();   $("#history_content").html(data.content); },
error:function(xhr){NALA.dialog.tip("清除失败，请稍后再试。("+xhr.status+")");}});},


addCart_success_dialog:function(){var $confirm=$('<div id="dg-confirm"><div class="dg_body"><div class="dg_box"><i class="iconfont green">&#xe644;</i>成功加入购物车！</div><div class="btm"><a href="javascript:NALA.dialog.close();" class="graybtn">继续逛逛</a><a href="flow.php?step=cart" class="btn">去结算</a></div></div></div>');
  $confirm.appendTo('body').addClass('pop_in'); NALA.dialog.$confirm=$confirm; },
  
 timeUp:function(_this,timeTo){
           
      var loops = 100,
          increment = timeTo/loops;
		  loopCount = 0,
          value = 0,
          interval = setInterval(updateTimer, 10);
		  
		  function updateTimer() {
                value += increment;
                loopCount++;
                _this.html(value.toFixed(2));

                if (loopCount >= loops) {
                    clearInterval(interval);
                    value = timeTo;
				      
                }             
            }
        


    }

  };

$(function(){NALA.myUsers.init();});