var NALA=NALA||{};
NALA.myUsers={ service:0, rtnMoney:0,$goods_list:null,
	
init:function(){var that=this , $claimType = $('#claimType'),$re_desc = $('#re_desc'),$submit=$('#submit'),
$desc_area =$('#desc_area'),$goodsStatus = $('#goodsStatus'),$exchange = $('#exchange');
 that.$goods_list= $('#goods_list'), that.listEvent(); that.updateRe(); that.shouhuo();
$claimType.change(function(){ var mySelect =this,selectIndex = 0,$rtn_money=$('#rtn_money'),
$return = $("#return"),$exchange = $('#exchange'),
selectIndex = $(mySelect).children('option:selected').val();
that.service=selectIndex;
if(selectIndex==1){$rtn_money.show();$exchange.hide();$return.show();}
else{$rtn_money.hide(); $return.hide();$exchange.show();}
});
$goodsStatus.change(function(){ var myGoods =this,selectIndex = 0,$shipping=$('#shipping'),
$invoice_no = $("#invoice_no"),
selectIndex = $(myGoods).children('option:selected').val();
if(selectIndex==1){$shipping.hide();$invoice_no.hide();}
else{$shipping.show();$invoice_no.show();}
});
$exchange.change(function(){ var myReason =this,selectIndex = 0,$shipping=$('#shipping'),
$invoice_no = $("#invoice_no");
$shipping.show(); $invoice_no.show();
});

$desc_area.focus(function(){$re_desc.height(106); $desc_area.height(100);})
.blur(function(){$re_desc.height(52); $desc_area.height(46);});
$submit.click(function(){ that.formCheck();});
},


listEvent:function(){var that=this,
$list=that.$goods_list.find('li'),
timeJson=NALA.timeJson,setTime=null,isEdit=false,numReg=/^[1-9]\d*$/; 
$list.each(function(){var _input=$(this).find('input[type=number]'),
_val=_input.val(),$time=$(this).find('em.time');
_input.data('oval',_val);});

$list.on('click','a.check',
function(){var $a=$(this);if($a.hasClass('check_no')){$a.removeClass('check_no');}
else{$a.addClass('check_no');}
that.updateRe();}).on('click','span',function(){var _this=$(this),_input=_this.siblings('input'),
_limit=parseInt(_input.attr('max')),num=_input.val();if(_this.hasClass('disabled')){return false;}
clearTimeout(setTime);if(!numReg.test(num)){_input.val(_input.data('oval'));return false;}
num=parseInt(num);if(_this.hasClass('add')){num++;}else{num--;}
if(num>_limit){num=_limit;_this.addClass('disabled');
  NALA.dialog.tip('您的本次订单中总共购买了'+_limit+'件该商品，您不能够选择更多商品');}
else if(num<=1){num=1;_this.addClass('disabled');}
_this.siblings('span').removeClass('disabled');
_input.val(num).data('oval',num);
setTime=setTimeout(function(){var id=_this.parents('li').find('input[name=lineid]').val();
that.updateRe();},500);}).on('keyup','input[type=number]',
function(){var _input=$(this),oval=_input.data('oval'),_limit=parseInt(_input.attr('max')),val=_input.val();
clearTimeout(setTime);
if(val==0){setTime=setTimeout(function(){_input.val(oval);},500);}
else{setTime=setTimeout(function(){if(numReg.test(val)){if(val>_limit){val=_limit;_input.val(val);}
_input.siblings('span').removeClass('disabled');
_input.data('oval',val).trigger('blur');
var id=_input.parents('li').find('input[name=lineid]').val();
that.updateRe();}
else{_input.val(oval);}},500);}});
},

updateRe:function(){var that=this, _totalPrice=0,
$rtnFee=$('#rtnFee'),$list=that.$goods_list.find('li');
$list.each(function(){var $li=$(this),$input,$one,n=p=np=item_num=0;
if($li.hasClass('disabled')||$li.find('a.check_no').length>0){return;}
$input=$li.find('input[type=number]');
$one=$li.find('em.p-one');
if($input.length>0){n=parseInt($input.val());}
else{n=1;}
p=parseFloat($one.text());np=n*p; _totalPrice+=np; });
$rtnFee.html(_totalPrice);
that.rtnMoney = _totalPrice;
},

formCheck:function(){
   var check = this, $goodsStatus = $('#goodsStatus'),$re_reason=$('#re_reason'),
   $rtnType=$('#rtnType'),$shipping_name =$('#shipping_name').val(),_submit = $('#submit'),
    $invoices = $('#invoices').val(), _gStatus=_reason=0;
	if(_submit.find('img').length>0){return false;}
   _gStatus = $goodsStatus.children('option:selected').val();
   _reason  = $re_reason.children('option:selected').val();
   $rtnType.val(check.service);
	if(!check.service){
	 NALA.dialog.tip('请选择您需要申请的服务');
	  return false;
	  }
	 else if(check.service ==1){
		 if(!check.rtnMoney){
			  NALA.dialog.tip('请选择您需要退款货物!');
	         return false;
			 }
		 if(!_gStatus){
			  NALA.dialog.tip('请选择货物状态!');
	         return false;
			 } 
		 
		 if(_gStatus ==2){
			 if($shipping_name==''){
			  NALA.dialog.tip('请输入配送方式!');
	         return false;
			 } 
			 if($invoices==''){
			  NALA.dialog.tip('请输发货单号!');
	         return false;
			  } 
			  if(!NALA.check.isNum($invoices)){
			  NALA.dialog.tip('请输入正确的快递单号 ！');
			  return false;}
			 
			 }
			 
		 }
	  else if(check.service ==2){
		   if(!check.rtnMoney){
			  NALA.dialog.tip('请选择您需要更换的货物!');
	         return false;
			 }
			 
		 if(!_reason){
			  NALA.dialog.tip('请选择您换货的原因');
	         return false;
			 }
			 
			 if($shipping_name==''){
			  NALA.dialog.tip('请输入配送方式!');
	          return false;
			 } 
			 if($invoices==''){
			  NALA.dialog.tip('请输发货单号!');
	          return false;
			  } 
			  if(!NALA.check.isNum($invoices)){
			  NALA.dialog.tip('请输入正确的快递单号 ！');
			  return false;}
			
		 }
		  check.updateReNum();
		 _submit.html('<img src="images/loading.gif" /> 提交中');
		 setTimeout(function(){$('#subForm').submit();},1500);
		 
	},
updateReNum:function(){var that=this, $list=that.$goods_list.find('li');
$list.each(function(){var $li=$(this),_id=_num=0; 
if($li.hasClass('disabled')||$li.find('a.check_no').length>0){_num = 0;}
else{_num=$li.find('input[type=number]').val();
   _id=$li.find('input[name=lineid]').val();
   var param={lineItemId:_id,lineCount:_num};
  $.post('user.php?act=ajax_update_return',param);
   
}

 });

},
shouhuo:function(){$("#confim_goods").click(function(event){var _this=$(this),callback=null;
 callback=function(){var _id=_this.data('id');
 $.ajax({url:'user.php?act=rtn_received',type:'post',dataType:'json',data:{'id':_id},
 success:function(data){
if(data.status==1){NALA.dialog.close(); location.href=data.url;  }
else{NALA.dialog.tip('确认收货失败。');}},
error:function(xhr){NALA.dialog.tip('服务器忙，请稍后再试。('+xhr.status+')');}});}
NALA.dialog.confirm('您确定收到货了吗？',callback);return false;});}

 };

$(function(){NALA.myUsers.init();});