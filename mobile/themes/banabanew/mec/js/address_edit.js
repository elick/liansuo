var NALA=NALA||{};NALA.address={init:function(){this.addrForm();},
addrForm:function(){var that=this,$box=$('#address_form'),$btn=$('#submit'),$name=$box.find('input[name=name]'),
$phone=$box.find('input[name=phone]'),$province=$box.find('select[name=province]'),
$city=$box.find('select[name=city]'),$area=$box.find('select[name=area]'),$street=$box.find('textarea[name=street]');
if(address_obj.id!=undefined){$name.val(address_obj.name);
$street.val(address_obj.street); $phone.val(address_obj.phone);
$province.val(address_obj.province_id);
switchArea($province,$city,address_obj.city_id,function(){switchArea($city,$area,address_obj.area_id);});}

switchAreaInit($province,$city,$area);
$btn.on('click',function(){var _name=$.trim($name.val()),_province=$province.val(),
_city=$city.val(),_area=$area.val(),_street=$.trim($street.val()),_phone=$.trim($phone.val()),params={};
if(!NALA.check.isNick(_name)){NALA.dialog.tip('请输入正确的收货人姓名！');return false;}
if(_area==0){NALA.dialog.tip('请选择所在地区！');return false;}
if(_street==""){NALA.dialog.tip('请输入街道地址！');return false;}
if(!NALA.check.isMobile(_phone)){NALA.dialog.tip('请输入正确的手机号码！');return false;}
params={'id':'','receiver':_name,'phone':_phone,'street':_street,'province':_province,'city':_city,'area':_area};
if(address_obj.id!=undefined){params.id=address_obj.id}
NALA.loading.show();
$.ajax({url:"flow.php?step=ajax_consignee",type:"post",cache:false,data:params,dataType:'json',
success:function(data){NALA.loading.close();
 switch(data.status){case 1:NALA.dialog.tip('收货地址保存成功！');
   setTimeout(function(){location.href=data.backurl;},2000);break;
   case 2:NALA.dialog.tip("保存失败：街道地址不规范");break;
   case 4:NALA.dialog.tip("保存失败：收货地址数量超过上限");break;
   case 7:NALA.dialog.tip(data.msg);break;
   default:NALA.dialog.tip("保存失败!");break;}},
   error:function(xhr){NALA.loading.close();NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");}});});}};
   $(function(){NALA.address.init();});
  
function switchAreaInit($province,$city,$area){
	$province.change(function(){switchArea($province,$city,null,
    function(){switchArea($city,$area,null);});});
    $city.change(function(){switchArea($city,$area,null);});}

function switchArea($parent,$child,selected_id,fn){
  var pid=$parent.val();
  if(pid==0){$child.empty().append('<option value="0">请选择</option>');fn&&fn();}
  else{$.ajax({url:"region.php",type:'post',data:{id:pid},dataType:'json',
  success:function(data){var options='<option value="0">请选择</option>';
  $.each(data,function(i){if(data[i].id==selected_id)
    {options+='<option value="'+this.id+'" selected="selected">'+this.name+'</option>';}
	else{options+='<option value="'+this.id+'" >'+this.name+'</option>';}});
	$child.empty().append(options);fn&&fn();}});}}