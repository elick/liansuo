  var NALA=NALA||{};
  NALA.login={init:function(){this.loginNala();
   if(location.href.indexOf('lotter')>0){
	   $('#header').hide();
       $('#footer').hide();}},
   
   loginNala:function(){
	   var $form = $('#login-form'),
		   $name = $form.find('input[name=username]'),
		   $pwd  = $form.find('input[name=password]'),
		   $back = $form.find('input[name=back_act]'),
		   $submit = $form.find('input[type=submit]');
		  
		   $form.submit(
		    function(){ 
				 var _name=_pwd=_remb=text='', params=null;
			      if($submit.hasClass('disabled'))
				      {return false;}
                       _name = $.trim($name.val()); 
					   _pwd  = $.trim($pwd.val());
					   _back = $.trim($back.val());
				   if(_name==''||_pwd==''){
						  text=(_name=='')?'请输入用户名':'请输入密码';
					       NALA.dialog.tip(text);
						     return false;
					    }
                     $submit.addClass('disabled').val('登录中');
	                 params={'username':_name,'password':_pwd,'back_act':_back,
					    '_spring_security_remember_me':'on'};
					 $.ajax({url:"user.php?act=signin",
					   type:'post', data:params, dataType:'json',
					    success:function(data){
						if(data.status==1){location.href=data.url;}
						else if(data.status==2){
							  $submit.removeClass('disabled').val('登 录');
						NALA.dialog.tip('您输入的帐号与密码不匹配！');}},
						
						error:function(xhr){if(xhr.status==200){location.href='user.php?act=order_list';}
						else{$submit.removeClass('disabled').val('登 录');
						NALA.dialog.tip('服务器忙，请稍后再试。('+xhr.status+')');}}});
						  return false;});}};
						$(function(){NALA.login.init();});