var NALA=NALA||{};NALA.distributor={subStep:1,init:function(){var that=this;that.dis_submit(); that.switch_friends();that.edit_account(); that.check_bonus(); that.switch_bonus();},
dis_submit:function(){var that=this,$form=$('#reg_form'),$pwdform=$('#pwd_form'), $submit=$('#reg_submit'),
$mobile=$form.find('input[name=mobile]'),$name=$form.find('input[name=realname]'),$pwd=$pwdform.find('input[name=password]'),
$pwd2=$pwdform.find('input[name=password2]'),$wxid=$form.find('input[name=wxid]');
$submit.on('click',function(){var _name=$.trim($name.val()),_wxid = $.trim($wxid.val()),  _mobile=$.trim($mobile.val()),_pwd = _pwd2 = '';
  if($submit.find('img').length>0){return false;}
  if( that.subStep ==2){sub_info(_name, _mobile,_wxid); }
  else{
  if(_name.length==0){NALA.dialog.tip('您的姓名不能为空！');return false;}
  if(!NALA.check.isMobile(_mobile)){NALA.dialog.tip('请输入正确的手机号！');return false;}
  else{$form.addClass('fly_out'); $pwdform.addClass('fly_in'); 
     that.subStep = 2; return false;}
   } });
function sub_info(_name,_mobile,_wxid){
   _pwd = $.trim($pwd.val());_pwd2 = $.trim($pwd2.val());
   if(_pwd.length<6){NALA.dialog.tip('请设置6位数以上的密码！');return false;}
   if(_pwd!=_pwd2){NALA.dialog.tip('两次密码输入不一致！');return false;}
   $submit.html('<img src="images/loading.gif" /> 提交中');
params={'name':_name, 'password':_pwd,'mobile':_mobile,'wxid':_wxid};

$.ajax({url:"distributor.php?act=ajax_apply_join",type:'post',data:params,dataType:'json',
success:function(data){ if(data.status==1){NALA.dialog.tip('提交成功！');setTimeout(function(){location.href=data.url;},1000);}
else{NALA.dialog.tip("提交失败！");$submit.html('马上提交');}},
error:function(xhr){NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");$submit.html('马上提交');}});
	
	}

},

edit_account:function(){
	var $zfb_form = $("#zfb_form"),$name = $zfb_form.find('input[name=ownername]'),$number = $zfb_form.find('input[name=number]'),$mobile1 = $zfb_form.find('input[name=mobile]'),$id1 = $zfb_form.find('input[name=card_id]'),
	$card_form = $("#card_form"),$card_name = $card_form.find('input[name=ownername]'),$card_number = $card_form.find('input[name=card_number]'),
	$confirm_number = $card_form.find('input[name=confirm_number]'), $open_bank = $card_form.find('input[name=open_bank]'), $mobile2 = $card_form.find('input[name=mobile]'),$id2 = $card_form.find('input[name=card_id]'), $sub_edit = $("#sub_edit"),
	_type = $sub_edit.data("type");
	$sub_edit.click(function(){var _this = this; 
		if($(_this).find('img').length>0){return false;}
	if(_type == 'zfb'){
		var id = _name = _number =_mobile=""; id= $.trim($id1.val()),_name = $.trim($name.val()),_number = $.trim($number.val()),
		_mobile = $.trim($mobile1.val());
		 if(_name.length==0){NALA.dialog.tip('账户姓名不能为空！');return false;}
		 if(_number.length==0){NALA.dialog.tip('支付宝账号不能为空');return false;}
		 if(!NALA.check.isMobile(_mobile)){NALA.dialog.tip('请输入正确的手机号');return false;}
		 params={'name':_name, 'number':_number,'id':id,'mobile':_mobile,'type':_type};
		}
	if(_type == 'bank_card'){
		var id= _name = _card_number = _card_number2 = _open_bank =_mobile= "";id= $.trim($id2.val());
		_name = $.trim($card_name.val()); _card_number = $.trim($card_number.val()); _card_number2 = $.trim($confirm_number.val());  _mobile = $.trim($mobile2.val());
		_open_bank = $.trim($open_bank.val());
		 if(_name.length==0){NALA.dialog.tip('持卡人姓名不能为空');return false;}
		 if (!(_card_number.match(/^\d+$/) && _card_number.length >= 16 && _card_number.length <=19)){
         NALA.dialog.tip('卡号必须在16-19位之间的数字！'); return false;
        }
		 if(_card_number!=_card_number2){
			 NALA.dialog.tip('两次输入的卡号不一致！'); return false;
			 }
		 if(!NALA.check.isMobile(_mobile)){NALA.dialog.tip('请输入正确的手机号');return false;}
		 if(_open_bank.length==0){
			 NALA.dialog.tip('开户银行不能为空！'); return false;
			 }
		params={'name':_name, 'number':_card_number,'open_bank':_open_bank,'mobile':_mobile,'id':id,'type':_type};
		
		}
		
 $(_this).html('<img src="images/loading.gif" /> 提交中');
 $.ajax({url:"distributor.php?act=ajax_update_account",type:'post',data:params,dataType:'json',
success:function(data){ if(data.status==1){NALA.dialog.tip('提交成功！');setTimeout(function(){location.href=data.url;},2000);}
else{NALA.dialog.tip("提交失败！");$(_this).html('马上提交');}},
error:function(xhr){NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");$(_this).html('马上提交');}});
	});
	},
	
check_bonus:function(){
	 var $bonus_id = $("#bonus_id"),$sub_bonus = $("#sub_bonus");
	 $sub_bonus.click(function(){ var _this = this;
		if($(_this).find('img').length>0){return false;}
		 $(_this).html('<img src="images/loading.gif" /> 提交中');
	     $("#bonus_form").submit();
	 });
	 },	
switch_bonus:function(){
	 var $bonus_tab= $("#bonus_tab");
	 $bonus_tab.on('click', 'a', function(){
	  var $a = $(this),_type = $a.data("type");
	  if($a.hasClass("on")){ return false;}
	     $a.addClass('on').siblings().removeClass("on");
	    if(_type==1){$("#mainiframe").attr("src","distributor.php?act=bonus_rank");}
		else{ $("#mainiframe").attr("src","distributor.php?act=bonus_desc"); }
	 });
	 },	
switch_friends:function(){
	var $switch_tab = $("#switch_tab"),$sanjiao = $("#sanjiao"),$team_list = $("#team_list");
	 $switch_tab.on('click','a',function(){
		 var _this = this,rank = $(_this).data("rank");
		 rank == 1?$sanjiao.animate({left:"23%"}):$sanjiao.animate({left:"73%"});
		 $team_list.html('<img class="loading" src="images/loading400.gif" /> ');
		 
$.ajax({url:"distributor.php?act=ajax_get_teams",type:'post',data:{'rank':rank},dataType:'json',
success:function(data){ if(data.status==1){$team_list.html(data.content);$(".next_page").html(data.page);}
else{NALA.dialog.tip("加载失败！");}},
error:function(xhr){NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");}});	 
		 });
	
	}

};


$(function(){NALA.distributor.init();});