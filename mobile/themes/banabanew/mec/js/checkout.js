var NALA=NALA||{};
NALA.checkout={Trade:{isFree:false,itemsPrice:0,pay_postage:0,surplus:0,pay_inte:0,bonus:0,postage:5,postage_cod:8},
$topay:$('#topay'),init:function(){var that=this, Trade=that.Trade, $bonus = $('#bonus'); 
Trade.itemsPrice=parseFloat(that.$topay.find('em.total').data('price'));Trade.bonus=$bonus.data('bonus');
that.usejifen();that.useSurplus();that.shipping();that.checkPay();that.updateTotalPrice();that.submit();},

usejifen:function(){var that=this,Trade=that.Trade,$topay=that.$topay,$inte=$topay.find('input[name=integral]'),$btn=$('#usejifen'),_num=parseFloat($btn.find('em.num').text());
$btn.click(function(){if($btn.hasClass('used')){$btn.removeClass('used');$inte.val(0);Trade.pay_inte=0;}
else{$btn.addClass('used');$inte.val(_num);Trade.pay_inte=_num;}
that.updateTotalPrice();return false;});},

useSurplus:function(){var that=this,Trade=that.Trade,$topay=that.$topay,$Surplus_ma=$('#Surplus_ma'),
ship_id=$topay.find('input[name=shipping_id]').val(), $btn=$('#surplus_btn'),
$surplus=$Surplus_ma.find('input[name=surplus]'),$postSurp=$topay.find('input[name=surplus]');
$postSurp.val(Trade.surplus);
$btn.click(function(){
 var _surplus=parseFloat($surplus.val());
 if(!NALA.check.isNum(_surplus)){NALA.dialog.tip('请输入正确的余额！'); return false;}
 if($btn.hasClass('used')){$btn.removeClass('used');}
 else{ $btn.addClass('used');
  $.ajax({url:"flow.php?step=change_surplus",data:{surplus:_surplus.toFixed(2),shipping_id:ship_id},dataType:'json',
  success:function(data){ 
      if(data.status==1)
	  { Trade.surplus = parseFloat(_surplus);$postSurp.val(Trade.surplus);
	    var rem_surplus = $Surplus_ma.data('surplus');
	    $Surplus_ma.html('<p class="used_surplus" >（您的账号余额：'+rem_surplus +'）<b class="green">已使用余额'+Trade.surplus+'</b></p>');
	    that.updateTotalPrice();return false;
	  }
	  else{ NALA.dialog.tip(data.msg);return false;}

  },
  error:function(xhr){NALA.dialog.tip("使用余额失败。("+xhr.status+")");}});
 }});},


shipping:function(){var that=this,$topay=that.$topay,Trade=that.Trade,$ship_id=$topay.find('input[name=shipping_id]'),
$addr_id=$topay.find('input[name=address_radio]'), $list=$('#ship_list').find('li'), $default = $list.first(),
default_id = $default.data('id'),_postpage = parseFloat($default.find('em.postage_num').text()); 
Trade.pay_postage= _postpage; $default.addClass('current');$ship_id.val(default_id);
$list.on('click',function(){var _this=$(this), _id=_this.data('id'), _addr_id=$addr_id.val();
if(_this.hasClass('current')){return false;}
else{ _this.siblings('li').removeClass('current'); _this.addClass('current');
     that.updatePostPrice(_addr_id, _id); } return false;});},

checkPay:function(){var that=this,$topay=that.$topay,Trade=that.Trade,_paytype='net',$type=$topay.find('input[name=payType]'),
adr_id=$topay.find('input[name=address_radio]').val(), ship_id=$topay.find('input[name=shipping_id]').val(),
$pay_id=$topay.find('input[name=payment]'),
$list=$('#pay_list').find('li');
$list.on('click',function(){var _this=$(this),_type=_this.data('type'),_payid=_this.data('payid'),_ptxt='微信支付';
if(_this.hasClass('current')){return false;}
else{_this.siblings('li').removeClass('current');_this.addClass('current');
if(_type=='bonuspay'){_ptxt='红包支付';that.updateBonusPay(_payid,_ptxt);}
else{$pay_id.val(_payid); $topay.find('.pay_tip').html(_ptxt);}
that.updatePostPrice(adr_id,ship_id);  }return false;});
},

updatePostPrice:function (addr_id, ship_id){var that=this,$topay=that.$topay,Trade=that.Trade ,
 $ship_id=$topay.find('input[name=shipping_id]');
 $.ajax({url:"flow.php?step=select_shipping",
 data:{address_id:addr_id, shipping_id:ship_id}, dataType:'json',
 success:function(data){ 
 if(data.status==1){Trade.pay_postage = parseFloat(data.shipping_fee);  $ship_id.val(ship_id); that.updateTotalPrice();}
 else{NALA.dialog.tip(data.msg);} },
 error:function(xhr){ NALA.dialog.tip("邮费更新失败。("+xhr.status+")");}});},
 
 updateBonusPay:function (_payid,_ptxt){var that=this,$topay=that.$topay,Trade=that.Trade ,
 $pay_id=$topay.find('input[name=payment]');
 $.ajax({url:"flow.php?step=bonus_pay",
 data:{payid:_payid}, dataType:'json',
 success:function(data){ 
 if(data.status==1){$pay_id.val(_payid);$topay.find('.pay_tip').html(_ptxt);}
 else{NALA.dialog.tip(data.msg);} },
 error:function(xhr){ NALA.dialog.tip("支付方式更换失败。("+xhr.status+")");}});},


updateTotalPrice:function(){var that=this,Trade=that.Trade,$total=that.$topay.find('em.total'),_total=0; 
_total=Trade.itemsPrice-Trade.pay_inte-Trade.surplus-Trade.bonus+Trade.pay_postage; $total.html(_total.toFixed(2));},
submit:function(){var $topay=this.$topay,$btn=$topay.find('a.btn'),$form=$topay.find('form');
$btn.click(function(){NALA.loading.show();setTimeout(function(){$form.submit();},30);});}};
$(function(){NALA.checkout.init();});