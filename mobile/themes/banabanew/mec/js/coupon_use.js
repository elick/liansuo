var NALA=NALA||{};NALA.coupon_use={$form:null,$coupon:null,_used_coupon:'',
init:function(){var that=this; that.$form=$('#coupon_form');
that.$coupon=that.$form.find('input[name=couponId]');
that._used_coupon=that.$coupon.val();that.checkCoupon();that.use_code_coupon();that.submit();},

checkCoupon:function(){var that=this,$list=$('#coupon_list').find('li');
$list.on('click',function(){var _this=$(this),_id=_this.data('id');
if(_this.hasClass('used')){return false;}
else{$('#coupon_code').html('<p class="input"><input type="text"></p><a href="javascript:;" class="btn">使用</a>');
_this.siblings('li').removeClass('used');_this.addClass('used');that.$coupon.val(_id);}
return false;});},

use_code_coupon:function(){var that=this,$box=$('#coupon_code'),Reg_code=/^[a-z0-9A-Z]+$/;
$box.on('click','a.btn',function(){var _val=$.trim($box.find('input').val());
if(!Reg_code.test(_val)){NALA.dialog.tip('请输入正确的优惠券号码。')
return false;}
check_coupon(_val);return false;});
$box.on('click','a.graybtn',function(){$box.html('<p class="input"><input type="text"></p><a href="javascript:;" class="btn">使用</a>');
that.$coupon.val('');return false;});
function check_coupon(_code){$.ajax({url:"flow.php?step=validate_bonus",type:'post',data:{"code":_code},dataType:'json',
 success:function(data){var _coup=data.judgeCouponData;
if(_coup==0){NALA.dialog.tip(data.msg);}
else{that.$coupon.val(data.couponId);
$box.html('<p class="tip">满'+data.man+'减'+data.jian+'<i class="iconfont">&#xf0012;</i></p><a href="javascript:;" class="graybtn">撤销</a>');
$('#coupon_list').find('li').removeClass('used');}}});}},
submit:function(){var that=this;$('#btm_btn').on('click','a',function(){if($(this).hasClass('btn')){
if(that.$coupon.val()==''){NALA.dialog.tip('请先选择优惠券！');return false;}}
else{that.$coupon.val('');}
setTimeout(function(){that.$form.submit();},30);return false;});}};
$(function(){NALA.coupon_use.init();});