var NALA=NALA||{};
    NALA.detail={
		init:function(){
		 var that=this; that.itempic_slides();
	     that.buy2cart(); that.sku_num(); that.more_detail();
		 that.favorites();$('#footer').addClass('footer_detail');},
		 itempic_slides:function(){var len=$('#item-pics ul li').length,
		   $count=$(".current_count"); $count.html('1/'+len);
		   $('#item-pics').Swipe({callback:function(pos){$(".current_count").html(pos+1+'/'+len);}})},

 more_detail:function(){var $click_more=$('#click_more');
   $click_more.click(function(){var _this=$(this),_id=item_info.id;
if(_this.find('img').length>0){return;}
     _this.html('<img id="ajax-loading" src="http://img.nala.com.cn/wap/images/loading.gif" />');
     $.ajax({url:'goods.php?act=detail',type:'post',cache:false,dataType:'html',data:{'id':_id},
           success:function(data){
			    if(data!='error'){ _this.parent().html(data);}
				else{_this.html('加载失败，重新加载..');}},
		error:function(xhr){_this.html('加载失败，重新加载..');}});})},

favorites:function(){var $fav_btn=$('#fav-btn');
    $fav_btn.click(function(){
		 var _this=$(this), _id=item_info.id,
		  _class=_this.attr('class'),flag=true;
		  if(!NALA.userInfo.login){
			    NALA.dialog.confirm('登录后才能收藏哦~',function(){location.href="user.php?act=login";});return;}
         if(!flag){return;}
           flag=false;
		   $.ajax({url:'user.php?act=collect',type:'post',cache:false,dataType:'json',data:{'id':_id},
		   success:function(data){
			  if(data.status==1){
			      if(data.isInFav){_this.addClass('fav-btn-on').find('em').html('已收藏');}
				  else{_this.removeClass('fav-btn-on').find('em').html('收藏');}
                  flag=true;}
             else{NALA.dialog.tip('服务器忙，重新收藏~');flag=true;}},
		   error:function(xhr){NALA.dialog.tip('服务器忙，重新收藏~'+xhr.status);flag=true;}});})},


       buy2cart:function(){ 
             var that=this,numReg=/^[1-9]\d*$/,$cbtn=$('#cart-btn'),
			     $buy_warp=$cbtn.parent(),$cpage=$('#cart-page'),$page=$('#page'),
                 $btn=$('#buy-btn'),$cart=$('#header-cart'),$input=$('#skunum').find('input'),
				 $skulist=$('#sku-list'),$storage=$('#storage'),
                 $clonePic=$('<img class="clone-pic" src="'+item_info.pic+'" />').appendTo("body"),
                 btn_pos=$btn.offset();
				if(item_info.time!=undefined){
					  var _time=parseInt(item_info.time/1000),
					   timeJson=NALA.timeJson,gotime=null,TimeDown=null,$time=null;
					   $cbtn.before('<div class="timedown"><i class="iconfont">&#xf004e;</i><span id="timedown"></span></div>');
					   $time=$('#timedown');
					   TimeDown=function(){var _TIME=timeJson(_time);
					   if(_time<0){clearInterval(gotime);location.reload();}
					   else{$time.html('<em>'+_TIME.hours+'</em>时<em>'+_TIME.mins+'</em>分<em>'+_TIME.secs+'</em>秒');_time--;}};
						TimeDown();gotime=setInterval(TimeDown,1000);}
                        $cbtn.click(function(){NALA.pageScroll.stop();$buy_warp.hide();
					    $page.addClass('pagehide');$cpage.addClass('pageshow');return false;});
						$cpage.on('click','a.close-btn',function(){$cpage.removeClass('pageshow');
						$page.removeClass('pagehide');$buy_warp.show();NALA.pageScroll.allow();
						return false;}).on('click',function(e){if(e.target.id.toLowerCase()=='cart-page'){
							$cpage.find('a.close-btn').trigger('click');} return false;});

        $btn.on('click',function(){
			   var _url='flow.php?step=add_to_cart',_buyNum=$.trim($input.val()),_max=parseInt($storage.text()),pram={};
			   if($btn.hasClass('clicked')){return false;}
               if(!numReg.test(_buyNum)){NALA.dialog.tip('请输入正确的购买数量！');$input.val(1);return false;}
               if(item_info.skuid==''){$skulist.addClass('warn-shark');
			     setTimeout(function(){$skulist.removeClass('warn-shark');},1050);return false;}
               _buyNum=parseInt(_buyNum);if(_max>99){_max=99;}
              if(_buyNum>_max){NALA.dialog.tip('您最多只能购买'+_max+'件商品！');$input.val(_max);return false;}
               $btn.addClass('clicked');
			   pram={goods_id:item_info.id,attr_id:item_info.skuid,itemNumber:_buyNum,atype:item_info.atype,exclusive:false,page:'detail'};
			 if(pram.atype=='qdzx'){pram.exclusive=true;}
             if(item_info.type=='jbhg'){pram.type='jbhg';_url='exchange.php?act=ajaxIntegralBuy';}
             $.ajax({url:_url,type:'post',data:pram,dataType:"json",
			  success:function(result){$btn.removeClass('clicked');
			  if(result.status==0){NALA.dialog.tip(result.text);}
			  else if(result.status==2){NALA.dialog.tip('请先登录！');}
			  else if(result.status==1){var yu_num=_max-_buyNum; 
			   $storage.text(yu_num); $skulist.find('a.on').data('storage',yu_num);
			   if(item_info.type=='jbhg'){location.href=result.url;}
			  else{$clonePic.addClass('fly2cart'); 
			     NALA.userInfo.cart=result.count;
			   setTimeout(function(){addCart_success_dialog();$clonePic.removeClass('fly2cart');},1250);}}},
			  error:function(xhr){$btn.removeClass('clicked');NALA.dialog.tip('服务器忙，请稍后再试。('+xhr.status+')');}});return false;});

function addCart_success_dialog(){var $confirm=$('<div id="dg-confirm"><div class="dg_body"><div class="dg_box"><i class="iconfont green">&#xe644;</i>成功加入购物车！</div><div class="btm"><a href="javascript:NALA.dialog.close();" class="graybtn">继续逛逛</a><a href="flow.php?step=cart" class="btn">去结算</a></div></div></div>');$confirm.appendTo('body').addClass('pop_in');NALA.dialog.$confirm=$confirm;NALA.pageScroll.stop();}},

sku_num:function(){
	  var that=this,$box=$('#skunum'),$input=$box.find('input'),$storage=$('#storage');$total=$('#total-price'),
          $sku_list=$('#sku-list'),$storage=$('#storage'),$sku_a=$sku_list.find('a'),numReg=/^[1-9]\d*$/,setTime=null;
		  $box.on('click','span', function(e){if(e.target.className=="add"||e.target.parentNode.className=="add"){countNum(1);}
		   else{countNum(-1);}return false;});
		  $input.on('keyup',function(){var _input=$(this);clearTimeout(setTime);
		  setTime=setTimeout(function(){var _total=_limit=0,val='';val=_input.val();
		  if(numReg.test(val)){_limit=parseInt($storage.text());val=parseInt(val);
		  if(val>_limit){val=_limit;NALA.dialog.tip('您最多只能购买'+val+'件商品！');}
         _input.val(val);_total=(val*item_info.price).toFixed(2);$total.text(_total);}
		 else{NALA.dialog.tip('请输入正确的购买数量！');_input.val(1);
		 $total.text(item_info.price);}},500);}).on('paste',function(){return false;});
		 $sku_list.on('click','a',function(){var _this=$(this);$sku_a.removeClass('on');_this.addClass('on');
		 item_info.skuid=_this.data('skuid');if($storage.data('limit')!=1){$storage.html(_this.data('storage'));}
		 item_info.price=(parseFloat(_this.data('attrprice'))+parseFloat(_this.data('price'))).toFixed(2);
		   val=$input.val();_total=(val*item_info.price).toFixed(2);$total.text(_total); 
		 });
		 function countNum(i){var num=$input.val(),max=parseInt($storage.text()),_total=0;
		 if(!numReg.test(num)){$input.val('1');return;}
          num=parseInt(num)+i;
		  switch(true){case num<1:num=1;break;case num>max:num=max;
		  NALA.dialog.tip('您最多只能购买'+max+'件商品！');break;default:break;}
          $input.val(num);
		  _total=(num*item_info.price).toFixed(2);
		  $total.text(_total);return false;}}};
		  $(function(){NALA.detail.init();});