var NALA=NALA||{};
NALA.myCash={

init:function(){ var that=this, $submit_wd = $("#submit_wd"),$save_card=$("#btn_save_card");
 
 $submit_wd.on('click',function(){var _this = this, $rem_cash=$("#rem_cash"), $wd_form=$('#wd_form'), 
 $wd_num =$wd_form.find('input[name=wd_num]'), $card_id =$wd_form.find('input[name=card_id]'),
 $user_note =$wd_form.find('textarea[name=user_note]'),
 _rem_cash=_wd_num=_card_id=_user_note=0;
 if($(_this).find('img').length>0){return false;}
 _rem_cash= $rem_cash.data("cash");_wd_num=$wd_num.val();
 _card_id=$card_id.val(); _user_note=$user_note.val();
 if(!NALA.check.isNum(_wd_num)){NALA.dialog.tip('请输入正确的提现金额！'); return false;}
 if(parseInt(_wd_num)<100){NALA.dialog.tip('提现金额不能少于100元！');  return false;}
 if(parseInt(_rem_cash)<parseInt(_wd_num)){NALA.dialog.tip('您输入的提现金额大于您的账户余额，请重新输入！');  return false;}
  $(_this).html('<img src="images/loading.gif" /> 提交中');
 $.ajax({url:'user.php?act=act_account',type:'post',dataType:'json',data:{'amount':_wd_num,'card_id':_card_id,'user_note':_user_note,'surplus_type':1},
 success:function(result){
if(result.status==1){location.href=result.url;}
else{NALA.dialog.tip(result.msg);$(_this).html('立即提交');}},
error:function(xhr){NALA.dialog.tip("提交失败，请稍后再试。("+xhr.status+")");}});});
that.accList(); 
 
$("#bank_card").on('click',function(){$(".chose").find('a.cur').removeClass('cur');$(this).addClass('cur');
  $("#zfb_form").hide(); $("#form_card").show();});
$("#zhifubao").on('click',function(){$(".chose").find('a.cur').removeClass('cur');$(this).addClass('cur'); $("#zfb_form").show();$("#form_card").hide();});

$("#redEnvelope").on('click',function(){$(".chose").find('div.on').removeClass('on');$(this).addClass('on');
  $("#withdrawalsbox").hide(); $("#redEnvelopebox").show();});
$("#withdrawals").on('click',function(){$(".chose").find('div.on').removeClass('on');$(this).addClass('on'); $("#withdrawalsbox").show();$("#redEnvelopebox").hide();});

$save_card.on('click', function(){ var $submit = $(this),$a=$(".chose").find('a.cur'), $flag=$a.data('id');
	if($submit.find('img').length>0){return false;}
 $flag==1? that.submit_card($submit):that.submit_zfb($submit);});
  
 },
 
submit_card:function($submit){ var $form_card = $("#form_card"),$owner_name=$form_card.find('input[name=owner_name]'),$mobile=$form_card.find('input[name=mobile]'),
$number=$form_card.find('input[name=number]'),$number1=$form_card.find('input[name=number1]'),$open_bank=$form_card.find('input[name=open_bank]'),$refund_id=$form_card.find('input[name=refund_id]'),
$card_id=$form_card.find('input[name=card_id]'),$upload_cer=$form_card.find('input[name=upload]'),
_id= _name=_mobile=_number=_number1=_open_bank=_upload_cer="";
_name=$.trim($owner_name.val());_mobile=$.trim($mobile.val());_number=$.trim($number.val());_number1=$.trim($number1.val());
_id=$.trim($card_id.val());_open_bank=$.trim($open_bank.val());
_refund_id=$.trim($refund_id.val());
 if(_name==""){NALA.dialog.tip('持卡人姓名不能为空！'); return false;}
 if(_mobile==""){NALA.dialog.tip('电话不能为空！'); return false;}
 if(!NALA.check.isMobile(_mobile)){NALA.dialog.tip('请输入正确的手机号！');return false;}
 if(_number==""||_number1==""){NALA.dialog.tip('卡号不能为空！'); return false;}
 if (!(_number.match(/^\d+$/) && _number.length >= 16 && _number.length <=19)){
    NALA.dialog.tip('卡号必须在16-19位之间的数字！'); return false;
  }
 if(_number!=_number1){NALA.dialog.tip('两次输入的卡号不一致！'); return false;}
 if(_open_bank==""){NALA.dialog.tip('开户银行不能为空！'); return false;}
 $submit.html('<img src="images/loading.gif" /> 提交中');
 $.ajax({url:'user.php?act=submit_card',type:'post',dataType:'json',data:{'id':_id,'name':_name, 'mobile':_mobile,'number':_number,'open_bank':_open_bank,'refund_id':_refund_id},
success:function(result){
if(result.status==1){NALA.dialog.tip('卡号提交成功！');setTimeout(function(){location.href=result.url;},1000);}
 else{NALA.dialog.tip('卡号提交失败！');}
 },
error:function(xhr){NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");}});
},

submit_zfb:function($submit){ var $form_zfb = $("#zfb_form"),$owner_name=$form_zfb.find('input[name=owner_name]'),$mobile=$form_zfb.find('input[name=mobile]'),
$number=$form_zfb.find('input[name=number]'),$card_id=$form_zfb.find('input[name=card_id]'),
_id= _name=_mobile=_number="";
_name=$.trim($owner_name.val());_mobile=$.trim($mobile.val());_number=$.trim($number.val());
_id=$.trim($card_id.val());
 if(_name==""){NALA.dialog.tip('账号姓名不能为空！'); return false;}
 if(_mobile==""){NALA.dialog.tip('电话不能为空！'); return false;}
 if(!NALA.check.isMobile(_mobile)){NALA.dialog.tip('请输入正确的手机号！');return false;}
 if(_number==""){NALA.dialog.tip('账号不能为空！'); return false;}
  $submit.html('<img src="images/loading.gif" /> 提交中');
 $.ajax({url:'user.php?act=submit_zfb',type:'post',dataType:'json',data:{'id':_id,'name':_name, 'mobile':_mobile,'number':_number},
success:function(result){
if(result.status==1){NALA.dialog.tip('支付宝提交成功！');setTimeout(function(){location.href=result.url;},1000);}
 else{NALA.dialog.tip('支付宝提交失败！');}
 },
error:function(xhr){NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");}});
},


accList:function(){var $list=$('#list_account').find('li');
 $list.each(function(){var $li=$(this),_id=$li.data('id'); 
 $li.on('click','input',function(){ var radio = this; 
  setDefault(radio, _id); return false;});

function setDefault(radio, _id){ var $radio = $(radio);  $.ajax({url:'user.php?act=setDefault',type:'post',dataType:'json',data:{'id':_id},
success:function(data){if(data.status==1){ $(".radio").removeAttr("checked"); NALA.dialog.tip('设置默认成功！'); $("#check_"+_id).attr("checked","checked");return false;}
else{NALA.dialog.tip('设置默认失败，请稍后再试。');}},
error:function(xhr){NALA.dialog.tip('服务器忙，请稍后再试。('+xhr.status+')');}});}

function setDel($dl,_id){$.ajax({url:'flow.php?step=drop_consignee',type:'post',dataType:'json',data:{'id':_id},
success:function(data){
if(data.status==1){NALA.dialog.close();NALA.dialog.tip('删除成功！');setTimeout(function(){$dl.remove();},100);}
else{NALA.dialog.tip('删除失败，请稍后再试。');}},
error:function(xhr){NALA.dialog.tip('服务器忙，请稍后再试。('+xhr.status+')');}});}});}
};

$(function(){NALA.myCash.init();});