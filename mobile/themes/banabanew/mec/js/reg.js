var NALA=NALA||{};NALA.register={regStep:1,psdStep:1,init:function(){var that=this;that.sendCaptcha();},
sendCaptcha:function(){var that=this,$form=$('#reg_form'),$pwdform=$('#pwd_form'),$submit=$('#reg_submit'),
$nosubmit=$('#no_submit'),$psd_submit=$('#psd_submit'),
$mobile=$form.find('input[name=mobile]'),$code=$form.find('input[name=code]'),$flag=$form.find('input[name=flag]'),$code_btn=$("#send-captcha");
$code_btn.on('click',function(){var phnumber=$.trim($mobile.val()); var _flag=$.trim($flag.val());
  if($code_btn.hasClass('disabled')){return false;}
if(!NALA.check.isMobile(phnumber)){NALA.dialog.tip('请输入正确的手机号！');return false;}
NALA.loading.show();$.ajax({url:'sms.php?act=send',type:'post',dataType:'json',data:{"mobile":phnumber,'flag':_flag},
success:function(data){NALA.loading.close();

if(data.status==1){var time=null,countdown=60;$code_btn.addClass('disabled');NALA.dialog.tip('短信验证码已发送，请注意查收');
$code_btn.text("60秒后重新发送");
timer=setInterval(function(){if(countdown<1){$code_btn.text("发送验证码");$code_btn.removeClass('disabled');clearTimeout(timer)}
else{$code_btn.text((--countdown)+"秒后重新发送");}},1000);}
else if(data.status>1){NALA.dialog.tip(data.text);}
else{NALA.dialog.tip('验证码发送失败，请稍后再试。');}},
error:function(xhr){NALA.loading.close();NALA.dialog.tip('服务器忙，请稍后再试。('+xhr.status+')');}});return false;});

$submit.click(function(){var phnumber= _code='';if(that.regStep==2){that.pwdSubmit(this,$pwdform);return false;}
if($submit.find('img').length>0){return false;}
_code=$.trim($code.val()); phnumber=$.trim($mobile.val()); 
 if(!_code.length){NALA.dialog.tip('验证码不能为空！');return false;}
 if(_code.length!=6){NALA.dialog.tip('验证码错误！');return false;}
$submit.html('<img src="images/loading.gif" /> 提交中');
$.ajax({url:'sms.php?act=check',type:'post',dataType:'json',data:{'code':_code, 'mobile':phnumber},
success:function(result){
if(result.status==1){$form.addClass('fly_out');$pwdform.addClass('fly_in');that.regStep=2;}
 else{NALA.dialog.tip('验证码错误！');}
setTimeout(function(){$submit.html('立即注册');},1000);},
error:function(xhr){NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");setTimeout(function(){$submit.html('立即注册');},1000);}});
 return false;});
 
$nosubmit.click(function(){NALA.dialog.tip('亲！没有推荐人是不能注册！');return false;});
 
$psd_submit.click(function(){var phnumber = _code='';
  phnumber=$.trim($mobile.val());
if(that.psdStep==2){that.pwdSend(this,phnumber);return false;}
if($psd_submit.find('img').length>0){return false;}
_code=$.trim($code.val());if(_code.length!=6){NALA.dialog.tip('验证码错误！');return false;}
$psd_submit.html('<img src="images/loading.gif" /> 提交中');
$.ajax({url:'sms.php?act=check',type:'post',dataType:'json',data:{'code':_code, 'mobile':phnumber},
success:function(result){
if(result.status==1){$form.addClass('fly_out');$pwdform.addClass('fly_in');that.psdStep=2;}
 else{NALA.dialog.tip('验证码错误！');}
setTimeout(function(){$psd_submit.html('立即找回');},1000);},
error:function(xhr){NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");setTimeout(function(){$psd_submit.html('立即找回');},1000);}});
return false;});

},


pwdSubmit:function(btn,$form){var _btn=$(btn),_username=_wxid=_recmded= _pwd=_pwd2=text=_back_act='',params={},$reg_form=$('#reg_form'),
$username=$reg_form.find('input[name=mobile]'),$recmded=$reg_form.find('input[name=recmded]'),$wxid=$reg_form.find('input[name=wxid]'),
$back_act=$form.find('input[name=back_act]'),$pwd=$form.find('input[name=password]'),$pwd2=$form.find('input[name=password2]');
if(_btn.find('img').length>0){return false;}
_username=$.trim($username.val()); _recmded=$.trim($recmded.val());_wxid=$.trim($wxid.val());_back_act=$.trim($back_act.val());
_pwd=$.trim($pwd.val());_pwd2=$.trim($pwd2.val());if(_pwd==''||_pwd2==''){NALA.dialog.tip('请输入密码！');return false;}
if(_pwd.length<6){NALA.dialog.tip('请设置6位数以上的密码！');return false;}
if(_pwd!=_pwd2){NALA.dialog.tip('两次密码输入不一致！');return false;}
_btn.html('<img src="images/loading.gif" /> 提交中');
params={'username':_username, 'password':_pwd,'recmded':_recmded,'wxid':_wxid,'back_act':_back_act};
$.ajax({url:"user.php?act=ajax_register",type:'post',data:params,dataType:'json',
success:function(data){ if(data.status==1){NALA.dialog.tip('注册成功！');setTimeout(function(){location.href=data.url;},2000);}
else{NALA.dialog.tip(data.text);_btn.html('立即注册');}},
error:function(xhr){NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");_btn.html('立即注册');}});return false;},

pwdSend:function(btn, phnumber){var _pwd=_pwd2='',_btn=$(btn),$pwdform=$('#pwd_form'),params={};
$pwd=$pwdform.find('input[name=password]'),$pwd2=$pwdform.find('input[name=password2]');
_pwd=$.trim($pwd.val());_pwd2=$.trim($pwd2.val());if(_pwd==''||_pwd2==''){NALA.dialog.tip('请输入密码！');return false;}
if(_pwd.length<6){NALA.dialog.tip('请设置6位数以上的密码！');return false;}
if(_pwd!=_pwd2){NALA.dialog.tip('两次密码输入不一致！');return false;}
_btn.html('<img src="images/loading.gif" /> 提交中');params={'mobile':phnumber, 'password':_pwd};
$.ajax({url:"user.php?act=reset_pwd",type:'post',data:params,dataType:'json',
success:function(data){if(data.status==1){NALA.dialog.tip('新密码已设置成功，请您及时登陆！');setTimeout(function(){location.href='user.php?act=login';},2000);}
else{NALA.dialog.tip('密码找回失败，请稍后再试！');_btn.html('立即找回');}},
error:function(xhr){NALA.dialog.tip("服务器忙，请稍后再试。("+xhr.status+")");_btn.html('立即找回');}});return false;}


};


$(function(){NALA.register.init();});