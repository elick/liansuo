var $window=$(window);var NALA={userInfo:{login:0},check:{isNick:function(str){var nickReg=/^[\u4e00-\u9fa5A-Za-z0-9-_]+$/;return nickReg.test(str);},isEmail:function(str){var emailReg=/^[a-z0-9][\w\.]*@([a-z0-9][a-z0-9-]*\.)+[a-z]{2,5}$/i;return emailReg.test(str);},isMobile:function(str){var mobileReg=/^1[34578][0-9]{9}$/;return mobileReg.test(str);},isUrl:function(str){var urlReg=/^http:\/\/([\w-]+\.)+[\w-]+(\/[\w-.\/?%&=]*)?$/;return urlReg.test(str);},isNum:function(str){var numReg=/^[0-9]\d*$/;return numReg.test(str);},
isPrice:function(str){var priceReg=/^(d*.d{0,2}|d+).*$/;return priceReg.test(str);}},

dialog:{$confirm:null,confirm:function(html,callback,initFn){var $confirm=null;if(this.$confirm!==null||$('#dg-tip').length>0){return;}else{$confirm=$('<div id="dg-confirm"><div class="dg_body"><div class="dg_box"><i class="iconfont blue">&#xe62b;</i>'+html+'</div><div class="btm"><a href="javascript:NALA.dialog.close();" class="graybtn">取消</a><a href="javascript:;" class="btn">确定</a></div></div></div>');}
$confirm.appendTo('body').addClass('pop_in');this.$confirm=$confirm;NALA.pageScroll.stop();$confirm.on('click','a.btn',function(){setTimeout(function(){callback();},50);return false;});initFn&&initFn();},close:function(){var that=this,$confirm=that.$confirm; 
if($confirm===null){return;}else{$confirm.addClass('pop_out');setTimeout(function(){$confirm.off().remove();that.$confirm=null;NALA.pageScroll.allow();},350);}},tip:function(html){var $Tip=null;if($('#dg-tip').length>0){return;}else{$Tip=$('<div id="dg-tip">'+html+'</div>');}
$Tip.appendTo('body').fadeIn();setTimeout(function(){$Tip.fadeOut(function(){$Tip.remove();});},2500);}},


loading:{$loading:null,show:function(){var $loading=null;if(this.$loading!==null||$('#dg-tip').length>0){return;}
 else{$loading=$('<div id="dg-loading"><div class="bd"><p><i class="iconfont">&#x3455;</i></p><h4>loading</h4></div></div>');}
$loading.appendTo('body');this.$loading=$loading;},
close:function(){var $loading=this.$loading;if($loading===null){return;}else{$loading.remove();this.$loading=null;}}},
pageScroll:{event:function(e){e.preventDefault();},
stop:function(){var fn=this.event;document.body.addEventListener('touchmove',fn,false);},
allow:function(){var fn=this.event;document.body.removeEventListener('touchmove',fn,false);}},
init:function(){this.showLoginInfo(); this.lazyload();this.appHideBar();this.header_more();},


showLoginInfo:function(){
	var that=this,
	$info=$('#footer_user_info'),
	$cart=$('#header-cart');
	$.ajax({url:'user.php?act=islogin',cache:false,dataType:'json',
	success:function(result){var _html='';that.userInfo=result;
	NALA.showLiziFixed();
	if($info.length>0&&result.login){
		_html='<a href="user.php" class="on">'+result.name+'</a>';
		$info.html(_html);}
if($cart.length>0&&result.cart>0){$cart.html('<i class="iconfont">&#xf0179;</i><em>'+result.cart+'</em>');}},
error:function(xhr){NALA.showLiziFixed();}});},


header_more:function(){var that=this,$more=$('#header_more');if($more.length==0){return;}
$more.click(function(){if($more.hasClass('header_more_active')){that.hideNavFixed();$more.removeClass('header_more_active');}else{that.showNavFixed('top');$more.addClass('header_more_active');}});},

showLiziFixed:function(){var that=this,_num=that.userInfo.cart||0,_cnum='<em class="cart_num">0</em>',$lizi=null;
if($('#index_ad').length==0){return;}
 if(_num>0){_cnum='<em class="cart_num" style="visibility:visible;">'+_num+'</em>';}
$lizi=$('<div class="lizi_fixed" id="lizi_fixed"><div class="bd">'+_cnum+'</div><div class="bg"></div></div>').appendTo('body');
$lizi.on('click','div.bd',function(){if($lizi.hasClass("lizi_fixed_opened")){that.hideNavFixed();$lizi.removeClass("lizi_fixed_opened");}
 else{$lizi.addClass("lizi_fixed_opened");that.showNavFixed('bottom');}
return false;}).on('click','div.bg',function(){if($lizi.hasClass("lizi_fixed_opened")){that.hideNavFixed();$lizi.removeClass("lizi_fixed_opened");}
return false;});},

showNavFixed:function(flag){var $nav=$('#nav_fixed'),_num=this.userInfo.cart||0;if(flag=='top'){flag='position: absolute; top:50px; right: 5px;';}else{flag='position: fixed; bottom: 75px; left: 10px;';}
if($nav.length>0){$nav.addClass('showNavFixed');}else{$nav=$('<div class="nav_fixed" style="'+flag+'" id="nav_fixed"><ul><li class="num1"><a href="./"><img src="../mobile/data/kong.gif" />首页</a></li><li class="num2"><a href="user.php"><img src="../mobile/data/kong.gif" />会员中心</a></li><li class="num3"><a href="cat_all.php"><img src="../mobile/data/kong.gif" />商品分类</a></li><li class="num4"><a href="flow.php?step=cart"><img src="../mobile/data/kong.gif" />购物车</a><em class="cart_num" id="header_cartnum">'+_num+'</em></li><li class="num5"><a href="distributor.php?act=apply_join"><img src="../mobile/data/kong.gif" />连锁中心</a></li></ul></div>').appendTo('body');setTimeout(function(){$nav.addClass('showNavFixed');},10);}
if(_num>0){$('#header_cartnum').html(_num).addClass('cartnum_anim');}},

hideNavFixed:function(){$('#nav_fixed').removeClass('showNavFixed');$('#header_cartnum').removeClass('cartnum_anim');},

timeJson:function(times){var oTime={};oTime.secs=Math.floor(times%60);oTime.mins=Math.floor(times/60%60);oTime.hours=Math.floor(times/60/60);oTime.days=0;if(oTime.hours>23){oTime.days=Math.floor(oTime.hours/24);oTime.hours=oTime.hours-oTime.days*24;}
if(oTime.secs<10){oTime.secs='0'+oTime.secs;}
if(oTime.mins<10){oTime.mins='0'+oTime.mins;}
if(oTime.hours<10){oTime.hours='0'+oTime.hours;}
if(oTime.days<10){oTime.days='0'+oTime.days;}
return oTime;},lazyload:function(){var winheight=$window.height(),$images=null,_timeout=null;$images=$("img").filter(function(){return $(this).attr("original")!==undefined;});function showImg(){var _winStop=0;if($images.length<1){return;}
_winStop=$window.scrollTop();$images.each(function(){var _this=$(this),_top=0,_src=_this.attr("original");if(_src===''){return;}
_top=_this.offset().top;if(_top>=_winStop-200&&_top<=(winheight+_winStop+50)){_this.hide().attr('src',_src).fadeIn();_this.attr("original","");_this.error(function(){this.src='images/kong.gif';_this.addClass('img_error');});}});$images=$images.filter(function(){return $(this).attr("original")!=='';});}
showImg();$window.on("scroll",function(){if(_timeout!==null){clearTimeout(_timeout);}
_timeout=setTimeout(function(){showImg();},50);});},appHideBar:function(){var _url=location.href;if(_url.match('android')){appHide();}else if(_url.match('ios')){appHide();}
function appHide(){$('#header').hide();$('#footer').hide();}}};
$(function(){NALA.init();});