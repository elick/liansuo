var NALA=NALA||{};NALA.myaddress={
 init:function(){this.addrList();},
 addrList:function(){var $list=$('#address_list').find('dl');
 $list.each(function(){var $dl=$(this),_id=$dl.data('id');
 $dl.on('click','a',function(){var className=this.className.toLowerCase();
 if(className.indexOf('current')>0){return false;}
 else if(className=='moren'){setMoren(this,_id);}
 else if(className=='del'){NALA.dialog.confirm('确定要删除此地址吗？',function(){setDel($dl,_id);});}
 else if(className=='edit'){location.href='flow.php?step=edit_consignee&id='+_id;}
return false;}).on('click','dd',function(){NALA.dialog.confirm('使用此地址完成订单吗？',
function(){location.href='flow.php?step=checkout&addressId='+_id;});return false;});});
function setMoren(a,_id){var $a=$(a);$.ajax({url:'flow.php?step=setMoren',type:'post',dataType:'json',data:{'id':_id},
success:function(data){if(data.status==1){NALA.dialog.tip('设置默认成功！');
$list.find('a.current').removeClass('current');$a.addClass('current');}
else{NALA.dialog.tip('设置默认失败，请稍后再试。');}},
error:function(xhr){NALA.dialog.tip('服务器忙，请稍后再试。('+xhr.status+')');}});}

function setDel($dl,_id){$.ajax({url:'flow.php?step=drop_consignee',type:'post',dataType:'json',data:{'id':_id},
success:function(data){
if(data.status==1){NALA.dialog.close();NALA.dialog.tip('删除成功！');setTimeout(function(){$dl.remove();},100);}
else{NALA.dialog.tip('删除失败，请稍后再试。');}},
error:function(xhr){NALA.dialog.tip('服务器忙，请稍后再试。('+xhr.status+')');}});}}};
$(function(){NALA.myaddress.init();});