var NALA=NALA||{};NALA.myorder={$list:null,
init:function(){var that=this;that.$list=$('#order_list'); that.$time=$('.time');that.cancleOrder();
that.$time.each(function(){ var _this=$(this); that.timeDown(_this);}); that.shouhuo();},
cancleOrder:function(){this.$list.on('click','a.cancle-order',function(event){var _this=$(this),callback=null;
callback=function(){var _id=_this.data('id');$.ajax({url:'user.php?act=cancel_order',type:'post',dataType:'json',
data:{'id':_id,'isbackToCart':0},success:function(data){
  if(data.status==1){NALA.dialog.close();_this.parent('p').html('已取消').parents('.order').addClass('disabled');}
  else{NALA.dialog.tip('取消订单失败。');}},error:function(xhr){NALA.dialog.tip('服务器忙，请稍后再试。('+xhr.status+')');}});}
 NALA.dialog.confirm('您确定要取消此订单吗？',callback);return false;});},

timeDown: function(_this){var that=this,timeJson=NALA.timeJson,  gotime=null,TimeDown=null,_time=parseInt(_this.data('time')/100);
TimeDown=function(){var _TIME=timeJson(_time); if(_time<0){clearInterval(gotime);
_this.replaceWith('<span class="error">此订单已收货</span>');}
else{_this.html(_TIME.days+'天'+_TIME.hours+'时'+_TIME.mins+'分'+_TIME.secs+'秒');_time--;}};
TimeDown();gotime=setInterval(TimeDown,1000);},

shouhuo:function(){this.$list.on('click','a.shou_btn',function(event){var _this=$(this),callback=null;
 callback=function(){var _id=_this.data('id');
 $.ajax({url:'user.php?act=affirm_received',type:'post',dataType:'json',data:{'id':_id},
 success:function(data){
if(data.status==1){NALA.dialog.close(); _this.parent('p').html('<span class="ok"><i class="iconfont">&#xf0013;</i>已完成</span>');
  $('#Down-'+_id).html('');
   }
else{NALA.dialog.tip('确认收货失败。');}},
error:function(xhr){NALA.dialog.tip('服务器忙，请稍后再试。('+xhr.status+')');}});}
NALA.dialog.confirm('您确定收到货了吗？',callback);return false;});}}
$(function(){NALA.myorder.init();});