<?php

header('content-type:text/html;charset = utf-8'); 
define('IN_ECTOUCH', true);

require(dirname(__FILE__) . '/include/init.php');

$action = $_GET['action'];

if($action == ''){
	$smarty->display('dianpuliebiao.dwt');
}elseif($action != ''){
	$shop_num = $_GET['shopnum'];
	if($shop_num == 'nandajie'){
		$smarty->assign('shopname' , $action);
		$smarty->assign('name' , "昌平二中");
		$smarty->assign('xy', "116.244375,40.226605");
		$smarty->assign('addr' , "北京市昌平区政府街5号");
		$smarty->display('map.dwt');
	}
	//东关店地址
	if($shop_num == 'dongguandian'){
		$smarty->assign('shopname' , $action);
		$smarty->assign('name' , "昌平东关宁馨苑");
		$smarty->assign('xy', "116.266199,40.227765");
		$smarty->assign('addr' , "北京市昌平区东关宁馨苑底商");
		$smarty->display('map.dwt');
	}
	//西关店地址
	if($shop_num == 'xiguandian'){
		$smarty->assign('shopname' , $action);
		$smarty->assign('name' , "昌平西关苏宁电器一层");
		$smarty->assign('xy', "116.240355,40.229924");
		$smarty->assign('addr' , "北京市昌平区西关苏宁电器(一层)");
		$smarty->display('map.dwt');
	}
	//阳光店地址
	if($shop_num == 'yangguangdian'){
		$smarty->assign('shopname' , $action);
		$smarty->assign('name' , "北京市昌平区东关二条阳光商厦");
		$smarty->assign('xy', "116.249922,40.227111");
		$smarty->assign('addr' , "北京市昌平区东关二条(麦当劳西侧)");
		$smarty->display('map.dwt');
	}
	//西环店地址
	if($shop_num == 'xihuandian'){
		$smarty->assign('shopname' , $action);
		$smarty->assign('name' , "北京市昌平区西环路建设银行");
		$smarty->assign('xy', "116.233088,40.231635");
		$smarty->assign('addr' , "北京市昌平区西环路建设银行（对面）");
		$smarty->display('map.dwt');
	}
	//南环店地址
	if($shop_num == 'nanhuandian'){
		$smarty->assign('shopname' , $action);
		$smarty->assign('name' , "北京市昌平区西环南路345车站");
		$smarty->assign('xy', "116.233941,40.220508");
		$smarty->assign('addr' , "北京市昌平区西环南路345车站");
		$smarty->display('map.dwt');
	}
	//北农店地址
	if($shop_num == 'beinongdian'){
		$smarty->assign('shopname' , $action);
		$smarty->assign('name' , "北京市昌平区回龙观朱辛庄华北电力大学");
		$smarty->assign('xy', "116.311945,40.096593");
		$smarty->assign('addr' , "北京市昌平区回龙观朱辛庄华北电力大学");
		$smarty->display('map.dwt');
	}
	//回龙店地址
	if($shop_num == 'huilongguan'){
		$smarty->assign('shopname' , $action);
		$smarty->assign('name' , "北京市昌平区回龙观龙腾苑五区楼一层底商");
		$smarty->assign('xy', "116.345164,40.081146");
		$smarty->assign('addr' , "北京市昌平区回龙观龙腾苑五区楼一层底商");
		$smarty->display('map.dwt');
	}
	//大兴旧宫1店地址
	if($shop_num == 'daxing1dian'){
		$smarty->assign('shopname' , $action);
		$smarty->assign('name' , "北京市大兴区旧宫佳和园小区一层底商");
		$smarty->assign('xy', "116.450546,39.812212");
		$smarty->assign('addr' , "北京市大兴区旧宫佳和园小区一层底商");
		$smarty->display('map.dwt');
	}
	//大兴旧宫2店地址
	if($shop_num == 'daxing2dian'){
		$smarty->assign('shopname' , $action);
		$smarty->assign('name' , "北京市大兴区旧宫镇旧宫西路40号");
		$smarty->assign('xy', "116.439886,39.809915");
		$smarty->assign('addr' , "北京市大兴区旧宫镇旧宫西路40号");
		$smarty->display('map.dwt');
	}
	//新世纪店地址
	if($shop_num == 'xinshiji'){
		$smarty->assign('shopname' , $action);
		$smarty->assign('name' , "北京市昌平区鼓楼南大街17号新世纪商城");
		$smarty->assign('xy', "116.239898,40.224382");
		$smarty->assign('addr' , "北京市昌平区鼓楼南大街17号新世纪商城");
		$smarty->display('map.dwt');
	}
}

?>