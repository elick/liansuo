<?php



/**

 * ECSHOP 首页文件

 * ============================================================================

 * * 版权所有 2005-2012 北京招聚网络科技有限公司，并保留所有权利。

 * 网站地址: http://www.ECSHOP.com；

 * ----------------------------------------------------------------------------

 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和

 * 使用；不允许对程序代码以任何形式任何目的的再发布。

 * ============================================================================

 * $Author: liubo $

 * $Id: index.php 17217 2011-01-19 06:29:08Z liubo $

*/

//

define('IN_ECTOUCH', true);

require(dirname(__FILE__) . '/include/init.php');
	
if ((DEBUG_MODE & 2) != 2)

{

    $smarty->caching = true;

}


//判断是否有ajax请求

$act = !empty($_GET['act']) ? $_GET['act'] : '';

if ($act == 'cat_rec')

{

    $rec_array = array(1 => 'best', 2 => 'new', 3 => 'hot');

    $rec_type = !empty($_REQUEST['rec_type']) ? intval($_REQUEST['rec_type']) : '1';

    $cat_id = !empty($_REQUEST['cid']) ? intval($_REQUEST['cid']) : '0';

    include_once('include/cls_json.php');

    $json = new JSON;

    $result   = array('error' => 0, 'content' => '', 'type' => $rec_type, 'cat_id' => $cat_id);
	
	$smarty->assign('new_goods',       get_recommend_goods('new'));     // 最新商品

    $smarty->assign('cat_rec_sign', 1);

    $result['content'] = $smarty->fetch('library/recommend_' . $rec_array[$rec_type] . '.lbi');

    die($json->encode($result));

}



/*------------------------------------------------------ */

//-- 判断是否存在缓存，如果存在则调用缓存，反之读取相应内容

/*------------------------------------------------------ */

/* 缓存编号 */

$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang']));



if (!$smarty->is_cached('new.dwt', $cache_id))

{

    
    /* 首页推荐分类 */

    $cat_recommend_res = $db->getAll("SELECT c.cat_id, c.cat_name, cr.recommend_type FROM " . $ecs->table("cat_recommend") . " AS cr INNER JOIN " . $ecs->table("category") . " AS c ON cr.cat_id=c.cat_id");

    if (!empty($cat_recommend_res))

    {

        $cat_rec_array = array();

        foreach($cat_recommend_res as $cat_recommend_data)

        {

            $cat_rec[$cat_recommend_data['recommend_type']][] = array('cat_id' => $cat_recommend_data['cat_id'], 'cat_name' => $cat_recommend_data['cat_name']);

        }

        $smarty->assign('cat_rec', $cat_rec);

    }

    /* 页面中的动态内容 */

    assign_dynamic('new');

}
	$smarty->assign('new_goods',get_recommend_goods('new'));     // 最新商品
	$smarty->display('new.dwt', $cache_id);
	
	exit();  //why?

?>