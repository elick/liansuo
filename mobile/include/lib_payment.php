<?php

/**
 * ECSHOP 支付接口函数库
 * ============================================================================
 * 版权所有 2005-2010 北京招聚网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ECSHOP.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: yehuaixiao $
 * $Id: lib_payment.php 17218 2011-01-24 04:10:41Z yehuaixiao $
 */

if (!defined('IN_ECTOUCH'))
{
    die('Hacking attempt');
}

/**
 * 取得返回信息地址
 * @param   string  $code   支付方式代码
 */
function return_url($code)
{
    return $GLOBALS['ecs']->url() . 'respond.php?code=' . $code;
}

/**
 *  取得某支付方式信息
 *  @param  string  $code   支付方式代码
 */
function get_payment($code)
{
    $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('touch_payment').
           " WHERE pay_code = '$code' AND enabled = '1'";
    $payment = $GLOBALS['db']->getRow($sql);

    if ($payment)
    {
        $config_list = unserialize($payment['pay_config']);

        foreach ($config_list AS $config)
        {
            $payment[$config['name']] = $config['value'];
        }
    }

    return $payment;
}

/**
 *  通过订单sn取得订单ID
 *  @param  string  $order_sn   订单sn
 *  @param  blob    $voucher    是否为会员充值
 */
function get_order_id_by_sn($order_sn, $voucher = 'false')
{
    if ($voucher == 'true')
    {
        if(is_numeric($order_sn))
        {
              return $GLOBALS['db']->getOne("SELECT log_id FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE order_id=" . $order_sn . ' AND order_type=1');
        }
        else
        {
            return "";
        }
    }
    else
    {
        if(is_numeric($order_sn))
        {
            $sql = 'SELECT order_id FROM ' . $GLOBALS['ecs']->table('order_info'). " WHERE order_sn = '$order_sn'";
            $order_id = $GLOBALS['db']->getOne($sql);
        }
        if (!empty($order_id))
        {
            $pay_log_id = $GLOBALS['db']->getOne("SELECT log_id FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE order_id='" . $order_id . "'");
            return $pay_log_id;
        }
        else
        {
            return "";
        }
    }
}

/**
 *  通过订单ID取得订单商品名称
 *  @param  string  $order_id   订单ID
 */
function get_goods_name_by_id($order_id)
{
    $sql = 'SELECT goods_name FROM ' . $GLOBALS['ecs']->table('order_goods'). " WHERE order_id = '$order_id'";
    $goods_name = $GLOBALS['db']->getCol($sql);
    return implode(',', $goods_name);
}

/**
 * 检查支付的金额是否与订单相符
 *
 * @access  public
 * @param   string   $log_id      支付编号
 * @param   float    $money       支付接口返回的金额
 * @return  true
 */
function check_money($log_id, $money)
{
    if(is_numeric($log_id))
    {
        $sql = 'SELECT order_amount FROM ' . $GLOBALS['ecs']->table('pay_log') .
              " WHERE log_id = '$log_id'";
        $amount = $GLOBALS['db']->getOne($sql);
    }
    else
    {
        return false;
    }
    if ($money == $amount)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * 修改订单的支付状态
 *
 * @access  public
 * @param   string  $log_id     支付编号
 * @param   integer $pay_status 状态
 * @param   string  $note       备注
 * @return  void
 */
function order_paid($log_id, $pay_status = PS_PAYED, $note = '')
{
    /* 取得支付编号 */
    $log_id = intval($log_id);
    if ($log_id > 0)
    {
        /* 取得要修改的支付记录信息 */
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') .
                " WHERE log_id = '$log_id'";
        $pay_log = $GLOBALS['db']->getRow($sql);
        if ($pay_log && $pay_log['is_paid'] == 0)
        {
            /* 修改此次支付操作的状态为已付款 */
            $sql = 'UPDATE ' . $GLOBALS['ecs']->table('pay_log') .
                    " SET is_paid = '1' WHERE log_id = '$log_id'";
            $GLOBALS['db']->query($sql);

            /* 根据记录类型做相应处理 */
            if ($pay_log['order_type'] == PAY_ORDER)
            {
                /* 取得订单信息 */
                $sql = 'SELECT order_id, user_id, order_sn, consignee, address, tel, shipping_id, extension_code, extension_id, goods_amount ,pay_name ' .
                        'FROM ' . $GLOBALS['ecs']->table('order_info') .
                       " WHERE order_id = '$pay_log[order_id]'";
                $order    = $GLOBALS['db']->getRow($sql);
                $order_id = $order['order_id'];
                $order_sn = $order['order_sn'];

                /* 修改订单状态为已付款 */
                $sql = 'UPDATE ' . $GLOBALS['ecs']->table('order_info') .
                            " SET order_status = '" . OS_CONFIRMED . "', " .
                                " confirm_time = '" . gmtime() . "', " .
                                " pay_status = '$pay_status', " .
                                " pay_time = '".gmtime()."', " .
                                " money_paid = order_amount," .
                                " order_amount = 0 ".
                       "WHERE order_id = '$order_id'";
                $GLOBALS['db']->query($sql);


                /* 记录订单操作记录 */
                order_action($order_sn, OS_CONFIRMED, SS_UNSHIPPED, $pay_status, $note, $GLOBALS['_LANG']['buyer']);
				
				 /* 发微信信息 */
                //send_weixin_info($order_sn, $order['goods_amount'] , $order['user_id']);
				//csbcscbcsb
				send_weixin_message($order_sn , $order['consignee'] , $order['order_id'] , $order['user_id'] , $order['pay_name'], $order['goods_amount']);
				

                /* 如果需要，发短信 */
                if ($GLOBALS['_CFG']['sms_order_payed'] == '1' && $GLOBALS['_CFG']['sms_shop_mobile'] != '')
                {
                    include_once(ROOT_PATH.'include/cls_sms.php');
                    $sms = new sms();
					$sms_error = array();
                    if(!$sms->send($GLOBALS['_CFG']['sms_shop_mobile'], sprintf($GLOBALS['_LANG']['order_payed_sms'], $order_sn, $order['consignee'], $order['tel']), $sms_error)){
						echo $sms_error;
					}
                }
				
				
                /* 对虚拟商品的支持 */
                $virtual_goods = get_virtual_goods($order_id);
                if (!empty($virtual_goods))
                {
                    $msg = '';
                    if (!virtual_goods_ship($virtual_goods, $msg, $order_sn, true))
                    {
                        $GLOBALS['_LANG']['pay_success'] .= '<div style="color:red;">'.$msg.'</div>'.$GLOBALS['_LANG']['virtual_goods_ship_fail'];
                    }

                    /* 如果订单没有配送方式，自动完成发货操作 */
                    if ($order['shipping_id'] == -1)
                    {
                        /* 将订单标识为已发货状态，并记录发货记录 */
                        $sql = 'UPDATE ' . $GLOBALS['ecs']->table('order_info') .
                               " SET shipping_status = '" . SS_SHIPPED . "', shipping_time = '" . gmtime() . "'" .
                               " WHERE order_id = '$order_id'";
                        $GLOBALS['db']->query($sql);

                         /* 记录订单操作记录 */
                        order_action($order_sn, OS_CONFIRMED, SS_SHIPPED, $pay_status, $note, $GLOBALS['_LANG']['buyer']);
                        $integral = integral_to_give($order);
                        log_account_change($order['user_id'], 0, 0, intval($integral['rank_points']), intval($integral['custom_points']), sprintf($GLOBALS['_LANG']['order_gift_integral'], $order['order_sn']));
                    }
                }

            }
            elseif ($pay_log['order_type'] == PAY_SURPLUS)
            {
                $sql = 'SELECT `id` FROM ' . $GLOBALS['ecs']->table('user_account') .  " WHERE `id` = '$pay_log[order_id]' AND `is_paid` = 1  LIMIT 1";
                $res_id=$GLOBALS['db']->getOne($sql);
                if(empty($res_id))
                {
                    /* 更新会员预付款的到款状态 */
                    $sql = 'UPDATE ' . $GLOBALS['ecs']->table('user_account') .
                           " SET paid_time = '" .gmtime(). "', is_paid = 1" .
                           " WHERE id = '$pay_log[order_id]' LIMIT 1";
                    $GLOBALS['db']->query($sql);

                    /* 取得添加预付款的用户以及金额 */
                    $sql = "SELECT user_id, amount FROM " . $GLOBALS['ecs']->table('user_account') .
                            " WHERE id = '$pay_log[order_id]'";
                    $arr = $GLOBALS['db']->getRow($sql);

                    /* 修改会员帐户金额 */
                    $_LANG = array();
                    include_once(ROOT_PATH . 'lang/' . $GLOBALS['_CFG']['lang'] . '/user.php');
                    log_account_change($arr['user_id'], $arr['amount'], 0, 0, 0, $_LANG['surplus_type_0'], ACT_SAVING);
                }
            }
        elseif ($pay_log['order_type'] == 88)
            { 
			        $sql = 'UPDATE ' . $GLOBALS['ecs']->table('prepaid_bonus') .
                           " SET  is_paid = 1 ,pay_time = '" .gmtime(). "'" .
                           " WHERE pre_id = '$pay_log[order_id]' LIMIT 1";
                   $GLOBALS['db']->query($sql);
				   $user_id = $_SESSION['user_id'];
				   send_weixin_normally( '您的红包充值成功,可以去分销中心查看' , $user_id , 'hongbao' );
				
			}
			elseif ($pay_log['order_type'] == 111)
            { 
			        $sql = 'UPDATE ' . $GLOBALS['ecs']->table('join') .
                           " SET  status = 1 ,pay_time = '" .gmtime(). "'" .
                           " WHERE jn_id = '$pay_log[order_id]' LIMIT 1";
                   $GLOBALS['db']->query($sql);
				   //$user_id = $_SESSION['user_id'];
				   $user_id = $GLOBALS['db']->getOne("SELECT j.user_id FROM ecs_join j LEFT JOIN ecs_pay_log p ON p.order_id=j.jn_id WHERE p.log_id = '".$pay_log['log_id']."'");
				   //file_put_contents('a0.txt',"SELECT j.user_id FROM ecs_join j LEFT JOIN ecs_pay_log p ON p.order_id=j.jn_id WHERE p.log_id = '".$pay_log['log_id']."'");
				   send_weixin_normally( '        申请已经提交成功！我们将对您的信息进行审核（预计2-10个工作日），审核结果我们的客服专员会与您电话联系,请保持电话畅通。
        想马上揭秘就去商城看看，消费'.$GLOBALS['_CFG']['baseline'].'元即可提前了解招聚云商城的秘密！点击我去查看~' , $user_id, 'ad_apply' );
			}
			
        }
        else
        {
            /* 取得已发货的虚拟商品信息 */
            $post_virtual_goods = get_virtual_goods($pay_log['order_id'], true);

            /* 有已发货的虚拟商品 */
            if (!empty($post_virtual_goods))
            {
                $msg = '';
                /* 检查两次刷新时间有无超过12小时 */
                $sql = 'SELECT pay_time, order_sn FROM ' . $GLOBALS['ecs']->table('order_info') . " WHERE order_id = '$pay_log[order_id]'";
                $row = $GLOBALS['db']->getRow($sql);
                $intval_time = gmtime() - $row['pay_time'];
                if ($intval_time >= 0 && $intval_time < 3600 * 12)
                {
                    $virtual_card = array();
                    foreach ($post_virtual_goods as $code => $goods_list)
                    {
                        /* 只处理虚拟卡 */
                        if ($code == 'virtual_card')
                        {
                            foreach ($goods_list as $goods)
                            {
                                if ($info = virtual_card_result($row['order_sn'], $goods))
                                {
                                    $virtual_card[] = array('goods_id'=>$goods['goods_id'], 'goods_name'=>$goods['goods_name'], 'info'=>$info);
                                }
                            }

                            $GLOBALS['smarty']->assign('virtual_card',      $virtual_card);
                        }
                    }
                }
                else
                {
                    $msg = '<div>' .  $GLOBALS['_LANG']['please_view_order_detail'] . '</div>';
                }

                $GLOBALS['_LANG']['pay_success'] .= $msg;
            }

           /* 取得未发货虚拟商品 */
           $virtual_goods = get_virtual_goods($pay_log['order_id'], false);
           if (!empty($virtual_goods))
           {
               $GLOBALS['_LANG']['pay_success'] .= '<br />' . $GLOBALS['_LANG']['virtual_goods_ship_fail'];
           }
        }
    }
}
function send_weixin_normally( $msg , $user_id , $flag ){

	//初始化微信
	include_once(ROOT_PATH .'wechat/wechat.class.php');
	$sql_ewm = "SELECT  token, appid, appsecret, access_token  FROM  wxch_config ORDER BY dateline ASC ";

	// 连接微信配置数据库，读取appid等信息，赋值给数组，进行初始化。 
	$row = $GLOBALS['db']->getRow($sql_ewm);    //getall 为获取多维数组， getone 为获得一个变量

	$options = array(
		'token'=> $row['token'],               //填写你设定的key
		'appid'=>$row['appid'],
		'access_token'=>$row['access_token'],
		'appsecret'=>$row['appsecret'] );
	$weObj = new Wechat($options);        //实例化 

	//获取基本信息
	$time = local_date($GLOBALS['_CFG']['time_format'], gmtime());
	//$user_id = $_SESSION['user_id'];
	$sql="SELECT  u.wxid , w.nickname , u.recmded_id , u.real_name FROM " . $GLOBALS['ecs']->table('users') . " u LEFT JOIN wxch_user w on w.wxid=u.wxid WHERE user_id='$user_id'";
	$wx=$GLOBALS['db']->getRow($sql);
	
	//红包充值后发送的消息
	if($flag == 'hongbao'){
		$msg = '亲爱的'.$wx['nickname'].' , '.$msg;
		$data_up=array(
			'touser'   =>$wx['wxid'],                                    //提示每个客服有信息来了
			'msgtype'  =>'news',
			'news'    =>array('articles'=>array(array('title'=>'红包充值成功' , 'description'=> $msg , 'url'=> "http://".$_SERVER['HTTP_HOST']."/mobile/distributor.php?act=available_bonus" , 'picurl'=> '')))      //将话术与用户发来的内容拼接，发给客服人员。.$arr["Content"]
		);	
		$weObj->sendCustomMessage($data_up);     //发送客服消息
	}
	
	if($flag == 'ad_apply'){
		//$msg = '亲爱的'.$wx['nickname'].' , '.$msg;
		$data_up=array(
			'touser'   =>$wx['wxid'],                                    //提示每个客服有信息来了
			'msgtype'  =>'news',
			'news'    =>array('articles'=>array(array('title'=>'申请成功' , 'description'=> $msg , 'url'=> "http://".$_SERVER['HTTP_HOST']."/mobile" , 'picurl'=> '')))
		);	
		$weObj->sendCustomMessage($data_up);     //发送客服消息
	}
	
}

function send_weixin_message($order_sn , $consignee , $order_id , $user_id , $pay_name ,$goods_amount){

	//初始化微信
	include_once(ROOT_PATH .'wechat/wechat.class.php');
	$sql_ewm = "SELECT  token, appid, appsecret,access_token  FROM  wxch_config ORDER BY dateline ASC ";
		
	// 连接微信配置数据库，读取appid等信息，赋值给数组，进行初始化。 
	$row = $GLOBALS['db']->getRow($sql_ewm);    //getall 为获取多维数组， getone 为获得一个变量
	$options = array(
		'token'=> $row['token'],               //填写你设定的key
		'appid'=>$row['appid'],
		'access_token'=>$row['access_token'],
		'appsecret'=>$row['appsecret'] );
	$weObj = new Wechat($options);        //实例化 
	
	//获取基本信息
	$time = local_date($GLOBALS['_CFG']['time_format'], gmtime());
	//$user_id = $_SESSION['user_id'];
	$sql="SELECT  u.wxid , w.nickname , u.recmded_id , u.real_name FROM " . $GLOBALS['ecs']->table('users') . " u LEFT JOIN wxch_user w on w.wxid=u.wxid WHERE user_id='$user_id'";
	$wx=$GLOBALS['db']->getRow($sql);
	
	//先给自己推送一条消费信息
	$msg_up = '亲爱的'.$wx['nickname'].'，您的订单支付成功'."\r\n".'订单号：'. $order_sn ."\r\n".'订单金额：'. $goods_amount."\r\n".'支付时间：'. $time ."\r\n".'支付方式：'. $pay_name. '';
	$data_up=array(
		'touser'   =>$wx['wxid'],                                    //提示每个客服有信息来了
		'msgtype'  =>'news',
		'news'    =>array('articles'=>array(array('title'=>'订单支付成功' , 'description'=> $msg_up , 'url'=> "http://".$_SERVER['HTTP_HOST']."/mobile/user.php" , 'picurl'=> '')))      //将话术与用户发来的内容拼接，发给客服人员。.$arr["Content"]
	);	
	$weObj->sendCustomMessage($data_up);     //发送客服消息
	
	//如果用户不是分销商，并且消费满$GLOBALS['_CFG']['baseline']，则提示可以成为分销商
	if(empty($wx['real_name'])){
		$sql_xiaofeie = "SELECT sum(goods_amount) FROM ". $GLOBALS['ecs']->table('order_info') ." WHERE user_id = '".$user_id."' AND pay_status = 2 AND order_status != 2";
		$xiaofeie=$GLOBALS['db']->getOne($sql_xiaofeie);
		if( $xiaofeie >= $GLOBALS['_CFG']['baseline'] ){
			//符合条件，推送消息
			$sql="SELECT  real_name , mobile_phone  FROM " . $GLOBALS['ecs']->table('users') . " WHERE user_id='".$wx['recmded_id']."'";
			$shangxian=$GLOBALS['db']->getRow($sql);
			$msg_up = '亲爱的'.$wx['nickname'].'，很高兴通知您，您符合我们的条件，可以申请成为我们的大东家了，快去填写自己的真实信息，开始转发赚钱吧~'."\r\n".'您的事业辅导：'.$shangxian['real_name']."\r\n".'联系方式：'.$shangxian['mobile_phone'].'';
			$data_up=array(
				'touser'   =>$wx['wxid'],                                    //提示每个客服有信息来了
				'msgtype'  =>'news',
				'news'    =>array('articles'=>array(array('title'=>'可以去开店啦' , 'description'=> $msg_up , 'url'=> "http://".$_SERVER['HTTP_HOST']."/mobile/distributor.php?act=apply_join" , 'picurl'=> '')))      //将话术与用户发来的内容拼接，发给客服人员。.$arr["Content"]
			); 
			$weObj->sendCustomMessage($data_up);     //发送客服消息
		}
	}
	
	//给两级推荐人推送消息
	$recmded_id = $wx['recmded_id'];
	for($i=1;$i<=2;$i++){
				if($recmded_id !=''){
					$sql="SELECT  u.wxid , w.nickname , u.recmded_id FROM " . $GLOBALS['ecs']->table('users') . " u LEFT JOIN wxch_user w on w.wxid=u.wxid WHERE user_id='$recmded_id'";
					$wx_up=$GLOBALS['db']->getRow($sql);
				}
				if($wx_up != ''){ 
					if($i == 1){ 
						$fencheng = 0;
						$jibie = "一级" ;
						$rs_goods = $GLOBALS['db']->getAll('SELECT o.goods_id , o.goods_number , o.goods_price , g.percent_sep FROM ' . $GLOBALS['ecs']->table('order_goods') . ' o LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' g on g.goods_id = o.goods_id WHERE order_id = '.$order_id );
						foreach ($rs_goods as $key => $val)
       					{
							$fencheng = $fencheng + $val['goods_price']*$val['goods_number']*$val['percent_sep']*2/300;
      					}
					}elseif($i == 2){ 
						$fencheng = 0;
						$jibie = "二级" ; 
						$rs_goods = $GLOBALS['db']->getAll('SELECT o.goods_id , o.goods_number , o.goods_price , g.percent_sep FROM ' . $GLOBALS['ecs']->table('order_goods') . ' o LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' g on g.goods_id = o.goods_id WHERE order_id = '.$order_id );
						foreach ($rs_goods as $key => $val)
       					{
							$fencheng = $fencheng + $val['goods_price']*$val['goods_number']*$val['percent_sep']*1/300;
      					}
					}
					
					$fencheng_dj = getRankCommission($recmded_id) * $goods_amount /100;//等级分成,调用函数，获取分成比例来计算得到
					$recmded_id = $wx_up['recmded_id'];
					
					$msg_up = '主人，您的店铺成功售出一笔订单，继续加油哦！'."\r\n".'订单号：'.$order_sn."\r\n".'收件人：'.$consignee."\r\n".'支付时间：'.$time."\r\n".'支付金额：'.$goods_amount."\r\n".$jibie.'提成：'.$fencheng."\r\n".'等级提成：'.$fencheng_dj;
					$data_up=array(
						'touser'   =>$wx_up['wxid'],                                    //提示每个客服有信息来了
						'msgtype'  =>'news',
						'news'    =>array('articles'=>array(array('title'=>'消费通知' , 'description'=> $msg_up , 'url'=> "http://".$_SERVER['HTTP_HOST']."/mobile/distributor.php?act=profile"  , 'picurl'=> '')))      //将话术与用户发来的内容拼接，发给客服人员。.$arr["Content"]
					); 
					$weObj->sendCustomMessage($data_up);     //发送客服消息
				}
	}
}


function send_weixin_info($order_sn, $amount , $user_id)
{    include_once(ROOT_PATH .'wechat/wechat.class.php');
    /* require(ROOT_PATH . 'include/init.php');*/
        		
	    	$sql = "SELECT  token,appid,appsecret,access_token  FROM wxch_config ORDER BY dateline ASC  LIMIT 1";
		
         // 连接微信配置数据库，读取appid等信息，赋值给数组，进行初始化。 
       
            $row = $GLOBALS['db']->getRow($sql);    //getall 为获取多维数组， getone 为获得一个变量
		 
        	$options = array(
			'token'    => $row['token'],               //填写你设定的key
			'appid'    => $row['appid'],
			'access_token'=> $row['access_token'],
			'appsecret'=> $row['appsecret'] );

		    $weObj = new Wechat($options);        //实例化
			
			$sql     = "SELECT cfg_value FROM wxch_cfg WHERE 1 order by cfg_id ASC LIMIT 2";
			$baseurl = $GLOBALS['db']->getAll($sql);
			$murl    = $baseurl[1]['cfg_value'].$baseurl['0']['cfg_value']; 
			$wxid    =  $GLOBALS['db']->getOne("SELECT wxid  FROM ". $GLOBALS['ecs']->table('users')." WHERE user_id = '$user_id'");
			
			$sql      = "SELECT  nickname FROM wxch_user WHERE wxid='$wxid'";
			$wx_name  = $GLOBALS['db']->getOne($sql);
		     
		 if(!empty($wxid))
	       { 
 
		  $msg = $wx_name.'您好，您的订单'.$order_sn.'已支付成功，请点击进入'.'<a href="'.$murl.'user.php?act=order_list">我的订单</a>'.'进行查看订单状态';
	         $data=array(
								 'touser'  => $wxid,                            //提示每个客服有信息来了
								 'msgtype' => 'text',
								 'text'    => array('content'=> $msg )          //将话术与用户发来的内容拼接，发给客服人员。.$arr["Content"]

			        	  );
	  
	         
	        $weObj->sendCustomMessage($data);     //发送客服消息
		    $sql="INSERT INTO" . $GLOBALS['ecs']->table('weixin_get') . "(FromUserName, Nickname, dateline, MsgType, Content) VALUES ('$wxid', '$nickname', now(), 'send_dingdan', '$msg')";
		    $GLOBALS['db']->query($sql);
	       }
			
		$affiliate['config']['level_money_all'] = 100;	
		$affiliate['config']['level_money_all'] = (float)$affiliate['config']['level_money_all'];  
        if ($affiliate['config']['level_money_all'])
        {
            $affiliate['config']['level_money_all'] /= 100;
        }
        $money = round($affiliate['config']['level_money_all'] * $amount, 2);	
		$row['user_id'] = $user_id;	
        //$num = count($affiliate['item']);
		
		$num = 2;
		$affiliate['item'][0]['level_money'] = 20;
		$affiliate['item'][1]['level_money'] = 10;
		
        for ($i=0; $i < $num; $i++)
          {
              $affiliate['item'][$i]['level_money'] = (float)$affiliate['item'][$i]['level_money'];
              if ($affiliate['item'][$i]['level_money'])
               {
                  $affiliate['item'][$i]['level_money'] /= 100;
               }
           $setmoney = round($money * $affiliate['item'][$i]['level_money'], 2);
      	        
				
                $row = $GLOBALS['db']->getRow("SELECT o.recmded_id as user_id, u.wxid, u.user_name, u.down_num, r.welfare FROM 
				        " . $GLOBALS['ecs']->table('users') . " o" ." LEFT JOIN" . $GLOBALS['ecs']->table('users') . " u ON o.recmded_id = u.user_id".
                        " LEFT JOIN ". $GLOBALS['ecs']->table('user_rank') ." r ON r.min_points <= u.down_num AND r.max_points >u.down_num ".
						" WHERE o.user_id = '$row[user_id]'" );
				 
				$sql = "SELECT u.user_id, u.down_num, r.welfare FROM " . $GLOBALS['ecs']->table('users') . " u " .
                       " LEFT JOIN ". $GLOBALS['ecs']->table('user_rank') ." r ON u.user_rank =r.rank_id AND r.special_rank=1  ".
		               " WHERE u.user_id = '$row[user_id]'";
				$spe = $GLOBALS['db']->getRow($sql);
				
				if(!empty($spe['welfare'])){
					
					$row['welfare'] = $spe['welfare'];
					}
				 
                $up_uid = $row['user_id'];
				$setdmoney = round($row['welfare']*$money/100, 2);
				
                if (empty($up_uid) || empty($row['user_name']))
                {
                    break;
                }
                else
                {   
				 $total = $setmoney + $setdmoney;
           
             
		  if(!empty($row['wxid']))
	       { 
		       $sql     =  "SELECT nickname FROM wxch_user WHERE wxid='$row[wxid]'";
		       $nickname =  $GLOBALS['db']->getOne($sql);
		       
	          $msg_up=$nickname."您好很高兴的通知到您，您团队的伙伴". $wx_name ."已成功支付订单,您将获得". $setmoney."元基础的返利，".$setdmoney."元的等级返利,总共为".$total."元，成功是属于像您这样愿意付出的人！继续加油，帮助你的团队创造更高业绩！让自己早攀顶峰";
		 
	          $data_up = array(
								 'touser'   => $row['wxid'],                     //提示每个客服有信息来了
								 'msgtype'  => 'text',
								 'text'     => array('content'=> $msg_up )          //将话术与用户发来的内容拼接，发给客服人员。.$arr["Content"]

			         	   );
				 
	         $weObj->sendCustomMessage($data_up);     //发送客服消息
			
			 $sql="INSERT INTO " . $GLOBALS['ecs']->table('weixin_get') . "(FromUserName, Nickname, dateline, MsgType, Content) VALUES ('$up_user[wxid]',                  '$up_user[nickname]', now(), 'send_dingdan_up', '$msg_up')";
			$GLOBALS['db']->query($sql);
	       }
			

	   
	  }
  }


}

function getRankCommission($user_id){
		//提成比例
		$rankPoint = 0;
		
		//先判断是否是充值而成的‘金董’级别
		$rankPoint_sql = "SELECT r.welfare , r.special_rank FROM ecs_user_rank r LEFT JOIN ecs_users u ON u.user_rank = r.rank_id WHERE u.user_id = '".$user_id."'";
		$rs = $GLOBALS['db']->getRow($rankPoint_sql);
		$isSpecial = $rs['special_rank'];
		
		if($isSpecial == 1){
			$rankPoint = $rs['welfare'];
			return $rankPoint;
		}
		
		//获取等级
		$rankPoint_sql = "SELECT r.welfare FROM ecs_user_rank r LEFT JOIN ecs_users u ON u.shop_num >= min_points and u.shop_num < max_points WHERE u.user_id = '".$user_id."'";
		$rs = $GLOBALS['db']->getRow($rankPoint_sql);
		$rankPoint = $rs['welfare'];
		return $rankPoint;
	}	
?>