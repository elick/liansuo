<?php
//mod by coolvee.com 酷唯软件出品
function can_refund($order_id) 
{
	$row = $GLOBALS['db']->getRow("select shipping_status,order_status from ".$GLOBALS['ecs']->table("order_info")." where order_id='$order_id'");
	return $row['shipping_status']==SS_RECEIVED;
}


function refund_add($refund)
{


    $upload_size_limit = $GLOBALS['_CFG']['upload_size_limit'] == '-1' ? ini_get('upload_max_filesize') : $GLOBALS['_CFG']['upload_size_limit'];

    $last_char = strtolower($upload_size_limit{strlen($upload_size_limit)-1});

    switch ($last_char)
    {
        case 'm':
            $upload_size_limit *= 1024*1024;
            break;
        case 'k':
            $upload_size_limit *= 1024;
            break;
    }
    $upload_size_limit = 5*$upload_size_limit;
    $refund['refund_pic1'] = refund_apply_order_goods_upload_ex($refund, 'refund_pic1', $upload_size_limit);
	$refund['refund_pic2'] = refund_apply_order_goods_upload_ex($refund, 'refund_pic2', $upload_size_limit); 
	$refund['refund_pic3'] = refund_apply_order_goods_upload_ex($refund, 'refund_pic3', $upload_size_limit); 
	
	$sql = "SELECT order_sn,order_id, invoice_no,goods_amount, add_time,shipping_id,shipping_name,user_id, 
	        consignee,address, country,province,city,district,sign_building,email,zipcode,tel,mobile,best_time,
		    postscript,how_oos,insure_fee,shipping_fee,agency_id,is_separate as seprt_status
	        FROM ".$GLOBALS['ecs']->table('order_info')." WHERE order_id = '$refund[order_id]'";
	$order = $GLOBALS['db']->getRow($sql);
	$order['return_time'] = gmtime();
	$order['rtnType'] =$refund['rtnType'];
	
    $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('back_order'), $order, 'INSERT');
	$backId = $GLOBALS['db']->insert_id();
	
	$sql = "SELECT goods_id ,product_id, goods_number, goods_name, goods_sn, is_real,goods_attr,
	       refund_num,goods_price, send_number FROM ".$GLOBALS['ecs']->table('order_goods')."
		   WHERE order_id = '$refund[order_id]' AND refund_num >0";
	$backGoods = $GLOBALS['db']->getAll($sql);
	
	foreach($backGoods as $key=>$goodVal){
	 $goodVal['back_id'] =  $backId;
	 $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('back_goods'), $goodVal, 'INSERT');
	}
	
	 $sql ="SELECT SUM(goods_number) as buy_number, SUM(refund_num) as rtn_number FROM ".$GLOBALS['ecs']->table('order_goods')."
	        WHERE order_id = '$refund[order_id]' group by order_id";
	 $Num = $GLOBALS['db']->getRow($sql);
	
	 if($refund['rtnType']==1){
		$order_status = $Num['buy_number']==$Num['rtn_number']? 7:8;
		
		}
	 else{
		$order_status = $Num['buy_number']==$Num['rtn_number']? 9:10;
		}
	$sql = "UPDATE ".$GLOBALS['ecs']->table('order_info')." SET order_status = '$order_status' WHERE order_id = '$refund[order_id]'";
	$GLOBALS['db']->query($sql);
	$refund['back_id']  = $backId;
	$refund['add_time'] = gmtime();
	$refund['user_id']  = $_SESSION['user_id'];
	$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('back_reason'), $refund, 'INSERT');

    return $refund['back_id'];
}

function refund_apply_order_goods_upload_ex($refund, $pic_name, $upload_size_limit) 
{
	if ($refund[$pic_name])
    {
        if($_FILES[$pic_name]['size'] / 1024 > $upload_size_limit)
        {
            $GLOBALS['err']->add(sprintf($GLOBALS['_LANG']['upload_file_limit'], $upload_size_limit));
            return -1;
        }
        $refund_pic1 = upload_file($_FILES[$pic_name], 'feedbackimg');

        if ($refund_pic1 === false)
        {
			$GLOBALS['err']->add("无法上传");
            return -1;
        }
		$refund_pic1 = "mobile/data/feedbackimg/".$refund_pic1;
    }
    else
    {
        $refund_pic1 = '';
    }
	return $refund_pic1;
}
//mod by coolvee.com 酷唯软件出品
function get_order_goods_list($order_id, $where_ex="") 
{
	$sql = "select g.goods_name,g.goods_thumb,g.goods_id,og.goods_price,g.shop_price,og.refund_status,og.goods_number,og.refund_add_time,og.refund_confirm_time,og.goods_price*og.goods_number as refund_money,og.refund_reason,og.refund_desc,og.rec_id,og.refund_pic1,og.refund_pic2,og.refund_pic3 from ".$GLOBALS['ecs']->table("order_goods")." as og left join ".$GLOBALS['ecs']->table("goods")." as g on g.goods_id=og.goods_id where og.order_id='$order_id' ".$where_ex." group by g.goods_id";
	$arr = $GLOBALS['db']->getAll($sql);
	foreach($arr as $k=>$v) 
	{
		$arr[$k]['url'] = build_uri('goods', array('gid'=>$v['goods_id']) );
		$arr[$k]['shop_price_fmt'] = price_format($v['shop_price']);
		$arr[$k]['goods_price_fmt'] = price_format($v['goods_price']);
		$arr[$k]['refund_add_time_fmt'] = local_date('Y-m-d H:i:s', $v['refund_add_time']);
		$arr[$k]['refund_confirm_time_fmt'] = local_date('Y-m-d H:i:s', $v['refund_confirm_time']);
		$arr[$k]['refund_pic1'] = empty($v['refund_pic1']) ? "" : "../data/feedbackimg/".$v['refund_pic1'];
		$arr[$k]['refund_pic2'] = empty($v['refund_pic2']) ? "" : "../data/feedbackimg/".$v['refund_pic2'];
		$arr[$k]['refund_pic3'] = empty($v['refund_pic3']) ? "" : "../data/feedbackimg/".$v['refund_pic3'];

	}
	return $arr;

}

//mod by coolvee.com 酷唯软件出品
function refund_confirm_order_goods($rec_id, $refund_status) 
{
	$row = $GLOBALS['db']->getRow("select og.rec_id,o.user_id,og.goods_price*og.goods_number as refund_money,o.order_sn,og.goods_number,og.goods_name from ".$GLOBALS['ecs']->table("order_goods")." as og,".$GLOBALS['ecs']->table("order_info")." as o  where o.order_id=og.order_id and refund_status='1' and rec_id='$rec_id'");
	empty($row) ? die("inalid") : extract($row);
	if($rec_id>0)
	{
		if($refund_status == 2)
		{
			$change_desc = "订单{$order_sn}中的{$goods_name}退款成功,返还余额";
			log_account_change($user_id, $refund_money, 0, 0, 0, $change_desc, ACT_OTHER);
		}
		$GLOBALS['db']->query("update ".$GLOBALS['ecs']->table("order_goods")." set refund_status='$refund_status',refund_confirm_time='".gmtime()."' where rec_id='$rec_id'");
	}
}

//mod by coolvee.com 酷唯软件出品
function get_order_goods_info($rec_id) 
{
	$sql = "select og.goods_name,og.goods_price,og.goods_price*og.goods_number as subtotal,og.goods_number,og.goods_id,g.goods_thumb,og.refund_status,og.rec_id,o.order_id from ".$GLOBALS['ecs']->table("order_goods")." as og,".$GLOBALS['ecs']->table("order_info")." as o,".$GLOBALS['ecs']->table("goods")." as g where og.goods_id=g.goods_id and og.order_id=o.order_id and og.rec_id='$rec_id'";
	$row = $GLOBALS['db']->getRow($sql);
	$row['url'] = build_uri('goods', array('gid'=>$row['goods_id']) );
	return $row;
}
// 用户退换货记录
function get_rtn_orders($user_id, $num = 5, $start = 0)
{ 
   $arr = array();
   
   $sql = "SELECT b.order_sn, b.back_id, b.order_id, b.return_time,b.status,b.rtnType,b.goods_amount, SUM(bg.goods_price*bg.refund_num) 
           as rtn_amount , SUM(bg.refund_num) as rtn_num ,SUM(bg.send_number) as org_num  FROM " .$GLOBALS['ecs']->table('back_order'). 
		   " b LEFT JOIN " .$GLOBALS['ecs']->table('back_goods')." bg on b.back_id=bg.back_id  WHERE  b.user_id = '$user_id' 
		    GROUP by bg.back_id ORDER BY b.return_time DESC";
   $res = $GLOBALS['db']->SelectLimit($sql, $num, $start);
   
    while ($row = $GLOBALS['db']->fetchRow($res)){
	  $arr[] = array(  'back_id'        => $row['back_id'],
	                   'order_id'       => $row['order_id'],
                       'order_sn'       => $row['order_sn'],
					   'goods_amount'   => $row['goods_amount'],
					   'rtnType'        => $row['rtnType'],
			           'add_time'       => local_date($GLOBALS['_CFG']['time_format'], $row['return_time']),
                       'status'         => $row['status'],
			           'rtn_amount'     => $row['rtn_amount'],
					   'org_num'        => $row['org_num'],
					   'rtn_num'        => $row['rtn_num']);
    }

    return $arr;
	
	
	 

}
// 用户退换货记录详情
function get_bko_detail($id)
{ 
   $arr = array();
   
   $sql = "SELECT bo.back_id, bo.status, bo.rtnType,bo.return_time,bo.order_id,bo.order_sn,bo.update_time,bo.shipping_time,bo.invoice_no ,
           bo.goods_status, bo.goods_amount, SUM(bg.goods_price*bg.refund_num)  as rtn_amount , SUM(bg.refund_num) as rtn_num ,
		   SUM(bg.send_number) as org_num FROM ".$GLOBALS['ecs']->table('back_order')." bo  LEFT JOIN ".$GLOBALS['ecs']->table('back_goods')." bg 
		   on bo.back_id = bg.back_id WHERE bo.back_id = '$id' GROUP by bg.back_id";
   $bk_info = $GLOBALS['db']->getRow($sql);
   $bk_info['rtnTypeM'] =  $bk_info['rtnType']==1?"退货":"换货";
   $bk_info['return_time'] = local_date($GLOBALS['_CFG']['time_format'], $bk_info['return_time']);
   $bk_info['update_time'] = local_date($GLOBALS['_CFG']['time_format'], $bk_info['update_time']);
   $sql = "SELECT bg.*, bg.goods_price*bg.refund_num as price, g.goods_thumb as thumb FROM ".$GLOBALS['ecs']->table('back_goods')." 
           bg LEFT JOIN ".$GLOBALS['ecs']->table('goods')." g on bg.goods_id = g.goods_id WHERE back_id = '$id'";
   $bk_goods = $GLOBALS['db']->getAll($sql);
   
   $sql = "SELECT * FROM ".$GLOBALS['ecs']->table('back_reason')." WHERE back_id ='$id'";
   $bk_reason = $GLOBALS['db']->getRow($sql);
   $arr['bk_info']  = $bk_info;
   $arr['bk_goods'] = $bk_goods;
   $arr['bk_reason']= $bk_reason;
    return $arr;
	
	
	 

}

//mod by coolcps.com 酷盟软件出品
function get_user_orders_ex($user_id, $num = 10, $start = 0)
{
    /* 取得订单列表 */
    $arr    = array();

    $sql = "SELECT order_id, order_sn, order_status, shipping_status, pay_status, add_time, " .
           "(goods_amount + shipping_fee + insure_fee + pay_fee + pack_fee + card_fee + tax - discount) AS total_fee ".
           " FROM " .$GLOBALS['ecs']->table('order_info') .
           " WHERE user_id = '$user_id' ORDER BY add_time DESC";
    $res = $GLOBALS['db']->SelectLimit($sql, $num, $start);

    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        if ($row['order_status'] == OS_UNCONFIRMED)
        {
            $row['handler'] = "<a href=\"user.php?act=cancel_order&order_id=" .$row['order_id']. "\" onclick=\"if (!confirm('".$GLOBALS['_LANG']['confirm_cancel']."')) return false;\">".$GLOBALS['_LANG']['cancel']."</a>";
        }
        else if ($row['order_status'] == OS_SPLITED)
        {
            /* 对配送状态的处理 */
            if ($row['shipping_status'] == SS_SHIPPED)
            {
                @$row['handler'] = "<a href=\"user.php?act=affirm_received&order_id=" .$row['order_id']. "\" onclick=\"if (!confirm('".$GLOBALS['_LANG']['confirm_received']."')) return false;\">".$GLOBALS['_LANG']['received']."</a>";
            }
            elseif ($row['shipping_status'] == SS_RECEIVED)
            {
                @$row['handler'] = '<span style="color:red">'.$GLOBALS['_LANG']['ss_received'] .'</span>';
            }
            else
            {
                if ($row['pay_status'] == PS_UNPAYED)
                {
                    @$row['handler'] = "<a href=\"user.php?act=order_detail&order_id=" .$row['order_id']. '">' .$GLOBALS['_LANG']['pay_money']. '</a>';
                }
                else
                {
                    @$row['handler'] = "<a href=\"user.php?act=order_detail&order_id=" .$row['order_id']. '">' .$GLOBALS['_LANG']['view_order']. '</a>';
                }

            }
        }
        else
        {
            $row['handler'] = '<span style="color:red">'.$GLOBALS['_LANG']['os'][$row['order_status']] .'</span>';
        }

        $row['shipping_status'] = ($row['shipping_status'] == SS_SHIPPED_ING) ? SS_PREPARING : $row['shipping_status'];
        $row['order_status'] = $GLOBALS['_LANG']['os'][$row['order_status']] . ',' . $GLOBALS['_LANG']['ps'][$row['pay_status']] . ',' . $GLOBALS['_LANG']['ss'][$row['shipping_status']];
		
		//mod by coolvee.com 酷唯软件出品
		$row['goods_list'] = get_order_goods_list($row['order_id']);
		$row['can_refund'] = can_refund($row['order_id']);

        $arr[] = array('order_id'       => $row['order_id'],
                       'order_sn'       => $row['order_sn'],
			           'goods_list' => $row['goods_list'],
			           'goods_num' => count($row['goods_list']),
                       'order_time'     => local_date($GLOBALS['_CFG']['time_format'], $row['add_time']),
                       'order_status'   => $row['order_status'],
                       'total_fee'      => price_format($row['total_fee'], false),
			           'can_refund' => $row['can_refund'],
                       'handler'        => $row['handler']);
    }

    return $arr;
}
?>