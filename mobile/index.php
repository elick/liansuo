<?php



/**

 * ECSHOP 首页文件

 * ============================================================================

 * * 版权所有 2005-2012 北京招聚网络科技有限公司，并保留所有权利。

 * 网站地址: http://www.ECSHOP.com；

 * ----------------------------------------------------------------------------

 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和

 * 使用；不允许对程序代码以任何形式任何目的的再发布。

 * ============================================================================

 * $Author: liubo $

 * $Id: index.php 17217 2011-01-19 06:29:08Z liubo $

*/

//

define('IN_ECTOUCH', true);

require(dirname(__FILE__) . '/include/init.php');
require(ROOT_PATH . 'include/lib_wxch.php');
require(ROOT_PATH . 'include/get_brand.php');
	
if ((DEBUG_MODE & 2) != 2)

{

    $smarty->caching = true;

}



//csb   微信端打开页面后台自动登陆或者自动注册代码
$shop_name2 = "";//默认店铺名称
$shop_head = "http://".$_SERVER['HTTP_HOST']."/mobile/images/portrait.jpg";//默认首页图片
$recmded_id = 0; 
if($_SESSION['user_id'] >0){
 
	//已经登陆的用户不需要再执行登陆或者注册操作,但是需要判断下是否成为了分销商,是否可以分享自己的店铺!
	$is_member_sql = "SELECT real_name , recmded_id FROM ".$ecs->table("users")." WHERE user_id = '".$_SESSION['user_id']."'";
	$rs = $db->getRow($is_member_sql);
	$recmded_id = $rs['recmded_id'];
	$real_name = $rs['real_name'];
	$changeable = 0;
	
	//如果真实姓名为空,店铺所有者就是自己的推荐人,分享时带推荐人的ID，店铺的名字也显示是推荐人的名字，否则显示自己的信息。特别的：$_SESSION['user_id']为空时，即用户没有登陆，显示商城自己的名字。
	
	
	if(!empty($real_name)){
		//有分销权限,显示的都是自己的信息
		$shop_owner = $_SESSION['user_id'] ;
		//file_put_contents("a.txt",$shop_owner);
		$changeable = 1;
	}
	else{
		$shop_owner =  $recmded_id;
	}
		
	//获取店铺所有者的信息
	$owner_info = get_owner_info($shop_owner);
	if(!empty($owner_info['shop_name2'])){
		$shop_name2 = $owner_info['shop_name2'];	
	}
	if(!empty($owner_info['headimgurl'])){
		$shop_head = $owner_info['headimgurl'];
	}
	
	$smarty->assign('changeable' , $changeable);
	$smarty->assign('shop_owner',$shop_owner);
	
	
 }else{ 

	//获取微信openid,如果存在了,跳到登陆页面,直接自登陆,否则直接跳到注册页,直接注册登陆
	require(dirname(__FILE__) . '/include/get_openid.php');
	$jsApi = new JsApi_pub();
	if (!isset($_GET['code'])){
		//触发微信返回code码
		$u = isset($_GET['u'])?intval($_GET['u']):0;
		$redirect = urlencode($GLOBALS['ecs']->url().'index.php?u='.$u);
		$url = $jsApi->createOauthUrlForCode($redirect);
		
		ecs_header("Location: $url"); 
		exit();
		
	}else{
	//echo "可以获取openid";
		//获取code码，以获取openid
	    $code = $_GET['code'];
		
		$jsApi->setCode($code);
		$openid = $jsApi->getOpenId();
	    
		$sql = "SELECT user_id , user_name , real_name ,recmded_id FROM ".$ecs->table("users")." WHERE wxid = '".$openid."'";
		
		$rs = $db->getRow($sql);
		$user_id    = $rs['user_id'];
		$username   = $rs['user_name'];
		$real_name  = $rs['real_name'];
		$recmded_id = $rs['recmded_id'];
		if($user_id>0){//用户已经有帐号,自动登录
			//$password = "password_".$user_id;
			
			if(strpos($username, "tourist") === false){
				 header("location: user.php?act=login&index_back=index.php");
				}
			else{
			 $password = "password_".substr($username,8);
			 $user->login($username, $password,1);
			 header("Location:index.php");
			}
			
		}else{//用户是第一次打开网页,没有保存过微信的openid,自动注册
		    $u = isset($_GET['u'])?intval($_GET['u']):0;
			header("location: user.php?act=test&back_url=index.php&openid=".$openid."&u=".$u);
			
		}
	}
} 
$smarty->assign("shop_name2" , $shop_name2);
$smarty->assign("shop_head" , $shop_head);

//XXX 判断是否有一元优惠权限
$sql = 'SELECT msn  FROM' . $ecs->table('users') . " WHERE `user_id`='" .$_SESSION['user_id']. "'";
$msn = $db->getOne($sql);
//$msn =0;
$smarty->assign("yiyuan" , $msn);



//csb end of 微信端打开页面后台自动登陆或者自动注册代码

	//判断店铺等级 begin
	
	$rank_point = getRankCommission($shop_owner);
	$xingzhuan = '';
	$shuliang = '';
	if($rank_point == '0%'){ $shuliang = '&#xe600;' ; $xingzhuan = 'xinxing';}
	elseif($rank_point == '2%'){ $shuliang = '&#xe600;' ; $xingzhuan = 'zuanshi';}
	elseif($rank_point == '3%'){ $shuliang = '&#xe600;&#xe600;' ; $xingzhuan = 'zuanshi';}
	elseif($rank_point == '4%'){ $shuliang = '&#xe600;&#xe600;&#xe600;' ; $xingzhuan = 'zuanshi';}
	elseif($rank_point == '5%'){ $shuliang = '&#xe600;' ; $xingzhuan = 'huangguan';}
	elseif($rank_point == '6%'){ $shuliang = '&#xe600;&#xe600;' ; $xingzhuan = 'huangguan';}
	elseif($rank_point == '7%'){ $shuliang = '&#xe600;&#xe600;&#xe600;' ; $xingzhuan = 'huangguan';}
	$smarty->assign('xingzhuan' , $xingzhuan);
	$smarty->assign('shuliang' , $shuliang);
	
	if(!$recmded_id&&$_SESSION['user_id']>0){
		ecs_header("Location:addrecmded.php\n");
	}

	//判断是否有ajax请求

	$act = !empty($_GET['act']) ? $_GET['act'] : '';

if ($act == 'cat_rec')

{

    $rec_array = array(1 => 'best', 2 => 'new', 3 => 'hot');

    $rec_type = !empty($_REQUEST['rec_type']) ? intval($_REQUEST['rec_type']) : '1';

    $cat_id = !empty($_REQUEST['cid']) ? intval($_REQUEST['cid']) : '0';

    include_once('include/cls_json.php');

    $json = new JSON;

    $result   = array('error' => 0, 'content' => '', 'type' => $rec_type, 'cat_id' => $cat_id);



    $children = get_children($cat_id);

    $smarty->assign($rec_array[$rec_type] . '_goods',      get_category_recommend_goods($rec_array[$rec_type], $children));    // 推荐商品

    $smarty->assign('cat_rec_sign', 1);

    $result['content'] = $smarty->fetch('library/recommend_' . $rec_array[$rec_type] . '.lbi');

    die($json->encode($result));

}



/*------------------------------------------------------ */

//-- 判断是否存在缓存，如果存在则调用缓存，反之读取相应内容

/*------------------------------------------------------ */

/* 缓存编号 */

$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang']));



if (!$smarty->is_cached('index.dwt', $cache_id))

{

    assign_template();



    $position = assign_ur_here();

    $smarty->assign('page_title',      $position['title']);    // 页面标题

    $smarty->assign('ur_here',         $position['ur_here']);  // 当前位置



    /* meta information */

    $smarty->assign('keywords',        htmlspecialchars($_CFG['shop_keywords']));

    $smarty->assign('description',     htmlspecialchars($_CFG['shop_desc']));

    $smarty->assign('flash_theme',     $_CFG['flash_theme']);  // Flash轮播图片模板



    $smarty->assign('feed_url',        ($_CFG['rewrite'] == 1) ? 'feed.xml' : 'feed.php'); // RSS URL



    $smarty->assign('categories',      get_categories_tree()); // 分类树

    $smarty->assign('helps',           get_shop_help());       // 网店帮助

    $smarty->assign('top_goods',       get_top10());           // 销售排行
	
    $smarty->assign('user_id',         $_SESSION['user_id']);      //给guanggao产品加上会员ID

   
    $smarty->assign('best_goods',      get_recommend_goods('best'));    // 推荐商品

    $smarty->assign('new_goods',       get_recommend_goods('new'));     // 最新商品

    $smarty->assign('hot_goods',       get_recommend_goods('hot'));     // 热点文章

    //$smarty->assign('promotion_goods', get_promote_goods()); // 特价商品

    $smarty->assign('brand_list',      get_brands());

    $smarty->assign('promotion_info',  get_promotion_info()); // 增加一个动态显示所有促销信息的标签栏



    $smarty->assign('invoice_list',    index_get_invoice_query());  // 发货查询

    $smarty->assign('new_articles',    index_get_new_articles());   // 最新文章

    $smarty->assign('group_buy_goods', index_get_group_buy());      // 团购商品

    $smarty->assign('auction_list',    index_get_auction());        // 拍卖活动

    $smarty->assign('shop_notice',     $_CFG['shop_notice']);       // 商店公告
	

    /* 首页主广告设置 */

    $smarty->assign('index_ad',     $_CFG['index_ad']);

    if ($_CFG['index_ad'] == 'cus')

    {

        $sql = 'SELECT ad_type, content, url FROM ' . $ecs->table("ad_custom") . ' WHERE ad_status = 1';

        $ad = $db->getRow($sql, true);

        $smarty->assign('ad', $ad);

    }
	
	$sql = " SELECT * FROM " . $ecs->table("ad") ." WHERE position_id = 1 ORDER BY ad_id ASC";
    $top_ad = $db->getAll($sql);
    foreach($top_ad as $key => $item_ad){
		// $top_ad [$key]['ad_code']="data/afficheimg/".$item_ad['ad_code'];
	}
		  
	// 热卖广告
	 $sql = " SELECT * FROM " . $ecs->table("ad") ." WHERE position_id=2 ORDER BY ad_id ASC";
	 $ad_goods = $db->getAll($sql);
	 //$sql = " SELECT * FROM " . $ecs->table("touch_ad") ." WHERE position_id=8 ORDER BY ad_id ASC";
	 //$ad_8 = $db->getRow($sql);
	 
	// $sql = " SELECT * FROM " . $ecs->table("touch_ad") ." WHERE position_id = 4 ORDER BY ad_id ASC";
    // $cul_ad = $db->getAll($sql);
	// foreach($cul_ad as $key => $item_ad){
		// $cul_ad [$key]['ad_code']="data/afficheimg/".$item_ad['ad_code'];
	// }
	
	// 热卖广告
	// $sql = " SELECT * FROM " . $ecs->table("ad") ." WHERE position_id=30 ORDER BY ad_id ASC";
	// $ad30 = $db->getAll($sql);
	// $smarty->assign('ad30', $ad30);
    
	// $smarty->assign('cul_ad',        $cul_ad);
	// $smarty->assign('ad_8',          $ad_8);

    $links = index_get_links();
	
	$promote_goods = get_promote_goods();

	$smarty->assign('promote_goods' , $promote_goods);
	$smarty->assign('ad_goods' , $ad_goods);
    $smarty->assign('top_ad',          $top_ad);
	$smarty->assign('ad_center',       $ad_center);
	$smarty->assign('big_ad',          $big_ad);
    $smarty->assign('img_links',       $links['img']);

    $smarty->assign('txt_links',       $links['txt']);

    $smarty->assign('data_dir',        DATA_DIR);       // 数据目录



    /* 首页推荐分类 */

    $cat_recommend_res = $db->getAll("SELECT c.cat_id, c.cat_name, cr.recommend_type FROM " . $ecs->table("cat_recommend") . " AS cr INNER JOIN " . $ecs->table("category") . " AS c ON cr.cat_id=c.cat_id");

    if (!empty($cat_recommend_res))

    {

        $cat_rec_array = array();

        foreach($cat_recommend_res as $cat_recommend_data)

        {

            $cat_rec[$cat_recommend_data['recommend_type']][] = array('cat_id' => $cat_recommend_data['cat_id'], 'cat_name' => $cat_recommend_data['cat_name']);

        }

        $smarty->assign('cat_rec', $cat_rec);

    }

    /* 页面中的动态内容 */

    assign_dynamic('index');

}
	

/* $sql_fx = "SELECT * FROM ". $ecs->table('shop_config') . "WHERE code = 'indexfx_title'" ;
$row = $db->getRow($sql_fx);
$indexfx_tilte = $row['value'];

$sql_fx = "SELECT * FROM ". $ecs->table('shop_config') . "WHERE code = 'indexfx_desc'" ;
$row = $db->getRow($sql_fx);
$indexfx_desc = $row['value']; */

$indexfx_tilte = $GLOBALS['_CFG']['indexfx_title'];
$indexfx_desc = $GLOBALS['_CFG']['indexfx_desc'];

$smarty->assign('indexfx_tilte', $indexfx_tilte);
$smarty->assign('indexfx_desc', $indexfx_desc);
	
	//csb
	$guanzhu_sql = "SELECT w.subscribe FROM wxch_user w LEFT JOIN ecs_users u ON w.wxid = u.wxid WHERE user_id = '".$_SESSION['user_id']."'";
	$subscribe = $db->getOne($guanzhu_sql);
	$guanzhu = ($subscribe > 0) ? 1 : 0 ;
	
	$smarty->assign('guanzhu',$guanzhu);
	$smarty->assign('up_uid',$up_uid);
	$smarty->assign('nickname',$nickname);
	$smarty->assign('headimgurl',$headimgurl);
	
	$smarty->display('index.dwt', $cache_id);
	
	exit();  //why?


/*------------------------------------------------------ */

//-- PRIVATE FUNCTIONS

/*------------------------------------------------------ */



/**

 * 调用发货单查询

 *

 * @access  private

 * @return  array

 */


function index_get_invoice_query()

{

    $sql = 'SELECT o.order_sn, o.invoice_no, s.shipping_code FROM ' . $GLOBALS['ecs']->table('order_info') . ' AS o' .

            ' LEFT JOIN ' . $GLOBALS['ecs']->table('touch_shipping') . ' AS s ON s.shipping_id = o.shipping_id' .

            " WHERE invoice_no > '' AND shipping_status = " . SS_SHIPPED .

            ' ORDER BY shipping_time DESC LIMIT 10';

    $all = $GLOBALS['db']->getAll($sql);



    foreach ($all AS $key => $row)

    {

        $plugin = ROOT_PATH . 'include/modules/shipping/' . $row['shipping_code'] . '.php';



        if (file_exists($plugin))

        {

            include_once($plugin);



            $shipping = new $row['shipping_code'];

            $all[$key]['invoice_no'] = $shipping->query((string)$row['invoice_no']);

        }

    }



    clearstatcache();



    return $all;

}



/**

 * 获得最新的文章列表。

 *

 * @access  private

 * @return  array

 */

function index_get_new_articles()

{

    $sql = 'SELECT a.article_id, a.title, ac.cat_name, a.add_time, a.file_url, a.open_type, ac.cat_id, ac.cat_name ' .

            ' FROM ' . $GLOBALS['ecs']->table('article') . ' AS a, ' .

                $GLOBALS['ecs']->table('article_cat') . ' AS ac' .

            ' WHERE a.is_open = 1 AND a.cat_id = ac.cat_id AND ac.cat_type = 1' .

            ' ORDER BY a.article_type DESC, a.add_time DESC LIMIT ' . $GLOBALS['_CFG']['article_number'];

    $res = $GLOBALS['db']->getAll($sql);



    $arr = array();

    foreach ($res AS $idx => $row)

    {

        $arr[$idx]['id']          = $row['article_id'];

        $arr[$idx]['title']       = $row['title'];

        $arr[$idx]['short_title'] = $GLOBALS['_CFG']['article_title_length'] > 0 ?

                                        sub_str($row['title'], $GLOBALS['_CFG']['article_title_length']) : $row['title'];

        $arr[$idx]['cat_name']    = $row['cat_name'];

        $arr[$idx]['add_time']    = local_date($GLOBALS['_CFG']['date_format'], $row['add_time']);

        $arr[$idx]['url']         = $row['open_type'] != 1 ?

                                        build_uri('article', array('aid' => $row['article_id']), $row['title']) : trim($row['file_url']);

        $arr[$idx]['cat_url']     = build_uri('article_cat', array('acid' => $row['cat_id']), $row['cat_name']);

    }



    return $arr;

}



/**

 * 获得最新的团购活动

 *

 * @access  private

 * @return  array

 */

function index_get_group_buy()

{

    $time = gmtime();

    $limit = get_library_number('group_buy', 'index');



    $group_buy_list = array();

    if ($limit > 0)

    {

        $sql = 'SELECT gb.act_id AS group_buy_id, gb.goods_id, gb.ext_info, gb.goods_name, g.goods_thumb, g.goods_img ' .

                'FROM ' . $GLOBALS['ecs']->table('goods_activity') . ' AS gb, ' .

                    $GLOBALS['ecs']->table('goods') . ' AS g ' .

                "WHERE gb.act_type = '" . GAT_GROUP_BUY . "' " .

                "AND g.goods_id = gb.goods_id " .

                "AND gb.start_time <= '" . $time . "' " .

                "AND gb.end_time >= '" . $time . "' " .

                "AND g.is_delete = 0 " .

                "ORDER BY gb.act_id DESC " .

                "LIMIT $limit" ;

        $res = $GLOBALS['db']->query($sql);



        while ($row = $GLOBALS['db']->fetchRow($res))

        {

            /* 如果缩略图为空，使用默认图片 */

            $row['goods_img'] = get_image_path($row['goods_id'], $row['goods_img']);

            $row['thumb'] = get_image_path($row['goods_id'], $row['goods_thumb'], true);



            /* 根据价格阶梯，计算最低价 */

            $ext_info = unserialize($row['ext_info']);

            $price_ladder = $ext_info['price_ladder'];

            if (!is_array($price_ladder) || empty($price_ladder))

            {

                $row['last_price'] = price_format(0);

            }

            else

            {

                foreach ($price_ladder AS $amount_price)

                {

                    $price_ladder[$amount_price['amount']] = $amount_price['price'];

                }

            }

            ksort($price_ladder);

            $row['last_price'] = price_format(end($price_ladder));

            $row['url'] = build_uri('group_buy', array('gbid' => $row['group_buy_id']));

            $row['short_name']   = $GLOBALS['_CFG']['goods_name_length'] > 0 ?

                                           sub_str($row['goods_name'], $GLOBALS['_CFG']['goods_name_length']) : $row['goods_name'];

            $row['short_style_name']   = add_style($row['short_name'],'');

            $group_buy_list[] = $row;

        }

    }



    return $group_buy_list;

}



/**

 * 取得拍卖活动列表

 * @return  array

 */

function index_get_auction()

{

    $now = gmtime();

    $limit = get_library_number('auction', 'index');

    $sql = "SELECT a.act_id, a.goods_id, a.goods_name, a.ext_info, g.goods_thumb ".

            "FROM " . $GLOBALS['ecs']->table('goods_activity') . " AS a," .

                      $GLOBALS['ecs']->table('goods') . " AS g" .

            " WHERE a.goods_id = g.goods_id" .

            " AND a.act_type = '" . GAT_AUCTION . "'" .

            " AND a.is_finished = 0" .

            " AND a.start_time <= '$now'" .

            " AND a.end_time >= '$now'" .

            " AND g.is_delete = 0" .

            " ORDER BY a.start_time DESC" .

            " LIMIT $limit";

    $res = $GLOBALS['db']->query($sql);



    $list = array();

    while ($row = $GLOBALS['db']->fetchRow($res))

    {

        $ext_info = unserialize($row['ext_info']);

        $arr = array_merge($row, $ext_info);

        $arr['formated_start_price'] = price_format($arr['start_price']);

        $arr['formated_end_price'] = price_format($arr['end_price']);

        $arr['thumb'] = get_image_path($row['goods_id'], $row['goods_thumb'], true);

        $arr['url'] = build_uri('auction', array('auid' => $arr['act_id']));

        $arr['short_name']   = $GLOBALS['_CFG']['goods_name_length'] > 0 ?

                                           sub_str($arr['goods_name'], $GLOBALS['_CFG']['goods_name_length']) : $arr['goods_name'];

        $arr['short_style_name']   = add_style($arr['short_name'],'');

        $list[] = $arr;

    }



    return $list;

}



/**

 * 获得所有的友情链接

 *

 * @access  private

 * @return  array

 */

function index_get_links()

{

    $sql = 'SELECT link_logo, link_name, link_url FROM ' . $GLOBALS['ecs']->table('friend_link') . ' ORDER BY show_order';

    $res = $GLOBALS['db']->getAll($sql);



    $links['img'] = $links['txt'] = array();



    foreach ($res AS $row)

    {

        if (!empty($row['link_logo']))

        {

            $links['img'][] = array('name' => $row['link_name'],

                                    'url'  => $row['link_url'],

                                    'logo' => $row['link_logo']);

        }

        else

        {

            $links['txt'][] = array('name' => $row['link_name'],

                                    'url'  => $row['link_url']);

        }

    }



    return $links;

}
function get_owner_info($user_id){
	$owener_info_sql = "SELECT w.headimgurl , u.shop_name2 , u.real_name FROM ".$GLOBALS['ecs']->table("users")." u LEFT JOIN wxch_user w ON u.wxid = w.wxid WHERE u.user_id ='".$user_id."'";
	$rs = $GLOBALS['db']->getRow($owener_info_sql);
	$owner_info = array();
	$owner_info['shop_name2'] = empty( $rs['shop_name2'] ) ? $rs['real_name'] : $rs['shop_name2'];
	$owner_info['headimgurl'] = $rs['headimgurl'];
	return $owner_info ;
}



function getRankCommission($user_id){
		//提成比例
		$rankPoint = 0;
		
		//先判断是否是充值而成的‘金董’级别
		$rankPoint_sql = "SELECT r.welfare , r.special_rank FROM ecs_user_rank r LEFT JOIN ecs_users u ON u.user_rank = r.rank_id WHERE u.user_id = '".$user_id."'";
		$rs = $GLOBALS['db']->getRow($rankPoint_sql);
		$isSpecial = $rs['special_rank'];
		
		if($isSpecial == 1){
			$rankPoint = $rs['welfare'];
			return $rankPoint;
		}
		
		//获取等级
		$rankPoint_sql = "SELECT r.welfare FROM ecs_user_rank r LEFT JOIN ecs_users u ON u.down_num >= min_points and u.down_num < max_points WHERE u.user_id = '".$user_id."'";
		$rs = $GLOBALS['db']->getRow($rankPoint_sql);
		$rankPoint = $rs['welfare'];
		return $rankPoint;
}

?>