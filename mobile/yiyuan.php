<?php

define('IN_ECTOUCH', true);

require(dirname(__FILE__) . '/include/init.php');
	
if ((DEBUG_MODE & 2) != 2)

{
    $smarty->caching = true;
}


//判断是否有ajax请求

$act = !empty($_GET['act']) ? $_GET['act'] : '';

/*------------------------------------------------------ */

//-- 判断是否存在缓存，如果存在则调用缓存，反之读取相应内容

/*------------------------------------------------------ */

/* 缓存编号 */

$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang']));

if (!$smarty->is_cached('friday.dwt', $cache_id))

{
    /* 页面中的动态内容 */

    assign_dynamic('new');

}


    //XXX 判断是否有一元优惠权限
    $sql = 'SELECT msn  FROM' . $ecs->table('users') . " WHERE `user_id`='" .$_SESSION['user_id']. "'";
    $msn = $db->getOne($sql);
    //echo $msn;
    //$msn =0;
    if ($msn < 1) {
        echo "<script type='text/javascript'>alert('请推荐会员开店后,才可参与活动!');window.location.href='index.php';</script>";
        exit();
    }


	$sql = "SELECT * FROM " . $ecs->table('topic') . " WHERE topic_id = '4'";
    
    $topic = $db->getRow($sql);
    $topic['data'] = addcslashes($topic['data'], "'");
    $tmp = @unserialize($topic["data"]);
    $arr = (array)$tmp;

    $goods_id = array();

    foreach ($arr AS $key=>$value)
    {
        foreach($value AS $k => $val)
        {
            $opt = explode('|', $val);
            $arr[$key][$k] = $opt[1];
            $goods_id[] = $opt[1];
        }
    }

    $sql = 'SELECT g.goods_id, g.goods_name, g.goods_name_style, g.market_price, g.is_new, g.is_best, g.is_hot, g.shop_price AS org_price, ' .
                "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, g.promote_price, " .
                'g.promote_start_date, g.promote_end_date, g.goods_brief, g.goods_thumb , g.goods_img ' .
                'FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g ' .
                'LEFT JOIN ' . $GLOBALS['ecs']->table('member_price') . ' AS mp ' .
                "ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' " .
                "WHERE " . db_create_in($goods_id, 'g.goods_id');

    $res = $GLOBALS['db']->query($sql);

    $sort_goods_arr = array();

    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        if ($row['promote_price'] > 0)
        {
            $promote_price = bargain_price($row['promote_price'], $row['promote_start_date'], $row['promote_end_date']);
            //$row['promote_price'] = $promote_price > 0 ? price_format($promote_price) : '';
			$row['promote_price'] = $promote_price > 0 ? $promote_price : '';
        }
        else
        {
            $row['promote_price'] = '';
        }

        if ($row['shop_price'] > 0)
        {
            //$row['shop_price'] =  price_format($row['shop_price']);
        }
        else
        {
            $row['shop_price'] = '';
        }

		$row['salesnum']		 = get_goods_salesnum($row['goods_id']);
        $row['url']              = build_uri('goods', array('gid'=>$row['goods_id']), $row['goods_name']);
        $row['goods_style_name'] = add_style($row['goods_name'], $row['goods_name_style']);
        $row['short_name']       = $GLOBALS['_CFG']['goods_name_length'] > 0 ?
                                    sub_str($row['goods_name'], $GLOBALS['_CFG']['goods_name_length']) : $row['goods_name'];
        $row['goods_thumb']      = get_image_path($row['goods_id'], $row['goods_thumb'], true);
        $row['short_style_name'] = add_style($row['short_name'], $row['goods_name_style']);

        foreach ($arr AS $key => $value)
        {
            foreach ($value AS $val)
            {
                if ($val == $row['goods_id'])
                {
                    $key = $key == 'default' ? $_LANG['all_goods'] : $key;
                    $sort_goods_arr[$key][] = $row;
                }
            }
        }
    }
	
	$smarty->assign('new_goods',$sort_goods_arr['全部商品']);     // 最新商品
	$smarty->display('yiyuan.dwt', $cache_id);
	
	exit();  //why?

	
	// 获取商品的销量 by wang
function get_goods_salesnum($goods_id)
{
    /* 统计时间段 */
    //$period = intval($GLOBALS['_CFG']['top10_time']);
    $period = 4; //近一个月（30天）
    if ($period == 1) { // 一年
        $ext = " AND o.add_time > '" . local_strtotime('-1 years') . "'";
    } elseif ($period == 2) { // 半年
        $ext = " AND o.add_time > '" . local_strtotime('-6 months') . "'";
    } elseif ($period == 3) { // 三个月
        $ext = " AND o.add_time > '" . local_strtotime('-3 months') . "'";
    } elseif ($period == 4) { // 一个月
        $ext = " AND o.add_time > '" . local_strtotime('-1 months') . "'";
    } else {
        $ext = '';
    }

    /* 查询该商品销量 */
	$sql_2 = "SELECT sales_volume_base FROM ecs_goods WHERE goods_id = '".$goods_id."'";
	$v = $GLOBALS['db']->getOne($sql_2);
	
    $sql = 'SELECT IFNULL(SUM(g.goods_number), 0) ' .
        'FROM ' . $GLOBALS['ecs']->table('order_info') . ' AS o, ' .
            $GLOBALS['ecs']->table('order_goods') . ' AS g ' .
        "WHERE o.order_id = g.order_id " .
        "AND o.order_status " . db_create_in(array(OS_CONFIRMED, OS_SPLITED)) .
        "AND o.shipping_status " . db_create_in(array(SS_SHIPPED, SS_RECEIVED)) .
        " AND o.pay_status " . db_create_in(array(PS_PAYED, PS_PAYING)) .
        " AND g.goods_id = '$goods_id'" . $ext;
    $salesnum = $GLOBALS['db']->getOne($sql) + $v;

    return intval($salesnum);
}
?>