<?php

/**
 * ECSHOP 会员中心
 * ============================================================================
 * * 版权所有 2005-2012 北京招聚网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ECSHOP.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: user.php 17217 2011-01-19 06:29:08Z liubo $
*/

define('IN_ECTOUCH', true);

require(dirname(__FILE__) . '/include/init.php');
require(ROOT_PATH . 'include/lib_weixintong.php');
/* 载入语言文件 */
require_once(ROOT_PATH . 'lang/' .$_CFG['lang']. '/user.php');
$user_id = $_SESSION['user_id'];
$action  = isset($_REQUEST['act']) ? trim($_REQUEST['act']) : 'default';
$affiliate = unserialize($GLOBALS['_CFG']['affiliate']);
$smarty->assign('affiliate', $affiliate);
$back_act='';

// 不需要登录的操作或自己验证是否登录（如ajax处理）的act
$not_login_arr =
array('login','act_login','register','ajax_register','act_edit_password','get_password','send_pwd_email','reset_pwd','password', 'signin', 'add_tag', 
    'collect', 'return_to_cart', 'logout', 'email_list', 'validate_email', 'send_hash_mail', 'order_query', 'is_registered', 'check_email','clear_history','qpassword_name', 
    'get_passwd_question', 'check_answer', 'oath', 'oath_login','islogin' , 'test' , 'history_list' , 'bangding');

/* 显示页面的action列表 */
$ui_arr = array('register', 'login', 'profile', 'order_list', 'order_detail', 'order_tracking', 'address_list', 'act_edit_address', 'collection_list', 'async_order_list',
'message_list', 'tag_list', 'get_password', 'reset_password', 'booking_list', 'add_booking', 'account_raply', 'order_express' , 'ajax_set_new_name' ,
'account_deposit', 'account_log', 'account_detail', 'act_account', 'async_down_line' ,'down_line' , 'detail' , 'async_detail' , 'pay', 'default', 'bonus', 'group_buy', 'group_buy_detail', 'affiliate', 'comment_list','validate_email','track_packages', 'transform_points','qpassword_name', 'get_passwd_question', 'check_answer', 'qr_share','dengdengji','submit_zfb','submit_card','surplus_suc','setDefault','account_add','refund','act_refund','ajax_update_return','my_return_list', 'my_return_detail' , 'goods_share','user_golds' );
/* 未登录处理 */
if (empty($_SESSION['user_id']))
{
    if (!in_array($action, $not_login_arr))
    {
        if (in_array($action, $ui_arr))
        {
            /* 如果需要登录,并是显示页面的操作，记录当前操作，用于登录后跳转到相应操作
            if ($action == 'login')
            {
                if (isset($_REQUEST['back_act']))
                {
                    $back_act = trim($_REQUEST['back_act']);
                }
            }
            else
            {}*/
            if (!empty($_SERVER['QUERY_STRING']))
            {
                $back_act = 'user.php?' . strip_tags($_SERVER['QUERY_STRING']);
            }
            $action = 'login';
        }
        else
        {
            //未登录提交数据。非正常途径提交数据！
            die($_LANG['require_login']);
        }
    }
}

/* 如果是显示页面，对页面进行相应赋值 */
if (in_array($action, $ui_arr))
{
    assign_template();
    $position = assign_ur_here(0, $_LANG['user_center']);
    $smarty->assign('page_title', $position['title']); // 页面标题
    $smarty->assign('ur_here',    $position['ur_here']);
    $sql = "SELECT value FROM " . $ecs->table('touch_shop_config') . " WHERE id = 419";
    $row = $db->getRow($sql);
    $car_off = $row['value'];
    $smarty->assign('car_off',       $car_off);
    /* 是否显示积分兑换 */
    if (!empty($_CFG['points_rule']) && unserialize($_CFG['points_rule']))
    {
        $smarty->assign('show_transform_points',     1);
    }
    $smarty->assign('helps',      get_shop_help());        // 网店帮助
    $smarty->assign('data_dir',   DATA_DIR);   // 数据目录
    $smarty->assign('action',     $action);
    $smarty->assign('lang',       $_LANG);
}

//用户中心欢迎页
if ($action == 'default')
{
	//XXX 登录这里加上检测 一元返利的订单是否到达30天返款日
 	$sql = 'SELECT *  FROM' . $ecs->table('yyhd') . " WHERE  status = 0  and   `uid`='" .$_SESSION['user_id']. "'";
	$msn = $db->getAll($sql);
	//var_dump($msn);
	// echo "<br>";
  
	//echo date("Y-m-d","1433059655");
	//echo strtotime(" 30 days");


	if($msn){
		foreach ($msn as $v) {
			//echo $v['end_time']."<br>";
			//订单状态
			//order_status
			//0未确认,1确认,2取消,3无效,4已退货,5已分单,6部分分单,7申请退货,8申请部分退货,9申请换货中,10申请部分换货,11已换货
			//shipping_status
			//0未发货,1已发货,2已收货(完成交易),3配货中,4已发货(部分商品),5发货中
			$sql = 'SELECT order_status,shipping_status  FROM' . $ecs->table('order_info') . " WHERE   `order_id`='" .$v['oid']. "'";
			$statusAll = $db->getRow($sql);
			$status = $statusAll['order_status'];

			if (time() >= $v['end_time'] &&  $statusAll['shipping_status'] == 2 ) {
				$sql = 'UPDATE ' . $ecs->table('yyhd') . "SET status = 1 WHERE  id = '".$v[id]."'";
				$db->query($sql);

				//XXX 返款
				$sql = 'UPDATE ' . $ecs->table('users') . "SET user_money = user_money+'".$v['price']."' WHERE   `user_id`='" .$v['uid']. "'";
				$db->query($sql);
				

				//XXX 返款日志 (因为两个余额显示)
				$sql = "INSERT INTO ".$ecs->table('account_log')." (`user_id`,`user_money`,`frozen_money`,`rank_points`,`pay_points`,`pay_golds`,`change_time`,`change_desc`,`change_type`) VALUES ('".$v['uid']."','".$v['price']."','0','0','0','0','".time()."','订单id".$v['oid'].",一元活动返还资金".$v['price']."','99')";
				$db->query($sql); 
				

			}elseif($status > 6 || $status ==  4 || $status == 3 || $status == 2){
				$sql = 'UPDATE ' . $ecs->table('yyhd') . "SET status = 2 WHERE  id = '".$v[id]."'";
				$db->query($sql);
			}
		}
	}

    include_once(ROOT_PATH .'include/lib_clips.php');
    if ($rank = get_rank_info())
    {
        $smarty->assign('rank_name', sprintf($_LANG['your_level'], $rank['rank_name']));
        if (!empty($rank['next_rank_name']))
        {
            $smarty->assign('next_rank_name', sprintf($_LANG['next_level'], $rank['next_rank'] ,$rank['next_rank_name']));
        }
    }
	$info = get_user_default($user_id);
	
	$sql = "SELECT wxid FROM " .$GLOBALS['ecs']->table('users'). " WHERE user_id = '$user_id'";
    $wxid = $GLOBALS['db']->getOne($sql);
	if(!empty($wxid)){
		$weixinInfo = $GLOBALS['db']->getRow("SELECT nickname, headimgurl FROM wxch_user WHERE wxid = '$wxid'");
		$info['avatar'] = empty($weixinInfo['headimgurl']) ? 'themes/mectouch/ZZY_style/moren.png':$weixinInfo['headimgurl'];
		$info['username'] = empty($weixinInfo['nickname']) ? "贵宾":$weixinInfo['nickname'];
	}
    $smarty->assign('info',        $info);
	$smarty->assign('user_id',     $user_id);
    $smarty->assign('prompt',      get_user_prompt($user_id));
    $smarty->display('user_clips.dwt');
}


//  第三方登录接口
elseif($action == 'oath')
{
	$type = empty($_REQUEST['type']) ?  '' : $_REQUEST['type'];
	
	include_once(ROOT_PATH . 'include/website/jntoo.php');

	$c = &website($type);
	if($c)
	{
		if (empty($_REQUEST['callblock']))
		{
			if (empty($_REQUEST['callblock']) && isset($GLOBALS['_SERVER']['HTTP_REFERER']))
			{
				$back_act = strpos($GLOBALS['_SERVER']['HTTP_REFERER'], 'user.php') ? 'index.php' : $GLOBALS['_SERVER']['HTTP_REFERER'];
			}
			else
			{
                           
				$back_act = 'user.php';
			}
		}
		else
		{
			$back_act = trim($_REQUEST['callblock']);
		}

		if($back_act[4] != ':') $back_act = $ecs->url().$back_act;
		$open = empty($_REQUEST['open']) ? 0 : intval($_REQUEST['open']);
		
		$url = $c->login($ecs->url().'user.php?act=oath_login&type='.$type.'&callblock='.urlencode($back_act).'&open='.$open);

		if(!$url)
		{
			show_message( $c->get_error() , '首页', $ecs->url() , 'error');
		}
		header('Location: '.$url);
	}
	else
	{
		show_message('服务器尚未注册该插件！' , '首页',$ecs->url() , 'error');
	}
}

//  处理第三方登录接口
elseif ($action == 'oath_login') {
    $type = empty($_REQUEST['type']) ? '' : $_REQUEST['type'];

    include_once(ROOT_PATH . 'include/website/jntoo.php');
    $c = &website($type);

    if ($c) {
        $access = $c->getAccessToken();

        if (!$access) {
            show_message($c->get_error(), '首页', $ecs->url(), 'error');
        }

        $c->setAccessToken($access);
        $info = $c->getMessage();
        if($type =='renn' ){
            
             $info =  $info['response'];
             $info['user_id'] = $info['id'];
            
        }
        
        if (!$info) {
            show_message($c->get_error(), '首页', $ecs->url(), 'error', false);
        }
        if (!$info['user_id'] || !$info['user_id']) {

            show_message($c->get_error(), '首页', $ecs->url(), 'error', false);
        }
        $info_user_id = $type . '_' . $info['user_id']; //  加个标识！！！防止 其他的标识 一样  // 以后的ID 标识 将以这种形式 辨认
        $info['name'] = str_replace("'", "", $info['name']); // 过滤掉 逗号 不然出错  很难处理   不想去  搞什么编码的了
        if (!$info['user_id'])
            show_message($c->get_error(), '首页', $ecs->url(), 'error', false);


        $sql = 'SELECT user_name,password,aite_id FROM ' . $ecs->table('users') . ' WHERE aite_id = \'' . $info_user_id . '\'';

        $count = $db->getRow($sql);


        if (!$count) {   // 没有当前数据
            if ($user->check_user($info['name'])) {  // 重名处理
                $info['name'] = $info['name'] . '_' . $type . (rand(10000, 99999));
            }
            $user_pass = $user->compile_password(array('password' => $info['user_id']));
            $sql = 'INSERT INTO ' . $ecs->table('users') . '(user_name , password, aite_id , sex , reg_time , user_rank , is_validated) VALUES ' .
                    "('$info[name]' , '$user_pass' , '$info_user_id' , '$info[sex]' , '" . gmtime() . "' , '$info[rank_id]' , '1')";

            $db->query($sql);
        } else {
            $sql = '';
            if ($count['aite_id'] == $info['user_id']) {
                $sql = 'UPDATE ' . $ecs->table('users') . " SET aite_id = '$info_user_id' WHERE aite_id = '$count[aite_id]'";
                $db->query($sql);
            }
            if ($info['name'] != $count['user_name']) {   // 这段可删除
                if ($user->check_user($info['name'])) {  // 重名处理
                    $info['name'] = $info['name'] . '_' . $type . (rand() * 1000);
                }
                $sql = 'UPDATE ' . $ecs->table('users') . " SET user_name = '$info[name]' WHERE aite_id = '$info_user_id'";
                $db->query($sql);
            }
        }
        $user->set_session($info['name']);
        $user->set_cookie($info['name']);
        update_user_info();
        recalculate_price();

        if (!empty($_REQUEST['open'])) {
            die('<script>window.opener.window.location.reload(); window.close();</script>');
        } else {
            ecs_header('Location: ' . $_REQUEST['callblock']);
        }
    }
}

/* 显示会员注册界面 */
if ($action == 'register')
{ require(dirname(__FILE__) . '/include/get_openid.php');
    if ((!isset($back_act)||empty($back_act)) && isset($GLOBALS['_SERVER']['HTTP_REFERER']))
    {
        $back_act = strpos($GLOBALS['_SERVER']['HTTP_REFERER'], 'user.php') ? './index.php' : $GLOBALS['_SERVER']['HTTP_REFERER'];
    }
		 
	$jsApi = new JsApi_pub();

	/*//=========步骤1：网页授权获取用户openid============
	//通过code获得openid
	if (!isset($_GET['code']))
	{
		//触发微信返回code码
		$redirect = urlencode($GLOBALS['ecs']->url().'user.php?act=register');
		$url = $jsApi->createOauthUrlForCode($redirect);
		Header("Location: $url"); 
	}else
	{
		//获取code码，以获取openid
	    $code = $_GET['code'];
		$jsApi->setCode($code);
		$openid = $jsApi->getOpenId();
		$smarty->assign('wxid',  $openid);
	}*/

    /* 取出注册扩展字段 */
    $sql = 'SELECT * FROM ' . $ecs->table('reg_fields') . ' WHERE type < 2 AND display = 1 ORDER BY dis_order, id';
    $extend_info_list = $db->getAll($sql);
    $smarty->assign('extend_info_list', $extend_info_list);

    /* 验证码相关设置 */
    if ((intval($_CFG['captcha']) & CAPTCHA_REGISTER) && gd_version() > 0)
    {
        $smarty->assign('enabled_captcha', 1);
        $smarty->assign('rand',            mt_rand());
    }
	
	$up_uid  = get_affiliate();
	
	if($up_uid)
	{ 
	  $sql = "SELECT user_name FROM " .$ecs->table('users') . "  WHERE user_id = '$up_uid'";
	  $name = $db->getOne($sql);
	  $smarty->assign('recmd_name', $name);
	  $smarty->assign('up_uid', $up_uid);
	}
    
    /* 短信发送设置 by carson */
    if(intval($_CFG['sms_signin']) > 0){
        $smarty->assign('enabled_sms_signin', 1);
    }

    /* 密码提示问题 */
    $smarty->assign('passwd_questions', $_LANG['passwd_questions']);

    /* 增加是否关闭注册 */
    $smarty->assign('shop_reg_closed', $_CFG['shop_reg_closed']);
//    $smarty->assign('back_act', $back_act);
    $smarty->display('user_passport.dwt');
}

//csb
elseif($action == "test"){
	include_once(ROOT_PATH . 'include/lib_passport.php');
	
	//csb    根据目前最大的用户ID，获取现在用户的ID
	$sql="SELECT max(user_id) FROM ".  $ecs->table('users') ;
	$maxId = $db->getOne($sql);
	$id=$maxId+1;
	
	$username = "tourist_".$id;
	$password = "password_".$id;
	$recmded  = empty($_GET['u'])?get_affiliate():intval($_GET["u"]);
	
	$back_act = isset($_GET['back_url']) ? trim($_GET['back_url']) : '';
	if($back_act == "goods.php"){
		$goods_id = $_GET['id'];
		$back_act = $back_act."?id=".$goods_id;
	}elseif($back_act == "article.php"){
		$article_id = $_GET['id'];
		$back_act = $back_act."?id=".$article_id;
	}
	$wxid = isset($_GET['openid']) ? trim($_GET['openid']) : '';
	
	$email = $id.'@qq.com';
	
	//下面的sql查询，为的是确保不重复注册
	$sql = "SELECT user_id FROM ".  $ecs->table('users') ." WHERE wxid = '".$wxid . "'";
	$zhixing = $db->getOne($sql);
	if(empty($zhixing)){
		if (register($username, $password, $email, $other) != false){
			//为了确保user_id和用户名中的ID保持一致，下面这段代码是必须的
			
			
			$sql = 'UPDATE ' . $ecs->table('users') . " SET `wxid`='$wxid', `recmded_id`='$recmded' ,`wxch_bd`='ok' WHERE `user_id`='" . $_SESSION['user_id'] . "'";
			$db->query($sql); 
			
			//将注册需要执行的操作的代码,全部写在下面
			
			empty($affiliate) && $affiliate = array();
            //推荐注册分成 给上线增加人数
            $num = count($affiliate['item']);
			for($i=1;$i<=$num;$i++){
				$sql = 'UPDATE ' . $ecs->table('users') . " SET `down_num`=down_num+1 WHERE `user_id`='" .$recmded. "'";
				$db->query($sql);
				$sql = "SELECT recmded_id FROM " .$ecs->table('users')." WHERE user_id='$recmded'";
				if(!$recmded=$db->getOne($sql)){
					break;
				}
			}
			
			include_once(ROOT_PATH .'wechat/wechat.class.php');
			$sql_ewm = "SELECT  token, appid, appsecret  FROM  wxch_config ORDER BY dateline ASC ";
		
			// 连接微信配置数据库，读取appid等信息，赋值给数组，进行初始化。 
			$row = $db->getRow($sql_ewm);    //getall 为获取多维数组， getone 为获得一个变量
			$options = array(
				'token'=> $row['token'],               //填写你设定的key
				'appid'=>$row['appid'],
				'appsecret'=>$row['appsecret'] );
			$weObj = new Wechat($options);        //实例化 
			
			//调取数据库当中网站地址
			$sql    = "SELECT cfg_value FROM wxch_cfg  WHERE cfg_name='murl'";
			$murl	= $db->getOne($sql);
			$sql    = "SELECT cfg_value FROM wxch_cfg  WHERE cfg_name='baseurl'";
			$baseurl= $db->getOne($sql); 
			
			// 调取该用户的名称id    
			$weixin_uname = $_SESSION['user_name'];
			$user_id      = $_SESSION['user_id'];
		
			//给两位推荐人发送消息
			$sql="SELECT recmded_id FROM" . $ecs->table('users') . "WHERE user_id='$user_id'";
			$recmded_id=$db->getOne($sql);
			$time = local_date($GLOBALS['_CFG']['time_format'], gmtime());
			
			for($i=1;$i<=$num;$i++){
				if($i == 1){ $jibie = "一级" ; $fenchengbili = "20%"; }
				elseif($i == 2){ $jibie = "二级" ; $fenchengbili = "10%"; }
				if($recmded_id !=''){
					$sql="SELECT  u.wxid , w.nickname , u.recmded_id FROM " . $ecs->table('users') . " u LEFT JOIN wxch_user w on w.wxid=u.wxid WHERE user_id='$recmded_id'";
					$wx_up=$db->getRow($sql);
				}
				if($wx_up != ''){ 
					$recmded_id = $wx_up['recmded_id'];
					$msg_up = '亲爱的'.$wx_up['nickname'].'，通过您的努力,您的团队中新增了一个'. $jibie .'会员，关注时间：'. $time .'，他在商城的消费，您都将有'. $fenchengbili. '的提成！';
					$data_up=array(
						'touser'   =>$wx_up['wxid'],                                    //提示每个客服有信息来了
						'msgtype'  =>'news',
						'news'    =>array('articles'=>array(array('title'=>'温馨提示' , 'description'=> $msg_up , 'url'=> "http://".$_SERVER['HTTP_HOST']."/mobile/distributor.php?act=profile" , 'picurl'=> '')))      //将话术与用户发来的内容拼接，发给客服人员。.$arr["Content"]
					); 
					$weObj->sendCustomMessage($data_up);     //发送客服消息
				}
			}
			header("location: ".$back_act);
			
			//下面的是回复文字信息
			/* if($wx_up != ''){ 
				$msg_up = $wx_up['nickname'].'哦！太棒了！您的团队中又增加了一位新伙伴'. $weixin_uname .'可到'.'<a href="'.$baseurl.$murl.'user.php?act=down_line">团队业绩</a>'.'中查看,记得帮助他绑定账户到微信，首先请您的朋友关注官方微信,然后选择点击会员中心按钮菜单中的绑定账号后按照系统提示完成绑定操作。';
				$data_up=array(
					'touser'   =>$wx_up['wxid'],                                    //提示每个客服有信息来了
					'msgtype'  =>'text',
					'text'     =>array('content'=> $msg_up )       //将话术与用户发来的内容拼接，发给客服人员。.$arr["Content"]
				); 
				$weObj->sendCustomMessage($data_up);     //发送客服消息
			}
			 */
			
		}
	}
}



/* 注册会员的处理 */
elseif ($action == 'ajax_register')
{
    /* 增加是否关闭注册 */
    if ($_CFG['shop_reg_closed'])
    {
        $smarty->assign('action',     'register');
        $smarty->assign('shop_reg_closed', $_CFG['shop_reg_closed']);
        $smarty->display('user_passport.dwt');
    }
    else
    {
        include_once(ROOT_PATH . 'include/lib_passport.php');
		include_once('include/cls_json.php');
        $json = new JSON;
        
		$result   = array('status' => 0, 'text'=>'', 'url'=>'');
		 
				
		 		
		$username = isset($_POST['username']) ? trim($_POST['username']) : '';
		$password = isset($_POST['password']) ? trim($_POST['password']) : '';
		$recmded  = isset($_POST['recmded'])  ? trim($_POST['recmded']) : 0;
        $back_act = isset($_POST['back_act']) ? trim($_POST['back_act']) : '';
		$wxid     = isset($_POST['wxid']) ? trim($_POST['wxid']) : '';
		$email    = $username .'@qq.com';
		
		if(!empty($back_act)){
		  $result['url'] = $back_act;
		  }
		else{
			$result['url'] = 'user.php';
			}
        
		$mobile = $username;
        if (strlen($username) < 5)
        {
		    $result['status'] = 2;
			$result['text'] = $_LANG['passport_js']['username_shorter'];
			die($json->encode($result));
			
        }

        if (strlen($password) < 6)
        {
			$result['status'] = 3;
			$result['text'] = $_LANG['passport_js']['password_shorter'];
			die($json->encode($result));
        }

        if (strpos($password, ' ') > 0)
        {
			$result['status'] = 4;
			$result['text'] = $_LANG['passwd_balnk'];
			die($json->encode($result));
        }
       
	     
	   /* 提交的手机号是否已经注册帐号 */
	      if(!empty($mobile)){
			$sql = "SELECT COUNT(user_id) FROM " . $ecs->table('users') . " WHERE mobile_phone = '$mobile'";

			if ($db->getOne($sql) > 0)
			{
			   $result['status'] = 5;
               $result['text']   = $_LANG['mobile_phone_registered'];
               die($json->encode($result));
			}
		  }

        if (register($username, $password, $email, $other) != false)
        {
			 $sql = 'UPDATE ' . $ecs->table('users') . " SET `mobile_phone`='$username', `recmded_id`='$recmded'  WHERE `user_id`='" . $_SESSION['user_id'] . "'";
             $db->query($sql);
			 
			
			empty($affiliate) && $affiliate = array();
            //推荐注册分成 给上线增加人数
            $num = count($affiliate['item']);
		for($i=1;$i<=$num;$i++){
				$sql = 'UPDATE ' . $ecs->table('users') . " SET `down_num`=down_num+1 WHERE `user_id`='" .$recmded. "'";
				$db->query($sql);
				$sql = "SELECT recmded_id FROM " .$ecs->table('users')." WHERE user_id='$recmded'";
				if(!$recmded=$db->getOne($sql)){
					break;
				}
			}
			
			
	
			/*.......发送微信消息start.......*/
	
	
		//require_once(ROOT_PATH . 'includes/lib_sms.php');
		
		// wuli  初始化wechat类，调取客服接口，向外发送微信。

		  include_once(ROOT_PATH .'wechat/wechat.class.php');
        		
	    	$sql_ewm = "SELECT  token, appid, appsecret  FROM  wxch_config ORDER BY dateline ASC ";
		
         // 连接微信配置数据库，读取appid等信息，赋值给数组，进行初始化。 
       
            $row = $db->getRow($sql_ewm);    //getall 为获取多维数组， getone 为获得一个变量
		 
        	$options = array(
			'token'=> $row['token'],               //填写你设定的key
			'appid'=>$row['appid'],
			'appsecret'=>$row['appsecret'] );

		    $weObj = new Wechat($options);        //实例化 
			
			//songkai 调取数据库当中网站地址
			$sql    = "SELECT cfg_value FROM wxch_cfg  WHERE cfg_name='murl'";
			$murl= $db->getOne($sql);
			$sql    = "SELECT cfg_value FROM wxch_cfg  WHERE cfg_name='baseurl'";
			$baseurl= $db->getOne($sql); 
			
			//songkai 调取该用户的名称id    
			$weixin_uname = $_SESSION['user_name'];
			$user_id      = $_SESSION['user_id'];
			
			
			//songkai 调取该用户伙伴的wxid、昵称
			$sql="SELECT recmded_id FROM" . $ecs->table('users') . "WHERE user_id='$user_id'";
			$recmded_id=$db->getOne($sql);
			if($recmded_id !='')
			{
			$sql="SELECT  u.wxid , w.nickname FROM " . $ecs->table('users') . " u LEFT JOIN wxch_user w on w.wxid=u.wxid WHERE user_id='$recmded_id'";
			$wx_up=$db->getRow($sql);
			}
			 
			

			
			//songkai 发给伙伴
	       if($wx_up != '')
	       { 
	         $msg_up = $wx_up['nickname'].'哦！太棒了！您的团队中又增加了一位新伙伴'. $weixin_uname .'可到'.'<a href="'.$baseurl.$murl.'user.php?act=down_line">团队业绩</a>'.'中查看,记得帮助他绑定账户到微信，首先请您的朋友关注官方微信 ,然后选择点击会员中心按钮菜单中的绑定账号后按照系统提示完成绑定操作。';
		 
	         $data_up=array(
								 'touser'   =>$wx_up['wxid'],                                    //提示每个客服有信息来了
								 'msgtype'  =>'text',
								 'text'     =>array('content'=> $msg_up )       //将话术与用户发来的内容拼接，发给客服人员。.$arr["Content"]

			         	  ); 
	   
	         $weObj->sendCustomMessage($data_up);     //发送客服消息
			 
			
	       }
	   
	   
	 if(!empty($wxid))
	       { 
		     $sql = " SELECT nickname FROM wxch_user WHERE wxid = '".$wxid."'";
		    /* $nickname = $db->getOne($sql);
	         $msg=$nickname.'你好！您刚刚获得了一个'.$bonus['type_money'].'元的红包，请您尽快购物使用！';
		 
	         $data_up=array(
								 'touser'=>$wxid,                                    //提示每个客服有信息来了
								 'msgtype'=>'text',
								 'text'=>array('content'=> $msg )          //将话术与用户发来的内容拼接，发给客服人员。.$arr["Content"]

			         	  ); 
	   
	         $weObj->sendCustomMessage($data_up);     //发送客服消息*/
			  
			 
			  $sql = 'UPDATE ' . $ecs->table('users') . " SET `wxid`='$wxid', `wxch_bd`='ok'  WHERE `user_id`='" . $_SESSION['user_id'] . "'";
			  
              $db->query($sql);
			  
			  $sql = "UPDATE wxch_user SET `uname`='$username'  WHERE `wxid`='" . $wxid. "'";

              $db->query($sql);

	       }
	

	/*.......发送微信消息end.......*/

			
				
				
           $result['status'] = 1;
           $result['text']   = "注册成功";
           die($json->encode($result));
        }
        else
        {
			$result['status'] = 6;
            $result['text']   = $_LANG['sign_up'];
            die($json->encode($result));
        }
    }
}

/* 验证用户注册邮件 */
elseif ($action == 'validate_email')
{
    $hash = empty($_GET['hash']) ? '' : trim($_GET['hash']);
    if ($hash)
    {
        include_once(ROOT_PATH . 'include/lib_passport.php');
        $id = register_hash('decode', $hash);
        if ($id > 0)
        {
            $sql = "UPDATE " . $ecs->table('users') . " SET is_validated = 1 WHERE user_id='$id'";
            $db->query($sql);
            $sql = 'SELECT user_name, email FROM ' . $ecs->table('users') . " WHERE user_id = '$id'";
            $row = $db->getRow($sql);
            show_message(sprintf($_LANG['validate_ok'], $row['user_name'], $row['email']),$_LANG['profile_lnk'], 'user.php');
        }
    }
    show_message($_LANG['validate_fail']);
}
elseif ($action == 'user_golds'){
  $sql = "SELECT pay_golds FROM " .$ecs->table('users') . "  WHERE user_id = '$user_id'";
  $golds = $db->getOne($sql);
  $golds = empty($golds)?0:intval($golds);
  
  $smarty->assign('golds', $golds);
  $smarty->display('user_clips.dwt');
}

/* 验证用户注册用户名是否可以注册 */
elseif ($action == 'is_registered')
{
    include_once(ROOT_PATH . 'include/lib_passport.php');

    $username = trim($_GET['username']);
    $username = json_str_iconv($username);

    if ($user->check_user($username) || admin_registered($username))
    {
        echo 'false';
    }
    else
    {
        echo 'true';
    }
}

/* 验证用户邮箱地址是否被注册 */
elseif($action == 'check_email')
{
    $email = trim($_GET['email']);
    if ($user->check_email($email))
    {
        echo 'false';
    }
    else
    {
        echo 'ok';
    }
}
/* 用户登录界面 */
elseif ($action == 'login')
{
    if (empty($back_act)) {
        if (empty($back_act) && isset($GLOBALS['_SERVER']['HTTP_REFERER'])) {
            $back_act = strpos($GLOBALS['_SERVER']['HTTP_REFERER'], 'user.php') ? './index.php' : $GLOBALS['_SERVER']['HTTP_REFERER'];
        } else {
            $back_act = 'user.php';
        }
    }

    /* 短信发送设置 by carson */
    if(intval($_CFG['sms_signin']) > 0){
        $smarty->assign('enabled_sms_signin', 1);
    }

    $captcha = intval($_CFG['captcha']);
    if (($captcha & CAPTCHA_LOGIN) && (!($captcha & CAPTCHA_LOGIN_FAIL) || (($captcha & CAPTCHA_LOGIN_FAIL) && $_SESSION['login_fail'] > 2)) && gd_version() > 0 || (intval($_CFG['captcha']) & CAPTCHA_REGISTER))
    {
        $GLOBALS['smarty']->assign('enabled_captcha', 1);
        $GLOBALS['smarty']->assign('rand', mt_rand());
    }
    $back_act   = isset($_GET['index_back'])?trim($_GET['index_back']):"";
	$dis_act	= isset($_GET['dis_act'])?trim($_GET['dis_act']):"";
	$i			= isset($_GET['goods_id'])?intval($_GET['goods_id']):0;
	$article_id	= isset($_GET['article_id'])?intval($_GET['article_id']):0;
	if(!empty($ad_apply_act)){
		$back_act = "ad_apply.php?act=".$ad_apply_act;
	}
	
	if(!empty($dis_act)){
		$back_act = "distributor.php?act=".$dis_act;
	}
		
	if($id>0){
		$back_act = "goods.php?id=".$id;
	}
	if($article_id>0){
		$back_act = "article.php?id=".$article_id;
	}
	if(empty($back_act)){
		$back_act = 'user.php';
	}
    $smarty->assign('back_act', $back_act);
    $smarty->display('user_passport.dwt');
}

/* 处理会员的登录 */
elseif ($action == 'act_login')
{
    $username = isset($_POST['username']) ? trim($_POST['username']) : '';
    $password = isset($_POST['password']) ? trim($_POST['password']) : '';
    $back_act = isset($_POST['back_act']) ? trim($_POST['back_act']) : '';

    /* 关闭验证码 by wang
    $captcha = intval($_CFG['captcha']);
    if (($captcha & CAPTCHA_LOGIN) && (!($captcha & CAPTCHA_LOGIN_FAIL) || (($captcha & CAPTCHA_LOGIN_FAIL) && $_SESSION['login_fail'] > 2)) && gd_version() > 0)
    {
        if (empty($_POST['captcha']))
        {
            show_message($_LANG['invalid_captcha'], $_LANG['relogin_lnk'], 'user.php', 'error');
        }

        // 检查验证码
        include_once('include/cls_captcha.php');

        $validator = new captcha();
        $validator->session_word = 'captcha_login';
        if (!$validator->check_word($_POST['captcha']))
        {
            show_message($_LANG['invalid_captcha'], $_LANG['relogin_lnk'], 'user.php', 'error');
        }
    }
    */

    //用户名是邮箱格式 by wang
    if(is_email($username))
    {
        $sql ="select user_name from ".$ecs->table('users')." where email='".$username."'";
        $username_try = $db->getOne($sql);
        $username = $username_try ? $username_try:$username;
    }

    //用户名是手机格式 by wang
    if(is_mobile($username))
    {
        $sql ="select user_name from ".$ecs->table('users')." where mobile_phone='".$username."'";
        $username_try = $db->getOne($sql);
        $username = $username_try ? $username_try:$username;
    }

    if ($user->login($username, $password,isset($_POST['remember'])))
    {
        update_user_info();
        recalculate_price();

        $ucdata = isset($user->ucdata)? $user->ucdata : '';
        show_message($_LANG['login_success'] . $ucdata , array($_LANG['back_up_page'], $_LANG['profile_lnk']), array($back_act,'user.php'), 'info');
    }
    else
    {
        $_SESSION['login_fail'] ++ ;
        show_message($_LANG['login_failure'], $_LANG['relogin_lnk'], 'user.php', 'error');
    }
}

/* 处理 ajax 的登录请求 */
elseif ($action == 'signin')
{
    include_once('include/cls_json.php');
    $json = new JSON;

    $username = !empty($_POST['username']) ? json_str_iconv(trim($_POST['username'])) : '';
    $password = !empty($_POST['password']) ? trim($_POST['password']) : '';
	$back_act = isset($_POST['back_act']) ? trim($_POST['back_act']) : 'user.php';
    $result   = array('error' => 0, 'status' => '', 'url'=>'');
    
	
	$result['url'] = $back_act;
	
    if ($user->login($username, $password))
    {
		
        update_user_info();  //更新用户信息
        recalculate_price(); // 重新计算购物车中的商品价格
        $smarty->assign('user_info', get_user_info());
        $ucdata = empty($user->ucdata)? "" : $user->ucdata;
        $result['status'] = 1;
        $result['content'] = $smarty->fetch('library/member_info.lbi');
    }
    else
    {
        $_SESSION['login_fail']++;
        if ($_SESSION['login_fail'] > 2)
        {
            $smarty->assign('enabled_captcha', 1);
            $result['html'] = $smarty->fetch('library/member_info.lbi');
        }
        $result['status']   = 2;
        $result['content'] = $_LANG['login_failure'];
    }
    die($json->encode($result));
}

/* 处理 ajax 判断是否登陆 */
elseif ($action == 'islogin')
{
    include_once('include/cls_json.php');
    $json = new JSON;

   
    $result   = array('login' => 0, 'name' => '', 'cart'=>0);
	
	 $sql = 'SELECT SUM(goods_number) AS number FROM ' . $GLOBALS['ecs']->table('cart') .
           " WHERE session_id = '" . SESS_ID . "' AND rec_type = '" . CART_GENERAL_GOODS . "'";
     $number = $GLOBALS['db']->getOne($sql);
    
	if($user_id){
		 $result['login'] = 1;
		 $result['name']  = $_SESSION['user_name'];
		 $result['cart']  = intval($number);
		}
	if(strpos($result['name'], "tourist") !== false){
		 $result['name'] = "贵宾";
		}
	
    die($json->encode($result));
}

/* 退出会员中心 */
elseif ($action == 'logout')
{
    if ((!isset($back_act)|| empty($back_act)) && isset($GLOBALS['_SERVER']['HTTP_REFERER']))
    {
        $back_act = strpos($GLOBALS['_SERVER']['HTTP_REFERER'], 'user.php') ? './index.php' : $GLOBALS['_SERVER']['HTTP_REFERER'];
    }

    $user->logout();
    $ucdata = empty($user->ucdata)? "" : $user->ucdata;
    show_message($_LANG['logout'] . $ucdata, array($_LANG['back_up_page'], $_LANG['back_home_lnk']), array($back_act, 'index.php'), 'info');
}

/* 个人资料页面 */

elseif ($action == 'profile')

{

    include_once(ROOT_PATH . 'include/lib_transaction.php');

include_once(ROOT_PATH .'include/lib_clips.php');
    if ($rank = get_rank_info())
    {
        $smarty->assign('rank_name', sprintf($_LANG['your_level'],  $rank['rank_name']));
        if (!empty($rank['next_rank_name']))
        {
            $smarty->assign('next_rank_name', sprintf($_LANG['next_level'], $rank['next_rank'] ,$rank['next_rank_name']));
        }
    }
	$info = get_user_default($user_id);
	
	$sql = "SELECT wxid FROM " .$GLOBALS['ecs']->table('users'). " WHERE user_id = '$user_id'";
    $wxid = $GLOBALS['db']->getOne($sql);
	if(!empty($wxid)){
		$weixinInfo = $GLOBALS['db']->getRow("SELECT nickname, headimgurl FROM wxch_user WHERE wxid = '$wxid'");
		$info['avatar'] = empty($weixinInfo['headimgurl']) ? '':$weixinInfo['headimgurl'];
		$info['username'] = empty($weixinInfo['nickname']) ? $info['username']:$weixinInfo['nickname'];
	}
    $smarty->assign('info',        $info);
	$smarty->assign('user_id',     $user_id);
    $smarty->assign('prompt',      get_user_prompt($user_id));
	
	$sql = "SELECT recmded_id FROM ". $ecs->table('users') ."  WHERE user_id = '". $_SESSION['user_id'] ."'";
	$recmded_id = $db->getOne($sql);
	$sql = "SELECT user_name FROM ". $ecs->table('users') ."  WHERE user_id = ". $recmded_id ;
    $re_name = $db->getOne($sql);
	$smarty->assign('name',        $re_name);
    
	$user_info = get_profile($user_id);


    /* 取出注册扩展字段 */

    $sql = 'SELECT * FROM ' . $ecs->table('reg_fields') . ' WHERE type < 2 AND display = 1 ORDER BY dis_order, id';

    $extend_info_list = $db->getAll($sql);



    $sql = 'SELECT reg_field_id, content ' .

           'FROM ' . $ecs->table('reg_extend_info') .

           " WHERE user_id = $user_id";

    $extend_info_arr = $db->getAll($sql);



    $temp_arr = array();

    foreach ($extend_info_arr AS $val)

    {

        $temp_arr[$val['reg_field_id']] = $val['content'];

    }



    foreach ($extend_info_list AS $key => $val)

    {

        switch ($val['id'])

        {

            case 1:     $extend_info_list[$key]['content'] = $user_info['msn']; break;

            case 2:     $extend_info_list[$key]['content'] = $user_info['qq']; break;

            case 3:     $extend_info_list[$key]['content'] = $user_info['office_phone']; break;

            case 4:     $extend_info_list[$key]['content'] = $user_info['home_phone']; break;

            case 5:     $extend_info_list[$key]['content'] = $user_info['mobile_phone']; break;

            default:    $extend_info_list[$key]['content'] = empty($temp_arr[$val['id']]) ? '' : $temp_arr[$val['id']] ;

        }

    }



    $smarty->assign('extend_info_list', $extend_info_list);



    /* 密码提示问题 */

    $smarty->assign('passwd_questions', $_LANG['passwd_questions']);



    $smarty->assign('profile', $user_info);

    $smarty->display('user_clips.dwt');

}

/* 修改个人资料的处理 */
elseif ($action == 'act_edit_profile')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');

    $birthday = trim($_POST['birthdayYear']) .'-'. trim($_POST['birthdayMonth']) .'-'.
    trim($_POST['birthdayDay']);
    $email = trim($_POST['email']);
    $other['msn'] = $msn = isset($_POST['extend_field1']) ? trim($_POST['extend_field1']) : '';
    $other['qq'] = $qq = isset($_POST['extend_field2']) ? trim($_POST['extend_field2']) : '';
    $other['office_phone'] = $office_phone = isset($_POST['extend_field3']) ? trim($_POST['extend_field3']) : '';
    $other['home_phone'] = $home_phone = isset($_POST['extend_field4']) ? trim($_POST['extend_field4']) : '';
    $other['mobile_phone'] = $mobile_phone = isset($_POST['extend_field5']) ? trim($_POST['extend_field5']) : '';
    $sel_question = empty($_POST['sel_question']) ? '' : compile_str($_POST['sel_question']);
    $passwd_answer = isset($_POST['passwd_answer']) ? compile_str(trim($_POST['passwd_answer'])) : '';

    /* 更新用户扩展字段的数据 */
    $sql = 'SELECT id FROM ' . $ecs->table('reg_fields') . ' WHERE type = 0 AND display = 1 ORDER BY dis_order, id';   //读出所有扩展字段的id
    $fields_arr = $db->getAll($sql);

    foreach ($fields_arr AS $val)       //循环更新扩展用户信息
    {
        $extend_field_index = 'extend_field' . $val['id'];
        if(isset($_POST[$extend_field_index]))
        {
            $temp_field_content = strlen($_POST[$extend_field_index]) > 100 ? mb_substr(htmlspecialchars($_POST[$extend_field_index]), 0, 99) : htmlspecialchars($_POST[$extend_field_index]);
            $sql = 'SELECT * FROM ' . $ecs->table('reg_extend_info') . "  WHERE reg_field_id = '$val[id]' AND user_id = '$user_id'";
            if ($db->getOne($sql))      //如果之前没有记录，则插入
            {
                $sql = 'UPDATE ' . $ecs->table('reg_extend_info') . " SET content = '$temp_field_content' WHERE reg_field_id = '$val[id]' AND user_id = '$user_id'";
            }
            else
            {
                $sql = 'INSERT INTO '. $ecs->table('reg_extend_info') . " (`user_id`, `reg_field_id`, `content`) VALUES ('$user_id', '$val[id]', '$temp_field_content')";
            }
            $db->query($sql);
        }
    }

    /* 写入密码提示问题和答案 */
    if (!empty($passwd_answer) && !empty($sel_question))
    {
        $sql = 'UPDATE ' . $ecs->table('users') . " SET `passwd_question`='$sel_question', `passwd_answer`='$passwd_answer'  WHERE `user_id`='" . $_SESSION['user_id'] . "'";
        $db->query($sql);
    }

    if (!empty($office_phone) && !preg_match( '/^[\d|\_|\-|\s]+$/', $office_phone ) )
    {
        show_message($_LANG['passport_js']['office_phone_invalid']);
    }
    if (!empty($home_phone) && !preg_match( '/^[\d|\_|\-|\s]+$/', $home_phone) )
    {
         show_message($_LANG['passport_js']['home_phone_invalid']);
    }
    if (!is_email($email))
    {
        show_message($_LANG['msg_email_format']);
    }
    if (!empty($msn) && !is_email($msn))
    {
         show_message($_LANG['passport_js']['msn_invalid']);
    }
    if (!empty($qq) && !preg_match('/^\d+$/', $qq))
    {
         show_message($_LANG['passport_js']['qq_invalid']);
    }
    if (!empty($mobile_phone) && !preg_match('/^[\d-\s]+$/', $mobile_phone))
    {
        show_message($_LANG['passport_js']['mobile_phone_invalid']);
    }


    $profile  = array(
        'user_id'  => $user_id,
        'email'    => isset($_POST['email']) ? trim($_POST['email']) : '',
        'sex'      => isset($_POST['sex'])   ? intval($_POST['sex']) : 0,
        'birthday' => $birthday,
        'other'    => isset($other) ? $other : array()
        );


    if (edit_profile($profile))
    {
        show_message($_LANG['edit_profile_success'], $_LANG['profile_lnk'], 'user.php?act=profile', 'info');
    }
    else
    {
        if ($user->error == ERR_EMAIL_EXISTS)
        {
            $msg = sprintf($_LANG['email_exist'], $profile['email']);
        }
        else
        {
            $msg = $_LANG['edit_profile_failed'];
        }
        show_message($msg, '', '', 'info');
    }
}

/* 密码找回-->修改密码界面 */
elseif ($action == 'get_password')
{
    include_once(ROOT_PATH . 'include/lib_passport.php');

    if (isset($_GET['code']) && isset($_GET['uid'])) //从邮件处获得的act
    {
        $code = trim($_GET['code']);
        $uid  = intval($_GET['uid']);

        /* 判断链接的合法性 */
        $user_info = $user->get_profile_by_id($uid);
        if (empty($user_info) || ($user_info && md5($user_info['user_id'] . $_CFG['hash_code'] . $user_info['reg_time']) != $code))
        {
            show_message($_LANG['parm_error'], $_LANG['back_home_lnk'], './', 'info');
        }

        $smarty->assign('uid',    $uid);
        $smarty->assign('code',   $code);
        $smarty->assign('action', 'reset_password');
        $smarty->display('user_passport.dwt');
    }
    else
    {
        /* 短信发送设置 by carson */
        if(intval($_CFG['sms_signin']) > 0){
            $smarty->assign('enabled_sms_signin', 1);
        }
        //显示用户名和email表单
        $smarty->display('user_passport.dwt');
    }
}

/* 密码找回-->输入用户名界面 */
elseif ($action == 'qpassword_name')
{
    //显示输入要找回密码的账号表单
    $smarty->display('user_passport.dwt');
}

/* 密码找回-->根据注册用户名取得密码提示问题界面 */
elseif ($action == 'get_passwd_question')
{
    if (empty($_POST['user_name']))
    {
        show_message($_LANG['no_passwd_question'], $_LANG['back_home_lnk'], './', 'info');
    }
    else
    {
        $user_name = trim($_POST['user_name']);
    }

    //取出会员密码问题和答案
    $sql = 'SELECT user_id, user_name, passwd_question, passwd_answer FROM ' . $ecs->table('users') . " WHERE user_name = '" . $user_name . "'";
    $user_question_arr = $db->getRow($sql);

    //如果没有设置密码问题，给出错误提示
    if (empty($user_question_arr['passwd_answer']))
    {
        show_message($_LANG['no_passwd_question'], $_LANG['back_home_lnk'], './', 'info');
    }

    $_SESSION['temp_user'] = $user_question_arr['user_id'];  //设置临时用户，不具有有效身份
    $_SESSION['temp_user_name'] = $user_question_arr['user_name'];  //设置临时用户，不具有有效身份
    $_SESSION['passwd_answer'] = $user_question_arr['passwd_answer'];   //存储密码问题答案，减少一次数据库访问

    $captcha = intval($_CFG['captcha']);
    if (($captcha & CAPTCHA_LOGIN) && (!($captcha & CAPTCHA_LOGIN_FAIL) || (($captcha & CAPTCHA_LOGIN_FAIL) && $_SESSION['login_fail'] > 2)) && gd_version() > 0)
    {
        $GLOBALS['smarty']->assign('enabled_captcha', 1);
        $GLOBALS['smarty']->assign('rand', mt_rand());
    }

    $smarty->assign('passwd_question', $_LANG['passwd_questions'][$user_question_arr['passwd_question']]);
    $smarty->display('user_passport.dwt');
}

/* 密码找回-->根据提交的密码答案进行相应处理 */
elseif ($action == 'check_answer')
{
    $captcha = intval($_CFG['captcha']);
    if (($captcha & CAPTCHA_LOGIN) && (!($captcha & CAPTCHA_LOGIN_FAIL) || (($captcha & CAPTCHA_LOGIN_FAIL) && $_SESSION['login_fail'] > 2)) && gd_version() > 0)
    {
        if (empty($_POST['captcha']))
        {
            show_message($_LANG['invalid_captcha'], $_LANG['back_retry_answer'], 'user.php?act=qpassword_name', 'error');
        }

        /* 检查验证码 */
        include_once('include/cls_captcha.php');

        $validator = new captcha();
        $validator->session_word = 'captcha_login';
        if (!$validator->check_word($_POST['captcha']))
        {
            show_message($_LANG['invalid_captcha'], $_LANG['back_retry_answer'], 'user.php?act=qpassword_name', 'error');
        }
    }

    if (empty($_POST['passwd_answer']) || $_POST['passwd_answer'] != $_SESSION['passwd_answer'])
    {
        show_message($_LANG['wrong_passwd_answer'], $_LANG['back_retry_answer'], 'user.php?act=qpassword_name', 'info');
    }
    else
    {
        $_SESSION['user_id'] = $_SESSION['temp_user'];
        $_SESSION['user_name'] = $_SESSION['temp_user_name'];
        unset($_SESSION['temp_user']);
        unset($_SESSION['temp_user_name']);
        $smarty->assign('uid',    $_SESSION['user_id']);
        $smarty->assign('action', 'reset_password');
        $smarty->display('user_passport.dwt');
    }
}

/* 发送密码修改确认邮件 */
elseif ($action == 'send_pwd_email')
{
    include_once(ROOT_PATH . 'include/lib_passport.php');

    /* 初始化会员用户名和邮件地址 */
    $user_name = !empty($_POST['user_name']) ? trim($_POST['user_name']) : '';
    $email     = !empty($_POST['email'])     ? trim($_POST['email'])     : '';

    //用户名和邮件地址是否匹配
    $user_info = $user->get_user_info($user_name);

    if ($user_info && $user_info['email'] == $email)
    {
        //生成code
         //$code = md5($user_info[0] . $user_info[1]);

        $code = md5($user_info['user_id'] . $_CFG['hash_code'] . $user_info['reg_time']);
        //发送邮件的函数
        if (send_pwd_email($user_info['user_id'], $user_name, $email, $code))
        {
            show_message($_LANG['send_success'] . $email, $_LANG['back_home_lnk'], './', 'info');
        }
        else
        {
            //发送邮件出错
            show_message($_LANG['fail_send_password'], $_LANG['back_page_up'], './', 'info');
        }
    }
    else
    {
        //用户名与邮件地址不匹配
        show_message($_LANG['username_no_email'], $_LANG['back_page_up'], '', 'info');
    }
}

/* 短信找回设置密码 */
elseif ($action == 'reset_pwd')
{
    include_once(ROOT_PATH . 'include/lib_passport.php');

    /* 初始化会员手机 */
    $mobile   = !empty($_POST['mobile']) ? trim($_POST['mobile']) : '';
	$password = !empty($_POST['password']) ? trim($_POST['password']) : '';
	$data = array('status'=>0, 'text'=>'');
	$m = 0;
    
    $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE mobile_phone='$mobile'";
    $user_id = $db->getOne($sql);
    if ($user_id > 0)
    {
       if(!empty($password)){
	   
	    $sql="UPDATE ".$ecs->table('users'). "SET `ec_salt`='0',password='". md5($password) ."' WHERE mobile_phone= '".$mobile."'";
        $m = $db->query($sql);
		if($m){
			$data['status'] = 1;
		    $data['text']   ="新密码设置成功";
			
			}
        else{
		   $data['status'] = 2;
		   $data['text']   ="新密码设置失败";
			  }
	   }
	  else{
		  $data['status'] = 4;
		  $data['text']   ="密码不能为空";
		  
		  }
	   
    }
    else
    {
			$data['status'] = 3;
			$data['text']   = $_LANG['username_no_mobile'];
    }
	
	exit(json_encode($data));
}

/* 重置新密码 */
elseif ($action == 'reset_password')
{
    //显示重置密码的表单
    $smarty->display('user_passport.dwt');
}

/* 修改会员密码 */
elseif ($action == 'act_edit_password')
{
    include_once(ROOT_PATH . 'include/lib_passport.php');

    $old_password = isset($_POST['old_password']) ? trim($_POST['old_password']) : null;
    $new_password = isset($_POST['new_password']) ? trim($_POST['new_password']) : '';
    $user_id      = isset($_POST['uid'])  ? intval($_POST['uid']) : $user_id;
    $code         = isset($_POST['code']) ? trim($_POST['code'])  : '';

    if (strlen($new_password) < 6)
    {
        show_message($_LANG['passport_js']['password_shorter']);
    }

    $user_info = $user->get_profile_by_id($user_id); //论坛记录

    if (($user_info && (!empty($code) && md5($user_info['user_id'] . $_CFG['hash_code'] . $user_info['reg_time']) == $code)) || ($_SESSION['user_id']>0 && $_SESSION['user_id'] == $user_id && $user->check_user($_SESSION['user_name'], $old_password)))
    {
		
        if ($user->edit_user(array('username'=> (empty($code) ? $_SESSION['user_name'] : $user_info['user_name']), 'old_password'=>$old_password, 'password'=>$new_password), empty($code) ? 0 : 1))
        {
			$sql="UPDATE ".$ecs->table('users'). "SET `ec_salt`='0' WHERE user_id= '".$user_id."'";
			$db->query($sql);
            $user->logout();
            show_message($_LANG['edit_password_success'], $_LANG['relogin_lnk'], 'user.php?act=login', 'info');
        }
        else
        {
            show_message($_LANG['edit_password_failure'], $_LANG['back_page_up'], '', 'info');
        }
    }
    else
    {
        show_message($_LANG['edit_password_failure'], $_LANG['back_page_up'], '', 'info');
    }

}

/* 添加一个红包 */
elseif ($action == 'act_add_bonus')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
	include_once('include/cls_json.php');
    $json = new JSON;
	$result = array('status'=>0, 'msg'=>'' ,'url'=>'');

    $bouns_sn = isset($_POST['bonus_sn']) ? intval($_POST['bonus_sn']) : '';

    if (add_bonus($user_id, $bouns_sn))
    {
		$result['status'] = 1;
		$result['url']    = "user.php?act=bonus";
    }
    else
    {
		$result['status'] = 2;
		
    }
	 die($json->encode($result));
}

/* 查看订单列表 */
elseif ($action == 'order_list')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');

    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
	$type = isset($_REQUEST['type'])?intval($_REQUEST['type']):0;
  
    if($type ==1){
		$exwhere = " AND order_status=0 AND  pay_status = !2";
		}
	elseif($type==2){
		$exwhere = " AND order_status=1 AND  pay_status = 2 AND shipping_status = 0";
		}
	elseif($type==3){
		$exwhere = " AND order_status=5 AND  pay_status = 2 AND shipping_status = 1";
		}
	else{
		$exwhere = "";
		}
    $record_count = $db->getOne("SELECT COUNT(*) FROM " .$ecs->table('order_info'). " WHERE user_id = '$user_id'".$exwhere);

    $pager  = get_pager('user.php', array('act' => $action), $record_count, $page);

    $orders = get_user_orders($user_id, $pager['size'], $pager['start'], $exwhere);
	
	foreach($orders as $vo){
            //获取订单第一个商品的图片
           $order_goods = $db->getRow("SELECT g.goods_thumb as thumb, g.goods_name as name FROM " .$ecs->table('order_goods'). " as og left join " .$ecs->table('goods'). " g on og.goods_id = g.goods_id WHERE og.order_id = ".$vo['order_id']." limit 1");
		   $goods_num = $db->getOne("SELECT SUM(goods_number) FROM ".$ecs->table('order_goods')."WHERE order_id = ".$vo['order_id']);
		   $Downtime = 0;
			
		 if($vo['order_status']==2)
		  {
			 $hander = '已取消';	
		   }
		 elseif($vo['order_status']==4){
			  $hander = '已退款';	
			  }
		 elseif($vo['order_status']==7){
			  $hander = '申请退款中';	
			  }
		 elseif($vo['order_status']==8){
			  $hander = '申请部分退款中';	
		 }
		 elseif($vo['order_status']==9){
			  $hander = '申请换货中';	
		 }
		 elseif($vo['order_status']==10){
			  $hander = '申请部分换货中';	
			  } 
		 elseif($vo['order_status']==11){
			  $hander = '已换货';	
			  }
		 else
		  {
			 if($vo['pay_status']==2)
			 {
		       if(!$vo['shipping_status'])
			    {
				 $hander = '已付款,等待发货';	 
				 
			    }
		
			   if($vo['shipping_status']==1)
			    {
				 $hander = '<a href="user.php?act=order_tracking&order_id='.$vo['order_id'].'" class="graybtn " >查看物流</a>
						   <a href="javascript:;" class="bluebtn shou_btn"  data-id='.$vo['order_id'].' >确认收货</a>';
				 $Downtime = (intval($vo['shipping_time'])+30*24*3600-time())*100;	
			     }
		      if($vo['shipping_status']==2){
				  $hander = '<span class="ok"><i class="iconfont">&#xf0013;</i>已完成</span>';	
				 }
				 
			  if($vo['shipping_status']>2)
			    {
				 $hander = '已付款,正在发货中...';
			 
			     }
			
			  }
		  else{
				 $hander = '<a href="flow.php?step=act_pay&order_id='.$vo['order_id'].'" class="btn gotopay">去付款</a>
                        <a href="javascript:;" class="graybtn cancle-order" data-id="'.$vo['order_id'].'">取消订单</a>';	
				 
			  }
			
		  } 
            $orderList[] = array(
                'goods-thumb'=> '<a class="prodt-img" href="user.php?act=order_detail&order_id='.$vo['order_id'].'">
				<img src="'.$config['site_url'].$order_goods['thumb'].'"></a>',
                'goods_name' =>'<a href="user.php?act=order_detail&order_id='.$vo['order_id'].'">'. $order_goods['name'].'<a>',
                'flex-box'   => '<span class="count">共<em>'.$goods_num.'</em>件商品</span>
                               <span class="price"><i class="iconfont icon-rmb">㑌</i>'.$vo['total_fee'].'</span>',
				 'id'       => $vo['order_id'],
                'btn_warp'  => $hander,
				'time' =>$Downtime
            );
        }
	
	$order_title = "我的订单";
    $merge  = get_user_merge($user_id);

	$smarty->assign('title', $order_title);
    $smarty->assign('merge',    $merge);
	$smarty->assign('orders',   $orderList);
    $smarty->assign('pager',    $pager);
    $smarty->display('order.dwt');
}

/* 查看有物流信息的订单 */
elseif ($action == 'order_express')
{	
    include_once(ROOT_PATH . 'include/lib_transaction.php');

    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

    $record_count = $db->getOne("SELECT COUNT(*) FROM " .$ecs->table('order_info'). " WHERE user_id = '$user_id' AND shipping_status = 1");

    $pager  = get_pager('user.php', array('act' => $action), $record_count, $page);

    $orders = get_user_orders_barnabas($user_id, $pager['size'], $pager['start'] , " AND  shipping_status = 1 ");
	
	foreach($orders as $vo){
            //获取订单第一个商品的图片
           $order_goods = $db->getRow("SELECT g.goods_thumb as thumb, g.goods_name as name FROM " .$ecs->table('order_goods'). " as og left join " .$ecs->table('goods'). " g on og.goods_id = g.goods_id WHERE og.order_id = ".$vo['order_id']." limit 1");
		   $goods_num = $db->getOne("SELECT SUM(goods_number) FROM ".$ecs->table('order_goods')."WHERE order_id = ".$vo['order_id']);
		   $Downtime = 0;
			
		 if($vo['order_status']==2)
		  {
			 $hander = '已取消';	
		   }
		 else
		  {
			 if($vo['pay_status']==2)
			 {
				 $hander = '<a href="user.php?act=order_tracking&order_id='.$vo['order_id'].'" class="graybtn " >查看物流</a>
						   <a href="javascript:;" class="bluebtn shou_btn"  data-id='.$vo['order_id'].' >确认收货</a>';
				 $Downtime = (intval($vo['shipping_time'])+7*24*3600-time())*100;
			
			  }
		  else{
				 $hander = '<a href="flow.php?step=act_pay&order_id='.$vo['order_id'].'" class="btn gotopay">去付款</a>
                        <a href="javascript:;" class="graybtn cancle-order" data-id="'.$vo['order_id'].'">取消订单</a>';	
				 
			  }
			
		  } 
            $orderList[] = array(
                'goods-thumb'=> '<a class="prodt-img" href="user.php?act=order_detail&order_id='.$vo['order_id'].'">
				<img src="'.$config['site_url'].$order_goods['thumb'].'"></a>',
                'goods_name' =>'<a href="user.php?act=order_detail&order_id='.$vo['order_id'].'">'. $order_goods['name'].'<a>',
                'flex-box'   => '<span class="count">共<em>'.$goods_num.'</em>件商品</span>
                               <span class="price"><i class="iconfont icon-rmb">㑌</i>'.$vo['total_fee'].'</span>',
				 'id'       => $vo['order_id'],
                'btn_warp'  => $hander,
				'time' =>$Downtime
            );
        }
	
	
    $merge  = get_user_merge($user_id);
	$order_title = "我的物流";
	
	$smarty->assign('title', $order_title);
    $smarty->assign('merge',    $merge);
	$smarty->assign('orders',   $orderList);
    $smarty->assign('pager',    $pager);
    $smarty->display('order.dwt');
}





elseif( $action == 'ajax_set_new_name' )
{ 
	include_once('include/cls_json.php');
	$json = new JSON;
	$result  = array('status' => 0, 'text'=>'', 'url'=>'');
	
	$sql = "UPDATE ". $ecs->table('users'). " SET shop_name2 = '". $_POST['new_name'] . "' WHERE user_id = '".$_SESSION['user_id']."'";
	$db->query($sql);
	
	$result['status'] = 1;
	$result['url']   = "distributor.php?act=profile";
	die($json->encode($result));

}


elseif ($action == 'history_list'){
	$smarty->assign('action' , $action);
	
	$smarty->display('order.dwt');
}



/* 异步显示订单列表 by wang */
elseif ($action == 'async_order_list')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
    
    $start = $_POST['last'];
    $limit = $_POST['amount'];
	
	
    $orders = get_user_orders($user_id, $limit, $start);
    if(is_array($orders)){
		
		 foreach($orders as $vo){
            //获取订单第一个商品的图片
           $order_goods = $db->getRow("SELECT g.goods_thumb as thumb, g.goods_name as name FROM " .$ecs->table('order_goods'). " as og left join " .$ecs->table('goods'). " g on og.goods_id = g.goods_id WHERE og.order_id = ".$vo['order_id']." limit 1");
		   $goods_num = $db->getOne("SELECT SUM(goods_number) FROM ".$ecs->table('order_goods')."WHERE order_id = ".$vo['order_id']);
			
		 if($vo['order_status']==2)
		  {
			 $hander = '已取消';	
		   }
		 else
		  {
			 if($vo['pay_status']!=2)
			 {
			$hander = '<a href="user.php?act=order_detail&order_id='.$vo['order_id'].'" class="btn gotopay">去付款</a>
                        <a href="javascript:;" class="graybtn cancle-order" data-id="'.$vo['order_id'].'">取消订单</a>';	
			 }
			 
			 if(!$vo['shipping_status'])
			 {
				$hander = '已付款,等待发货';	 
				 
			 }
		
			 if($vo['shipping_status']==1)
			 {
				$hander = '
				  <a href="user.php?act=order_detail&order_id='.$vo['order_id'].'" class="bluebtn" >确认收货</a>
                        <a href="user.php?act=order_tracking&order_id='.$vo['order_id'].'" class="graybtn " >查看物流</a>';	 
				 
			 }
		   if($vo['shipping_status']==2){
				   $hander = '已收货';	
				}
		  }
            
            $asyList[] = array(
                'goods-thumb' => '<a class="prodt-img" href="user.php?act=order_detail&order_id='.$vo['order_id'].'">
				<img src="'.$config['site_url'].$order_goods['thumb'].'"></a>',
                'goods_name' =>'<a href="user.php?act=order_detail&order_id='.$vo['order_id'].'">'. $order_goods['name'].'<a>',
                'flex-box' => '<span class="count">共<em>'.$goods_num.'</em>件商品</span>
                               <span class="price"><i class="iconfont icon-rmb">㑌</i>'.$vo['total_fee'].'</span>',
                'btn_warp' => $hander,
				'time'=>'86291276'
            );
        }
	
    }
  else{
	    $asyList[] = "没有更多订单";
	   }
	
    echo json_encode($asyList);
}

/* 包裹跟踪 by wang */
elseif ($action == 'order_tracking')
{
    $order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;
    $ajax     = isset($_GET['ajax']) ? intval($_GET['ajax']) : 0;
	$bk       = isset($_GET['bk'])?intval($_GET['bk']):0;
    
    include_once(ROOT_PATH . 'include/lib_transaction.php');
    include_once(ROOT_PATH .'include/lib_order.php');

   if($bk){
	    $sql = "SELECT order_id,order_sn,invoice_no,shipping_name,shipping_id FROM " .$ecs->table('back_order').
               " WHERE user_id = '$user_id' AND order_id = ".$order_id;
	   }
	else{
    $sql = "SELECT order_id,order_sn,invoice_no,shipping_name,shipping_id FROM " .$ecs->table('order_info').
           " WHERE user_id = '$user_id' AND order_id = ".$order_id;
	}
    
	$orders = $db->getRow($sql);
	
    $sql = "SELECT shipping_name FROM ".$ecs->table('touch_shipping'). " WHERE shipping_id = '$orders[shipping_id]' AND enabled = 1";
	$shipping_name = $db->getOne($sql);
	if($shipping_name){
	//生成快递100查询接口链接
      $shipping   = get_shipping_object($orders['shipping_id']);
      $query_link = $shipping->kuaidi100($orders['invoice_no']);
	   
	  }
    //优先使用curl模式发送数据
    if (function_exists('curl_init') == 1){
      $curl = curl_init();
      curl_setopt ($curl, CURLOPT_URL, $query_link);
      curl_setopt ($curl, CURLOPT_HEADER,0);
      curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt ($curl, CURLOPT_USERAGENT,$_SERVER['HTTP_USER_AGENT']);
      curl_setopt ($curl, CURLOPT_TIMEOUT,5);
      $get_content = curl_exec($curl);
      curl_close ($curl);
    }
    
    $smarty->assign('trackinfo',      $get_content);
    $smarty->display('user_clips.dwt');
}

/* 查看订单详情 */
elseif ($action == 'order_detail')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
    include_once(ROOT_PATH . 'include/lib_payment.php');
    include_once(ROOT_PATH . 'include/lib_order.php');
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;

    /* 订单详情 */
    $order = get_order_detail($order_id, $user_id);

    if ($order === false)
    {
        $err->show($_LANG['back_home_lnk'], './');

        exit;
    }

    /* 是否显示添加到购物车 */
    if ($order['extension_code'] != 'group_buy' && $order['extension_code'] != 'exchange_goods')
    {
        $smarty->assign('allow_to_cart', 1);
    }

    /* 订单商品 */
    $goods_list = order_goods($order_id);
    foreach ($goods_list AS $key => $value)
    {
        $goods_list[$key]['market_price'] = price_format($value['market_price'], false);
        $goods_list[$key]['goods_price']  = price_format($value['goods_price'], false);
        $goods_list[$key]['subtotal']     = price_format($value['subtotal'], false);
    }

     /* 设置能否修改使用余额数 */
    if ($order['order_amount'] > 0)
    {
        if ($order['order_status'] == OS_UNCONFIRMED || $order['order_status'] == OS_CONFIRMED)
        {
            $user = user_info($order['user_id']);
            if ($user['user_money'] + $user['credit_line'] > 0)
            {
                $smarty->assign('allow_edit_surplus', 1);
                $smarty->assign('max_surplus', sprintf($_LANG['max_surplus'], $user['user_money']));
            }
        }
    }

    /* 未发货，未付款时允许更换支付方式 */
    if ($order['order_amount'] > 0 && $order['pay_status'] == PS_UNPAYED && $order['shipping_status'] == SS_UNSHIPPED)
    {
        $payment_list = available_payment_list(false, 0, true);

        /* 过滤掉当前支付方式和余额支付方式 */
        if(is_array($payment_list))
        {
            foreach ($payment_list as $key => $payment)
            {
                if ($payment['pay_id'] == $order['pay_id'] || $payment['pay_code'] == 'balance')
                {
                    unset($payment_list[$key]);
                }
            }
        }
        $smarty->assign('payment_list', $payment_list);
    }

    /* 订单 支付 配送 状态语言项 */
    $order['order_status'] = $_LANG['os'][$order['order_status']];
    $order['pay_status'] = $_LANG['ps'][$order['pay_status']];
    $order['shipping_status'] = $_LANG['ss'][$order['shipping_status']];
	$sql = "SELECT og.rec_id FROM ".$ecs->table('order_info')." oi LEFT JOIN ".$ecs->table('order_goods')." og 
	        on og.order_id = oi.order_id  WHERE og.is_comment = 0 AND oi.order_status = 5 AND oi.shipping_status = 2
		    AND oi.order_id = '$order_id'";
	$coms = $db->getAll($sql);
	$is_need_com = 0;
	foreach($coms as $value){
	  if($value['rec_id']){
	  $is_need_com = 1;
	  }
	}
	
	$time = time() - 86400 * 7;
   	$sql = "SELECT order_sn FROM ".$ecs->table('order_info')." WHERE pay_status = 2 AND shipping_time > '".$time."' AND order_id =".$order_id;
  	$need_serve = $db->getOne($sql);

    /* 订单 支付 配送 状态语言项 */
    $order['order_status'] = $_LANG['os'][$order['order_status']];
    $order['pay_status'] = $_LANG['ps'][$order['pay_status']];
    $order['shipping_status'] = $_LANG['ss'][$order['shipping_status']];

    $smarty->assign('order',         $order);
	$smarty->assign('is_need_com',   $is_need_com);
	$smarty->assign('need_serve',    $need_serve);

    $smarty->assign('order',      $order);
    $smarty->assign('goods_list', $goods_list);
    $smarty->display('order.dwt');
}

/* 取消订单 */
elseif ($action == 'cancel_order')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
    include_once(ROOT_PATH . 'include/lib_order.php');
	include_once('include/cls_json.php');
    $json = new JSON;

    $order_id = isset($_POST['id']) ? intval($_POST['id']) : 0;
	$data = array('status'=>0, 'msg'=>'');


	//判断是否为一元活动商品
	$goods_id_arr = get_topic_goods(4);
	$sql = "SELECT goods_id FROM ecs_order_goods WHERE order_id =".$order_id;
	$good_id = $db->getOne($sql);
    if( in_array($good_id,$goods_id_arr)){
	    //XXX 取消订单返还一次	
		$sql = 'UPDATE ' . $ecs->table('users') . " SET `msn`=msn+1 WHERE `user_id`='" .$user_id. "'";
		$db->query($sql);

		//XXX 取消返款单
		$sql = 'UPDATE ' . $ecs->table('yyhd') . " SET `status`=2 WHERE `oid`='" .$order_id. "'";
		$db->query($sql);
	}


    if (cancel_order($order_id, $user_id))
    {
        $data['status'] = 1;
    }
    else
    {
		$data['status'] = 2;
    }
	 die($json->encode($data));
}

/* 收货地址列表界面*/
elseif ($action == 'address_list')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
    include_once(ROOT_PATH . 'lang/' .$_CFG['lang']. '/shopping_flow.php');
    $smarty->assign('lang',  $_LANG);

    /* 取得国家列表、商店所在国家、商店所在国家的省列表 */
    $smarty->assign('country_list',       get_regions());
    $smarty->assign('shop_province_list', get_regions(1, $_CFG['shop_country']));

    /* 获得用户所有的收货人信息 */
    $consignee_list = get_consignee_list($_SESSION['user_id']);

    if (count($consignee_list) < 5 && $_SESSION['user_id'] > 0)
    {
        /* 如果用户收货人信息的总数小于5 则增加一个新的收货人信息 by wang */
        //$consignee_list[] = array('country' => $_CFG['shop_country'], 'email' => isset($_SESSION['email']) ? $_SESSION['email'] : '');
    }

    $smarty->assign('consignee_list', $consignee_list);

    //取得国家列表，如果有收货人列表，取得省市区列表
    foreach ($consignee_list AS $region_id => $consignee)
    {
        $consignee['country']  = isset($consignee['country'])  ? intval($consignee['country'])  : 0;
        $consignee['province'] = isset($consignee['province']) ? intval($consignee['province']) : 0;
        $consignee['city']     = isset($consignee['city'])     ? intval($consignee['city'])     : 0;

        $province_list[$region_id] = get_regions(1, $consignee['country']);
        $city_list[$region_id]     = get_regions(2, $consignee['province']);
        $district_list[$region_id] = get_regions(3, $consignee['city']);
    }

    /* 获取默认收货ID */
    $address_id  = $db->getOne("SELECT address_id FROM " .$ecs->table('users'). " WHERE user_id='$user_id'");

    //赋值于模板
    $smarty->assign('real_goods_count', 1);
    $smarty->assign('shop_country',     $_CFG['shop_country']);
    $smarty->assign('shop_province',    get_regions(1, $_CFG['shop_country']));
    $smarty->assign('province_list',    $province_list);
    $smarty->assign('address',          $address_id);
    $smarty->assign('city_list',        $city_list);
    $smarty->assign('district_list',    $district_list);
    $smarty->assign('currency_format',  $_CFG['currency_format']);
    $smarty->assign('integral_scale',   $_CFG['integral_scale']);
    $smarty->assign('name_of_region',   array($_CFG['name_of_region_1'], $_CFG['name_of_region_2'], $_CFG['name_of_region_3'], $_CFG['name_of_region_4']));

    $smarty->display('user_transaction.dwt');
}
/* 添加/编辑收货地址的处理 */
elseif ($action == 'act_edit_address')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
    include_once(ROOT_PATH . 'lang/' .$_CFG['lang']. '/shopping_flow.php');
    $smarty->assign('lang', $_LANG);
    
    if($_GET['flag'] == 'display'){
        $id = intval($_GET['id']);
        
        /* 取得国家列表、商店所在国家、商店所在国家的省列表 */
        $smarty->assign('country_list',       get_regions());
        $smarty->assign('shop_province_list', get_regions(1, $_CFG['shop_country']));

        /* 获得用户所有的收货人信息 */
        $consignee_list = get_consignee_list($_SESSION['user_id']);

        foreach ($consignee_list AS $region_id => $vo)
        {
            if($vo['address_id'] == $id){
                $consignee = $vo;
                $smarty->assign('consignee', $vo);                
            }
        }
        $province_list = get_regions(1, 1);
        $city_list     = get_regions(2, $consignee['province']);
        $district_list = get_regions(3, $consignee['city']);

        $smarty->assign('province_list',    $province_list);
        $smarty->assign('city_list',        $city_list);
        $smarty->assign('district_list',    $district_list);
        
        $smarty->display('user_transaction.dwt');
        return false;
    }

    $address = array(
        'user_id'    => $user_id,
        'address_id' => intval($_POST['address_id']),
        'country'    => isset($_POST['country'])   ? intval($_POST['country'])  : 0,
        'province'   => isset($_POST['province'])  ? intval($_POST['province']) : 0,
        'city'       => isset($_POST['city'])      ? intval($_POST['city'])     : 0,
        'district'   => isset($_POST['district'])  ? intval($_POST['district']) : 0,
        'address'    => isset($_POST['address'])   ? compile_str(trim($_POST['address']))    : '',
        'consignee'  => isset($_POST['consignee']) ? compile_str(trim($_POST['consignee']))  : '',
        'email'      => isset($_POST['email'])     ? compile_str(trim($_POST['email']))      : '',
        'tel'        => isset($_POST['tel'])       ? compile_str(make_semiangle(trim($_POST['tel']))) : '',
        'mobile'     => isset($_POST['mobile'])    ? compile_str(make_semiangle(trim($_POST['mobile']))) : '',
        'best_time'  => isset($_POST['best_time']) ? compile_str(trim($_POST['best_time']))  : '',
        'sign_building' => isset($_POST['sign_building']) ? compile_str(trim($_POST['sign_building'])) : '',
        'zipcode'       => isset($_POST['zipcode'])       ? compile_str(make_semiangle(trim($_POST['zipcode']))) : '',
        );

    if (update_address($address))
    {
        show_message($_LANG['edit_address_success'], $_LANG['address_list_lnk'], 'user.php?act=address_list');
    }
}

/* 删除收货地址 */
elseif ($action == 'drop_consignee')
{
    include_once('include/lib_transaction.php');

    $consignee_id = intval($_GET['id']);

    if (drop_consignee($consignee_id))
    {
        ecs_header("Location: user.php?act=address_list\n");
        exit;
    }
    else
    {
        show_message($_LANG['del_address_false']);
    }
}
/*我的收藏*/
elseif ($action == 'collection_list')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

    $record_count = $db->getOne("SELECT COUNT(*) FROM " .$ecs->table('collect_goods').
                                " WHERE user_id='$user_id' ORDER BY add_time DESC");

    $pager = get_pager('user.php', array('act' => $action), $record_count, $page);
	$collections = get_collection_goods($user_id, $pager['size'], $pager['start']);
	
	if(is_array($collections)){
        foreach($collections as $k=>$vo){
            $collections[$k]['thumb'] = $db->getOne("SELECT goods_thumb FROM " .$ecs->table('goods'). " WHERE goods_id = ".$vo['goods_id']);
            $collections[$k]['price'] = (empty($vo['promote_price']))? $vo['shop_price']:$vo['promote_price'];
            
            
        }
    }
	
    $smarty->assign('pager', $pager);
    $smarty->assign('goods_list',$collections );
    $smarty->assign('url',        $ecs->url());
    $lang_list = array(
        'UTF8'   => $_LANG['charset']['utf8'],
        'GB2312' => $_LANG['charset']['zh_cn'],
        'BIG5'   => $_LANG['charset']['zh_tw'],
    );
    $smarty->assign('lang_list',  $lang_list);
    $smarty->assign('user_id',  $user_id);
    $smarty->display('order.dwt');
}

/* 异步获取收藏 by wang */
elseif ($action == 'async_collection_list'){
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $start = $_POST['last'];
    $limit = $_POST['amount'];
    
    $collections = get_collection_goods($user_id, $limit, $start);
    if(is_array($collections)){
        foreach($collections as $vo){
            $img = $db->getOne("SELECT goods_thumb FROM " .$ecs->table('goods'). " WHERE goods_id = ".$vo['goods_id']);
            $t_price = (empty($vo['promote_price']))? $_LANG['shop_price'].$vo['shop_price']:$_LANG['promote_price'].$vo['promote_price'];
            
            $asyList[] = array(
                'collection' => '<a href="'.$vo['url'].'"><table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table_no_border">
            <tr>
                <td><img src="'.$config['site_url'].$img.'" width="50" height="50" /></td>
                <td>'.$vo['goods_name'].'<br>'.$t_price.'</td>
                <td align="right"><a href="'.$vo['url'].'" style="color:#1CA2E1">'.$_LANG['add_to_cart'].'</a><br><a href="javascript:if (confirm(\''.$_LANG['remove_collection_confirm'].'\')) location.href=\'user.php?act=delete_collection&collection_id='.$vo['rec_id'].'\'" style="color:#1CA2E1">'.$_LANG['drop'].'</a></td>
            </tr>
          </table></a>'
            );
        }
    }
    echo json_encode($asyList);
}

/* 删除收藏的商品 */
elseif ($action == 'delete_collection')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');
	
   include_once('include/cls_json.php');
    $json = new JSON;

    $result   = array('status' => 0, 'msg' => '');

    $collection_id = isset($_POST['col_id']) ? intval($_POST['col_id']) : 0;

    if ($collection_id > 0)
    {
        $db->query('DELETE FROM ' .$ecs->table('collect_goods'). " WHERE rec_id='$collection_id' AND user_id ='$user_id'" );
		$result['status'] = 1;	
    
	}
	else{
	  $result['status'] = 2;
	  $result['msg'] ="删除失败";
	}

    die($json->encode($result));
}

/* 添加关注商品 */
elseif ($action == 'add_to_attention')
{
    $rec_id = (int)$_GET['rec_id'];
    if ($rec_id)
    {
        $db->query('UPDATE ' .$ecs->table('collect_goods'). "SET is_attention = 1 WHERE rec_id='$rec_id' AND user_id ='$user_id'" );
    }
    ecs_header("Location: user.php?act=collection_list\n");
    exit;
}
/* 取消关注商品 */
elseif ($action == 'del_attention')
{
    $rec_id = (int)$_GET['rec_id'];
    if ($rec_id)
    {
        $db->query('UPDATE ' .$ecs->table('collect_goods'). "SET is_attention = 0 WHERE rec_id='$rec_id' AND user_id ='$user_id'" );
    }
    ecs_header("Location: user.php?act=collection_list\n");
    exit;
}
/* 显示留言列表 */
elseif ($action == 'message_list')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

    $order_id = empty($_GET['order_id']) ? 0 : intval($_GET['order_id']);
    $order_info = array();

    /* 获取用户留言的数量 */
    if ($order_id)
    {
        $sql = "SELECT COUNT(*) FROM " .$ecs->table('feedback').
                " WHERE parent_id = 0 AND order_id = '$order_id' AND user_id = '$user_id'";
        $order_info = $db->getRow("SELECT * FROM " . $ecs->table('order_info') . " WHERE order_id = '$order_id' AND user_id = '$user_id'");
        $order_info['url'] = 'user.php?act=order_detail&order_id=' . $order_id;
    }
    else
    {
        $sql = "SELECT COUNT(*) FROM " .$ecs->table('feedback').
           " WHERE parent_id = 0 AND user_id = '$user_id' AND user_name = '" . $_SESSION['user_name'] . "' AND order_id=0";
    }

    $record_count = $db->getOne($sql);
    $act = array('act' => $action);

    if ($order_id != '')
    {
        $act['order_id'] = $order_id;
    }

    $pager = get_pager('user.php', $act, $record_count, $page, 5);

    $smarty->assign('message_list', get_message_list($user_id, $_SESSION['user_name'], $pager['size'], $pager['start'], $order_id));
    $smarty->assign('pager',        $pager);
    $smarty->assign('order_info',   $order_info);
    $smarty->display('order.dwt');
}

/* 异步获取留言 */
elseif ($action == 'async_message_list'){
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $order_id = empty($_GET['order_id']) ? 0 : intval($_GET['order_id']);
    $start = $_POST['last'];
    $limit = $_POST['amount'];
    
    $message_list = get_message_list($user_id, $_SESSION['user_name'], $limit, $start, $order_id);
	$sql      = "SELECT w.headimgurl FROM ". $ecs->table('users')." u LEFT JOIN wxch_user w on u.wxid=w.wxid WHERE user_id=".$user_id;
	$head_url = $db->getOne($sql);
	$head_url = !empty($head_url)? $head_url : "data/avatar-1.png";
    if(is_array($message_list)){
        foreach($message_list as $key=>$vo){
		   $re_message = $vo['re_msg_content'] ? '<li class="odd">
             <a class="user" href="#"><img height="44px;" width="44px;"
			 class="img-responsive avatar_" src="data/kefu.jpg" alt=""><span class="user-name">客服</span></a>
                <div class="reply-content-box">
                <span class="reply-time">'.$vo['re_msg_time'].'</span>
                <div class="reply-content pr">
                <span class="arrow">&nbsp;</span>
                 '.$vo['re_msg_content'].'
                </div>
            </li>':'';
            $asyList[] = array(
			    'content-reply-box' => '<li class="even">
           <a class="user" href="#"><img  height="44px;" width="44px;"
		   class="img-responsive avatar_" src="'.$head_url.'" alt=""><span class="user-name">我</span></a>
                <div class="reply-content-box">
                	<span class="reply-time">'.$vo['msg_time'].'</span>
                    <div class="reply-content pr">
                    	<span class="arrow">&nbsp;</span>
				     '.$vo['msg_title'].':<br>
                    '.$vo['msg_content'].'
                    </div>
                </div>
            </li>'. $re_message
			  
            );
        }
    }

    echo json_encode($asyList);
}


/* 显示评论列表 */
elseif ($action == 'comment_list')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

    /* 获取用户留言的数量 */
    $sql = "SELECT COUNT(*) FROM " .$ecs->table('comment').
           " WHERE parent_id = 0 AND user_id = '$user_id'";
    $record_count = $db->getOne($sql);
    $pager = get_pager('user.php', array('act' => $action), $record_count, $page, 5);

    $smarty->assign('comment_list', get_comment_list($user_id, $pager['size'], $pager['start']));
    $smarty->assign('pager',        $pager);
    $smarty->display('user_clips.dwt');
}


/* 异步获取评论 */
elseif ($action == 'async_comment_list'){
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $start = $_POST['last'];
    $limit = $_POST['amount'];
    
    $comment_list = get_comment_list($user_id, $limit, $start);
    if(is_array($comment_list)){
        foreach($comment_list as $key=>$vo){
            $re_message = $vo['reply_content'] ? '<tr><td>'.$_LANG['reply_comment'].'<br>'.$vo['reply_content'].'</td></tr>':'';
            $asyList[] = array(
                'comment' => '<table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table_no_border">
            <tr>
                <td><span style="float:right"><a href="user.php?act=del_cmt&id='.$vo['comment_id'].'" onclick="if (!confirm(\''.$_LANG['confirm_remove_msg'].'\')) return false;" style="color:#1CA2E1">删除</a></span>评论：'.$vo['cmt_name'].' - '.$vo['formated_add_time'].' </td>
            </tr>
            <tr>
                <td>'.$vo['content'].'</td>
            </tr>'.$re_message.'
          </table>'
            );
        }
    }
    echo json_encode($asyList);
}

/* 添加我的留言 */
elseif ($action == 'act_add_message')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');
	
	include_once('include/cls_json.php');
    $json = new JSON;
    $result = array('ststus'=>0, 'url'=>'user.php?act=message_list');
   
    $message = array(
        'user_id'     => $user_id,
        'user_name'   => $_SESSION['user_name'],
        'user_email'  => $_SESSION['email'],
        'msg_type'    => isset($_POST['msg_type']) ? intval($_POST['msg_type'])     : 0,
        'msg_title'   => isset($_POST['msg_title']) ? trim($_POST['msg_title'])     : '',
        'msg_content' => isset($_POST['msg_content']) ? trim($_POST['msg_content']) : '',
        'order_id'=>empty($_POST['order_id']) ? 0 : intval($_POST['order_id']),
        'upload'      => (isset($_FILES['message_img']['error']) && $_FILES['message_img']['error'] == 0) || (!isset($_FILES['message_img']['error']) && isset($_FILES['message_img']['tmp_name']) && $_FILES['message_img']['tmp_name'] != 'none')
         ? $_FILES['message_img'] : array()
     );
     
    if (add_message($message))
    {
        $result['status'] = 1;
    }
    else
    {
         $result['status'] = 2;
    }
	
	die($json->encode($result));
}


/* 标签云列表 */
elseif ($action == 'tag_list')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $good_id = isset($_GET['id']) ? intval($_GET['id']) : 0;

    $smarty->assign('tags',      get_user_tags($user_id));
    $smarty->assign('tags_from', 'user');
    $smarty->display('user_clips.dwt');
}

/* 删除标签云的处理 */
elseif ($action == 'act_del_tag')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $tag_words = isset($_GET['tag_words']) ? trim($_GET['tag_words']) : '';
    delete_tag($tag_words, $user_id);

    ecs_header("Location: user.php?act=tag_list\n");
    exit;

}

/* 显示缺货登记列表 */
elseif ($action == 'booking_list')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

    /* 获取缺货登记的数量 */
    $sql = "SELECT COUNT(*) " .
            "FROM " .$ecs->table('booking_goods'). " AS bg, " .
                     $ecs->table('goods') . " AS g " .
            "WHERE bg.goods_id = g.goods_id AND user_id = '$user_id'";
    $record_count = $db->getOne($sql);
    $pager = get_pager('user.php', array('act' => $action), $record_count, $page);

    $smarty->assign('booking_list', get_booking_list($user_id, $pager['size'], $pager['start']));
    $smarty->assign('pager',        $pager);
    $smarty->display('user_clips.dwt');
}
/* 添加缺货登记页面 */
elseif ($action == 'add_booking')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $goods_id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    if ($goods_id == 0)
    {
        show_message($_LANG['no_goods_id'], $_LANG['back_page_up'], '', 'error');
    }

    /* 根据规格属性获取货品规格信息 */
    $goods_attr = '';
    if ($_GET['spec'] != '')
    {
        $goods_attr_id = $_GET['spec'];

        $attr_list = array();
        $sql = "SELECT a.attr_name, g.attr_value " .
                "FROM " . $ecs->table('goods_attr') . " AS g, " .
                    $ecs->table('attribute') . " AS a " .
                "WHERE g.attr_id = a.attr_id " .
                "AND g.goods_attr_id " . db_create_in($goods_attr_id);
        $res = $db->query($sql);
        while ($row = $db->fetchRow($res))
        {
            $attr_list[] = $row['attr_name'] . ': ' . $row['attr_value'];
        }
        $goods_attr = join(chr(13) . chr(10), $attr_list);
    }
    $smarty->assign('goods_attr', $goods_attr);

    $smarty->assign('info', get_goodsinfo($goods_id));
    $smarty->display('user_clips.dwt');

}

/* 添加缺货登记的处理 */
elseif ($action == 'act_add_booking')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $booking = array(
        'goods_id'     => isset($_POST['id'])      ? intval($_POST['id'])     : 0,
        'goods_amount' => isset($_POST['number'])  ? intval($_POST['number']) : 0,
        'desc'         => isset($_POST['desc'])    ? trim($_POST['desc'])     : '',
        'linkman'      => isset($_POST['linkman']) ? trim($_POST['linkman'])  : '',
        'email'        => isset($_POST['email'])   ? trim($_POST['email'])    : '',
        'tel'          => isset($_POST['tel'])     ? trim($_POST['tel'])      : '',
        'booking_id'   => isset($_POST['rec_id'])  ? intval($_POST['rec_id']) : 0
    );

    // 查看此商品是否已经登记过
    $rec_id = get_booking_rec($user_id, $booking['goods_id']);
    if ($rec_id > 0)
    {
        show_message($_LANG['booking_rec_exist'], $_LANG['back_page_up'], '', 'error');
    }

    if (add_booking($booking))
    {
        show_message($_LANG['booking_success'], $_LANG['back_booking_list'], 'user.php?act=booking_list',
        'info');
    }
    else
    {
        $err->show($_LANG['booking_list_lnk'], 'user.php?act=booking_list');
    }
}

/* 删除缺货登记 */
elseif ($action == 'act_del_booking')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    if ($id == 0 || $user_id == 0)
    {
        ecs_header("Location: user.php?act=booking_list\n");
        exit;
    }

    $result = delete_booking($id, $user_id);
    if ($result)
    {
        ecs_header("Location: user.php?act=booking_list\n");
        exit;
    }
}

/* 确认收货 */
elseif ($action == 'affirm_received')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
	include_once('include/cls_json.php');
    $json = new JSON;

    $order_id = isset($_POST['id']) ? intval($_POST['id']) : 0;

    if (affirm_received($order_id, $user_id))
    {
		 $data['status'] = 1;   
    }
    else
    {
        $data['status'] = 0;
    }
	
	die($json->encode($data));
}

/* 确认收货 */
elseif ($action == 'rtn_received')
{
	include_once('include/cls_json.php');
    $json = new JSON;

    $back_id = isset($_POST['id']) ? intval($_POST['id']) : 0;
    $sql = "UPDATE ".$ecs->table('back_order')." SET goods_status = 3, shipping_time = ".gmtime()." WHERE back_id ='$back_id'";
	$db->query($sql);
    $data['status'] = 1;  
	$data['url']    = "user.php?act=my_return_detail&id=".$back_id; 
	die($json->encode($data));
}


elseif($action=='refund'){
   $id  = isset($_GET['id'])?intval($_GET['id']):0;
   
   if($id>0){
   $sql = "SELECT back_id FROM ".$ecs->table('back_order')." WHERE order_id = '$id'";
   $is_sub = $db->getOne($sql);
   if($is_sub){
	   ecs_header("Location: user.php?act=my_return_detail&id=".$is_sub."\n");
	   }
   else{
   $sql = "SELECT og.rec_id,og.goods_name,og.goods_number, og.goods_id,og.goods_price, g.goods_thumb FROM ".$ecs->table('order_goods')." 
           og LEFT JOIN ".$ecs->table('goods')."  g on g.goods_id = og.goods_id WHERE order_id = '$id'";
   $goods_list = $db->getAll($sql);
   }
   }
   else{
	   ecs_header("Location: user.php\n");
	   }
    $smarty->assign('orderId',       $id);
	$smarty->assign('goods_list',    $goods_list);
	$smarty->display('order.dwt');
	
	}

elseif($action=='ajax_update_return'){

   $rec_id = isset($_POST['lineItemId'])? intval($_POST['lineItemId']):0;
   $re_num = isset($_POST['lineCount'])? intval($_POST['lineCount']):0;	
  
   $sql = "UPDATE ".$ecs->table('order_goods')." SET refund_num = '$re_num' WHERE rec_id='$rec_id'";
   $db->query($sql);

	}
//提交退换货
if('act_refund' == $action)
{   include_once(ROOT_PATH . 'include/lib_return.php');
	
	$refund['order_id']      = isset($_POST['orderId'])?intval($_POST['orderId']):0;
	$refund['rtnType']       = isset($_POST['rtnType'])?intval($_POST['rtnType']):0;
	$refund['rtnDesc']       = isset($_POST['rtnDesc'])?trim($_POST['rtnDesc']):"";
	$refund['shipping_name'] = isset($_POST['shipping_name'])?trim($_POST['shipping_name']):"";
	$refund['invoice_no']    = isset($_POST['invoice_no'])?trim($_POST['invoice_no']):"";
	$refund['re_reason']     = isset($_POST['re_reason'])?trim($_POST['re_reason']):"";
	$refund['re_reason']     = isset($_POST['re_reason'])?trim($_POST['re_reason']):"";
	$refund['gStatus']       = isset($_POST['goodsStatus'])?intval($_POST['goodsStatus']):0;
	
	$refund['refund_pic1'] = (isset($_FILES['refund_pic1']['error']) && $_FILES['refund_pic1']['error'] == 0) || (!isset($_FILES['refund_pic1']['error']) && isset($_FILES['refund_pic1']['tmp_name']) && $_FILES['refund_pic1']['tmp_name'] != 'none')
         ? $_FILES['refund_pic1'] : array();
	$refund['refund_pic2'] = (isset($_FILES['refund_pic2']['error']) && $_FILES['refund_pic2']['error'] == 0) || (!isset($_FILES['refund_pic2']['error']) && isset($_FILES['refund_pic2']['tmp_name']) && $_FILES['refund_pic2']['tmp_name'] != 'none')
         ? $_FILES['refund_pic2'] : array();
	$refund['refund_pic3'] = (isset($_FILES['refund_pic3']['error']) && $_FILES['refund_pic3']['error'] == 0) || (!isset($_FILES['refund_pic3']['error']) && isset($_FILES['refund_pic3']['tmp_name']) && $_FILES['refund_pic3']['tmp_name'] != 'none')
         ? $_FILES['refund_pic3'] : array();
	
	$refund_add = refund_add($refund);
	if($refund_add)
	{
		header("Location:user.php?act=account_add&from=refund&refund_id=".$refund_add);
		//show_message("成功申请退款", "订单列表", "user.php?act=order_list");
	}
	else
	{
		$GLOBALS['err']->show("订单列表", 'user.php?act=order_list');
	}

}

//退换货记录
if('my_return_list' == $action)
{   include_once(ROOT_PATH . 'include/lib_return.php');
    
	$page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
	$size = 5;
    $record_count = $db->getOne("SELECT COUNT(*) FROM " .$ecs->table('back_order'). " WHERE user_id = '$user_id'");
    $pager  = get_pager('user.php', array('act' => $action), $record_count, $page, $size);
    $back_order = get_rtn_orders($user_id, $pager['size'], $pager['start']);
    
    $smarty->assign('back_order',    $back_order);
    $smarty->assign('pager',         $pager);
    $smarty->display('order.dwt');
}
//退换货详情
if('my_return_detail' == $action)
{   include_once(ROOT_PATH . 'include/lib_return.php');
    $id  = isset($_GET['id'])?intval($_GET['id']):0;
	$arr  = get_bko_detail($id);
   
    $smarty->assign('bk_info',     $arr['bk_info']);
	$smarty->assign('bk_goods',    $arr['bk_goods']);
    $smarty->assign('bk_reason',   $arr['bk_reason']);
    $smarty->display('order.dwt');
}

/* 会员退款申请界面 */
elseif ($action == 'account_raply')
{   include_once(ROOT_PATH . 'include/lib_clips.php');
    $sql ="SELECT * FROM ".$ecs->table('users_card')." WHERE user_id= '$user_id' ORDER BY edit_time DESC LIMIT 1";
	$acc = $db->getRow($sql);
	//获取剩余余额
    $surplus_amount = get_user_surplus($user_id);
    if (empty($surplus_amount))
    {
        $surplus_amount = 0;
    } 
	if(empty($acc['card_id'])){
	ecs_header("Location: user.php?act=account_add\n");
        exit;
		
		}
	$last_log = get_account_log($user_id, 1, 0);
	$smarty->assign('surplus_amount', $surplus_amount);
	$smarty->assign('acc',            $acc);
	$smarty->assign('last_log',       $last_log['0']);
    $smarty->display('wd_cash.dwt');
}

/* 会员预付款界面 */
elseif ($action == 'account_deposit')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $surplus_id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    $account    = get_surplus_info($surplus_id);

    $smarty->assign('payment', get_online_payment_list(false));
    $smarty->assign('order',   $account);
    $smarty->display('user_transaction.dwt');
}



/* 会员充值和提现申请记录 */
elseif ($action == 'account_log')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

    /* 获取记录条数 */
    $sql = "SELECT COUNT(*) FROM " .$ecs->table('user_account').
           "  WHERE user_id = '$user_id'" .
           " AND is_paid = 1 AND process_type " . db_create_in(array(SURPLUS_SAVE, SURPLUS_RETURN));
    $record_count = $db->getOne($sql);
	
		   
	

    //分页函数
    $pager = get_pager('user.php', array('act' => $action), $record_count, $page);

    //获取剩余余额
    $surplus_amount = get_user_surplus($user_id);
    if (empty($surplus_amount))
    {
        $surplus_amount = 0;
    }

    //获取余额记录
    $account_log = get_account_log($user_id, $pager['size'], $pager['start']);
	$settle_log = get_settle_log($user_id, 10, 0);

    //模板赋值
    $smarty->assign('surplus_amount', price_format($surplus_amount, false));
    $smarty->assign('account_log',    $account_log);
	$smarty->assign('settle_log',     $settle_log);
    $smarty->assign('pager',          $pager);
    $smarty->display('wd_cash.dwt');
}

/* 对会员余额申请的处理 */
elseif ($action == 'act_account')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');
    include_once(ROOT_PATH . 'include/lib_order.php');
	include_once('include/cls_json.php');
	$result=array('status'=>0,'msg'=>'', 'url'=>'');
    $json = new JSON;
	
    $amount = isset($_POST['amount']) ? floatval($_POST['amount']) : 0;
    if ($amount <= 0)
    {
		$result['status']=2;
		$result['msg'] = $_LANG['amount_gt_zero'];
        die($json->encode($result));
    }

    /* 变量初始化 */
    $surplus = array(
            'user_id'      => $user_id,
            'rec_id'       => !empty($_POST['rec_id'])      ? intval($_POST['rec_id'])       : 0,
			'card_id'      => !empty($_POST['card_id'])     ? intval($_POST['card_id'])      : 0,
            'process_type' => isset($_POST['surplus_type']) ? intval($_POST['surplus_type']) : 0,
            'payment_id'   => isset($_POST['payment_id'])   ? intval($_POST['payment_id'])   : 0,
            'user_note'    => isset($_POST['user_note'])    ? trim($_POST['user_note'])      : '',
            'amount'       => $amount
    );

    /* 退款申请的处理 */
    if ($surplus['process_type'] == 1)
    {
		/* 判断是否满足月最低消费 */
		 $info = get_rank_info();
		 $time = time() - $info['crl_time']*24*3600;
		 $sql  = " SELECT SUM(goods_amount) as csm_money FROM ". $ecs->table('order_info') ." WHERE pay_status = 2
		           AND pay_time >='$time' AND user_id = '$user_id' GROUP BY user_id";
		 $min_cms = $db->getOne($sql);
       /* if ($min_cms < $info['crl_money'])
        {
            $result['status'] = 5;
		    $result['msg']    = "您最近".$info['crl_time']."天的消费，少于最低消费".$info['crl_money']."元，您不能够提现";
            die($json->encode($result));
			
        }
		*/
		
        /* 判断是否有足够的余额的进行退款的操作 */
        $sur_amount = get_user_surplus($user_id);
		$sql = "SELECT SUM(amount) FROM ". $ecs->table('user_account')." WHERE user_id ='$user_id' AND is_paid = 0 group by user_id";
		$u_amount = $db->getOne($sql);
		$unc_amount =abs($u_amount )+ $amount;
		if($unc_amount > $sur_amount){
		   $result['status'] = 3;
		    $result['msg']    = "您的待结算的体现金额大于了您的账户余额!";
            die($json->encode($result));
		}
        if ($amount > $sur_amount)
        {
            $result['status'] = 3;
		    $result['msg']    = $_LANG['surplus_amount_error'];
            die($json->encode($result));
			
        }

        //插入会员账目明细
        $amount = '-'.$amount;
        $surplus['payment'] = '';
        $surplus['rec_id']  = insert_user_account($surplus, $amount);

        /* 如果成功提交 */
        if ($surplus['rec_id'] > 0)
        {
			$result['status'] = 1;
			$result['url']    = "user.php?act=surplus_suc";
            die($json->encode($result));
           
        }
        else
        {
			$result['status'] = 4;
		    $result['msg']    = $_LANG['process_false'];
            die($json->encode($result));
        }
    }
    /* 如果是会员预付款，跳转到下一步，进行线上支付的操作 */
    else
    {
        if ($surplus['payment_id'] <= 0)
        {
            show_message($_LANG['select_payment_pls']);
        }

        include_once(ROOT_PATH .'include/lib_payment.php');

        //获取支付方式名称
        $payment_info = array();
        $payment_info = payment_info($surplus['payment_id']);
        $surplus['payment'] = $payment_info['pay_name'];

        if ($surplus['rec_id'] > 0)
        {
            //更新会员账目明细
            $surplus['rec_id'] = update_user_account($surplus);
        }
        else
        {
            //插入会员账目明细
            $surplus['rec_id'] = insert_user_account($surplus, $amount);
        }

        //取得支付信息，生成支付代码
        $payment = unserialize_config($payment_info['pay_config']);

        //生成伪订单号, 不足的时候补0
        $order = array();
        $order['order_sn']       = $surplus['rec_id'];
        $order['user_name']      = $_SESSION['user_name'];
        $order['surplus_amount'] = $amount;

        //计算支付手续费用
        $payment_info['pay_fee'] = pay_fee($surplus['payment_id'], $order['surplus_amount'], 0);

        //计算此次预付款需要支付的总金额
        $order['order_amount']   = $amount + $payment_info['pay_fee'];

        //记录支付log
        $order['log_id'] = insert_pay_log($surplus['rec_id'], $order['order_amount'], $type=PAY_SURPLUS, 0);

        /* 调用相应的支付方式文件 */
        include_once(ROOT_PATH . 'include/modules/payment/' . $payment_info['pay_code'] . '.php');

        /* 取得在线支付方式的支付按钮 */
        $pay_obj = new $payment_info['pay_code'];
        $payment_info['pay_button'] = $pay_obj->get_code($order, $payment);

        /* 模板赋值 */
        $smarty->assign('payment', $payment_info);
        $smarty->assign('pay_fee', price_format($payment_info['pay_fee'], false));
        $smarty->assign('amount',  price_format($amount, false));
        $smarty->assign('order',   $order);
        $smarty->display('user_transaction.dwt');
    }
}
elseif ($action == 'surplus_suc'){
	
	$smarty->display('wd_cash.dwt');
	}
	
elseif ($action == 'account_list'){
	
	$sql  = "SELECT * FROM ".$ecs->table('users_card')." WHERE user_id = '$user_id'";
	$acc_list = $db->getAll($sql);
	
	$smarty->assign('acc_list', $acc_list);
	$smarty->display('wd_cash.dwt');
}

elseif ($action == 'setDefault'){
  include_once('include/cls_json.php');
  $data=array('status'=>0,'msg'=>'' ,'url'=>'');
  $json = new JSON;
  $id = isset($_POST['id'])?intval($_POST['id']):0;
  $data['url'] ="user.php?act=account_list";
  $db->query("UPDATE " .$ecs->table('users_card')." SET `default` = 0 WHERE  user_id = '$user_id'");
  $data['status'] = $db->query("UPDATE " .$ecs->table('users_card')." SET `default` = 1 WHERE  card_id = '$id'");
  
  die($json->encode($data));
}

elseif ($action == 'account_add'){
	
	$sql  = "SELECT * FROM ".$ecs->table('users_card')." WHERE user_id = '$user_id'";
	$account_list = $db->getAll($sql);
	foreach( $account_list as $key=>$account){
		 if($account['card_type'] == "bank_card"){
			 $card_info = $account;
			 }
		  else{
			  $zfb_info = $account;
			  }
		 }
	$from = $_GET['from'];
	$smarty->assign('refund_id',		$_GET['refund_id']);
	$smarty->assign('from',		$from);
	$smarty->assign('card_info',    $card_info);
	$smarty->assign('zfb_info',     $zfb_info);
	$smarty->display('wd_cash.dwt');
}

/* 提交有效银行卡*/
elseif($action == 'submit_card')
{ 
  include_once(ROOT_PATH . 'include/lib_clips.php');
  include_once('include/cls_json.php');
  $result=array('status'=>0,'msg'=>'', 'url'=>'');
  $json = new JSON;

 $card_info = array(
     'card_id'     => isset($_POST['id']) ? intval($_POST['id']) : 0,
     'user_id'     => $user_id , 
     'owner_name'  => isset($_POST['name']) ? trim($_POST['name']) : '',
	 'mobile'      => isset($_POST['mobile']) ? trim($_POST['mobile']) : '',
	 'card_number' => isset($_POST['number']) ? trim($_POST['number']) : '',
	 'card_type'   => 'bank_card',
	 'open_bank'   => isset($_POST['open_bank']) ? trim($_POST['open_bank']) : '',
	 'bank_type'   => isset($_POST['bank_type']) ? trim($_POST['bank_type']) : '',
	 'upload'      => (isset($_FILES['upload']['error']) && $_FILES['upload']['error'] == 0) || (!isset($_FILES['upload']['error']) && isset($_FILES['upload']['tmp_name']) && $_FILES['upload']['tmp_name'] != 'none')
         ? $_FILES['upload'] : array(),
	 'add_time'   => time(),
	 'edit_time'  => time()
   );
   
  $refund_id = $_POST['refund_id'];
  
  if(!$card_info['card_id']){   
  	$card_add = add_card($card_info);
    if ($card_add)
    {
          $result['status'] = 1;
		  if($refund_id > 0){
		  	  $GLOBALS['db']->getRow("UPDATE ecs_back_order SET card_id = '".$card_add."' WHERE back_id = '".$refund_id."'");
		  	  $result['url']    = "user.php?act=my_return_list";
		  }else{
			  $result['url']    = "user.php?act=account_raply";
		  }
          die($json->encode($result));
    }
    else
    {
          $result['status'] = 2;
		  $result['msg']    = "卡号提交失败";
          die($json->encode($result));
    }
  }
  else{
	 if (update_card($card_info))
     {
          $result['status'] = 1;
		  if($refund_id > 0){
		  	  $GLOBALS['db']->getRow("UPDATE ecs_back_order SET card_id = '".$card_info['card_id']."' WHERE back_id = '".$refund_id."'");
		  	  $result['url']    = "user.php?act=my_return_list";
		  }else{
		  		$result['url']    = "user.php?act=account_raply";
		  }
          die($json->encode($result));
     }
    else
      {
          $result['status'] = 2;
		  $result['msg']    = "卡号提交失败";
          die($json->encode($result));
      }
	  
	  }
  
}

/* 提交有效银行卡*/
elseif($action == 'submit_zfb')
{ 
  include_once(ROOT_PATH . 'include/lib_clips.php');
  include_once('include/cls_json.php');
  $result=array('status'=>0,'msg'=>'', 'url'=>'');
  $json = new JSON;
   
 $card_info = array(
     'card_id'     => isset($_POST['id']) ? intval($_POST['id']) : 0,
     'user_id'     => $user_id , 
     'owner_name'  => isset($_POST['name']) ? trim($_POST['name']) : '',
	 'mobile'      => isset($_POST['mobile']) ? trim($_POST['mobile']) : '',
	 'card_number' => isset($_POST['number']) ? trim($_POST['number']) : '',
	 'card_type'  => 'zfb',
	 'add_time'   => time(),
	 'edit_time'  => time()
   );
 
  if(!$card_info['card_id']){  
    if (add_card($card_info))
    {   
          $result['status'] = 1;
		  $result['url']    = "user.php?act=account_raply";
          die($json->encode($result));
    }
    else
    {
          $result['status'] = 2;
		  $result['msg']    = "支付宝提交失败";
          die($json->encode($result));
    }
  }
  else{
	 if (update_card($card_info))
     {
          $result['status'] = 1;
		  $result['url']    = "user.php?act=account_raply";
          die($json->encode($result));
     }
    else
      {
          $result['status'] = 2;
		  $result['msg']    = "支付宝提交失败";
          die($json->encode($result));
      }
	  
	  }
  
}
/* 删除会员余额 */
elseif ($action == 'cancel')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    if ($id == 0 || $user_id == 0)
    {
        ecs_header("Location: user.php?act=account_log\n");
        exit;
    }

    $result = del_user_account($id, $user_id);
    if ($result)
    {
        ecs_header("Location: user.php?act=account_log\n");
        exit;
    }
}

/* 会员通过帐目明细列表进行再付款的操作 */
elseif ($action == 'pay')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');
    include_once(ROOT_PATH . 'include/lib_payment.php');
    include_once(ROOT_PATH . 'include/lib_order.php');

    //变量初始化
    $surplus_id = isset($_GET['id'])  ? intval($_GET['id'])  : 0;
    $payment_id = isset($_GET['pid']) ? intval($_GET['pid']) : 0;

    if ($surplus_id == 0)
    {
        ecs_header("Location: user.php?act=account_log\n");
        exit;
    }

    //如果原来的支付方式已禁用或者已删除, 重新选择支付方式
    if ($payment_id == 0)
    {
        ecs_header("Location: user.php?act=account_deposit&id=".$surplus_id."\n");
        exit;
    }

    //获取单条会员帐目信息
    $order = array();
    $order = get_surplus_info($surplus_id);

    //支付方式的信息
    $payment_info = array();
    $payment_info = payment_info($payment_id);

    /* 如果当前支付方式没有被禁用，进行支付的操作 */
    if (!empty($payment_info))
    {
        //取得支付信息，生成支付代码
        $payment = unserialize_config($payment_info['pay_config']);

        //生成伪订单号
        $order['order_sn'] = $surplus_id;

        //获取需要支付的log_id
        $order['log_id'] = get_paylog_id($surplus_id, $pay_type = PAY_SURPLUS);

        $order['user_name']      = $_SESSION['user_name'];
        $order['surplus_amount'] = $order['amount'];

        //计算支付手续费用
        $payment_info['pay_fee'] = pay_fee($payment_id, $order['surplus_amount'], 0);

        //计算此次预付款需要支付的总金额
        $order['order_amount']   = $order['surplus_amount'] + $payment_info['pay_fee'];

        //如果支付费用改变了，也要相应的更改pay_log表的order_amount
        $order_amount = $db->getOne("SELECT order_amount FROM " .$ecs->table('pay_log')." WHERE log_id = '$order[log_id]'");
        if ($order_amount <> $order['order_amount'])
        {
            $db->query("UPDATE " .$ecs->table('pay_log').
                       " SET order_amount = '$order[order_amount]' WHERE log_id = '$order[log_id]'");
        }

        /* 调用相应的支付方式文件 */
        include_once(ROOT_PATH . 'include/modules/payment/' . $payment_info['pay_code'] . '.php');

        /* 取得在线支付方式的支付按钮 */
        $pay_obj = new $payment_info['pay_code'];
        $payment_info['pay_button'] = $pay_obj->get_code($order, $payment);

        /* 模板赋值 */
        $smarty->assign('payment', $payment_info);
        $smarty->assign('order',   $order);
        $smarty->assign('pay_fee', price_format($payment_info['pay_fee'], false));
        $smarty->assign('amount',  price_format($order['surplus_amount'], false));
        $smarty->assign('action',  'act_account');
        $smarty->display('user_transaction.dwt');
    }
    /* 重新选择支付方式 */
    else
    {
        include_once(ROOT_PATH . 'include/lib_clips.php');

        $smarty->assign('payment', get_online_payment_list());
        $smarty->assign('order',   $order);
        $smarty->assign('action',  'account_deposit');
        $smarty->display('user_transaction.dwt');
    }
}

/* 添加标签(ajax) */
elseif ($action == 'add_tag')
{
    include_once('include/cls_json.php');
    include_once('include/lib_clips.php');

    $result = array('error' => 0, 'message' => '', 'content' => '');
    $id     = isset($_POST['id']) ? intval($_POST['id']) : 0;
    $tag    = isset($_POST['tag']) ? json_str_iconv(trim($_POST['tag'])) : '';

    if ($user_id == 0)
    {
        /* 用户没有登录 */
        $result['error']   = 1;
        $result['message'] = $_LANG['tag_anonymous'];
    }
    else
    {
        add_tag($id, $tag); // 添加tag
        clear_cache_files('goods'); // 删除缓存

        /* 重新获得该商品的所有缓存 */
        $arr = get_tags($id);

        foreach ($arr AS $row)
        {
            $result['content'][] = array('word' => htmlspecialchars($row['tag_words']), 'count' => $row['tag_count']);
        }
    }

    $json = new JSON;

    echo $json->encode($result);
    exit;
}

/* 添加收藏商品(ajax) */
elseif ($action == 'collect')
{
    include_once(ROOT_PATH .'include/cls_json.php');
    $json = new JSON();
    $result = array('status' => 0, 'message' => '', 'isInFav'=>0);
    $goods_id = isset($_POST['id'])? intval($_POST['id']):0;
	
    if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0)
    {
        $result['status'] = 0;
        $result['message'] = $_LANG['login_please'];
        die($json->encode($result));
    }
    else
    {
        /* 检查是否已经存在于用户的收藏夹 */
        $sql = "SELECT COUNT(*) FROM " .$GLOBALS['ecs']->table('collect_goods') .
            " WHERE user_id='$_SESSION[user_id]' AND goods_id = '$goods_id'";
        if ($GLOBALS['db']->GetOne($sql) > 0)
        {
            $result['status']  = 1;
			$result['isInFav'] = 1;
            $result['message'] = $GLOBALS['_LANG']['collect_existed'];
            die($json->encode($result));
        }
        else
        {
            $time = gmtime();
            $sql = "INSERT INTO " .$GLOBALS['ecs']->table('collect_goods'). " (user_id, goods_id, add_time)" .
                    "VALUES ('$_SESSION[user_id]', '$goods_id', '$time')";

            if ($GLOBALS['db']->query($sql) === false)
            {
                $result['status'] = 0;
                $result['message'] = $GLOBALS['db']->errorMsg();
                die($json->encode($result));
            }
            else
            {
                $result['error'] = 1;
				$result['isInFav'] = 1;
                $result['message'] = $GLOBALS['_LANG']['collect_success'];
                die($json->encode($result));
            }
        }
    }
}
//我的二微码分享
elseif($action =='qr_share'){
    $user_id  = $_SESSION['user_id'];
	$user_name= $_SESSION['user_name'];
    $wxid    =  $GLOBALS['db']->getOne("SELECT wxid  FROM ". $GLOBALS['ecs']->table('users')." WHERE user_id = '$user_id'");
	if(!empty($wxid)){
	$sql      = "SELECT nickname FROM wxch_user WHERE wxid='$wxid'";
	$nickname = $db->getOne($sql);
    }
	$smarty->assign('user_name',   $user_name);
	$smarty->assign('user_id',     $user_id);
	$smarty->assign('nickname',    $nickname);
    $smarty->display('user_clips.dwt');
}

//商品分享图片生成
elseif($action == 'goods_share'){
	
	require(dirname(__FILE__) . '/include/phpqrcode.php');
	
	include_once('include/cls_json.php');
    $json = new JSON;
    $result   = array('img_path' => '', 'status' => 0);
	
	$goods_id = $_POST['goods_id'];
	$user_id = $_SESSION['user_id'];
	
	$img_path = 'goodspic/'.$goods_id.'_'.$user_id.'.jpg';
	if(!file_exists($img_path)){
		//file_put_contents("a.txt",$img_path);
	
	$sql_goodsinfo = "SELECT shop_price , market_price , goods_img FROM ". $GLOBALS['ecs']->table('goods')." WHERE goods_id = '". $goods_id ."'";
	$goods_info = $GLOBALS['db']->getRow( $sql_goodsinfo );
	
	$shop_price = $goods_info['shop_price'];
	$market_price = $goods_info['market_price'];
	$goods_img = $goods_info['goods_img'];
	
	//生成二维码图片
	$data = 'http://'.$_SERVER['HTTP_HOST'].'/mobile/goods.php?id='.$goods_id.'&u='.$user_id; 
	//file_put_contents('aaa.txt',$data);
   	// 生成的文件名 
  	$erweima = 'goodspic/'.$goods_id.'_'.$user_id.'_erweima'.'.jpg'; 
  	// 纠错级别：L、M、Q、H 
 	$errorCorrectionLevel = 'L';  
 	// 点的大小：1到10 
 	$matrixPointSize = 8;  
   	QRcode::png($data, $erweima, $errorCorrectionLevel, $matrixPointSize, 1); 
	
	$beijing = 'goodspic/goods.jpg';//准备好的背景图片 
	$QR = $erweima;//已经生成的原始二维码图
	
	$hd = 'goodspic/'.$user_id."_head.jpg";//保存的头像名称，用用户的 ID 替代	
	if(!file_exists($hd)){
	//生成头像图片
	$sql_head = "SELECT headimgurl FROM wxch_user WHERE wxid = '".$GLOBALS['db'] -> getOne("SELECT wxid FROM ecs_users WHERE user_id =".$user_id)."'";
	$hd_res = $GLOBALS['db'] -> getOne($sql_head);
	$imageInfo_hd = downloadImage($hd_res);
		
	$local_file_hd = fopen($hd , 'w');
	if(false !== $local_file_hd){
		if(false !== fwrite($local_file_hd , $imageInfo_hd["body"])){
			fclose($local_file_hd);
		}
	}
	}
	
	$gd = 'goodspic/'.$goods_id."_img.jpg";//保存的头像名称，用用户的 ID 替代	
	if(!file_exists($gd)){
	//生成商品图片
	$sql_goodsimg = "SELECT goods_img FROM ecs_goods WHERE goods_id = '".$goods_id."'";
	$img_res = $GLOBALS['db'] -> getOne($sql_goodsimg);
	//file_put_contents('aacc.txt',strpos($img_res , 'http'));
	if(preg_match('/^http/' , $img_res) == 0){
		$img_res = 'http://'.$_SERVER['HTTP_HOST'].'/'.$img_res;
	}
	$imageInfo_gd = downloadImage($img_res);
		
	$local_file_gd = fopen($gd , 'w');
	if(false !== $local_file_gd){
		if(false !== fwrite($local_file_gd , $imageInfo_gd["body"])){
			fclose($local_file_gd);
		}
	}	
	}
	
	//开始处理图片
	$QR = imagecreatefromstring(file_get_contents($QR));
	$hd = imagecreatefromstring(file_get_contents($hd));
	$gd = imagecreatefromstring(file_get_contents($gd));
	$beijing = imagecreatefromstring(file_get_contents($beijing)); 
		
	$QR_width = imagesx($QR);//二维码图片宽度 
	$QR_height = imagesy($QR);//二维码图片高度 
 
	$gd_width = imagesx($gd);//二维码图片宽度 
	$gd_height = imagesy($gd);//二维码图片高度 
 			
	$beijing_width = imagesx($beijing);//beijing图片宽度 
	$beijing_height = imagesy($beijing);//beijing图片高度 
	
	$hd_width = imagesx($hd);
	$hd_height = imagesy($hd);
		
	//重新组合图片并调整大小 
	imagecopyresampled($beijing, $QR, 16, 545, 0, 0, 195, 195, $QR_width, $QR_height);
	//old pic : imagecopyresampled($beijing, $QR, 117, $beijing_height/2-15, 0, 0, $QR_width/1.6, $QR_height/1.6, $QR_width, $QR_height);
	imagecopyresampled($beijing, $gd, 18, 160, 0, 0, 390, 300, $gd_width, $gd_height);
	//imagecopyresampled($beijing, $hd, 18, 160, 0, 0, 410, 320, $hd_width, $hd_height);
	//头像	
	imagecopyresampled($beijing, $hd, 20, 25, 0, 0, 75, 75, $hd_width, $hd_height);
	
	$colorred=imagecolorallocate($beijing,0,0,255); //获得字体颜色
	//在源文件$beijing上用$colorred颜色,在x=150,y=55的地方开始用32大小的字体写上"my car"。数字：1字体大小，2左边位置（X轴），3右边位置（Y轴
	$red = imagecolorallocate($beijing,255,0,0); 
					
	$real_name = $GLOBALS['db'] -> getOne("SELECT real_name FROM ecs_users WHERE user_id =".$user_id);		
	imagettftext($beijing,12,0,190,57,$colorred,str_replace('mobile/user.php', '', str_replace('\\', '/', __FILE__)).'/wechat/msyh.ttf',$real_name);
	imagettftext($beijing,18,0,60,500,$red,str_replace('mobile/user.php', '', str_replace('\\', '/', __FILE__)).'/wechat/msyh.ttf',$shop_price);
	//imagettftext($beijing,18,0,60,500,$red,'msyh.ttf','55');
	imagettftext($beijing,12,0,60,520,$colorred,str_replace('mobile/user.php', '', str_replace('\\', '/', __FILE__)).'/wechat/msyh.ttf',$market_price);
	//imagettftext($beijing,12,0,60,520,$colorred,'msyh.ttf','79');
	//四个数字：字体大小、字的角度，X、Y位置
	
	//输出图片 
	//Header("Content-type: image/jpeg");
	//ImageJpeg($beijing,$user_id.'.jpg');
	ImageJpeg($beijing,'goodspic/'.$goods_id.'_'.$user_id.'.jpg');
	//sleep(2);
	}
	$result['img_path'] = $img_path;
	$result['status'] = 1;
	die($json->encode($result));
}


//我的下线
elseif($action =='down_line'){
   $goodsid = intval(isset($_REQUEST['goodsid']) ? $_REQUEST['goodsid'] : 0);
   
    if(empty($goodsid))
    {
        //我的推荐页面

        $page       = !empty($_REQUEST['page'])  && intval($_REQUEST['page'])  > 0 ? intval($_REQUEST['page'])  : 1;
        $size       = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;
        empty($affiliate) && $affiliate = array();
		
            //推荐注册分成
            $affdb = array();
            $num = count($affiliate['item']);
            $up_uid = "'$user_id'";
            $all_uid = "'$user_id'";
            for ($i = 1 ; $i <=$num ;$i++)
            {
                $count = 0;
				//$affdb[$i]['rank_id'][] =array();
                if ($up_uid)
                {
                    $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE recmded_id IN($up_uid)";
                    $query = $db->query($sql);
                    $up_uid = '';
                    while ($rt = $db->fetch_array($query))
                    {
                        $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
						$affdb[$i]['rank_id'][] = $rt['user_id'];
                        if($i < $num)
                        {
                            $all_uid .= ", '$rt[user_id]'";
							
                        }
                        $count++;
                    }
                }
				
                $affdb[$i]['num'] = $count;
                $affdb[$i]['point'] = $affiliate['item'][$i-1]['level_point'];
                $affdb[$i]['money'] = $affiliate['item'][$i-1]['level_money'];
            }
            

            $sqlcount = "SELECT count(*) FROM " . $ecs->table('users') ." WHERE recmded_id IN ($all_uid) ";
			
			
			$sousuo_state = isset($_GET['search_down_line'])? intval($_GET['search_down_line']) : 0;
			$keywords = isset($_GET['keywords'])? trim($_GET['keywords']) : "" ;
				
		
        }
		//统计不同等级人数
		
		
		$sql_search = "SELECT u.user_id, u.user_name, u.user_money, u.recmded_id , u.frozen_money, u.mobile_phone, u.down_num, u.reg_time, o.order_id ,SUM(a.money+a.dmoney) 
			as sum_money, w.nickname ,w.headimgurl FROM ". $ecs->table('users') ." u"." LEFT JOIN".$ecs->table('order_info')." o ON o.user_id = u.user_id LEFT JOIN " .$ecs->table('affiliate_log'). " a ON a.order_id= o.order_id  AND a.user_id='$user_id' and a.separate_type>=0 LEFT  JOIN wxch_user w ON w.wxid= u.wxid WHERE u.recmded_id IN ($all_uid) AND u.mobile_phone = '".$keywords."' GROUP BY u.user_name order by u.reg_time  DESC ";
		$search = $db->getRow($sql_search);
		if(empty($search)){$kong = 1;}
		else{$kong = 0;}
		$search['reg_time'] = local_date($GLOBALS['_CFG']['date_format'], $search['reg_time']);
		$search['sum_money'] = isset($search['sum_money']) ? $search['sum_money'] : 0;

		if(empty($search['headimgurl']))$search['headimgurl']="images/portrait.jpg";
		
        $logdb_search[0]['reg_time'] = local_date($GLOBALS['_CFG']['date_format'], $logdb[$i]['reg_time']);
		  
		$reg_time = $search['reg_time'];
		$nickname = $search['nickname'];
		$phone = empty($search['phone']) ? "无号码" : $search['phone'];
		if($search['recmded_id'] == $user_id){ $search['rank'] = 1; }
		else{ $search['rank'] = 2 ;}
		$sum_money = empty($search['sum_money']) ? 0 :  $search['sum_money'];
		
		$searchrs = $db->getRow($sql);

        $allcount = $db->getOne($sqlcount);
		$sql = 'UPDATE ' . $ecs->table('users') . " SET `down_num`='$allcount' WHERE `user_id`='" .$user_id. "'";
		$db->query($sql);
       
     
        $count = count($logdb);
		
	    $count_1=count($affdb[1]['rank_id']);
		$count_2=count($affdb[2]['rank_id']);
         
		
		//获取当前用户的微信名称和头像
		$sql = "SELECT w.nickname , w.headimgurl , u.user_id ,u.mobile_phone FROM wxch_user w RIGHT JOIN".$ecs->table('users')." u ON u.wxid = w.wxid WHERE u.user_id = '".$user_id."'";
		$weixin = $db->getRow($sql);

		$weixin['headimgurl']  =!empty($weixin['headimgurl']) ? $weixin['headimgurl'] : "images/portrait.jpg";
		$weixin['nickname']    =!empty($weixin['nickname']) ? $weixin['nickname'] : "未关注公众号";
		$weixin['count_1']  = $count_1;
		$weixin['count_2']  = $count_2;
		$weixin['allcount'] = $allcount;
		$xiaxian =  isset($_GET['xiaxian']) ? intval($_GET['xiaxian']) : 1;
		
		//获取当前用户等级
		$sql_curent_rank = "SELECT r.rank_name FROM " . $ecs->table('user_rank') . " r LEFT JOIN " .$ecs->table('users')." u ON u.down_num BETWEEN r.min_points AND r.max_points WHERE u.user_id = ".$user_id;
		
		//判断要显示的是几级下线
			
        $xiaxian =  isset($_GET['xiaxian']) ? intval($_GET['xiaxian']) : 1;
		if($xiaxian == '2' ){ $rank_xia=2;
			foreach($affdb[1]['rank_id'] as $item){
				$down_uid .=  $down_uid ? ",'$item'" : "'$item'";
			}
		}
		else{ $xiaxian = 1; $rank_xia=1;$down_uid = $user_id;}
						
       if(!empty($down_uid)){
	    $sqlcount = "SELECT count(*) FROM " . $ecs->table('users') ." WHERE recmded_id IN ($down_uid) ";
			
		$sql = "SELECT u.user_id, u.user_name, u.user_money, u.frozen_money, u.mobile_phone as phone, u.down_num, u.reg_time, o.order_id ,SUM(a.money+a.dmoney) as sum_money, w.nickname , w.headimgurl FROM ". $ecs->table('users') ." u LEFT JOIN".$ecs->table('order_info')." o ON o.user_id = u.user_id LEFT JOIN " .$ecs->table('affiliate_log'). " a ON a.order_id= o.order_id  AND a.user_id='$user_id' and a.separate_type>=0 LEFT JOIN wxch_user w ON w.wxid= u.wxid WHERE  u.recmded_id IN ($down_uid) GROUP BY u.user_name order by u.reg_time  DESC";
				
        $count = $db->getOne($sqlcount);
        
		$res = $db->SelectLimit($sql, $size, ($page - 1) * $size);
		$max_page = ($count> 0) ? ceil($count / $size) : 1;
        $logdb = array();
        while ($rt = $GLOBALS['db']->fetchRow($res)){ $logdb[] = $rt; }
        $count = count($logdb);
		$temp = $affiliate['config']['level_money_all']/100;
		for ($i=0; $i<$count; $i++)
		{   
	     for($j=1; $j<=$num; $j++){
		   if(in_array($logdb[$i]['user_id'],$affdb[$j]['rank_id'])){
			     $logdb[$i]['rank']=$j;
			   }
		 }  
			$logdb[$i]['reg_time'] = local_date($GLOBALS['_CFG']['date_format'], $logdb[$i]['reg_time']);
			$logdb[$i]['yanse'] = ($rank_xia == 1) ? "one" : "two";
			$logdb[$i]['sum_money'] = !empty($logdb[$i]['sum_money']) ? $logdb[$i]['sum_money'] : 0 ;
			$money_beijing = "";
			if($logdb[$i]['sum_money'] == 0 )$logdb[$i]['money_beijing'] = "zero";
			$logdb[$i]['headimgurl'] = $logdb[$i]['headimgurl'] ? $logdb[$i]['headimgurl'] : "images/portrait.jpg";			
			$logdb[$i]['nickname'] = $logdb[$i]['nickname'] ? $logdb[$i]['nickname'] : "未关注公众号";
			if($logdb[$i]['phone']){ $logdb[$i]['phone'] = "<a href='tel:".$logdb[$i]['phone']."'>联系电话：".$logdb[$i]['phone']."<i class='phone'>&#xe600;</i>"; }
			else{ $logdb[$i]['phone']  = "<a>联系电话：暂未填写";}
		}	
	   
		$url_format = "user.php?act=down_line&xiaxian=".$xiaxian."&page=";

        $pager = array(
                    'page'  => $page,
                    'size'  => $size,
                    'sort'  => '',
                    'order' => '',
                    'record_count' => $count,
                    'page_count'   => $max_page,
                    'page_first'   => $url_format. '1',
                    'page_prev'    => $page > 1 ? $url_format.($page - 1) : "javascript:;",
                    'page_next'    => $page < $max_page ? $url_format.($page + 1) : "javascript:;",
                    'page_last'    => $url_format. $max_page,
                    'array'        => array()
                );
				
        for ($i = 1; $i <= $max_page; $i++)
        {
            $pager['array'][$i] = $url_format.$i;
        }	
		
 }
		
		$rank_rs = $db->getOne($sql_curent_rank);		
		$smarty->assign('rank_rs', $rank_rs);		
		$smarty->assign('logdb', $logdb);	
		$smarty->assign('pager', $pager);
		
		$smarty->assign('xiaxian', $xiaxian);
		$smarty->assign('search', $search);
		$smarty->assign('url_format', $url_format);
		
		$smarty->assign('keywords', $keywords);
		$smarty->assign('kong', $kong);
		
        $smarty->assign('output', $weixin);
		$smarty->assign('state',  $sousuo_state);
				
        $smarty->display('user_transaction.dwt');
    

}

/* 异步显示下线*/
elseif($action =='async_down_line'){
   $goodsid = intval(isset($_REQUEST['goodsid']) ? $_REQUEST['goodsid'] : 0);
    if(empty($goodsid))
    {
        //我的推荐页面
   
        empty($affiliate) && $affiliate = array();
		
		
		
            //推荐注册分成
            $affdb = array();
            $num = count($affiliate['item']);
            $up_uid = "'$user_id'";
            $all_uid = "'$user_id'";
            for ($i = 1 ; $i <=$num ;$i++)
            {
                $count = 0;
				//$affdb[$i]['rank_id'][] =array();
                if ($up_uid)
                {
                    $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE recmded_id IN($up_uid)";
                    $query = $db->query($sql);
                    $up_uid = '';
                    while ($rt = $db->fetch_array($query))
                    {
                        $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
						$affdb[$i]['rank_id'][] = $rt['user_id'];
                        if($i < $num)
                        {
                            $all_uid .= ", '$rt[user_id]'";
                        }
                        $count++;
                    }
                }
				
                $affdb[$i]['num'] = $count;
                $affdb[$i]['point'] = $affiliate['item'][$i-1]['level_point'];
                $affdb[$i]['money'] = $affiliate['item'][$i-1]['level_money'];
            }
            
			//判断要显示的是几级下线
			
            $xiaxian =  isset($_GET['id']) ? intval($_GET['id']) : 1;
			if($xiaxian == '2' ){$xiaxian = 1; $rank_xia=2;
				foreach($affdb[1]['rank_id'] as $item){
					$down_uid .=  $down_uid ? ",'$item'" : "'$item'";
					}
			}
			else { $xiaxian = 2; $rank_xia=1;$down_uid = $user_id;}
			
            $sqlcount = "SELECT count(*) FROM " . $ecs->table('users') ." WHERE recmded_id IN ($all_uid) ";
			
		 $sql = "SELECT u.user_id, u.user_name, u.user_money, u.frozen_money, u.mobile_phone as phone, u.down_num, u.reg_time, o.order_id ,SUM(a.money+a.dmoney) as sum_money, w.nickname , w.headimgurl FROM ". $ecs->table('users') ." u"." LEFT JOIN".$ecs->table('order_info')." o ON o.user_id = u.user_id LEFT JOIN " .$ecs->table('affiliate_log'). " a ON a.order_id= o.order_id  AND a.user_id='$user_id' and a.separate_type>=0 LEFT JOIN wxch_user w ON w.wxid= u.wxid WHERE  u.recmded_id IN ($down_uid) GROUP BY u.user_name order by u.reg_time  DESC";
		 }
			
		
		$start = $_POST['last'];

        $limit = $_POST['amount'];
     

        $count = $db->getOne($sqlcount);
        $res = $db->SelectLimit($sql, $limit, $start);
		
        $logdb = array();
        while ($rt = $GLOBALS['db']->fetchRow($res))
        {
          
             
            $logdb[] = $rt;
			
        }
        $count = count($logdb);
		$temp = $affiliate['config']['level_money_all']/100;
		
	   for ($i=0; $i<$count; $i++)
       {   
	     for($j=1; $j<=$num; $j++){
		   if(in_array($logdb[$i]['user_id'],$affdb[$j]['rank_id'])){
			     $logdb[$i]['rank']=$j;
			   }
		 }
			   
			   
          $logdb[$i]['reg_time'] = local_date($GLOBALS['_CFG']['date_format'], $logdb[$i]['reg_time']);
       }
		if(!empty($logdb)){
		
		  foreach($logdb as $vo){

			$yanse = ($rank_xia == 1) ? "one" : "two";
			$yeji = $vo['sum_money'] ? $vo['sum_money'] : 0 ;
			$money_beijing = "";
			if($yeji == 0 )$money_beijing = "zero";
			$headimg = $vo['headimgurl'] ? $vo['headimgurl'] : "images/portrait.jpg";			
			$nickname = $vo['nickname'] ? $vo['nickname'] : "未关注公众号";
			if($vo['phone']){ $dianhua = "<a href='tel:".$vo['phone']."'>联系电话：".$vo['phone']."<i class='phone'>&#xe600;</i>"; }
			else{ $dianhua = "<a>联系电话：暂未填写";}
			
            $asyList[] = array(
			   'tm-portrait'=>'<img src="'.$headimg.'"> 
							<b class="'.$yanse.'">'.$rank_xia.'</b>',//$vo['rank'].'</b>',
			
                'team-info' => '
							<p>微信名称：'.$nickname.'</p>
							<p>加入时间：'.$vo['reg_time'].'</p>
							<p>'.$dianhua.' </a></p>',
					
					'achievement'=>'<a href="user.php?act=detail&id='.$vo['user_id'].'&rank='.$vo['rank'].'">
							<b class="'.$money_beijing.'">'.$yeji.'元</b>
							<span>总共带来的业绩</span>
							<i class="arrow"></i>
							</a>'
				
            );
          }
	  }
	else{
	 $asyList[] = array('down-list' => '<p>您的团队目前还没伙伴加入哦！加油！</p>');}
	
    echo json_encode($asyList);
    

}


elseif($action=='detail'){

        $id = isset($_GET['id'])? trim($_GET['id']) : 0;
	    $rank = isset($_GET['rank'])? trim($_GET['rank']):0;
	    $smarty->assign('id', $id);
		$smarty->assign('rank', $rank);
        $smarty->display('user_transaction.dwt');
	   
}


//下线账户明细
elseif($action=='async_detail'){
	  include_once(ROOT_PATH . 'includes/lib_order.php');
	  
	   
	   $id = isset($_GET['id'])?  intval($_GET['id']) : 0;
	   $rank = isset($_GET['rank'])? intval($_GET['rank']):0;
	   
	   $points = $affiliate['item'][$rank-1]['level_point']*(float)$affiliate['config']['level_point_all']/100;
       $money = $affiliate['item'][$rank-1]['level_money']*(float)$affiliate['config']['level_money_all']/100;
	  
	  $sqlcount = "SELECT o.* FROM " . $ecs->table('order_info') ." o LEFT JOIN ". $ecs->table('affiliate_log') ." a ON a.order_id=o.order_id 
	               AND a.separate_type>=0 and a.user_id='$user_id' WHERE o.user_id=".$id;
				   
	  $sql = "SELECT o.order_id ,o.order_sn, o.money_paid, o.pay_time ,a.* FROM " . $ecs->table('order_info') ." o LEFT JOIN ". $ecs->table('affiliate_log') ." a ON a.order_id=o.order_id AND a.separate_type>=0 AND a.user_id='$user_id' WHERE o.order_status = 5 AND o.pay_status = 2 AND o.user_id =  '$id' ";
	 
	  $d_name= $db->getOne("SELECT user_name FROM ". $ecs->table('users')." where user_id = '$id'");
	  
	 /* $sql_goods_detail = "SELECT o.order_id , g.* ,a.* FROM " . $ecs->table('order_info') ." o LEFT JOIN ". $ecs->table('order_goods') ." g on g.order_id = o.order_id LEFT JOIN ". $ecs->table('affiliate_log') ." a ON a.order_id=o.order_id AND a.separate_type>=0 AND a.user_id='$user_id' WHERE o.user_id =  '$id' ";
	  
	  原$sql语句
	   $sql = "SELECT o.order_id, o.order_sn, a.* FROM " . $ecs->table('order_info') ." o LEFT JOIN ". $ecs->table('affiliate_log') ." a ON a.order_id=o.order_id 
	               AND a.separate_type>=0 AND a.user_id='$user_id' WHERE o.user_id='$id' ";
	  
	  
	  $goods_detail_rs = $db->getAll($sql_goods_detail);
	  $logdb_1 = array();
	  while ($rt = $db->fetch_array($goods_detail_rs))
            {
              $logdb_1[$i] = $rt;
			  $logdb_1[$i]['confirm_time'] = local_date($GLOBALS['_CFG']['date_format'], $rt['time']); 
			  $logdb_1[$i++]['name'] = $d_name;			  
      }
	   file_put_contents("1a.txt",print_r($logdb_1,true));
	   */
	   $temp = $db->getAll($sqlcount);
	   $count = count($temp);
	   
		
		$start = $_POST['last'];
        $limit = $_POST['amount'];

        $res = $db->SelectLimit($sql, $limit, $start);
      
	    $logdb = array();
	    $i=0;
       while ($rt = $db->fetch_array($res))
            {
              $logdb[$i] = $rt;
			  $logdb[$i]['confirm_time'] = local_date($GLOBALS['_CFG']['date_format'], $rt['time']); 
			  $logdb[$i]['pay_time'] = local_date($GLOBALS['_CFG']['date_format'], $rt['time']); 
			  $logdb[$i++]['name'] = $d_name;
               }
		 
	  if(!count($logdb)&&!$start){
	          $asyList[] = array(
                 'box1' => '<p>亲！您的该伙伴还没有在本商城购买商品 您应该多推荐几款优质产品给他（她）</p>'
            );
	    }
	 else{
	    foreach($logdb as $vo){
	            				
			 $order_content = '
				<ul>
					<li>订单号码:'.$vo['order_sn'].'</li>
					<li class="right">订单时间:'.$vo['pay_time'].'</li>
				</ul>';
				  
				  if($vo['money']){
				   $order_content .= '
					<dl>
						<p class="bonus_1"><span>'.$vo['money_paid'].'</span></p>
						<p class="bonus_2"><span>'.$vo['money'].'</span></p>
						<p class="bonus_3"><span>'.$vo['dmoney'].'</span></p>
						<strong>订单价格</strong>
						<strong class="zj">基础奖励</strong>
						<strong class="youb">等级奖励</strong>
					</dl>';
				   }
				   else{
				  $order_content .= '
					<dl>
						<p class="bonus_1"><span>'.$vo['money_paid'].'</span></p>
						<p class="bonus_2"><span>正在交易...</span></p>
						<p class="bonus_3"><span>正在交易...</span></p>
						<strong>订单价格</strong>
						<strong class="zj">基础奖励</strong>
						<strong class="youb">等级奖励</strong>
					</dl>';
				     }
					 
		  
            $asyList[] = array(
                'box1' => $order_content
               );
             }
		}
	

    echo json_encode($asyList);
	
	}



/* 删除留言 */
elseif ($action == 'del_msg')
{
    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    $order_id = empty($_GET['order_id']) ? 0 : intval($_GET['order_id']);

    if ($id > 0)
    {
        $sql = 'SELECT user_id, message_img FROM ' .$ecs->table('feedback'). " WHERE msg_id = '$id' LIMIT 1";
        $row = $db->getRow($sql);
        if ($row && $row['user_id'] == $user_id)
        {
            /* 验证通过，删除留言，回复，及相应文件 */
            if ($row['message_img'])
            {
                @unlink(ROOT_PATH . DATA_DIR . '/feedbackimg/'. $row['message_img']);
            }
            $sql = "DELETE FROM " .$ecs->table('feedback'). " WHERE msg_id = '$id' OR parent_id = '$id'";
            $db->query($sql);
        }
    }
    ecs_header("Location: user.php?act=message_list&order_id=$order_id\n");
    exit;
}

/* 删除评论 */
elseif ($action == 'del_cmt')
{
    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    if ($id > 0)
    {
        $sql = "DELETE FROM " .$ecs->table('comment'). " WHERE comment_id = '$id' AND user_id = '$user_id'";
        $db->query($sql);
    }
    ecs_header("Location: user.php?act=comment_list\n");
    exit;
}

/* 合并订单 */
elseif ($action == 'merge_order')
{
    include_once(ROOT_PATH .'include/lib_transaction.php');
    include_once(ROOT_PATH .'include/lib_order.php');
    $from_order = isset($_POST['from_order']) ? trim($_POST['from_order']) : '';
    $to_order   = isset($_POST['to_order']) ? trim($_POST['to_order']) : '';
    if (merge_user_order($from_order, $to_order, $user_id))
    {
        show_message($_LANG['merge_order_success'],$_LANG['order_list_lnk'],'user.php?act=order_list', 'info');
    }
    else
    {
        $err->show($_LANG['order_list_lnk']);
    }
}
/* 将指定订单中商品添加到购物车 */
elseif ($action == 'return_to_cart')
{
    include_once(ROOT_PATH .'include/cls_json.php');
    include_once(ROOT_PATH .'include/lib_transaction.php');
    $json = new JSON();

    $result = array('error' => 0, 'message' => '', 'content' => '');
    $order_id = isset($_POST['order_id']) ? intval($_POST['order_id']) : 0;
    if ($order_id == 0)
    {
        $result['error']   = 1;
        $result['message'] = $_LANG['order_id_empty'];
        die($json->encode($result));
    }

    if ($user_id == 0)
    {
        /* 用户没有登录 */
        $result['error']   = 1;
        $result['message'] = $_LANG['login_please'];
        die($json->encode($result));
    }

    /* 检查订单是否属于该用户 */
    $order_user = $db->getOne("SELECT user_id FROM " .$ecs->table('order_info'). " WHERE order_id = '$order_id'");
    if (empty($order_user))
    {
        $result['error'] = 1;
        $result['message'] = $_LANG['order_exist'];
        die($json->encode($result));
    }
    else
    {
        if ($order_user != $user_id)
        {
            $result['error'] = 1;
            $result['message'] = $_LANG['no_priv'];
            die($json->encode($result));
        }
    }

    $message = return_to_cart($order_id);

    if ($message === true)
    {
        $result['error'] = 0;
        $result['message'] = $_LANG['return_to_cart_success'];
        die($json->encode($result));
    }
    else
    {
        $result['error'] = 1;
        $result['message'] = $_LANG['order_exist'];
        die($json->encode($result));
    }

}

/* 编辑使用余额支付的处理 */
elseif ($action == 'act_edit_surplus')
{
    /* 检查是否登录 */
    if ($_SESSION['user_id'] <= 0)
    {
        ecs_header("Location: ./\n");
        exit;
    }

    /* 检查订单号 */
    $order_id = intval($_POST['order_id']);
    if ($order_id <= 0)
    {
        ecs_header("Location: ./\n");
        exit;
    }

    /* 检查余额 */
    $surplus = floatval($_POST['surplus']);
    if ($surplus <= 0)
    {
        $err->add($_LANG['error_surplus_invalid']);
        $err->show($_LANG['order_detail'], 'user.php?act=order_detail&order_id=' . $order_id);
    }

    include_once(ROOT_PATH . 'include/lib_order.php');

    /* 取得订单 */
    $order = order_info($order_id);
    if (empty($order))
    {
        ecs_header("Location: ./\n");
        exit;
    }

    /* 检查订单用户跟当前用户是否一致 */
    if ($_SESSION['user_id'] != $order['user_id'])
    {
        ecs_header("Location: ./\n");
        exit;
    }

    /* 检查订单是否未付款，检查应付款金额是否大于0 */
    if ($order['pay_status'] != PS_UNPAYED || $order['order_amount'] <= 0)
    {
        $err->add($_LANG['error_order_is_paid']);
        $err->show($_LANG['order_detail'], 'user.php?act=order_detail&order_id=' . $order_id);
    }

    /* 计算应付款金额（减去支付费用） */
    $order['order_amount'] -= $order['pay_fee'];

    /* 余额是否超过了应付款金额，改为应付款金额 */
    if ($surplus > $order['order_amount'])
    {
        $surplus = $order['order_amount'];
    }

    /* 取得用户信息 */
    $user = user_info($_SESSION['user_id']);

    /* 用户帐户余额是否足够 */
    if ($surplus > $user['user_money'] + $user['credit_line'])
    {
        $err->add($_LANG['error_surplus_not_enough']);
        $err->show($_LANG['order_detail'], 'user.php?act=order_detail&order_id=' . $order_id);
    }

    /* 修改订单，重新计算支付费用 */
    $order['surplus'] += $surplus;
    $order['order_amount'] -= $surplus;
    if ($order['order_amount'] > 0)
    {
        $cod_fee = 0;
        if ($order['shipping_id'] > 0)
        {
            $regions  = array($order['country'], $order['province'], $order['city'], $order['district']);
            $shipping = shipping_area_info($order['shipping_id'], $regions);
            if ($shipping['support_cod'] == '1')
            {
                $cod_fee = $shipping['pay_fee'];
            }
        }

        $pay_fee = 0;
        if ($order['pay_id'] > 0)
        {
            $pay_fee = pay_fee($order['pay_id'], $order['order_amount'], $cod_fee);
        }

        $order['pay_fee'] = $pay_fee;
        $order['order_amount'] += $pay_fee;
    }

    /* 如果全部支付，设为已确认、已付款 */
    if ($order['order_amount'] == 0)
    {
        if ($order['order_status'] == OS_UNCONFIRMED)
        {
            $order['order_status'] = OS_CONFIRMED;
            $order['confirm_time'] = gmtime();
        }
        $order['pay_status'] = PS_PAYED;
        $order['pay_time'] = gmtime();
    }
    $order = addslashes_deep($order);
    update_order($order_id, $order);

    /* 更新用户余额 */
    $change_desc = sprintf($_LANG['pay_order_by_surplus'], $order['order_sn']);
    log_account_change($user['user_id'], (-1) * $surplus, 0, 0, 0, $change_desc);

    /* 跳转 */
    ecs_header('Location: user.php?act=order_detail&order_id=' . $order_id . "\n");
    exit;
}

/* 编辑使用余额支付的处理 */
elseif ($action == 'act_edit_payment')
{
    /* 检查是否登录 */
    if ($_SESSION['user_id'] <= 0)
    {
        ecs_header("Location: ./\n");
        exit;
    }

    /* 检查支付方式 */
    $pay_id = intval($_POST['pay_id']);
    if ($pay_id <= 0)
    {
        ecs_header("Location: ./\n");
        exit;
    }

    include_once(ROOT_PATH . 'include/lib_order.php');
    $payment_info = payment_info($pay_id);
    if (empty($payment_info))
    {
        ecs_header("Location: ./\n");
        exit;
    }

    /* 检查订单号 */
    $order_id = intval($_POST['order_id']);
    if ($order_id <= 0)
    {
        ecs_header("Location: ./\n");
        exit;
    }

    /* 取得订单 */
    $order = order_info($order_id);
    if (empty($order))
    {
        ecs_header("Location: ./\n");
        exit;
    }

    /* 检查订单用户跟当前用户是否一致 */
    if ($_SESSION['user_id'] != $order['user_id'])
    {
        ecs_header("Location: ./\n");
        exit;
    }

    /* 检查订单是否未付款和未发货 以及订单金额是否为0 和支付id是否为改变*/
    if ($order['pay_status'] != PS_UNPAYED || $order['shipping_status'] != SS_UNSHIPPED || $order['goods_amount'] <= 0 || $order['pay_id'] == $pay_id)
    {
        ecs_header("Location: user.php?act=order_detail&order_id=$order_id\n");
        exit;
    }

    $order_amount = $order['order_amount'] - $order['pay_fee'];
    $pay_fee = pay_fee($pay_id, $order_amount);
    $order_amount += $pay_fee;

    $sql = "UPDATE " . $ecs->table('order_info') .
           " SET pay_id='$pay_id', pay_name='$payment_info[pay_name]', pay_fee='$pay_fee', order_amount='$order_amount'".
           " WHERE order_id = '$order_id'";
    $db->query($sql);

    /* 跳转 */
    ecs_header("Location: user.php?act=order_detail&order_id=$order_id\n");
    exit;
}

/* 保存订单详情收货地址 */
elseif ($action == 'save_order_address')
{
    include_once(ROOT_PATH .'include/lib_transaction.php');
    
    $address = array(
        'consignee' => isset($_POST['consignee']) ? compile_str(trim($_POST['consignee']))  : '',
        'email'     => isset($_POST['email'])     ? compile_str(trim($_POST['email']))      : '',
        'address'   => isset($_POST['address'])   ? compile_str(trim($_POST['address']))    : '',
        'zipcode'   => isset($_POST['zipcode'])   ? compile_str(make_semiangle(trim($_POST['zipcode']))) : '',
        'tel'       => isset($_POST['tel'])       ? compile_str(trim($_POST['tel']))        : '',
        'mobile'    => isset($_POST['mobile'])    ? compile_str(trim($_POST['mobile']))     : '',
        'sign_building' => isset($_POST['sign_building']) ? compile_str(trim($_POST['sign_building'])) : '',
        'best_time' => isset($_POST['best_time']) ? compile_str(trim($_POST['best_time']))  : '',
        'order_id'  => isset($_POST['order_id'])  ? intval($_POST['order_id']) : 0
        );
    if (save_order_address($address, $user_id))
    {
        ecs_header('Location: user.php?act=order_detail&order_id=' .$address['order_id']. "\n");
        exit;
    }
    else
    {
        $err->show($_LANG['order_list_lnk'], 'user.php?act=order_list');
    }
}

/* 我的红包列表 */
elseif ($action == 'bonus')
{
    include_once(ROOT_PATH .'include/lib_transaction.php');

    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
    $record_count = $db->getOne("SELECT COUNT(*) FROM " .$ecs->table('user_bonus'). " WHERE user_id = '$user_id'");

    $pager = get_pager('user.php', array('act' => $action), $record_count, $page);
    $bonus = get_user_bouns_list($user_id, $pager['size'], $pager['start']);

    $smarty->assign('pager', $pager);
    $smarty->assign('bonus', $bonus);
    $smarty->display('order.dwt');
}

/* 我的团购列表 */
elseif ($action == 'group_buy')
{
    include_once(ROOT_PATH .'include/lib_transaction.php');

    //待议
    $smarty->display('user_transaction.dwt');
}

/* 团购订单详情 */
elseif ($action == 'group_buy_detail')
{
    include_once(ROOT_PATH .'include/lib_transaction.php');

    //待议
    $smarty->display('user_transaction.dwt');
}

// 用户推荐页面
elseif ($action == 'affiliate')
{
    $goodsid = intval(isset($_REQUEST['goodsid']) ? $_REQUEST['goodsid'] : 0);
    if(empty($goodsid))
    {
        //我的推荐页面

        $page       = !empty($_REQUEST['page'])  && intval($_REQUEST['page'])  > 0 ? intval($_REQUEST['page'])  : 1;
        $size       = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;

        empty($affiliate) && $affiliate = array();

        if(empty($affiliate['config']['separate_by']))
        {
            //推荐注册分成
            $affdb = array();
            $num = count($affiliate['item']);
            $up_uid = "'$user_id'";
            $all_uid = "'$user_id'";
            for ($i = 1 ; $i <=$num ;$i++)
            {
                $count = 0;
                if ($up_uid)
                {
                    $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE parent_id IN($up_uid)";
                    $query = $db->query($sql);
                    $up_uid = '';
                    while ($rt = $db->fetch_array($query))
                    {
                        $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
                        if($i < $num)
                        {
                            $all_uid .= ", '$rt[user_id]'";
                        }
                        $count++;
                    }
                }
                $affdb[$i]['num'] = $count;
                $affdb[$i]['point'] = $affiliate['item'][$i-1]['level_point'];
                $affdb[$i]['money'] = $affiliate['item'][$i-1]['level_money'];
            }
            $smarty->assign('affdb', $affdb);

            $sqlcount = "SELECT count(*) FROM " . $ecs->table('order_info') . " o".
        " LEFT JOIN".$ecs->table('users')." u ON o.user_id = u.user_id".
        " LEFT JOIN " . $ecs->table('affiliate_log') . " a ON o.order_id = a.order_id" .
        " WHERE o.user_id > 0 AND (u.parent_id IN ($all_uid) AND o.is_separate = 0 OR a.user_id = '$user_id' AND o.is_separate > 0)";

            $sql = "SELECT o.*, a.log_id, a.user_id as suid,  a.user_name as auser, a.money, a.point, a.separate_type FROM " . $ecs->table('order_info') . " o".
                    " LEFT JOIN".$ecs->table('users')." u ON o.user_id = u.user_id".
                    " LEFT JOIN " . $ecs->table('affiliate_log') . " a ON o.order_id = a.order_id" .
        " WHERE o.user_id > 0 AND (u.parent_id IN ($all_uid) AND o.is_separate = 0 OR a.user_id = '$user_id' AND o.is_separate > 0)".
                    " ORDER BY order_id DESC" ;

            /*
                SQL解释：

                订单、用户、分成记录关联
                一个订单可能有多个分成记录

                1、订单有效 o.user_id > 0
                2、满足以下之一：
                    a.直接下线的未分成订单 u.parent_id IN ($all_uid) AND o.is_separate = 0
                        其中$all_uid为该ID及其下线(不包含最后一层下线)
                    b.全部已分成订单 a.user_id = '$user_id' AND o.is_separate > 0

            */

            $affiliate_intro = nl2br(sprintf($_LANG['affiliate_intro'][$affiliate['config']['separate_by']], $affiliate['config']['expire'], $_LANG['expire_unit'][$affiliate['config']['expire_unit']], $affiliate['config']['level_register_all'], $affiliate['config']['level_register_up'], $affiliate['config']['level_money_all'], $affiliate['config']['level_point_all']));
        }
        else
        {
            //推荐订单分成
            $sqlcount = "SELECT count(*) FROM " . $ecs->table('order_info') . " o".
                    " LEFT JOIN".$ecs->table('users')." u ON o.user_id = u.user_id".
                    " LEFT JOIN " . $ecs->table('affiliate_log') . " a ON o.order_id = a.order_id" .
                    " WHERE o.user_id > 0 AND (o.parent_id = '$user_id' AND o.is_separate = 0 OR a.user_id = '$user_id' AND o.is_separate > 0)";


            $sql = "SELECT o.*, a.log_id,a.user_id as suid, a.user_name as auser, a.money, a.point, a.separate_type,u.parent_id as up FROM " . $ecs->table('order_info') . " o".
                    " LEFT JOIN".$ecs->table('users')." u ON o.user_id = u.user_id".
                    " LEFT JOIN " . $ecs->table('affiliate_log') . " a ON o.order_id = a.order_id" .
                    " WHERE o.user_id > 0 AND (o.parent_id = '$user_id' AND o.is_separate = 0 OR a.user_id = '$user_id' AND o.is_separate > 0)" .
                    " ORDER BY order_id DESC" ;

            /*
                SQL解释：

                订单、用户、分成记录关联
                一个订单可能有多个分成记录

                1、订单有效 o.user_id > 0
                2、满足以下之一：
                    a.订单下线的未分成订单 o.parent_id = '$user_id' AND o.is_separate = 0
                    b.全部已分成订单 a.user_id = '$user_id' AND o.is_separate > 0

            */

            $affiliate_intro = nl2br(sprintf($_LANG['affiliate_intro'][$affiliate['config']['separate_by']], $affiliate['config']['expire'], $_LANG['expire_unit'][$affiliate['config']['expire_unit']], $affiliate['config']['level_money_all'], $affiliate['config']['level_point_all']));

        }

        $count = $db->getOne($sqlcount);

        $max_page = ($count> 0) ? ceil($count / $size) : 1;
        if ($page > $max_page)
        {
            $page = $max_page;
        }

        $res = $db->SelectLimit($sql, $size, ($page - 1) * $size);
        $logdb = array();
        while ($rt = $GLOBALS['db']->fetchRow($res))
        {
            if(!empty($rt['suid']))
            {
                //在affiliate_log有记录
                if($rt['separate_type'] == -1 || $rt['separate_type'] == -2)
                {
                    //已被撤销
                    $rt['is_separate'] = 3;
                }
            }
            $rt['order_sn'] = substr($rt['order_sn'], 0, strlen($rt['order_sn']) - 5) . "***" . substr($rt['order_sn'], -2, 2);
            $logdb[] = $rt;
        }

        $url_format = "user.php?act=affiliate&page=";

        $pager = array(
                    'page'  => $page,
                    'size'  => $size,
                    'sort'  => '',
                    'order' => '',
                    'record_count' => $count,
                    'page_count'   => $max_page,
                    'page_first'   => $url_format. '1',
                    'page_prev'    => $page > 1 ? $url_format.($page - 1) : "javascript:;",
                    'page_next'    => $page < $max_page ? $url_format.($page + 1) : "javascript:;",
                    'page_last'    => $url_format. $max_page,
                    'array'        => array()
                );
        for ($i = 1; $i <= $max_page; $i++)
        {
            $pager['array'][$i] = $i;
        }

        $smarty->assign('url_format', $url_format);
        $smarty->assign('pager', $pager);


        $smarty->assign('affiliate_intro', $affiliate_intro);
        $smarty->assign('affiliate_type', $affiliate['config']['separate_by']);

        $smarty->assign('logdb', $logdb);
    }
    else
    {
        //单个商品推荐
        $smarty->assign('userid', $user_id);
        $smarty->assign('goodsid', $goodsid);

        $types = array(1,2,3,4,5);
        $smarty->assign('types', $types);

        $goods = get_goods_info($goodsid);
        $shopurl = $ecs->url();
        $goods['goods_img'] = (strpos($goods['goods_img'], 'http://') === false && strpos($goods['goods_img'], 'https://') === false) ? $shopurl . $goods['goods_img'] : $goods['goods_img'];
        $goods['goods_thumb'] = (strpos($goods['goods_thumb'], 'http://') === false && strpos($goods['goods_thumb'], 'https://') === false) ? $shopurl . $goods['goods_thumb'] : $goods['goods_thumb'];
        $goods['shop_price'] = price_format($goods['shop_price']);

        $smarty->assign('goods', $goods);
    }

    $smarty->assign('shopname', $_CFG['shop_name']);
    $smarty->assign('userid', $user_id);
    $smarty->assign('shopurl', $ecs->url());
    $smarty->assign('logosrc', 'themes/' . $_CFG['template'] . '/images/logo.gif');

    $smarty->display('user_clips.dwt');
}

//首页邮件订阅ajax操做和验证操作
elseif ($action =='email_list')
{
    $job = $_GET['job'];

    if($job == 'add' || $job == 'del')
    {
        if(isset($_SESSION['last_email_query']))
        {
            if(time() - $_SESSION['last_email_query'] <= 30)
            {
                die($_LANG['order_query_toofast']);
            }
        }
        $_SESSION['last_email_query'] = time();
    }

    $email = trim($_GET['email']);
    $email = htmlspecialchars($email);

    if (!is_email($email))
    {
        $info = sprintf($_LANG['email_invalid'], $email);
        die($info);
    }
    $ck = $db->getRow("SELECT * FROM " . $ecs->table('email_list') . " WHERE email = '$email'");
    if ($job == 'add')
    {
        if (empty($ck))
        {
            $hash = substr(md5(time()), 1, 10);
            $sql = "INSERT INTO " . $ecs->table('email_list') . " (email, stat, hash) VALUES ('$email', 0, '$hash')";
            $db->query($sql);
            $info = $_LANG['email_check'];
            $url = $ecs->url() . "user.php?act=email_list&job=add_check&hash=$hash&email=$email";
            send_mail('', $email, $_LANG['check_mail'], sprintf($_LANG['check_mail_content'], $email, $_CFG['shop_name'], $url, $url, $_CFG['shop_name'], local_date('Y-m-d')), 1);
        }
        elseif ($ck['stat'] == 1)
        {
            $info = sprintf($_LANG['email_alreadyin_list'], $email);
        }
        else
        {
            $hash = substr(md5(time()),1 , 10);
            $sql = "UPDATE " . $ecs->table('email_list') . "SET hash = '$hash' WHERE email = '$email'";
            $db->query($sql);
            $info = $_LANG['email_re_check'];
            $url = $ecs->url() . "user.php?act=email_list&job=add_check&hash=$hash&email=$email";
            send_mail('', $email, $_LANG['check_mail'], sprintf($_LANG['check_mail_content'], $email, $_CFG['shop_name'], $url, $url, $_CFG['shop_name'], local_date('Y-m-d')), 1);
        }
        die($info);
    }
    elseif ($job == 'del')
    {
        if (empty($ck))
        {
            $info = sprintf($_LANG['email_notin_list'], $email);
        }
        elseif ($ck['stat'] == 1)
        {
            $hash = substr(md5(time()),1,10);
            $sql = "UPDATE " . $ecs->table('email_list') . "SET hash = '$hash' WHERE email = '$email'";
            $db->query($sql);
            $info = $_LANG['email_check'];
            $url = $ecs->url() . "user.php?act=email_list&job=del_check&hash=$hash&email=$email";
            send_mail('', $email, $_LANG['check_mail'], sprintf($_LANG['check_mail_content'], $email, $_CFG['shop_name'], $url, $url, $_CFG['shop_name'], local_date('Y-m-d')), 1);
        }
        else
        {
            $info = $_LANG['email_not_alive'];
        }
        die($info);
    }
    elseif ($job == 'add_check')
    {
        if (empty($ck))
        {
            $info = sprintf($_LANG['email_notin_list'], $email);
        }
        elseif ($ck['stat'] == 1)
        {
            $info = $_LANG['email_checked'];
        }
        else
        {
            if ($_GET['hash'] == $ck['hash'])
            {
                $sql = "UPDATE " . $ecs->table('email_list') . "SET stat = 1 WHERE email = '$email'";
                $db->query($sql);
                $info = $_LANG['email_checked'];
            }
            else
            {
                $info = $_LANG['hash_wrong'];
            }
        }
        show_message($info, $_LANG['back_home_lnk'], 'index.php');
    }
    elseif ($job == 'del_check')
    {
        if (empty($ck))
        {
            $info = sprintf($_LANG['email_invalid'], $email);
        }
        elseif ($ck['stat'] == 1)
        {
            if ($_GET['hash'] == $ck['hash'])
            {
                $sql = "DELETE FROM " . $ecs->table('email_list') . "WHERE email = '$email'";
                $db->query($sql);
                $info = $_LANG['email_canceled'];
            }
            else
            {
                $info = $_LANG['hash_wrong'];
            }
        }
        else
        {
            $info = $_LANG['email_not_alive'];
        }
        show_message($info, $_LANG['back_home_lnk'], 'index.php');
    }
}

/* ajax 发送验证邮件 */
elseif ($action == 'send_hash_mail')
{
    include_once(ROOT_PATH .'include/cls_json.php');
    include_once(ROOT_PATH .'include/lib_passport.php');
    $json = new JSON();

    $result = array('error' => 0, 'message' => '', 'content' => '');

    if ($user_id == 0)
    {
        /* 用户没有登录 */
        $result['error']   = 1;
        $result['message'] = $_LANG['login_please'];
        die($json->encode($result));
    }

    if (send_regiter_hash($user_id))
    {
        $result['message'] = $_LANG['validate_mail_ok'];
        die($json->encode($result));
    }
    else
    {
        $result['error'] = 1;
        $result['message'] = $GLOBALS['err']->last_message();
    }

    die($json->encode($result));
}
else if ($action == 'track_packages')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
    include_once(ROOT_PATH .'include/lib_order.php');

    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

    $orders = array();

    $sql = "SELECT order_id,order_sn,invoice_no,shipping_id FROM " .$ecs->table('order_info').
            " WHERE user_id = '$user_id' AND shipping_status = '" . SS_SHIPPED . "'";
    $res = $db->query($sql);
    $record_count = 0;
    while ($item = $db->fetch_array($res))
    {
        $shipping   = get_shipping_object($item['shipping_id']);

        if (method_exists ($shipping, 'query'))
        {
            $query_link = $shipping->query($item['invoice_no']);
        }
        else
        {
            $query_link = $item['invoice_no'];
        }

        if ($query_link != $item['invoice_no'])
        {
            $item['query_link'] = $query_link;
            $orders[]  = $item;
            $record_count += 1;
        }
    }
    $pager  = get_pager('user.php', array('act' => $action), $record_count, $page);
    $smarty->assign('pager',  $pager);
    $smarty->assign('orders', $orders);
    $smarty->display('user_transaction.dwt');
}
else if ($action == 'order_query')
{
    $_GET['order_sn'] = trim(substr($_GET['order_sn'], 1));
    $order_sn = empty($_GET['order_sn']) ? '' : addslashes($_GET['order_sn']);
    include_once(ROOT_PATH .'include/cls_json.php');
    $json = new JSON();

    $result = array('error'=>0, 'message'=>'', 'content'=>'');

    if(isset($_SESSION['last_order_query']))
    {
        if(time() - $_SESSION['last_order_query'] <= 10)
        {
            $result['error'] = 1;
            $result['message'] = $_LANG['order_query_toofast'];
            die($json->encode($result));
        }
    }
    $_SESSION['last_order_query'] = time();

    if (empty($order_sn))
    {
        $result['error'] = 1;
        $result['message'] = $_LANG['invalid_order_sn'];
        die($json->encode($result));
    }

    $sql = "SELECT order_id, order_status, shipping_status, pay_status, ".
           " shipping_time, shipping_id, invoice_no, user_id ".
           " FROM " . $ecs->table('order_info').
           " WHERE order_sn = '$order_sn' LIMIT 1";

    $row = $db->getRow($sql);
    if (empty($row))
    {
        $result['error'] = 1;
        $result['message'] = $_LANG['invalid_order_sn'];
        die($json->encode($result));
    }

    $order_query = array();
    $order_query['order_sn'] = $order_sn;
    $order_query['order_id'] = $row['order_id'];
    $order_query['order_status'] = $_LANG['os'][$row['order_status']] . ',' . $_LANG['ps'][$row['pay_status']] . ',' . $_LANG['ss'][$row['shipping_status']];

    if ($row['invoice_no'] && $row['shipping_id'] > 0)
    {
        $sql = "SELECT shipping_code FROM " . $ecs->table('touch_shipping') . " WHERE shipping_id = '$row[shipping_id]'";
        $shipping_code = $db->getOne($sql);
        $plugin = ROOT_PATH . 'include/modules/shipping/' . $shipping_code . '.php';
        if (file_exists($plugin))
        {
            include_once($plugin);
            $shipping = new $shipping_code;
            $order_query['invoice_no'] = $shipping->query((string)$row['invoice_no']);
        }
        else
        {
            $order_query['invoice_no'] = (string)$row['invoice_no'];
        }
    }

    $order_query['user_id'] = $row['user_id'];
    /* 如果是匿名用户显示发货时间 */
    if ($row['user_id'] == 0 && $row['shipping_time'] > 0)
    {
        $order_query['shipping_date'] = local_date($GLOBALS['_CFG']['date_format'], $row['shipping_time']);
    }
    $smarty->assign('order_query',    $order_query);
    $result['content'] = $smarty->fetch('library/order_query.lbi');
    die($json->encode($result));
}
elseif ($action == 'transform_points')
{
    $rule = array();
    if (!empty($_CFG['points_rule']))
    {
        $rule = unserialize($_CFG['points_rule']);
    }
    $cfg = array();
    if (!empty($_CFG['integrate_config']))
    {
        $cfg = unserialize($_CFG['integrate_config']);
        $_LANG['exchange_points'][0] = empty($cfg['uc_lang']['credits'][0][0])? $_LANG['exchange_points'][0] : $cfg['uc_lang']['credits'][0][0];
        $_LANG['exchange_points'][1] = empty($cfg['uc_lang']['credits'][1][0])? $_LANG['exchange_points'][1] : $cfg['uc_lang']['credits'][1][0];
    }
    $sql = "SELECT user_id, user_name, pay_points, rank_points FROM " . $ecs->table('users')  . " WHERE user_id='$user_id'";
    $row = $db->getRow($sql);
    if ($_CFG['integrate_code'] == 'ucenter')
    {
        $exchange_type = 'ucenter';
        $to_credits_options = array();
        $out_exchange_allow = array();
        foreach ($rule as $credit)
        {
            $out_exchange_allow[$credit['appiddesc'] . '|' . $credit['creditdesc'] . '|' . $credit['creditsrc']] = $credit['ratio'];
            if (!array_key_exists($credit['appiddesc']. '|' .$credit['creditdesc'], $to_credits_options))
            {
                $to_credits_options[$credit['appiddesc']. '|' .$credit['creditdesc']] = $credit['title'];
            }
        }
        $smarty->assign('selected_org', $rule[0]['creditsrc']);
        $smarty->assign('selected_dst', $rule[0]['appiddesc']. '|' .$rule[0]['creditdesc']);
        $smarty->assign('descreditunit', $rule[0]['unit']);
        $smarty->assign('orgcredittitle', $_LANG['exchange_points'][$rule[0]['creditsrc']]);
        $smarty->assign('descredittitle', $rule[0]['title']);
        $smarty->assign('descreditamount', round((1 / $rule[0]['ratio']), 2));
        $smarty->assign('to_credits_options', $to_credits_options);
        $smarty->assign('out_exchange_allow', $out_exchange_allow);
    }
    else
    {
        $exchange_type = 'other';

        $bbs_points_name = $user->get_points_name();
        $total_bbs_points = $user->get_points($row['user_name']);

        /* 论坛积分 */
        $bbs_points = array();
        foreach ($bbs_points_name as $key=>$val)
        {
            $bbs_points[$key] = array('title'=>$_LANG['bbs'] . $val['title'], 'value'=>$total_bbs_points[$key]);
        }

        /* 兑换规则 */
        $rule_list = array();
        foreach ($rule as $key=>$val)
        {
            $rule_key = substr($key, 0, 1);
            $bbs_key = substr($key, 1);
            $rule_list[$key]['rate'] = $val;
            switch ($rule_key)
            {
                case TO_P :
                    $rule_list[$key]['from'] = $_LANG['bbs'] . $bbs_points_name[$bbs_key]['title'];
                    $rule_list[$key]['to'] = $_LANG['pay_points'];
                    break;
                case TO_R :
                    $rule_list[$key]['from'] = $_LANG['bbs'] . $bbs_points_name[$bbs_key]['title'];
                    $rule_list[$key]['to'] = $_LANG['rank_points'];
                    break;
                case FROM_P :
                    $rule_list[$key]['from'] = $_LANG['pay_points'];$_LANG['bbs'] . $bbs_points_name[$bbs_key]['title'];
                    $rule_list[$key]['to'] =$_LANG['bbs'] . $bbs_points_name[$bbs_key]['title'];
                    break;
                case FROM_R :
                    $rule_list[$key]['from'] = $_LANG['rank_points'];
                    $rule_list[$key]['to'] = $_LANG['bbs'] . $bbs_points_name[$bbs_key]['title'];
                    break;
            }
        }
        $smarty->assign('bbs_points', $bbs_points);
        $smarty->assign('rule_list',  $rule_list);
    }
    $smarty->assign('shop_points', $row);
    $smarty->assign('exchange_type',     $exchange_type);
    $smarty->assign('action',     $action);
    $smarty->assign('lang',       $_LANG);
    $smarty->display('user_transaction.dwt');
}
elseif ($action == 'act_transform_points')
{
    $rule_index = empty($_POST['rule_index']) ? '' : trim($_POST['rule_index']);
    $num = empty($_POST['num']) ? 0 : intval($_POST['num']);


    if ($num <= 0 || $num != floor($num))
    {
        show_message($_LANG['invalid_points'], $_LANG['transform_points'], 'user.php?act=transform_points');
    }

    $num = floor($num); //格式化为整数

    $bbs_key = substr($rule_index, 1);
    $rule_key = substr($rule_index, 0, 1);

    $max_num = 0;

    /* 取出用户数据 */
    $sql = "SELECT user_name, user_id, pay_points, rank_points FROM " . $ecs->table('users') . " WHERE user_id='$user_id'";
    $row = $db->getRow($sql);
    $bbs_points = $user->get_points($row['user_name']);
    $points_name = $user->get_points_name();

    $rule = array();
    if ($_CFG['points_rule'])
    {
        $rule = unserialize($_CFG['points_rule']);
    }
    list($from, $to) = explode(':', $rule[$rule_index]);

    $max_points = 0;
    switch ($rule_key)
    {
        case TO_P :
            $max_points = $bbs_points[$bbs_key];
            break;
        case TO_R :
            $max_points = $bbs_points[$bbs_key];
            break;
        case FROM_P :
            $max_points = $row['pay_points'];
            break;
        case FROM_R :
            $max_points = $row['rank_points'];
    }

    /* 检查积分是否超过最大值 */
    if ($max_points <=0 || $num > $max_points)
    {
        show_message($_LANG['overflow_points'], $_LANG['transform_points'], 'user.php?act=transform_points' );
    }

    switch ($rule_key)
    {
        case TO_P :
            $result_points = floor($num * $to / $from);
            $user->set_points($row['user_name'], array($bbs_key=>0 - $num)); //调整论坛积分
            log_account_change($row['user_id'], 0, 0, 0, $result_points, $_LANG['transform_points'], ACT_OTHER);
            show_message(sprintf($_LANG['to_pay_points'],  $num, $points_name[$bbs_key]['title'], $result_points), $_LANG['transform_points'], 'user.php?act=transform_points');

        case TO_R :
            $result_points = floor($num * $to / $from);
            $user->set_points($row['user_name'], array($bbs_key=>0 - $num)); //调整论坛积分
            log_account_change($row['user_id'], 0, 0, $result_points, 0, $_LANG['transform_points'], ACT_OTHER);
            show_message(sprintf($_LANG['to_rank_points'], $num, $points_name[$bbs_key]['title'], $result_points), $_LANG['transform_points'], 'user.php?act=transform_points');

        case FROM_P :
            $result_points = floor($num * $to / $from);
            log_account_change($row['user_id'], 0, 0, 0, 0-$num, $_LANG['transform_points'], ACT_OTHER); //调整商城积分
            $user->set_points($row['user_name'], array($bbs_key=>$result_points)); //调整论坛积分
            show_message(sprintf($_LANG['from_pay_points'], $num, $result_points,  $points_name[$bbs_key]['title']), $_LANG['transform_points'], 'user.php?act=transform_points');

        case FROM_R :
            $result_points = floor($num * $to / $from);
            log_account_change($row['user_id'], 0, 0, 0-$num, 0, $_LANG['transform_points'], ACT_OTHER); //调整商城积分
            $user->set_points($row['user_name'], array($bbs_key=>$result_points)); //调整论坛积分
            show_message(sprintf($_LANG['from_rank_points'], $num, $result_points, $points_name[$bbs_key]['title']), $_LANG['transform_points'], 'user.php?act=transform_points');
    }
}
elseif ($action == 'act_transform_ucenter_points')
{
    $rule = array();
    if ($_CFG['points_rule'])
    {
        $rule = unserialize($_CFG['points_rule']);
    }
    $shop_points = array(0 => 'rank_points', 1 => 'pay_points');
    $sql = "SELECT user_id, user_name, pay_points, rank_points FROM " . $ecs->table('users')  . " WHERE user_id='$user_id'";
    $row = $db->getRow($sql);
    $exchange_amount = intval($_POST['amount']);
    $fromcredits = intval($_POST['fromcredits']);
    $tocredits = trim($_POST['tocredits']);
    $cfg = unserialize($_CFG['integrate_config']);
    if (!empty($cfg))
    {
        $_LANG['exchange_points'][0] = empty($cfg['uc_lang']['credits'][0][0])? $_LANG['exchange_points'][0] : $cfg['uc_lang']['credits'][0][0];
        $_LANG['exchange_points'][1] = empty($cfg['uc_lang']['credits'][1][0])? $_LANG['exchange_points'][1] : $cfg['uc_lang']['credits'][1][0];
    }
    list($appiddesc, $creditdesc) = explode('|', $tocredits);
    $ratio = 0;

    if ($exchange_amount <= 0)
    {
        show_message($_LANG['invalid_points'], $_LANG['transform_points'], 'user.php?act=transform_points');
    }
    if ($exchange_amount > $row[$shop_points[$fromcredits]])
    {
        show_message($_LANG['overflow_points'], $_LANG['transform_points'], 'user.php?act=transform_points');
    }
    foreach ($rule as $credit)
    {
        if ($credit['appiddesc'] == $appiddesc && $credit['creditdesc'] == $creditdesc && $credit['creditsrc'] == $fromcredits)
        {
            $ratio = $credit['ratio'];
            break;
        }
    }
    if ($ratio == 0)
    {
        show_message($_LANG['exchange_deny'], $_LANG['transform_points'], 'user.php?act=transform_points');
    }
    $netamount = floor($exchange_amount / $ratio);
    include_once(ROOT_PATH . './include/lib_uc.php');
    $result = exchange_points($row['user_id'], $fromcredits, $creditdesc, $appiddesc, $netamount);
    if ($result === true)
    {
        $sql = "UPDATE " . $ecs->table('users') . " SET {$shop_points[$fromcredits]}={$shop_points[$fromcredits]}-'$exchange_amount' WHERE user_id='{$row['user_id']}'";
        $db->query($sql);
        $sql = "INSERT INTO " . $ecs->table('account_log') . "(user_id, {$shop_points[$fromcredits]}, change_time, change_desc, change_type)" . " VALUES ('{$row['user_id']}', '-$exchange_amount', '". gmtime() ."', '" . $cfg['uc_lang']['exchange'] . "', '98')";
        $db->query($sql);
        show_message(sprintf($_LANG['exchange_success'], $exchange_amount, $_LANG['exchange_points'][$fromcredits], $netamount, $credit['title']), $_LANG['transform_points'], 'user.php?act=transform_points');
    }
    else
    {
        show_message($_LANG['exchange_error_1'], $_LANG['transform_points'], 'user.php?act=transform_points');
    }
}
/* 清除商品浏览历史 */
elseif ($action == 'clear_history')
{   include_once('include/cls_json.php');
    $json = new JSON;
	$result   = array('status' => 0, 'text'=>'', 'url'=>'');
    setcookie('ECS[history]',   '', 1);
	$result['content'] = $smarty->fetch('library/no_records.lbi');
	
	die($json->encode($result));
}




elseif($action =='dengdengji'){	
	$smarty->assign('sql_dow_rs', "");
	$smarty->display('dengji.dwt');
}

//生成随机数 by wang
function random($length = 6, $numeric = 0) {
    PHP_VERSION < '4.2.0' && mt_srand((double) microtime() * 1000000);
    if ($numeric) {
        $hash = sprintf('%0' . $length . 'd', mt_rand(0, pow(10, $length) - 1));
    } else {
        $hash = '';
        $chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjkmnpqrstuvwxyz';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }
    }
    return $hash;
}

function downloadImage($url){
		$ch = curl_init($url);
		curl_setopt($ch , CURLOPT_HEADER , 0);
		curl_setopt($ch , CURLOPT_NOBODY , 0);
		curl_setopt($ch , CURLOPT_SSL_VERIFYPEER , 0);
		curl_setopt($ch , CURLOPT_SSL_VERIFYHOST , 0);
		curl_setopt($ch , CURLOPT_RETURNTRANSFER , 1);
		$package = curl_exec($ch);
		$httpinfo = curl_getinfo($ch);
		curl_close($ch);
		return array_merge(array('body' => $package) , array('header' => $httpinfo));
	}


//scb 获取超级星期五商品
function get_topic_goods($topic_id){
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('topic') . " WHERE topic_id = '".$topic_id."'";

    $topic = $GLOBALS['db']->getRow($sql);
    $topic['data'] = addcslashes($topic['data'], "'");
    $tmp = @unserialize($topic["data"]);
    $arr = (array)$tmp;

    $goods_id = array();

    foreach ($arr AS $key=>$value)
    {
        foreach($value AS $k => $val)
        {
            $opt = explode('|', $val);
            $arr[$key][$k] = $opt[1];
            $goods_id[] = $opt[1];
        }
    }
    return $goods_id;
}

?>