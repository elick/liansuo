<?php


define('IN_ECTOUCH', true);

require(dirname(__FILE__) . '/include/init.php');
require(ROOT_PATH . 'include/cls_json.php');
$action  = $_REQUEST['act']? trim($_REQUEST['act']):"";
$id = isset($_GET['id'])? intval($_GET['id']):0;

if($action =='add_comment'){
$sql = 'SELECT oi.order_status as status, oi.shipping_status as shipping FROM '.$ecs->table('order_goods').' og LEFT JOIN 
       '. $ecs->table('order_info') .' oi on og.order_id = oi.order_id  WHERE rec_id = '.$id;
$order = $db->getRow($sql);
if($order['status']==5&&$order['shipping']==2){
$sql = "SELECT og.rec_id, og.goods_name ,og.goods_id , g.goods_thumb, g.shop_price FROM ". $ecs->table('order_goods') ." 
        og LEFT JOIN ".$ecs->table('goods')." g on og.goods_id = g.goods_id WHERE is_comment = 0 AND og.rec_id='$id'";
$gc_list = $db->getAll($sql);
$smarty->assign('gc_list',         $gc_list);
 }
}
elseif($action =='goods_wtcom'){
	$sql="SELECT og.rec_id, og.goods_id as id, og.goods_number as num, g.goods_name as name, g.shop_price, g.goods_thumb 
	      as thumb FROM ". $ecs->table('order_goods') ." og LEFT JOIN ". $ecs->table('goods') ." g on g.goods_id = og.goods_id
		  WHERE og.is_comment=0 AND og.order_id = '$id'";
    $goods_list= $db->getAll($sql);
	$smarty->assign('goods_list',      $goods_list);
	}
elseif($action =='act_addcom'){
	
	$json   = new JSON;
    $result = array('status' => 0, 'msg' => '', 'url' => '');
	$rec_id = isset($_POST['rec_id'])?intval($_POST['rec_id']):0;
	$goods_id = isset($_POST['goods_id'])?intval($_POST['goods_id']):0;
	$rank     = isset($_POST['rank'])?intval($_POST['rank']):5;
	$content  = isset($_POST['content'])?trim($_POST['content']):"";
	$m=add_comment($goods_id,$rank,$content);
	if($m){
		$result['status'] = 1;
		$result['url'] = "ma_com.php?act=com_list&id=".$goods_id;
		}
		$db->query(" UPDATE ".$ecs->table('order_goods')." SET is_comment = 1 WHERE rec_id='$rec_id'");
		echo $json->encode($result);
		exit();
	}
	
elseif($action =='com_list'){
 $page  = isset($_GET['page'])?intval($_GET['page']):1;
 $type = 0;
 $count = $GLOBALS['db']->getOne('SELECT COUNT(*) FROM ' .$GLOBALS['ecs']->table('comment').
           " WHERE id_value = '$id' AND comment_type = '$type' AND status = 1 AND parent_id = 0");
 $size  = !empty($GLOBALS['_CFG']['comments_number']) ? $GLOBALS['_CFG']['comments_number'] : 5;
 $action1 = "com_list&id=".$id;
 $pager  = get_pager('ma_com.php', array('act' => $action1), $count, $page, $size);
 $comments = get_comment($id, $type, $pager['size'], $page);

$smarty->assign('comments',     $comments);
$smarty->assign('pager',        $pager);
	
}
$smarty->assign('action',       $action);
$smarty->display('ma_com.dwt');

function add_comment($id, $rank, $content)
{
    /* 评论是否需要审核 */
    $status = 1;
    $type = 0;
    $user_id = empty($_SESSION['user_id']) ? 0 : $_SESSION['user_id'];
    $user_name = !empty($_SESSION['user_name']) ? $_SESSION['user_name'] : '';
    $email = htmlspecialchars($email);
    $user_name = htmlspecialchars($user_name);

    /* 保存评论内容 */
    $sql = "INSERT INTO " .$GLOBALS['ecs']->table('comment') .
           "(comment_type, id_value, email, user_name, content, comment_rank, add_time, ip_address, status, parent_id, user_id) VALUES " .
           "('" .$type. "', '" .$id. "', '$email', '$user_name', '" .$content."', '".$rank."', ".gmtime().", '".real_ip()."', '$status', '0', '$user_id')";

    $m = $GLOBALS['db']->query($sql);
    
    return $m;
}

function get_comment($id, $type, $size = 5, $page = 1)
{
    /* 取得评论列表 */
   

    $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('comment') .
            " WHERE id_value = '$id' AND comment_type = '$type' AND status = 1 AND parent_id = 0".
            ' ORDER BY comment_id DESC';
    $res = $GLOBALS['db']->selectLimit($sql, $size, ($page-1) * $size);

    $arr = array();
    $ids = '';
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $ids .= $ids ? ",$row[comment_id]" : $row['comment_id'];
        $arr[$row['comment_id']]['id']       = $row['comment_id'];
        $arr[$row['comment_id']]['email']    = $row['email'];
        $arr[$row['comment_id']]['username'] = $row['user_name'];
        $arr[$row['comment_id']]['content']  = str_replace('\r\n', '<br />', htmlspecialchars($row['content']));
        $arr[$row['comment_id']]['content']  = nl2br(str_replace('\n', '<br />', $arr[$row['comment_id']]['content']));
        $arr[$row['comment_id']]['rank']     = $row['comment_rank'];
        $arr[$row['comment_id']]['add_time'] = local_date($GLOBALS['_CFG']['time_format'], $row['add_time']);
    }
    /* 取得已有回复的评论 */
    if ($ids)
    {
        $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('comment') .
                " WHERE parent_id IN( $ids )";
        $res = $GLOBALS['db']->query($sql);
        while ($row = $GLOBALS['db']->fetch_array($res))
        {
            $arr[$row['parent_id']]['re_content']  = nl2br(str_replace('\n', '<br />', htmlspecialchars($row['content'])));
            $arr[$row['parent_id']]['re_add_time'] = local_date($GLOBALS['_CFG']['time_format'], $row['add_time']);
            $arr[$row['parent_id']]['re_email']    = $row['email'];
            $arr[$row['parent_id']]['re_username'] = $row['user_name'];
        }
    }

    return $arr;
}
?>