<?php
define('IN_ECTOUCH', true);
require(dirname(__FILE__) . '/include/init.php');
require(ROOT_PATH . 'include/lib_wxch.php');
require(dirname(__FILE__) . '/include/lib_payment.php');
assign_template();

/* 载入语言文件 */
require_once(ROOT_PATH . 'lang/' .$_CFG['lang']. '/user.php');
$user_id = $_SESSION['user_id'];
$action  = isset($_REQUEST['act']) ? trim($_REQUEST['act']) : 'apply';
$smarty->assign('action',      $action);
$back_act='';

// 不需要登录的操作或自己验证是否登录（如ajax处理）的act

$not_login_arr = array();


/* 显示页面的action列表 */

$ui_arr = array('apply_ajax' , 'apply' , 'apply_pay');

/* 未登录处理 */

if (empty($_SESSION['user_id']))
{
    if (!in_array($action, $not_login_arr))
    {
        if (in_array($action, $ui_arr))
        {
          ecs_header('Location: user.php?act=login&ad_apply_act='.$action);
		}
        else
        {
            //未登录提交数据。非正常途径提交数据！
            die($_LANG['require_login']);
        }
    }
}
/*
if (!in_array($action, $not_login_arr)){
   $sql = "SELECT real_name FROM ".$ecs->table('users') ." WHERE user_id=".$_SESSION['user_id'];
   $real_name = $db->getOne($sql);
   if(empty($real_name)){
      $action = 'apply_join';
    }
}
 $smarty->assign('action',     $action);
*/
/* 显示会员注册界面 */



/* --- 广告位申请管理 --- */
/* --- begin --- */
if($action == "apply"){
	$smarty->display('shop_apply.dwt');
}
elseif($action == 'apply_ajax'){
	$contactor = $_REQUEST['contactor'];
	$com_name = $_REQUEST['com_name'];
	$com_note = $_REQUEST['com_note'];
	$com_addr = $_REQUEST['com_addr'];
	$phone = $_REQUEST['phone'];
	$status = '0';
	$time = time();
	
	$result = array();
	
	$exists = $GLOBALS['db']->getOne("SELECT jn_id FROM ecs_join WHERE com_name = '".$com_name."'");
	if($exists > 0){
		$result['status'] = 0;
		$result['code'] = '已经存在';
	}else{
		$ok = $GLOBALS['db']->query("INSERT INTO  `ecs_join` (  `user_id` ,  `status` ,  `jn_name` ,  `phone` ,  `jn_note` ,  `com_name` ,  `com_addr` ,  `add_time` ,  `pay_id` ,  `pay_name` ) VALUES ('". $_SESSION['user_id'] . "','". $status . "','". $contactor ."','". $phone ."','". $com_note ."','". $com_name ."','". $com_addr ."','". $time ."','4','微信支付')");
		$result['status'] = 1;
		$result['code'] = '添加成功';
	}
	echo json_encode($result);
}
elseif($action == "ad_apply_pay"){
	$smarty->display('pay_page.dwt');
}

elseif($action=='apply_pay'){
	  include_once('include/lib_clips.php');
	  include_once('include/lib_payment.php');
	  $payment_id = 4;//移动端的微信支付
	  
	  $jn_id = $GLOBALS['db']->getOne("SELECT jn_id FROM ecs_join WHERE user_id = '".$_SESSION['user_id']."' ORDER BY jn_id DESC LIMIT 0,1");
	  	  
	  $order['order_sn']       = $jn_id;
	  $order['type']  = "ad_apply";
      $order['order_amount']   = 1;//订单金额
	  $order['log_id'] = insert_pay_log($jn_id, $order['order_amount'], $type=111, 0);
	  
	  $payment_info = payment_info($payment_id);
	  include_once(ROOT_PATH . 'include/modules/payment/' . $payment_info['pay_code'] . '.php');
	  /* 取得在线支付方式的支付按钮 */
      $pay_obj = new $payment_info['pay_code'];
	  $pay_online = $pay_obj->get_code($order, unserialize_config($payment_info['pay_config']));
	  $order['pay_desc'] = $payment_info['pay_desc'];
	  $order['pay_name'] = $payment_info['pay_name'];
	  
	  $smarty->assign('order',      $order);	  
	  $smarty->assign('pay_online', $pay_online);
	  $smarty->display('pay_page.dwt');
}

elseif($action=='apply_wxpay'){
	$log_id = !empty($_REQUEST['log_id'])? intval($_REQUEST['log_id']):0;
	$sql = "SELECT p.*, b.pay_id ,b.jn_id as order_sn FROM ".$ecs->table('pay_log')." p LEFT JOIN ".$ecs->table('join')." b 
	       on p.order_id = b.jn_id WHERE log_id = '$log_id'";
	$order = $GLOBALS['db']->getRow($sql);
	if ($order['order_amount'] > 0)
    {
        $payment = payment_info($order['pay_id']);
		//file_put_contents('ac.txt',$order['pay_id']);
        include_once('include/modules/payment/' . $payment['pay_code'] . '.php');
        $pay_obj    = new $payment['pay_code'];
        $pay_online = $pay_obj->get_code($order, unserialize_config($payment['pay_config']));
    	$order['pay_desc'] = $payment['pay_desc'];
		$order['pay_name'] = $payment['pay_name'];
        
		$smarty->assign('order',      $order);
        $smarty->assign('pay_online', $pay_online);
		
    }
	 $smarty->display('pay_page.dwt');
}

//支付完成之后调用的
elseif($action=='zhifu'){
	$status = $_REQUEST['status']; 
	$name = $GLOBALS['db']->getOne("SELECT jn_name FROM ecs_join WHERE user_id = '".$_SESSION['user_id']."' ORDER BY jn_id DESC LIMIT 0,1");
	$smarty->assign('name', $name);
	$smarty->assign('status', $status);
	$smarty->assign('baseline', $GLOBALS['_CFG']['baseline']);
	$smarty->display('submit_win.dwt');
}

elseif($action == 'ceshi'){
	//send_weixin_normally('哈哈哈','904339','ad_apply');
	//order_paid(3397);
}
/* --- end --- */
/* --- 广告位申请管理 --- */

function unserialize_config($cfg)
{
    if (is_string($cfg) && ($arr = unserialize($cfg)) !== false)
    {
        $config = array();

        foreach ($arr AS $key => $val)
        {
            $config[$val['name']] = $val['value'];
        }

        return $config;
    }
    else
    {
        return false;
    }
}

function payment_info($pay_id)
{
    $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('touch_payment') .
            " WHERE pay_id = '$pay_id' AND enabled = 1";

    return $GLOBALS['db']->getRow($sql);
}

?>