<?php
define('IN_ECTOUCH', true);

require(dirname(__FILE__) . '/include/init.php');

require(ROOT_PATH . 'include/lib_wxch.php');
assign_template();

/* 载入语言文件 */

require_once(ROOT_PATH . 'lang/' .$_CFG['lang']. '/user.php');

$user_id = $_SESSION['user_id'];

$action  = isset($_REQUEST['act']) ? trim($_REQUEST['act']) : 'default';

$affiliate = unserialize($GLOBALS['_CFG']['affiliate']);

$smarty->assign('affiliate',  $affiliate);


$back_act='';



// 不需要登录的操作或自己验证是否登录（如ajax处理）的act

$not_login_arr =

array('apply_join','ajax_apply_join','ajax_get_teams','ajax_update_account','ajax_post_bonus','ajax_get_bonus','bonus_pay','bonus_wxpay');


/* 显示页面的action列表 */

$ui_arr = array( 'profile','user_teams','team_order','income','cash_account','edit_account', 'available_bonus', 'dis_bonus','bonus_rank',
               'bonus_desc', 'share_qr','detail_bonus','line_profile','line_detail');

/* 未登录处理 */

if (empty($_SESSION['user_id']))

{

    if (!in_array($action, $not_login_arr))

    {

        if (in_array($action, $ui_arr))

        {

          ecs_header('Location: user.php?act=login&dis_act='.$action);
		}

        else

        {

            //未登录提交数据。非正常途径提交数据！

            die($_LANG['require_login']);

        }

    }

}



/* 如果是显示页面，对页面进行相应赋值 */

if (in_array($action, $ui_arr))

{

    assign_template();

    $position = assign_ur_here(0, $_LANG['user_center']);

    $smarty->assign('page_title', $position['title']); // 页面标题

    $smarty->assign('ur_here',    $position['ur_here']);

    $sql = "SELECT value FROM " . $ecs->table('touch_shop_config') . " WHERE id = 419";

    $row = $db->getRow($sql);

    $car_off = $row['value'];

    $smarty->assign('car_off',       $car_off);

    /* 是否显示积分兑换 */

    if (!empty($_CFG['points_rule']) && unserialize($_CFG['points_rule']))

    {

        $smarty->assign('show_transform_points',     1);

    }

    $smarty->assign('helps',      get_shop_help());        // 网店帮助

    $smarty->assign('data_dir',   DATA_DIR);   // 数据目录
	
    $smarty->assign('lang',       $_LANG);

}
if (!in_array($action, $not_login_arr)){
   $sql = "SELECT real_name FROM ".$ecs->table('users') ." WHERE user_id=".$_SESSION['user_id'];
   $real_name = $db->getOne($sql);
   if(empty($real_name)){
      $action = 'apply_join';
    }
	}
 $smarty->assign('action',     $action);

/* 显示会员注册界面 */


if ($action == 'apply_join')
{   require(dirname(__FILE__) . '/include/get_openid.php');

   
   if($user_id<=0){
	//获取微信openid
     $jsApi = new JsApi_pub();
	if (!isset($_GET['code']))
	{
		//触发微信返回code码
		$redirect = urlencode($GLOBALS['ecs']->url().'distributor.php?act=apply_join');
		$url = $jsApi->createOauthUrlForCode($redirect);
		Header("Location: $url"); 
	}else
	{
		//获取code码，以获取openid
	    $code = $_GET['code'];
		$jsApi->setCode($code);
		$openid = $jsApi->getOpenId();
       $smarty->assign('wxid',  $openid);
	  
	 }
	
	     $exwhere = " wxid = '$openid'";
	}
	else{$exwhere = " user_id = '$user_id'"; }
	//$openid = "oKsjxjkeEz1rJvgZjFge6LfyaIkc";
	 
	 
	
	 $sql = "SELECT real_name FROM ".$ecs->table('users') ." WHERE ".$exwhere;
     $real_name = $db->getOne($sql);
	
   if(empty($real_name)){  
    if(!empty($openid)){
   $sql = "SELECT SUM(o.goods_amount + o.shipping_fee) as amount FROM ".$ecs->table('users') ." u LEFT JOIN ".$ecs->table('order_info') ." o 
          on u.user_id = o.user_id AND o.pay_status = 2 WHERE wxid='$openid' group by u.user_id";
	}
	else{
		 $sql = "SELECT SUM(goods_amount + shipping_fee) as amount FROM  ".$ecs->table('order_info') ." 
          WHERE user_id ='$user_id' AND pay_status = 2  group by user_id";
		}
   $amount = $db->getOne($sql);
   if($amount == ''){ $amount = 0; }
   $is_join = $amount < $GLOBALS['_CFG']['baseline']? 0:1;
   
   if(!empty($openid)){
   $sql = "SELECT o.consignee, o.mobile FROM ".$ecs->table('users') ." u LEFT JOIN ".$ecs->table('order_info') ." o 
             on u.user_id = o.user_id AND o.pay_status = 2 WHERE wxid='$openid' order by pay_time DESC";
   }
   else{
	   
	    $sql = "SELECT consignee, mobile FROM  ".$ecs->table('order_info') ."  WHERE user_id = '$user_id' 
		       AND pay_status = 2 order by pay_time DESC";
	   }
   $user_info = $db->getRow($sql);
  $smarty->assign('baseline', $GLOBALS['_CFG']['baseline']);
  $smarty->assign('is_join',     $is_join);
  $smarty->assign('amount',      $amount);
  $smarty->assign('user_info',   $user_info);
  $smarty->display('distributor.dwt');
  
   }
  else{
	  ecs_header("Location: distributor.php?act=profile\n");
      exit;
	  }
}
elseif ($action == 'ajax_apply_join'){
	 include_once(ROOT_PATH . 'include/lib_passport.php');
	 include_once('include/cls_json.php');
     $json = new JSON;
     $result  = array('status' => 0, 'text'=>'', 'url'=>'');
     
	 $name     = isset($_POST['name'])? trim($_POST['name']):"";
     $mobile   = isset($_POST['mobile'])? trim($_POST['mobile']):"";
     $password = isset($_POST['password'])? trim($_POST['password']):"";
	 $wxid     = isset($_POST['wxid'])? trim($_POST['wxid']):"";
	 
	
	 $exwhere = $user_id>0? "user_id = '$user_id'" : "wxid = '$wxid'";
	 $sql = "UPDATE ".$ecs->table('users')." set real_name = '$name',user_name = '$mobile',password = '".md5($password)."', `ec_salt`='0', mobile_phone = '$mobile' ,
	         open_time = '".gmtime()."' WHERE ".$exwhere;
			
	 $db->query($sql);
	 $sql = "SELECT recmded_id FROM ".$ecs->table('users')." WHERE ".$exwhere;
	 $recmded =$db->getOne($sql);

	//XXX 给推荐人增加一元返利机会		
	$sql = 'UPDATE ' . $ecs->table('users') . " SET `msn`=msn+1 WHERE `user_id`='" .$recmded. "'";
	$db->query($sql);	


	//提交开店资料,上线店铺加1
            $num = count($affiliate['item']);
			for($i=1;$i<=$num;$i++){
				$sql = 'UPDATE ' . $ecs->table('users') . " SET `shop_num`=shop_num+1 WHERE `user_id`='" .$recmded. "'";
				$db->query($sql);
				$sql = "SELECT recmded_id FROM " .$ecs->table('users')." WHERE user_id='$recmded'";
				if(!$recmded=$db->getOne($sql)){
					break;
				}
			}
			
	 $result['status'] = 1;
     $result['url']   = "distributor.php?act=profile";
     die($json->encode($result));
	}

//团队订单
elseif($action =='profile'){
	$sql = "SELECT u.real_name, u.down_num, u.user_money ,u.mobile_phone , u.recmded_id, w.headimgurl FROM ".$ecs->table('users')." u LEFT JOIN wxch_user w on u.wxid = w.wxid
	      WHERE user_id= '$user_id'";
	$user_info = $db->getRow($sql);
	$sql = "SELECT real_name FROM ".$ecs->table('users')." WHERE user_id =".$user_info['recmded_id'];
	$rec_name = $db->getOne($sql);
	$affbox = get_affdb_info($user_id ,$affiliate);
	$affdb   = $affbox['affdb'];
	$all_uid = empty($affdb[2]['all_uid'])?$affdb[1]['all_uid']: $affdb[1]['all_uid'].",".$affdb[2]['all_uid'];
	$all_uid = empty($all_uid)?0:$all_uid;
	$sqlcount = "SELECT count(*) FROM " . $ecs->table('users') ." WHERE user_id IN ($all_uid) AND real_name!=''";
	$user_info['shop_num'] = $db->getOne($sqlcount);
	$sql ="UPDATE ".$ecs->table('users')." SET shop_num = ".$user_info['shop_num']." WHERE user_id = '$user_id'";
	$db->query($sql);
    $smarty->assign('user_info',   $user_info);
	$smarty->assign('user_id',     $user_id);
	$smarty->assign('rec_name',    $rec_name);
    $smarty->display('distributor.dwt');
}
elseif($action=='line_profile'){
        $affbox = get_affdb_info($user_id ,$affiliate);
		$affdb  = $affbox['affdb'];
		$alluid = empty($affdb[1]['all_uid'])? $user_id:"'".$user_id."',".$affdb[1]['all_uid'];
		$time   = gmtime();
		$timestamp  =$time -$time%84600 - 8*60*60;

		$sqlcount = "SELECT count(*) FROM " . $ecs->table('users') ." WHERE recmded_id IN ($alluid) AND reg_time>'$timestamp'";
		$count = $db->getOne($sqlcount);
		$smarty->assign('count',   $count);
		
		//新增的店铺数量
		$sqlcount2 = "SELECT count(*) FROM " . $ecs->table('users') ." WHERE recmded_id IN ($alluid) AND open_time>'$timestamp' AND real_name != ''";
		$count2 = $db->getOne($sqlcount2);
		$smarty->assign('count2',   $count2);
		
        $smarty->display('distributor.dwt');


}
elseif($action == 'line_detail'){
	  $page  = !empty($_REQUEST['page'])  && intval($_REQUEST['page'])  > 0 ? intval($_REQUEST['page'])  : 1;
      $size  = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;
      $type   = empty($_GET['type'])? 1:intval($_GET['type']);
      $affbox = get_affdb_info($user_id ,$affiliate);
      $affdb  = $affbox['affdb'];
      if($type ==1||$type==2){

          $exwhere = $type == 1? "w.nickname  is not null ":"w.nickname is null";
          $sqlcount = "SELECT count(*) FROM " . $ecs->table('users') ." u LEFT JOIN wxch_user w on w.wxid =u.wxid   WHERE ".$exwhere." AND  u.recmded_id = '$user_id' ";
          $sql = "SELECT u.user_id, u.user_name, u.user_money, u.frozen_money, u.mobile_phone as mobile, u.down_num, u.reg_time,w.nickname , w.headimgurl, o.order_id 
		        ,SUM(a.money+a.dmoney)  as sum_money  FROM ". $ecs->table('users') ." u"." LEFT JOIN".$ecs->table('order_info')." o ON o.user_id = u.user_id LEFT 
				 JOIN " .$ecs->table('affiliate_log'). " a ON a.order_id= o.order_id  AND a.user_id='$user_id' and a.separate_type>=0 LEFT JOIN wxch_user w on 
				 w.wxid =u.wxid  WHERE ".$exwhere." AND u.recmded_id ='$user_id' GROUP BY u.user_name order by u.reg_time  DESC";
      }
    if($type ==3||$type==4){
        if(!empty($affdb[1]['all_uid'])){
         $exwhere = $type == 3? "w.nickname is not null ":"w.nickname is null";
         $sqlcount = "SELECT count(*) FROM " . $ecs->table('users') ." u LEFT JOIN  wxch_user w on w.wxid =u.wxid WHERE ".$exwhere." AND 
		             u.recmded_id IN (".$affdb[1]['all_uid'].") ";
         $sql = "SELECT u.user_id, u.user_name, u.user_money, u.frozen_money, u.mobile_phone as mobile, u.down_num, u.reg_time, w.nickname , w.headimgurl, o.order_id 
		    ,SUM(a.money+a.dmoney)  as sum_money FROM ". $ecs->table('users') ." u"." LEFT JOIN".$ecs->table('order_info')." o ON o.user_id = u.user_id 
			  LEFT JOIN " .$ecs->table('affiliate_log'). " a ON a.order_id= o.order_id   AND a.user_id='$user_id' and a.separate_type>=0 LEFT JOIN wxch_user w on 
			  w.wxid =u.wxid WHERE ".$exwhere." AND  u.recmded_id IN (".$affdb[1]['all_uid'].") GROUP BY u.user_name order by u.reg_time  DESC";
		}
	else{
		   $smarty->display('distributor.dwt');
		   exit();
		}

    }

        $allcount = $db->getOne($sqlcount);
        
        $max_page = ($allcount> 0) ? ceil($allcount / $size) : 1;
        if ($page > $max_page)
        {
            $page = $max_page;
        }

        $res = $db->SelectLimit($sql, $size, ($page - 1) * $size);
        $logdb = array();
		$nickname = $type%2==1? "未知微信名":"未关注服务号";
        while ($rt = $GLOBALS['db']->fetchRow($res))
        {
             $rt['reg_time']  = local_date($GLOBALS['_CFG']['date_format'], $rt['reg_time']);
			 $rt['sum_money'] = empty($rt['sum_money'])? 0:trim($rt['sum_money']);
			 $rt['nickname']  = empty($rt['nickname'])? $nickname:trim($rt['nickname']);
			 $rt['headimgurl']  = empty($rt['headimgurl'])? "img/default.jpg":trim($rt['headimgurl']);
			 $logdb[] = $rt;
        }
      
        if($page < $max_page){
          $pager['page_next'] = "distributor.php?act=line_detail&page=".($page + 1)."&type=".$type;
		  }
		 else{  $pager['page_next'] = ""; }
		  
     
        $smarty->assign('pager',   $pager);
        $smarty->assign('logdb',   $logdb);

   $smarty->display('distributor.dwt');

}

//我的团队
elseif($action =='user_teams' or $action =='ajax_get_teams'){

        $page  = !empty($_REQUEST['page'])  && intval($_REQUEST['page'])  > 0 ? intval($_REQUEST['page'])  : 1;
        $size  = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;
		$rank  = !empty($_REQUEST['rank'])? intval($_REQUEST['rank']):1;

        $size  = 5;
        $affbox = get_affdb_info($user_id ,$affiliate);
		$affdb  = $affbox['affdb'];
	
     if($rank == 2){
		   if(empty($affdb[1]['all_uid'])){
		 include_once('include/cls_json.php');
         $json = new JSON;
		 $result['status']  = 1;
         $result['content'] = $smarty->fetch('library/team_info.lbi');
		 $result['page']    = $smarty->fetch('library/team_page.lbi');;
		 
		 die($json->encode($result));
			   }
		   $sqlcount = "SELECT count(*) FROM " . $ecs->table('users') ." WHERE real_name!='' AND recmded_id IN (".$affdb[1]['all_uid'].") ";
          $sql = "SELECT u.user_id, u.user_name, u.real_name, u.user_money, u.frozen_money, u.mobile_phone as mobile, u.down_num, u.reg_time, w.nickname , w.headimgurl, o.order_id 
		        ,SUM(a.money+a.dmoney)  as sum_money  FROM ". $ecs->table('users') ." u"." LEFT JOIN".$ecs->table('order_info')." o ON o.user_id = u.user_id LEFT JOIN "
		        .$ecs->table('affiliate_log'). " a ON a.order_id= o.order_id  AND a.user_id='$user_id' and a.separate_type>=0 LEFT JOIN wxch_user w on w.wxid =u.wxid 
		        WHERE u.real_name!='' AND u.recmded_id IN (".$affdb[1]['all_uid'].") GROUP BY u.user_name order by u.reg_time  DESC";
		
			
		   }
	else{
		  $sqlcount = "SELECT count(*) FROM " . $ecs->table('users') ." WHERE real_name!='' AND recmded_id = '$user_id' ";
		
          $sql = "SELECT u.user_id, u.user_name, u.real_name, u.user_money, u.frozen_money, u.mobile_phone as mobile, u.down_num, u.reg_time,w.nickname , w.headimgurl, o.order_id 
		        ,SUM(a.money+a.dmoney)  as sum_money  FROM ". $ecs->table('users') ." u"." LEFT JOIN".$ecs->table('order_info')." o ON o.user_id = u.user_id LEFT JOIN 
				" .$ecs->table('affiliate_log'). " a ON a.order_id= o.order_id  AND a.user_id='$user_id' and a.separate_type>=0 LEFT JOIN wxch_user w on w.wxid =u.wxid  
				 WHERE u.real_name!='' AND u.recmded_id ='$user_id' GROUP BY u.user_name order by u.reg_time  DESC";
		  }

        $allcount = $db->getOne($sqlcount);

        $max_page = ($allcount> 0) ? ceil($allcount / $size) : 1;
        if ($page > $max_page)
        {
            $page = $max_page;
        }

        $res = $db->SelectLimit($sql, $size, ($page - 1) * $size);
        $logdb = array();
        while ($rt = $GLOBALS['db']->fetchRow($res))
        {
             $rt['reg_time']    = local_date($GLOBALS['_CFG']['date_format'], $rt['reg_time']);
			 $rt['sum_money']   = empty($rt['sum_money'])? 0:trim($rt['sum_money']);
			 $rt['nickname']    = empty($rt['nickname'])? "未关注服务号":trim($rt['nickname']);
			 $rt['is_shop']     = empty($rt['real_name'])? 0:1;
			 $rt['headimgurl']  = empty($rt['headimgurl'])? "img/default.jpg":trim($rt['headimgurl']);
			 $logdb[] = $rt;
        }
      
        if($page < $max_page){
          $pager['page_next'] = "distributor.php?act=user_teams&page=".($page + 1)."&rank=".$rank;
		  }
		 else{  $pager['page_next'] = ""; }
		  
     
        $smarty->assign('pager',   $pager);
        $smarty->assign('logdb',   $logdb);
		$smarty->assign('rank',    $rank);
		
		if($action =='ajax_get_teams'){
		 include_once('include/cls_json.php');
         $json = new JSON;
		 $result['status']  = 1;
         $result['content'] = $smarty->fetch('library/team_info.lbi');
		 $result['page']    = $smarty->fetch('library/team_page.lbi');;
		 
		 die($json->encode($result));
		}
		
		
        $smarty->display('distributor.dwt');
    

}

//团队订单
elseif($action =='team_order'){
	    $page  = !empty($_REQUEST['page'])  && intval($_REQUEST['page'])  > 0 ? intval($_REQUEST['page'])  : 1;
        $size  = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 5;
	    $size  = 5;
	     $affbox  = get_affdb_info($user_id ,$affiliate);
		 $affdb   = $affbox['affdb'];
		 $all_uid = empty($affdb[2]['all_uid'])?$affdb[1]['all_uid']: $affdb[1]['all_uid'].",".$affdb[2]['all_uid'];
		 $all_uid = empty($all_uid)?0:$all_uid;
	     $sqlcount = "SELECT count(*) FROM " . $ecs->table('order_info') ." WHERE user_id IN ($all_uid) ";
		
         $sql = "SELECT o.order_id, o.order_sn, o.user_id, o.order_status, o.pay_status, o.goods_amount, o.shipping_status, o.add_time ,o.mobile, o.is_separate, a.money, a.dmoney, SUM(a.money+a.dmoney) as sep_total,
		            a.separate_type FROM ". $ecs->table('order_info') ." o LEFT JOIN " .$ecs->table('affiliate_log'). " a ON a.order_id= o.order_id AND
					a.user_id = '$user_id' WHERE  o.user_id 
					 IN ($all_uid) group by o.order_id  order by o.add_time  DESC";
			
			 $allcount = $db->getOne($sqlcount);
        
        $max_page = ($allcount> 0) ? ceil($allcount / $size) : 1;
        if ($page > $max_page)
        {
            $page = $max_page;
        }

        $res = $db->SelectLimit($sql, $size, ($page - 1) * $size);
        $orders = array();
        while ($rt = $GLOBALS['db']->fetchRow($res))
        {
            $orders[] = $rt;
        }
       
	    $count = count($orders);
       for ($i=0; $i<$count; $i++)
       {   
	     for($j=1; $j<=2; $j++){
		   if(in_array($orders[$i]['user_id'], $affdb[$j]['rank_id'])){
			     $orders[$i]['rank'] = $j;
			   }
		  }
		  $order_id = empty($orders[$i]['order_id'])?0:intval($orders[$i]['order_id']);
		  $sql = "SELECT g.goods_thumb FROM ".$ecs->table('order_goods')." og LEFT JOIN ".$ecs->table('goods')." g on g.goods_id = og.goods_id  WHERE order_id = ".$order_id." order by rec_id DESC LIMIT 1";
		  $orders[$i]['thumb'] = $db->getOne($sql);
		  $orders[$i]['add_time'] = local_date($GLOBALS['_CFG']['date_format'], $orders[$i]['add_time']);
		  if($orders[$i]['order_status'] == 2){
			   $orders[$i]['status'] =  "订单已取消";
			  }
		  elseif($orders[$i]['order_status'] == 4){
			    $orders[$i]['status'] =  "已退货";
			  }
		  else{ if($orders[$i]['pay_status'] != 2 )
			     {
				  $orders[$i]['status'] =  "待付款";
				  }
			   else{
				 if($orders[$i]['shipping_status'] == 1){
				   $orders[$i]['status'] = "已发货，等待确认收货中..";
				   }
				  else{
				    $orders[$i]['status'] = $orders[$i]['shipping_status'] == 0? "正在配货中...":"";
				   } 
				 }
			  
			  }
		 
		  
		 
		
	   }
	 
         if($page < $max_page){
          $pager['page_next'] = "distributor.php?act=team_order&page=".($page + 1);
		  }
		 else{  $pager['page_next'] = ""; }
        $smarty->assign('pager',    $pager);
        $smarty->assign('orders',   $orders);
		
        $smarty->display('distributor.dwt');
	    
	
	}
	
//我的收益
elseif($action =='income'){
	include_once(ROOT_PATH . 'include/lib_clips.php');
	$affbox = get_affdb_info($user_id ,$affiliate);
	$affdb = $affbox['affdb'];
    empty($affiliate) && $affiliate = array();
	$num = count($affiliate['item']);
	$time = gmtime();
	$timestamp        = $time - $time%86400-8*60*60;
	$today_begin      = $timestamp;
	$today_end        = $timestamp + 24*60*60;
	$today_amount     = 0;
	$unconfirm_amount = 0;
	
    for ($i = 1 ; $i <=$num;$i++)
       { if(!empty($affdb[$i]['all_uid'])){
	   
			if($i == 1){
				$percent_sep = "percent_sep_1";
			}else{
				$percent_sep = "percent_sep_2";
			}
		
	    $sql = "SELECT SUM(goods_amount) as today_earn FROM ".$ecs->table('order_info')." WHERE user_id IN(".$affdb[$i]['all_uid'].") AND pay_status = 2 AND pay_time>'$today_begin' AND 
		    pay_time<='$today_end'";
		 $today_earn = $db->getOne($sql);
		 $sql = "SELECT SUM(og.goods_price*og.goods_number*g.".$percent_sep.") as amount FROM ".$ecs->table('order_info')." o LEFT JOIN  ".$ecs->table('order_goods')." 
               og on og.order_id = o.order_id LEFT JOIN ".$ecs->table('goods')." g on g.goods_id = og.goods_id WHERE o.user_id IN(".$affdb[$i]['all_uid'].") AND 
		       o.pay_status = 2 AND o.pay_time>'$today_begin' AND  o.pay_time<='$today_end'";
		 $amount_t1 = $db->getOne($sql);
		
		 $sql = "SELECT SUM(goods_amount) as unconfirm_earn FROM ".$ecs->table('order_info')." WHERE user_id IN(".$affdb[$i]["all_uid"].") AND pay_status = 2 AND shipping_status = 1 
		        AND is_separate = 0";
		 $unconfirm_earn = $db->getOne($sql);
		 $sql = "SELECT SUM(og.goods_price*og.goods_number*g.".$percent_sep.") as amount FROM ".$ecs->table('order_info')." o LEFT  JOIN ".$ecs->table('order_goods')." 
               og on og.order_id = o.order_id LEFT JOIN  ".$ecs->table('goods')." g on g.goods_id = og.goods_id WHERE o.user_id IN(".$affdb[$i]["all_uid"].") AND o.pay_status = 2 
			   AND o.shipping_status = 1 AND o.is_separate = 0";
		 $amount_un = $db->getOne($sql);
		 round($amount_t1/100, 2);
		 $unconfirm_amount += round($amount_un/100, 2);
		 $sql = "SELECT r.welfare FROM ".$ecs->table('users')." u LEFT JOIN ".$ecs->table('user_rank')." r on r.min_points<=u.shop_num AND
		         r.max_points > u.shop_num  WHERE u.user_id ='$user_id'";
		 $welfare = $db->getOne($sql);
		 $sql = "SELECT r.welfare FROM ".$ecs->table('users')." u LEFT JOIN ".$ecs->table('user_rank')." r on r.rank_id = u.user_rank AND r.special_rank=1 WHERE u.user_id = '$user_id'";
		 $spe_welfare = $db->getOne($sql);
		 if($spe_welfare){
			 $welfare = $spe_welfare;
			 }
			 
		$today_amount +=round($welfare*$today_earn/100,2); 
		$unconfirm_amount += round($welfare*$unconfirm_earn/100,2);
	     }
	   }
	   
	   $week_profit  = 0;
	   $month_profit = 0;
	   $total_profit = 0;
	   
	   $week_time_end  = $time - 7*24*60*60;
	   $month_time_end = $time - 30*24*60*60;
	   
	   $sql = "SELECT SUM(money+dmoney) as week_profit FROM ".$ecs->table('affiliate_log')." WHERE user_id = '$user_id' AND time<='$time' AND time>='$week_time_end'";
	   $week_profit = $db->getOne($sql);
	   $sql = "SELECT SUM(money+dmoney) as month_profit FROM ".$ecs->table('affiliate_log')." WHERE user_id = '$user_id' AND time<='$time' AND time>='$month_time_end'";
	   $month_profit = $db->getOne($sql);
	   $sql ="SELECT SUM(money+dmoney) as total_money FROM ".$ecs->table('affiliate_log')." WHERE user_id ='$user_id'";
	   $total_profit = $db->getOne($sql);
	   $surplus_amount = get_user_surplus($user_id);
	   
	   $surplus_amount   = empty($surplus_amount)?0:$surplus_amount;
	   $month_profit     = empty($month_profit)?0:$month_profit;
	   $unconfirm_amount = empty($unconfirm_amount)?0:$unconfirm_amount;
	   $total_profit     = empty($total_profit)?0:$total_profit;
	   $week_profit      = empty($week_profit)?0:$week_profit;
	   $today_amount     = empty($today_amount)?0:$today_amount;

   
	 $smarty->assign('today_amount',       $today_amount );
	 $smarty->assign('unconfirm_amount',   $unconfirm_amount);  
	 $smarty->assign('week_profit',        $week_profit );
	 $smarty->assign('month_profit',       $month_profit);
     $smarty->assign('total_profit',       $total_profit );
	 $smarty->assign('surplus_amount',     $surplus_amount);
	 
	 $smarty->display('distributor.dwt');
	}
elseif($action =='cash_account'){
	$sql = "SELECT * FROM ". $ecs->table('users_card')." WHERE user_id = '$user_id' order by card_id DESC 
	        LIMIT 2";
	$account_list = $db->getAll($sql);
	$smarty->assign('account_list',       $account_list);
	$smarty->display('distributor.dwt');
	}
elseif($action =='edit_account'){
	$id  = isset($_GET['id'])? intval($_GET['id']):1;
	$sql = "SELECT * FROM ". $ecs->table('users_card')." WHERE card_id = '$id'";
	$card_info = $db->getRow($sql);
	$smarty->assign('card_info',   $card_info);
	$smarty->display('distributor.dwt');
	}
elseif($action =='available_bonus'){
	$time = gmtime();
	$sql = "SELECT * FROM " .$ecs->table('prepaid_bonus')." WHERE is_paid=1 AND
	   $time< end_time AND user_id =".$_SESSION['user_id'];
	$bonus_info = $db->getRow($sql);
	$bonus_info['end_time'] = $bonus_info['end_time'];
	$smarty->assign('bonus_info',   $bonus_info);
	$smarty->display('dis_bonus.dwt');
	}
elseif($action =='detail_bonus'){
	$page = isset($_GET["page"])?intval($_GET["page"]):1;
	 $size  = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;
	
	$sqlcount = "SELECT count(*) FROM ".$ecs->table('bonus_log')." WHERE user_id =".$_SESSION['user_id'];
	$sql = "SELECT  *  FROM ".$ecs->table('bonus_log')." WHERE user_id =".$_SESSION['user_id'];
    $allcount = $db->getOne($sqlcount);

        $max_page = ($allcount> 0) ? ceil($allcount / $size) : 1;
        if ($page > $max_page)
        {
            $page = $max_page;
        }

        $res = $db->SelectLimit($sql, $size, ($page - 1) * $size);
        $bonus_log = array();
        while ($rt = $GLOBALS['db']->fetchRow($res))
        {
           $rt["change_time"] = local_date($GLOBALS['_CFG']['time_format'], $rt['change_time']);
		   $bonus_log[] = $rt;
        }
	    
	     if($page < $max_page){
          $pager['page_next'] = "distributor.php?act=detail_bonus&page=".($page + 1);
		  }
		 else{  $pager['page_next'] = ""; }
		 
	$smarty->assign('pager',       $pager);	
	$smarty->assign('bonus_log',   $bonus_log); 
	$smarty->display('dis_bonus.dwt');
	}
	
elseif($action =='dis_bonus'){
	
	$smarty->display('dis_bonus.dwt');
	}

elseif($action =='bonus_rank'){
	$sql = "SELECT rank_id, rank_name,upd_money FROM ".$ecs->table('user_rank')." WHERE 1 order by upd_money ASC ";
	
	$rank_list = $db->getAll($sql);
	$i = 1;
	foreach($rank_list as $key=>$value){
		$rank_list[$key]["id"] = $i++;
		}
	$rank_cur = $rank_list["0"];
	$smarty->assign('rank_cur',        $rank_cur);	
	$smarty->assign('rank_list',       $rank_list);	
	
	$smarty->display('dis_bonus.dwt');
	}

elseif($action =='bonus_desc'){
	
	$smarty->display('dis_bonus.dwt');
	}
elseif($action=='ajax_post_bonus'){
	 include_once('include/cls_json.php');
         $json = new JSON;
		 $id   =  isset($_REQUEST['id'])?intval($_REQUEST['id']):0;
		 $_SESSION['bonus_id'] = $id;
		  
		 $sql      = "SELECT upd_money, rank_name FROM ".$ecs->table('user_rank')." WHERE rank_id = '$id'";
		 $arr_rank = $db->getRow($sql);
		 
         $result['rank_name']   = $arr_rank['rank_name'];
		 $result['upd_money']   = $arr_rank['upd_money'];
		 
		 die($json->encode($result));
	
	}

elseif($action=='bonus_pay'){
	  include_once('include/lib_clips.php');
	  include_once('include/lib_payment.php');
	  $payment_id   = 4;
	  $time = gmtime();
	  
	  $rank_id = $_SESSION['bonus_id'];
	  if(empty($rank_id)){
        $sql = "SELECT upd_money, expire_time FROM ".$ecs->table('user_rank')." WHERE 1 ORDER BY rank_id ASC LIMIT 1"; }
		else{ $sql = "SELECT upd_money ,expire_time FROM ".$ecs->table('user_rank')." WHERE rank_id = '$rank_id'";}
	  
	  $rank_info = $db->getRow($sql);
	  $amount = $rank_info['upd_money'];
	  $end_time = $time + $rank_info['expire_time']*24*60*60;
      $sql = "INSERT INTO " .$ecs->table('prepaid_bonus'). " (user_id,user_name,total_money, rank_id, is_paid, available_money, pay_id, add_time, end_time)" .
                    "VALUES ('$_SESSION[user_id]','$_SESSION[user_name]', '$amount', '$rank_id','0', '$amount','$payment_id','$time', '$end_time')";
      $db->query($sql);
	  $pre_id = $db->insert_id();
	  $order['order_sn']       = $pre_id;
	  $order['type']  = "bonus";
      $order['order_amount']   = $amount;
	  $order['log_id'] = insert_pay_log($pre_id, $order['order_amount'], $type=88, 0);
	  
	  $payment_info = payment_info($payment_id);
	  include_once(ROOT_PATH . 'include/modules/payment/' . $payment_info['pay_code'] . '.php');
			/* 取得在线支付方式的支付按钮 */
      $pay_obj = new $payment_info['pay_code'];
	  $pay_online = $pay_obj->get_code($order, unserialize_config($payment_info['pay_config']));
	  $order['pay_desc'] = $payment_info['pay_desc'];
	  $order['pay_name'] = $payment_info['pay_name'];
	  
	  $smarty->assign('order',      $order);	  
	  $smarty->assign('pay_online', $pay_online);
	  unset($_SESSION['bonus_id']);
	  $smarty->display('distributor.dwt');
	}

elseif($action=='bonus_wxpay'){
	$log_id = !empty($_REQUEST['log_id'])? intval($_REQUEST['log_id']):0;
	$sql = "SELECT p.*, b.pay_id ,b.pre_id as order_sn FROM ".$ecs->table('pay_log')." p LEFT JOIN ".$ecs->table('prepaid_bonus')." b 
	       on p.order_id = b.pre_id WHERE log_id = '$log_id'";
	$order = $db->getRow($sql);
	 if ($order['order_amount'] > 0)
    {
        $payment = payment_info($order['pay_id']);
       
	   include_once('include/modules/payment/' . $payment['pay_code'] . '.php');

        $pay_obj    = new $payment['pay_code'];

        $pay_online = $pay_obj->get_code($order, unserialize_config($payment['pay_config']));

        $order['pay_desc'] = $payment['pay_desc'];
		$order['pay_name'] = $payment['pay_name'];
        
		$smarty->assign('order',      $order);
        $smarty->assign('pay_online', $pay_online);
		
    }
	 $smarty->display('distributor.dwt');
	}

elseif($action=='ajax_update_account'){
	 include_once('include/cls_json.php');
     $json = new JSON;
     $result  = array('status' => 0, 'text'=>'', 'url'=>'');
     
	 $name     = isset($_POST['name'])? trim($_POST['name']):"";
     $mobile   = isset($_POST['mobile'])? trim($_POST['mobile']):"";
     $id       = isset($_POST['id'])? intval($_POST['id']):0;
	 $type     = isset($_POST['type'])? trim($_POST['type']):"";
	 $number   = isset($_POST['number'])? trim($_POST['number']):"";
	 $open_bank= isset($_POST['open_bank'])? trim($_POST['open_bank']):"";
	  
	 $sql = "UPDATE ".$ecs->table('users_card')." set card_type = '$type', owner_name = '$name',mobile='$mobile', card_number = '$number',open_bank = '$open_bank',
	       edit_time=".time()." WHERE card_id = '$id'";
	 $db->query($sql);
	 $result['status'] = 1;
	 $result['url']    = "distributor.php?act=cash_account";
	 die($json->encode($result));
	}

function get_affdb_info($user_id ,$affiliate){
	
	  empty($affiliate) && $affiliate = array();
		
            $affdb  = array();
			$affbox = array();
            $num = count($affiliate['item']);
            $up_uid  = "'$user_id'";
            for ($i = 1 ; $i <=$num ;$i++)
            {
                  $count = 0;
				  $affdb[$i]['rank_id'][] =array();
                if ($up_uid)
                {
                    $sql = "SELECT user_id FROM " . $GLOBALS['ecs']->table('users') . " WHERE recmded_id IN($up_uid)";
                    $query = $GLOBALS['db']->query($sql);
                    $up_uid = '';
					$all_uid = '';
                    while ($rt = $GLOBALS['db']->fetch_array($query))
                    {
                        $all_uid .= $all_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
						$affdb[$i]['rank_id'][] = $rt['user_id'];
                     
						 if($i < $num)
                        {
                            $up_uid .= $up_uid?", '$rt[user_id]'":"'$rt[user_id]'";
							
                        }
                        $count++;
                    }
                }
				$affdb[$i]['all_uid'] = $all_uid;
                $affdb[$i]['num'] = $count;
              }
		$affbox['affdb']   = $affdb;	  
	    $affbox['up_uid']  = $up_uid;
		return $affbox;
	}
	
function unserialize_config($cfg)
{
    if (is_string($cfg) && ($arr = unserialize($cfg)) !== false)
    {
        $config = array();

        foreach ($arr AS $key => $val)
        {
            $config[$val['name']] = $val['value'];
        }

        return $config;
    }
    else
    {
        return false;
    }
}

function payment_info($pay_id)
{
    $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('touch_payment') .
            " WHERE pay_id = '$pay_id' AND enabled = 1";

    return $GLOBALS['db']->getRow($sql);
}

?>