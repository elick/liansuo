<?php
	define('IN_ECTOUCH', true);

	require(dirname(__FILE__) . '/include/init.php');
	require(ROOT_PATH . 'include/lib_wxch.php');
	require(ROOT_PATH . 'include/get_brand.php');
	require(dirname(__FILE__) . '/include/get_openid.php');
	
	$jsApi = new JsApi_pub();
	if (!isset($_GET['code'])){
		//触发微信返回code码
		$u = isset($_GET['u'])?intval($_GET['u']):0;
		$redirect = urlencode($GLOBALS['ecs']->url().'bangding.php');
		$url = $jsApi->createOauthUrlForCode($redirect);
		ecs_header("Location: $url"); 
		exit();
	}else{
		//获取code码，以获取openid
	    $code = $_GET['code'];
		$jsApi->setCode($code);
		$openid = $jsApi->getOpenId();
	}
	/*echo '<br><br><br><h1 align="center">需要绑定，先跟我们的客服联系下吧~</h1>';*/
echo '
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>账号绑定页</title>
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="minimal-ui=yes,width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="css/bangding.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
  <form action="user.php" class="login">
    <h1>已有账号绑定</h1>
    <input type="text" name="email" class="login-input" placeholder="用户名/手机号" autofocus>
    <input type="password" name="password" class="login-input" placeholder="密码">
    <input type="hidden" name="act" value="bangding">
    <input type="hidden" name="wxid" value="'.$openid.'">
    <input type="submit" value="绑定" class="login-submit">
  </form>
</body>
</html>';

?>