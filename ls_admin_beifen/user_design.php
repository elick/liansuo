<?php
define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');


/*------------------------------------------------------ */
//-- 设计品列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{   admin_priv('users_manage');
 
	$design_list = design_list();
    $smarty->assign('design_list',  $design_list['design_list']);
    $smarty->assign('filter',       $design_list['filter']);
    $smarty->assign('record_count', $design_list['record_count']);
    $smarty->assign('page_count',   $design_list['page_count']);
    $smarty->assign('full_page',    1);
    $smarty->assign('sort_card_id', '<img src="images/sort_desc.gif">');

    assign_query_info();
	$smarty->display('design_list.htm');


}

/*------------------------------------------------------ */
//-- ajax返回用户列表
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    $design_list = design_list();
    $smarty->assign('design_list',      $design_list['design_list']);
    $smarty->assign('filter',           $design_list['filter']);
    $smarty->assign('record_count',     $design_list['record_count']);
    $smarty->assign('page_count',       $design_list['page_count']);

    $sort_flag  = sort_flag($design_list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('design_list.htm'), '', array('filter' => $design_list['filter'], 'page_count' => $design_list['page_count']));
}
/*------------------------------------------------------ */
//-- 审核设计品
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'confirm_design')
{
    /* 检查权限 */
    admin_priv('surplus_manage');

    /* 初始化 */
    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;

    /* 如果参数不合法，返回 */
    if ($id == 0)
    {
        ecs_header("Location: users.php?act=card_list\n");
        exit;
    }

    /* 查询当前设计品的信息 */
    $design_info = array();
    $design_info = $db->getRow("SELECT * FROM " .$ecs->table('user_design'). " WHERE des_id = '$id'");
    $design_info['upload_time'] = local_date($_CFG['time_format'], $design_info['upload_time']);

 
    /* 模板赋值 */
  
    $smarty->assign('design_info',      $design_info);
  

    /* 页面显示 */
    assign_query_info();
    $smarty->display('user_design_confirm.htm');
}

/*------------------------------------------------------ */
//-- 审核信息更新
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'action_design')
{
    /* 检查权限 */
    admin_priv('surplus_manage');

    /* 初始化 */
    $id = isset($_POST['id']) ? intval($_POST['id']) : 0;
	$confirm = isset($_POST['confirm']) ? intval($_POST['confirm']) : 0;
	$admin_note = isset($_POST['admin_note']) ? trim($_POST['admin_note']) : 0;
    $time = time();

	if($confirm == 0){
	  $temp = 2 ;
	  }
	else $temp = 1;
	

     $sql = "UPDATE " .$ecs->table('user_design'). " SET ".
                   "admin_user    = '$_SESSION[admin_name]', ".
                   "admin_note    = '$admin_note', ".
				   "edit_time    = '$time', ".
                   "validate      = '$temp' WHERE des_id = '$id'";
     $db->query($sql);
	  /* 提示信息 */
      $link[0]['text'] = $_LANG['back_list'];
      $link[0]['href'] = 'user_design.php?act=list&' . list_link_postfix();

      sys_msg($_LANG['attradd_succed'], 0, $link);
}


/*------------------------------------------------------ */
//-- 编辑设计品
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_design')
{
    admin_priv('surplus_manage'); //权限判断
    $id = isset($_GET['id'])? intval($_GET['id']): 0;
	$sql = "SELECT * FROM ".$ecs->table('user_design')."WHERE des_id='$id'";
	$db_design = array();
	$db_design = $db->getRow($sql);
	
    $smarty->assign('db_design',        $db_design);
	
	$href = 'user_design.php?act=list&' . list_link_postfix();
    $smarty->assign('action_link', array('href' => $href, 'text' => '设计品管理'));
    assign_query_info();
    $smarty->display('edit_design_info.htm');
}

/*------------------------------------------------------ */
//-- 更新设计品信息
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'design_update')
{
    admin_priv('surplus_manage'); //权限判断
     $id = isset($_POST['id'])? intval($_POST['id']): 0;
	 $vote_num = isset($_POST['vote_num']) ? trim($_POST['vote_num']) : '';
	 $title = isset($_POST['title']) ? trim($_POST['title']) : '';
	 $design_idea = isset($_POST['design_idea']) ? trim($_POST['design_idea']) : '';
	 $validate = isset($_POST['confirm']) ? intval($_POST['confirm']) : 0;
	 $admin_note = isset($_POST['admin_note']) ? trim($_POST['admin_note']) : '';
	 $edit_time = time();
   
	$sql = "UPDATE ".$ecs->table('user_design'). " SET " .
	               "admin_user    = '$_SESSION[admin_name]', ".
                   "admin_note    = '$admin_note', ".
				   "vote_num      = '$vote_num', ".
				   "title         = '$title', ".
				   "design_idea   = '$design_idea', ".
				   "validate      = '$validate', ".
                   "edit_time     = '$edit_time' WHERE des_id = '$id'";
	
    $db->query($sql);
    /* 提示信息 */
      $link[0]['text'] = $_LANG['back_list'];
      $link[0]['href'] = 'user_design.php?act=list&' . list_link_postfix();

      sys_msg($_LANG['attradd_succed'], 0, $link);
}


/*------------------------------------------------------ */
//-- 编辑设计思路
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_desigin_idea')
{
    /* 检查权限 */
    check_authz_json('users_manage');

    $id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
    $design_idea = empty($_REQUEST['val']) ? '' : json_str_iconv(trim($_REQUEST['val']));
    $sql = "UPDATE " .$ecs->table('user_design'). " SET " .
				   "design_idea   = '$design_idea' WHERE des_id = '$id'";; 

        if ($m = $db->query($sql))
        {

            make_json_result(stripcslashes($design_idea));
        }
        else
        {
            $msg =  $GLOBALS['_LANG']['edit_user_failed'];
            make_json_error($msg);
        }
   
}

/*------------------------------------------------------ */
//-- 编辑投票数
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_vote_num')
{
    /* 检查权限 */
    check_authz_json('users_manage');

    $id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
    $vote_num = empty($_REQUEST['val']) ? '' : json_str_iconv(trim($_REQUEST['val']));
    $sql = "UPDATE " .$ecs->table('user_design'). " SET " .
				   "vote_num   = '$vote_num' WHERE des_id = '$id'";; 

        if ($m = $db->query($sql))
        {

            make_json_result(stripcslashes($vote_num));
        }
        else
        {
            $msg =  $GLOBALS['_LANG']['edit_user_failed'];
            make_json_error($msg);
        }
   
}

/*------------------------------------------------------ */
//-- 编辑s设计品标题
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_title')
{
    /* 检查权限 */
    check_authz_json('users_manage');

    $id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
    $title = empty($_REQUEST['val']) ? '' : json_str_iconv(trim($_REQUEST['val']));
    $sql = "UPDATE " .$ecs->table('user_design'). " SET " .
				   "title   = '$title' WHERE des_id = '$id'";; 

        if ($m = $db->query($sql))
        {

            make_json_result(stripcslashes($title));
        }
        else
        {
            $msg =  $GLOBALS['_LANG']['edit_user_failed'];
            make_json_error($msg);
        }
   
}



/*------------------------------------------------------ */
//-- 获取设计品信息
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'get_design_info')
{
    
    $cid = isset($_REQUEST['id'])?intval($_REQUEST['id']):0;
    if (empty($order_id))
    {
        make_json_response('', 1, $_LANG['error_get_goods_info']);
    }
    $card_info = array();
    $sql = "SELECT c.*, u.mobile_phone as phone " .
            "FROM " . $ecs->table('users_card') . " WHERE o.order_id = '{$order_id}' ";
    $card_info = $db->getRow($sql);
	     
    $smarty->assign('card_info', $card_info);
    $str = $smarty->fetch('bank_card_info.htm');
    $cards[] = array('card_id' => $id, 'str' => $str);
    make_json_result($cards);
}


/*------------------------------------------------------ */
//-- 批量删除设计品
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'batch_remove')
{
    /* 检查权限 */
    admin_priv('users_drop');

    if (isset($_POST['checkboxes']))
    {
        $sql = "SELECT title FROM " . $ecs->table('user_design') . " WHERE des_id " . db_create_in($_POST['checkboxes']);
        $col = $db->getCol($sql);
        $title = implode(',',addslashes_deep($col));
        $count = count($col);
        $sql = "DELETE  FROM " . $ecs->table('user_design') . " WHERE des_id " . db_create_in($_POST['checkboxes']);
		$m = $db->query($sql);
		if($m>0){
        admin_log($title, 'batch_remove', 'users');
        $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'user_design.php?act=list');
        sys_msg(sprintf('已经成功删除了 %d 个设计品。', $count), 0, $lnk);
		}
    }
    else
    {
        $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'user_design.php?act=list');
        sys_msg("没有该设计品信息", 0, $lnk);
    }
}


/*------------------------------------------------------ */
//-- 删除设计品
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'remove')
{
    /* 检查权限 */
    admin_priv('users_drop');

    $sql = "DELETE FROM " . $ecs->table('user_design') . " WHERE des_id = '" . $_GET['id'] . "'";
    $m = $db->query($sql);
    /* 通过插件来删除用户 */


    /* 提示信息 */
    $link[] = array('text' => $_LANG['go_back'], 'href'=>'user_design.php?act=list');
    sys_msg(sprintf('该设计品删除成功'), 0, $link);
}

/**
 *  返回设计品列表数据
 *
 * @access  public
 * @param
 *
 * @return void
 */
 
function design_list()
{
    $result = get_filter();
    if ($result === false)
    {
        /* 过滤条件 */
        $filter['keywords'] = empty($_REQUEST['keywords']) ? '' : trim($_REQUEST['keywords']);
        if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1)
        {
            $filter['keywords'] = json_str_iconv($filter['keywords']);
        }
        $filter['process_type'] = empty($_REQUEST['process_type']) ? -1 : intval($_REQUEST['process_type']);
        $filter['start_date'] = empty($_REQUEST['start_date']) ? 0 : local_strtotime($_REQUEST['start_date']);
        $filter['end_date'] = empty($_REQUEST['end_date']) ? 0 : local_strtotime($_REQUEST['end_date']);

        $filter['sort_by']    = empty($_REQUEST['sort_by'])    ? 'des_id'   : trim($_REQUEST['sort_by']);
        $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC'     : trim($_REQUEST['sort_order']);
        $ex_where = ' WHERE 1 ';
        if ($filter['keywords'])
        {
            $ex_where .= " AND user_name LIKE '%" . mysql_like_quote($filter['keywords']) ."%'";
        }
        if ($filter['process_type']>-1&&$filter['process_type']!=3)
        {
           $ex_where .= "AND validate =". $filter['process_type'];
        }
		elseif ($filter['process_type']==3)
        {
           $ex_where .= "AND validate = 0";
        }
		
        if ($filter['start_date'])
        {
             $ex_where .=" AND edit_time >= '$filter[start_date]' ";
        }
        if ($filter['end_date'])
        {
            $ex_where .=" AND edit_time < '$filter[end_date]' ";
        }

        $filter['record_count'] = $GLOBALS['db']->getOne("SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('user_design') . $ex_where);

        /* 分页大小 */
        $filter = page_and_size($filter);
        $sql = "SELECT *  FROM " . $GLOBALS['ecs']->table('user_design') . " " . $ex_where ." ORDER by " . $filter['sort_by'] . ' ' 
		       . $filter['sort_order'] ." LIMIT " . $filter['start'] . ',' . $filter['page_size'];

        $filter['keywords'] = stripslashes($filter['keywords']);
        set_filter($filter, $sql);
    }
    else
    {
        $sql    = $result['sql'];
        $filter = $result['filter'];
    }

    $design_list = $GLOBALS['db']->getAll($sql);

    $count = count($design_list);
    for ($i=0; $i<$count; $i++)
    { 
        $design_list[$i]['edit_time'] = local_date($GLOBALS['_CFG']['date_format'], $design_list[$i]['edit_time']);
    }

    $arr = array('design_list' => $design_list, 'filter' => $filter,
        'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}
?>