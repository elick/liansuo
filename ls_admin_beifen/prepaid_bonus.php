<?php
define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');


/*------------------------------------------------------ */
//-- 用户银行卡号列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{   admin_priv('users_manage');
 
	$bonus_list = prepaidbonus_list();
    $smarty->assign('bonus_list',      $bonus_list['bonus_list']);
    $smarty->assign('filter',          $bonus_list['filter']);
    $smarty->assign('record_count',    $bonus_list['record_count']);
    $smarty->assign('page_count',      $bonus_list['page_count']);
	 $smarty->assign('action_link',  array('text' => $_LANG['12_add_bonus'], 'href'=>'prepaid_bonus.php?act=add'));
    $smarty->assign('full_page',    1);
    $smarty->assign('sort_card_id', '<img src="images/sort_desc.gif">');

    assign_query_info();
	$smarty->display('preBonus_list.htm');


}

/*------------------------------------------------------ */
//-- ajax返回用户列表
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    $bonus_list = prepaidbonus_list();
    $smarty->assign('bonus_list',      $bonus_list['bonus_list']);
    $smarty->assign('filter',          $bonus_list['filter']);
    $smarty->assign('record_count',    $bonus_list['record_count']);
    $smarty->assign('page_count',      $bonus_list['page_count']);

    $sort_flag  = sort_flag($bonus_list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('preBonus_list.htm'), '', array('filter' => $bonus_list['filter'], 'page_count' => $bonus_list['page_count']));
}

/*------------------------------------------------------ */
//-- 添加会员帐号
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'add')
{
    /* 检查权限 */
    admin_priv('users_manage');

    /* 取出注册扩展字段 */
    $sql = 'SELECT * FROM ' . $ecs->table('user_rank') . ' WHERE 1 ';
    $rank_info_list = $db->getAll($sql);
    $smarty->assign('rank_info_list',  $rank_info_list);

    $smarty->assign('ur_here',          $_LANG['12_bonus_add']);
    $smarty->assign('action_link',      array('text' => $_LANG['11_prepaid_bonus'], 'href'=>'prepaid_bonus.php?act=list'));
    $smarty->assign('form_action',      'insert');
    $smarty->assign('user',             $user);

    assign_query_info();
    $smarty->display('bonus_info.htm');
}

/*------------------------------------------------------ */
//-- 审核会银行卡
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'confirm_card')
{
    /* 检查权限 */
    admin_priv('surplus_manage');

    /* 初始化 */
    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;

    /* 如果参数不合法，返回 */
    if ($id == 0)
    {
        ecs_header("Location: users.php?act=card_list\n");
        exit;
    }

    /* 查询当前用户银行卡的信息 */
    $card_info = array();
    $card_info = $db->getRow("SELECT * FROM " .$ecs->table('users_card'). " WHERE card_id = '$id'");
    $card_info['add_time'] = local_date($_CFG['time_format'], $card_info['add_time']);

 
    /* 模板赋值 */
  
    $smarty->assign('card_info',      $card_info);
  

    /* 页面显示 */
    assign_query_info();
    $smarty->display('user_card_confirm.htm');
}

/*------------------------------------------------------ */
//-- 审核会银行卡
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'action_bonus')
{
    /* 检查权限 */
    admin_priv('surplus_manage');

    /* 初始化 */
    $id = isset($_POST['id']) ? intval($_POST['id']) : 0;
	$confirm = isset($_POST['confirm']) ? intval($_POST['confirm']) : 0;
	$is_paid = isset($_POST['is_paid']) ? intval($_POST['is_paid']) : 0;
	$admin_note = isset($_POST['admin_note']) ? trim($_POST['admin_note']) : 0;
    $time = gmtime();

	if($confirm == 0){
	  $temp = 2 ;
	  }
	else $temp = 1;
	

     $sql = "UPDATE " .$ecs->table('prepaid_bonus'). " SET ".
                   "admin_user    = '$_SESSION[admin_name]', ".
                   "admin_note    = '$admin_note', ".
				   "is_paid       = '$is_paid', ".
				   "edit_time     = '$time', ".
                   "status        = '$temp' WHERE pre_id = '$id'";
     $db->query($sql);
	  /* 提示信息 */
      $link[0]['text'] = $_LANG['back_list'];
      $link[0]['href'] = 'prepaid_bonus.php?act=list&' . list_link_postfix();

      sys_msg($_LANG['attradd_succed'], 0, $link);
}


/*------------------------------------------------------ */
//-- 编辑银行卡页面
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_bonus' or $_REQUEST['act'] == 'desc_bonus')
{
    admin_priv('surplus_manage'); //权限判断
    $id = isset($_GET['id'])? intval($_GET['id']): 0;
	$sql = "SELECT * FROM ".$ecs->table('prepaid_bonus')."WHERE pre_id='$id'";
	$bonus_info = array();
	$bonus_info = $db->getRow($sql);
	$bonus_info['pay_status'] = $bonus_info['is_paid']==1? "已支付":"未支付";
	$bonus_info['pay_time'] = local_date($_CFG['time_format'], $bonus_info['pay_time']);
	
	
    $smarty->assign('bonus_info',        $bonus_info);
	
	$href = 'prepaid_bonus.php?act=list&' . list_link_postfix();
    $smarty->assign('action_link', array('href' => $href, 'text' => '预存红包列表'));
    assign_query_info();
    $smarty->display('edit_bonus.htm');
}

/*------------------------------------------------------ */
//-- 更新银行卡页面
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'bonus_add_act')
{
    admin_priv('surplus_manage'); //权限判断
    
	
     $username    = isset($_POST['username']) ? trim($_POST['username']) : '';
	 $rank_id     = isset($_POST['user_rank']) ? intval($_POST['user_rank']) : 0;
	 $is_paid     = isset($_POST['is_paid']) ? intval($_POST['is_paid']) : 0;
	 $status      = isset($_POST['status']) ? intval($_POST['status']) : 0;
	 $admin_note  = isset($_POST['admin_note']) ? trim($_POST['admin_note']) : '';
	 $time        = gmtime();
	 
	 $sql = "SELECT user_id FROM ".$ecs->table('users')." WHERE user_name = '$username'";
	 $user_id = $db->getOne($sql);
	 if($user_id<0){
		 $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'prepaid_bonus.php?act=list');
           sys_msg("您输入的用户不存在", 0, $lnk);
		 }
		 
	else{
	$sql = "SELECT rank_name, upd_money, expire_time FROM ".$ecs->table('user_rank')." WHERE rank_id=".$rank_id;
	$user_rank = $db->getRow($sql);
	$end_time = $user_rank['expire_time']*24*60*60 +  $time;
	
     $sql = 'INSERT INTO '. $ecs->table('prepaid_bonus') . " (`user_id`, `user_name`, `total_money`, `rank_id`, `status`, `is_paid`,`available_money`,
	         `add_time`, `pay_time`,`end_time`,`edit_time`, `admin_note`, `admin_user`) VALUES ('$user_id', '$username', '$user_rank[upd_money]', '$rank_id', 
			  '$status', '$is_paid', '$user_rank[upd_money]', '$time', '$time', '$end_time', '$time','$admin_note','$_SESSION[admin_name]')";
	
	
    $db->query($sql);
    /* 提示信息 */
      $link[0]['text'] = $_LANG['back_list'];
      $link[0]['href'] = 'prepaid_bonus.php?act=list&' . list_link_postfix();

      sys_msg($_LANG['attradd_succed'], 0, $link);
	}
}

/*------------------------------------------------------ */
//-- 删除用户帐户
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'remove')
{
    /* 检查权限 */
    admin_priv('users_drop');

    $sql = "DELETE FROM " . $ecs->table('prepaid_bonus') . " WHERE pre_id = '" . $_GET['id'] . "'";
    $m = $db->query($sql);
    /* 通过插件来删除用户 */


    /* 提示信息 */
    $link[] = array('text' => $_LANG['go_back'], 'href'=>'prepaid_bonus.php?act=list');
    sys_msg(sprintf('该预存红包纪录删除成功'), 0, $link);
}


/*------------------------------------------------------ */
//-- 批量删除用户帐户
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'batch_remove')
{
    /* 检查权限 */
    admin_priv('users_drop');

    if (isset($_POST['checkboxes']))
    {
        $sql = "SELECT user_name FROM " . $ecs->table('prepaid_bonus') . " WHERE pre_id " . db_create_in($_POST['checkboxes']);
        $col        = $db->getCol($sql);
        $user_bonus = implode(',',addslashes_deep($col));
        $count = count($col);
        $sql = "DELETE  FROM " . $ecs->table('prepaid_bonus') . " WHERE pre_id " . db_create_in($_POST['checkboxes']);
		$m = $db->query($sql);
		if($m>0){
        admin_log($user_bonus, 'batch_remove', 'users');
        $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'prepaid_bonus.php?act=list');
        sys_msg(sprintf('已经成功删除了 %d 个预存红包纪录。', $count), 0, $lnk);
		}
    }
    else
    {
        $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'prepaid_bonus.php?act=list');
        sys_msg("没有该预存红包纪的录信息", 0, $lnk);
    }
}



/**
 *  返回银行卡列表数据
 *
 * @access  public
 * @param
 *
 * @return void
 */
 
function prepaidbonus_list()
{
    $result = get_filter();
    if ($result === false)
    {
        /* 过滤条件 */
        $filter['keywords'] = empty($_REQUEST['keywords']) ? '' : trim($_REQUEST['keywords']);
        if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1)
        {
            $filter['keywords'] = json_str_iconv($filter['keywords']);
        }
        $filter['process_type'] = empty($_REQUEST['process_type']) ? -1 : intval($_REQUEST['process_type']);
        $filter['start_date'] = empty($_REQUEST['start_date']) ? 0 : local_strtotime($_REQUEST['start_date']);
        $filter['end_date'] = empty($_REQUEST['end_date']) ? 0 : local_strtotime($_REQUEST['end_date']);

        $filter['sort_by']    = empty($_REQUEST['sort_by'])    ? 'pre_id' : trim($_REQUEST['sort_by']);
        $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC'     : trim($_REQUEST['sort_order']);
         
        $ex_where = ' WHERE 1 ';
        if ($filter['keywords'])
        {
            $ex_where .= " AND user_name LIKE '%" . mysql_like_quote($filter['keywords']) ."%'";
        }
        if ($filter['process_type']==1)
        {
           $ex_where .= "AND is_paid =1";
        }
		elseif ($filter['process_type']==2)
        {
           $ex_where .= "AND is_paid = 0";
        }
		
        if ($filter['start_date'])
        {
             $ex_where .=" AND pay_time >= '$filter[start_date]' ";
        }
        if ($filter['end_date'])
        {
            $ex_where .=" AND pay_time < '$filter[end_date]' ";
        }

        $filter['record_count'] = $GLOBALS['db']->getOne("SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('prepaid_bonus') . $ex_where);

        /* 分页大小 */
        $filter = page_and_size($filter);
        $sql = "SELECT * ". " FROM " . $GLOBALS['ecs']->table('prepaid_bonus') . $ex_where ." ORDER by " . $filter['sort_by'] . ' ' 
		. $filter['sort_order'] ." LIMIT " . $filter['start'] . ',' . $filter['page_size'];

        $filter['keywords'] = stripslashes($filter['keywords']);
        set_filter($filter, $sql);
    }
    else
    {
        $sql    = $result['sql'];
        $filter = $result['filter'];
    }

    $bonus_list = $GLOBALS['db']->getAll($sql);

    $count = count($bonus_list);
    for ($i=0; $i<$count; $i++)
    {  
        $bonus_list[$i]['pay_time']  = local_date($GLOBALS['_CFG']['time_format'], $bonus_list[$i]['pay_time']);
		$bonus_list[$i]['end_time']  = local_date($GLOBALS['_CFG']['date_format'], $bonus_list[$i]['end_time']);
		$bonus_list[$i]['edit_time'] = local_date($GLOBALS['_CFG']['date_format'], $bonus_list[$i]['edit_time']);
	  
    }

    $arr = array('bonus_list' => $bonus_list, 'filter' => $filter,
        'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}
?>
