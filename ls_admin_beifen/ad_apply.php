<?php
define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');

/* 关于厂家申请广告发布的 */
/* ---begin--- */

//显示列表
if ($_REQUEST['act'] == 'ad_apply_list')
{   admin_priv('users_manage');
 
	$apply = ad_apply_list();
    $smarty->assign('apply_list',      $apply['apply_list']);
    $smarty->assign('filter',          $apply['filter']);
    $smarty->assign('record_count',    $apply['record_count']);
    $smarty->assign('page_count',      $apply['page_count']);
	//$smarty->assign('action_link',  array('text' => $_LANG['12_add_bonus'], 'href'=>'ad_apply.php?act=add'));
    $smarty->assign('full_page',    1);
    $smarty->assign('sort_card_id', '<img src="images/sort_desc.gif">');

    assign_query_info();
	$smarty->display('ad_apply_list.htm');


}

//删除申请
elseif ($_REQUEST['act'] == 'remove_apply')
{
    /* 检查权限 */
    admin_priv('users_drop');

    $sql = "DELETE FROM " . $ecs->table('join') . " WHERE jn_id = '" . $_GET['id'] . "'";
    $m = $db->query($sql);
    /* 通过插件来删除用户 */


    /* 提示信息 */
    $link[] = array('text' => $_LANG['go_back'], 'href'=>'ad_apply.php?act=ad_apply_list');
    sys_msg(sprintf('该申请已经删除！'), 0, $link);
}

//批量删除
elseif ($_REQUEST['act'] == 'remove_many_apply')
{
    /* 检查权限 */
    admin_priv('users_drop');

    if (isset($_POST['checkboxes']))
    {
        $sql = "SELECT user_id FROM " . $ecs->table('join') . " WHERE jn_id " . db_create_in($_POST['checkboxes']);
        $col        = $db->getCol($sql);
        $user_bonus = implode(',',addslashes_deep($col));
        $count = count($col);
        $sql = "DELETE  FROM " . $ecs->table('join') . " WHERE jn_id " . db_create_in($_POST['checkboxes']);
		$m = $db->query($sql);
		if($m>0){
			$lnk[] = array('text' => $_LANG['go_back'], 'href'=>'ad_apply.php?act=ad_apply_list');
			sys_msg(sprintf('已经成功删除了 %d 个申请纪录。', $count), 0, $lnk);
		}
    }
    else
    {
        $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'ad_apply.php?act=ad_apply_list');
        sys_msg("没有该申请的记录信息", 0, $lnk);
    }
}

//审核操作
elseif ($_REQUEST['act'] == 'edit_apply')
{
    admin_priv('surplus_manage'); //权限判断
    $id = isset($_GET['id'])? intval($_GET['id']): 0;
	$sql = "UPDATE ".$ecs->table('join')." SET status = 2 WHERE jn_id='$id'";
	$result = $db->query($sql);
	file_put_contents('a.txt',$result);
	echo "<script>alert('已审核');window.location.href='ad_apply.php?act=ad_apply_list'</script>";
}

//排序操作
elseif ($_REQUEST['act'] == 'query')
{
    $apply_list = ad_apply_list();
    $smarty->assign('apply_list',      $apply_list['apply_list']);
    $smarty->assign('filter',          $apply_list['filter']);
    $smarty->assign('record_count',    $apply_list['record_count']);
    $smarty->assign('page_count',      $apply_list['page_count']);

    $sort_flag  = sort_flag($apply_list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('ad_apply_list.htm'), '', array('filter' => $apply_list['filter'], 'page_count' => $apply_list['page_count']));
}

/* ---end--- */
/* 关于厂家申请广告发布的 */



function ad_apply_list(){
	$result = get_filter();
    if ($result === false)
    {
        /* 过滤条件 */
        $filter['keywords'] = empty($_REQUEST['keywords']) ? '' : trim($_REQUEST['keywords']);
        if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1)
        {
            $filter['keywords'] = json_str_iconv($filter['keywords']);
        }
        $filter['process_type'] = empty($_REQUEST['process_type']) ? -1 : intval($_REQUEST['process_type']);
		//$filter['jn_id'] = empty($_REQUEST['jn_id']) ? 0 : intval($_REQUEST['jn_id']);
        $filter['start_date'] = empty($_REQUEST['start_date']) ? 0 : local_strtotime($_REQUEST['start_date']);
        $filter['end_date'] = empty($_REQUEST['end_date']) ? 0 : local_strtotime($_REQUEST['end_date']);

        $filter['sort_by']    = empty($_REQUEST['sort_by'])    ? 'jn_id' : trim($_REQUEST['sort_by']);
        $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC'     : trim($_REQUEST['sort_order']);
         
        $ex_where = ' WHERE 1 ';
        if ($filter['keywords'])
        {
            $ex_where .= " AND phone LIKE '%" . mysql_like_quote($filter['keywords']) ."%'";
        }
        if ($filter['process_type']==1)
        {
           $ex_where .= "AND status =0";
        }
		elseif ($filter['process_type']==2)
        {
           $ex_where .= "AND status = 1";
        }
		elseif ($filter['process_type']==3)
        {
           $ex_where .= "AND status = 2";
        }
		
        if ($filter['start_date'])
        {
             $ex_where .=" AND pay_time >= '$filter[start_date]' ";
        }
        if ($filter['end_date'])
        {
            $ex_where .=" AND pay_time < '$filter[end_date]' ";
        }

        $filter['record_count'] = $GLOBALS['db']->getOne("SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('join') . $ex_where);

        /* 分页大小 */
        $filter = page_and_size($filter);
        $sql = "SELECT * ". " FROM " . $GLOBALS['ecs']->table('join') . $ex_where ." ORDER by " . $filter['sort_by'] . ' ' 
		. $filter['sort_order'] ." LIMIT " . $filter['start'] . ',' . $filter['page_size'];

        $filter['keywords'] = stripslashes($filter['keywords']);
        set_filter($filter, $sql);
    }
    else
    {
        $sql    = $result['sql'];
        $filter = $result['filter'];
    }
	
    $apply_list = $GLOBALS['db']->getAll($sql);

    $count = count($apply_list);
    for ($i=0; $i<$count; $i++)
    {
        $apply_list[$i]['pay_time']  = local_date($GLOBALS['_CFG']['time_format'], $apply_list[$i]['pay_time']);
		$apply_list[$i]['add_time']  = local_date($GLOBALS['_CFG']['date_format'], $apply_list[$i]['add_time']);
		$apply_list[$i]['edit_time'] = local_date($GLOBALS['_CFG']['date_format'], $apply_list[$i]['edit_time']);
    }

    $arr = array('apply_list' => $apply_list, 'filter' => $filter,
        'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}
?>
