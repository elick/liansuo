<?php

/**
 * ECSHOP 品牌列表
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: brand.php 17217 2011-01-19 06:29:08Z liubo $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

/*------------------------------------------------------ */
//-- INPUT
/*------------------------------------------------------ */
$sql = "SELECT article_id , add_time, title FROM ". $ecs->table('article') ."WHERE cat_id = 66 order by add_time DESC LIMIT 8";
$tips = $GLOBALS['db']->getAll($sql);
$num = count($tips);
$tip1 = $tips[0];
$tip2 = $tips[1];
$tip3 = $tips[2];
$tipc = array();
 for($i=3; $i<$num; $i++){
   $tips[$i]['add_time'] = local_date($GLOBALS['_CFG']['date_format'], $tips[$i]['add_time']);
   $tipc[]=$tips[$i];
    
 }


$sql = "SELECT article_id ,cat_id, title, file_url as url FROM ". $ecs->table('article') . " WHERE cat_id = 53 
         order by add_time DESC LIMIT 2";
$news1_L = $GLOBALS['db']->getAll($sql); 
$sql = "SELECT article_id ,cat_id, title, file_url as url FROM ". $ecs->table('article') . " WHERE cat_id = 54
        order by add_time DESC LIMIT 2";
$news1_R = $GLOBALS['db']->getAll($sql); 

$sql = "SELECT cat_id , cat_name FROM " .$ecs->table('article_cat') ." WHERE cat_type =6";
$temp_news = $GLOBALS['db']->getAll($sql);
$sql = "SELECT ad_code FROM " .$ecs->table('ad') ." WHERE position_id =1";
$news_ad = $GLOBALS['db']->getAll($sql);
 $i=0;
foreach($temp_news as $key => $item){
  $sql = "SELECT article_id, title, description FROM ". $ecs->table('article') ."WHERE cat_id='$item[cat_id]' order by 
          add_time ASC LIMIT 5";
  $temp_news[$key]['news']= $GLOBALS['db']->getAll($sql);
  $temp_news[$key]['ad'] ="data/afficheimg/".$news_ad[$i++]['ad_code'];
 }
 
     $sql = " SELECT * FROM " . $ecs->table("ad") ." WHERE position_id=20 ORDER BY ad_id ASC";
	 $ad = $db->getAll($sql);
	 
	 foreach($ad as $key => $item_ad){
		   $ad[$key]['ad_code']="data/afficheimg/".$item_ad['ad_code'];
		  }



$smarty->assign('helps',                get_shop_help());        // 网店帮助
$smarty->assign('categories',           get_categories_tree(0)); // 分类树
$smarty->assign('tip1',                   $tip1);
$smarty->assign('tip2',                   $tip2);
$smarty->assign('tip3',                   $tip3);
$smarty->assign('tipc',                   $tipc);
$smarty->assign('news1_L',                $news1_L);
$smarty->assign('news1_R',                $news1_R);
$smarty->assign('ad',                     $ad);
$smarty->assign('temp_news',              $temp_news);

$smarty->display('zixun.dwt');



?>