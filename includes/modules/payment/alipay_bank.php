<?php

/**
 * ECSHOP 支付宝纯网关(网银支付) 插件
 * ============================================================================
 * tomener的博客-专注于PHP技术
 * 网站地址: http://blog.sina.com/tomener
 * @author QQ 863758705
 * @date 2013-08-20
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/alipay.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'alipay_bank_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';

    /* 作者 */
    $modules[$i]['author']  = '梦时网';

    /* 网址 */
    $modules[$i]['website'] = 'http://www.alipay.com';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.2';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name' => 'alipay_account',           'type' => 'text',   'value' => ''),
        array('name' => 'alipay_key',               'type' => 'text',   'value' => ''),
        array('name' => 'alipay_partner',           'type' => 'text',   'value' => '')
    );

    return;
}

/**
 * 类
 */
class alipay_bank
{

    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function alipay_bank()
    {
    }

    function __construct()
    {
        $this->alipay_bank();
    }

    /**
     * 生成支付代码
     * @param   array   $order      订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        if (!defined('EC_CHARSET'))
        {
            $charset = 'utf-8';
        }
        else
        {
            $charset = EC_CHARSET;
        }


        $service = 'create_direct_pay_by_user'; //纯网关使用即时到帐接口
        $agent = 'C4335319945672464113';

        $parameter = array(
            'service'           => $service,
			'partner'           => $payment['alipay_partner'],
			'payment_type'      => 1,
            '_input_charset'    => $charset,
            'notify_url'        => return_url(basename(__FILE__, '.php')),
            //'return_url'        => $GLOBALS['ecs']->url() . 'respond.php',
			'defaultbank'			=> $order['default_bank'], //默认支付方式
			//'anti_phishing_key'	=> '', //防钓鱼时间戳
			//'exter_invoke_ip'	=> '', //客户端的IP地址
            /* 业务参数 */
            'subject'           => $order['order_sn'],
            'out_trade_no'      => $order['order_sn'] . $order['log_id'],
            'price'             => $order['order_amount'],
            'quantity'          => 1,
			'seller_email'      => $payment['alipay_account']
        );
		if(!empty($_SESSION["alipay_token"]))
		$parameter['token']=$_SESSION["alipay_token"];
        ksort($parameter);
        reset($parameter);

        $param = '';
        $sign  = '';

        foreach ($parameter AS $key => $val)
        {
            $param .= "$key=" .urlencode($val). "&";
            $sign  .= "$key=$val&";
        }

        $param = substr($param, 0, -1);
        $sign  = substr($sign, 0, -1). $payment['alipay_key'];

        $pay_url = 'https://mapi.alipay.com/gateway.do?'.$param. '&sign='.md5($sign).'&sign_type=MD5';
        return $pay_url;
    }

    /**
     * 响应操作
     */
    function respond()
    {  
        if (!empty($_POST))
        {
            foreach($_POST as $key => $data)
            {
                $_GET[$key] = $data;
            }
        }
        $payment  = get_payment($_GET['code']);
        $seller_email = rawurldecode($_GET['seller_email']);
        $order_sn = str_replace($_GET['subject'], '', $_GET['out_trade_no']);
        $order_sn = trim($order_sn);
         
        $this->logResult('订单号：'.$_GET['subject'].'; 订单号+支付id：'.$_GET['out_trade_no'].'; 订单状态：'.$_GET['trade_status']);
		/* 检查支付的金额是否相符 */
        if (!check_money($order_sn, $_GET['total_fee']))
        {
            return false;
        }
        
        /* 检查数字签名是否正确 */
        ksort($_GET);
        reset($_GET);

        $sign = '';
        foreach ($_GET AS $key=>$val)
        {
            if ($key != 'sign' && $key != 'sign_type' && $val != '' && $key != 'code')
            {
                $sign .= "$key=$val&";
            }
        }

        $sign = substr($sign, 0, -1) . $payment['alipay_key'];
        //$sign = substr($sign, 0, -1) . ALIPAY_AUTH;
		$this->logResult('get_sign='.$_GET['sign'].'md5_sign='.md5($sign));
        if (md5($sign) != $_GET['sign'])
        { 
            return false;
        }
		//$this->logResult('success!成功。');
         
        if ($_GET['trade_status'] == 'WAIT_BUYER_PAY')
        {
            /* 改变订单状态 */
            order_paid($order_sn, PS_PAYING);

            return true;
        }
        elseif ($_GET['trade_status'] == 'TRADE_FINISHED')
        {
            /* 改变订单状态 */
            order_paid($order_sn);

            return true;
        }
        elseif ($_GET['trade_status'] == 'TRADE_SUCCESS')
        {
            /* 改变订单状态 */
            order_paid($order_sn, 2);
			
		     $user_id = $_SESSION['user_id'];
	        $mobile = $db->getOne("SELECT mobile_phone FROM ".$ecs->table('users'). " WHERE user_id = '$user_id'");
	        if($mobile){
	         include_once(ROOT_PATH .'includes/m_sms.php');
            require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/sms.php');
	         $content = $smarty->fetch('str:' . "您的订单：".$_GET['subject']."，已于".date('Y-m-d H:i:s',time())."付款成功。感谢您的购买！工作人员马上为您配货！");
	         /* 发送注册手机短信 */
	         $ret = sendsms($mobile, $content);
	           }

            return true;
        }
        else
        {
            return false;
        }
    }
	/*
	 * 写日志
	 */
	function logResult($word='') {
		$fp = fopen("alipay_bank_log.txt","a");
		flock($fp, LOCK_EX) ;
		fwrite($fp,"执行日期：".local_date("Y-m-d H:i:s",gmtime())."\r\n".$word."\r\n");
		flock($fp, LOCK_UN);
		fclose($fp);
	}
}

?>