<?php

/**
 * ECSHOP 定期删除已取消的订单
 *
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/Iorderdel.php';
if (file_exists($cron_lang))
{
    global $_LANG;

    include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'Iorderdel_desc';

    /* 作者 */
    $modules[$i]['author']  = 'Mec';

    /* 网址 */
    $modules[$i]['website'] = 'http://www.ecshop.com';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name' => 'Iorderdel_day', 'type' => 'select', 'value' => '30'),
    );

    return;
}

empty($cron['Iorderdel_day']) && $cron['Iorderdel_day'] = 2;

$deltime = gmtime() - $cron['ipdel_day'] * 3600 * 24;
$sql = "DELETE FROM " . $ecs->table('order_info') .
       "WHERE order_status=2 AND   add_time < '$deltime'";
$db->query($sql);

?>