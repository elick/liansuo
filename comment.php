<?php

/**
 * ECSHOP 提交用户评论
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: comment.php 17217 2011-01-19 06:29:08Z liubo $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/cls_json.php');
if (!isset($_REQUEST['cmt']) && !isset($_REQUEST['act']))
{
    /* 只有在没有提交评论内容以及没有act的情况下才跳转 */
    ecs_header("Location: ./\n");
    exit;
}
$_REQUEST['cmt'] = isset($_REQUEST['cmt']) ? json_str_iconv($_REQUEST['cmt']) : '';

$json   = new JSON;
$result = array('error' => 0, 'content' => '', 'message'=>'');

if (empty($_REQUEST['act']))
{ 
          
           /* 检查验证码 */
		  $cmt  = $json->decode($_REQUEST['cmt']);
          include_once('includes/cls_captcha.php');
          $validator = new captcha();
            if (!$validator->check_word($cmt->captcha))
            {
                $result['error']   = 1;
                $result['message'] = $_LANG['invalid_captcha'];
				
            }
            else{     
			 add_comment($cmt);
             $_SESSION['send_time'] = $cur_time;
			 $cmt->kind = 0;
			 
			 $comments = assign_comment($cmt->id, $cmt->type, 1, $cmt->kind);
	         $percent['pl1'] =floor((100*$comments['pager']['pl1'])/$comments['pager']['plall']);
	         $percent['pl2'] =floor((100*$comments['pager']['pl2'])/$comments['pager']['plall']);
	         $percent['pl3'] =floor((100*$comments['pager']['pl3'])/$comments['pager']['plall']);
	
    
             $smarty->assign('comment_type', $cmt->type);
             $smarty->assign('id',           $cmt->id);
	         $smarty->assign('cur',          $cmt->kind);
	         $smarty->assign('percent',      $percent);
             $smarty->assign('username',     $_SESSION['user_name']);
             $smarty->assign('email',        $_SESSION['email']);
             $smarty->assign('comments',     $comments['comments']);
             $smarty->assign('pager',        $comments['pager']);
			
			 $result['error']   = 0;
			 $result['content'] = $smarty->fetch("library/comments_list.lbi");
			 }
		
	die($json->encode($result));
 }

if (!empty($_REQUEST['act'])&&$_REQUEST['act']=='com')
{
    /*
     * act 参数为空
     * 默认为添加评论内容
     */
    $id = empty($_POST['id']) ? 0 : intval($_POST['id']);
	

    if ($_SESSION['user_id'] >0)
    {     
	              $sql = "SELECT COUNT(o.order_id)".  
                   " FROM " . $ecs->table('order_info'). " AS o, ".  
                   $ecs->table('order_goods') . " AS og ".  
                   " WHERE o.order_id = og.order_id".  
                   " AND o.user_id = '" . $_SESSION['user_id'] . "'".  
                   " AND og.goods_id = '" . $id . "'".  
                   " AND (o.order_status = '" . OS_CONFIRMED . "' or o.order_status = '" . OS_SPLITED . "') ".  
                   " AND (o.pay_status = '" . PS_PAYED . "' OR o.pay_status = '" . PS_PAYING . "') ".  
                   " AND (o.shipping_status = '" . SS_SHIPPED . "' OR o.shipping_status = '" . SS_RECEIVED . "') ";  
                   $bought_count = $db->getOne($sql);  
                if (!$bought_count)  
                 {  
                  $result['error']   = 1;  
                  $result['content'] = $_LANG['comment_brought'];  
                  }   
			    else{  
                $sql = "SELECT COUNT(comment_id) FROM " . $ecs->table('comment') .  
                    " WHERE comment_type = 0 AND user_id = '" . $_SESSION['user_id'] . "'".  
                    " AND id_value= '" . $id . "'";  
                $comment_count = $db->getOne($sql);  
                
				if($comment_count >= $bought_count){  
                    $result['error']   = 1;  
                    $result['content'] = '您已对此商品进行过评价！您可以继续购买以便再次评论。';                                        
                  }  
								 
				else{
				         
							
						     $sql = "SELECT goods_name ,goods_thumb FROM ".$GLOBALS['ecs']->table('goods')." WHERE goods_id = ".$id;
                             $temp = $db->getAll($sql);
                             $goods =$temp['0'];
						      $result['content'] = '<tbody><tr><td class="top-left"></td><td class="top"></td><td class="top-right"></td></tr><tr>
							  <td class="left"></td><td class="Msg-inner"><div class="Msg-content" style="display: block;"><div class="addComment mt20">
      <div class="noticeBox zxDetail">
	    <dl name="tips_1" style="display: block;">
          <dd>您可在购买完成后，对商品进行评论，您的评价对我们很重要。我们会做出更大的努力让您满意！</dd>
          <dd class="c_ba2636">温馨提示：您可以在会员中心查看留言回复等消息动态。</dd>
        </dl>
      </div>
      <div class="bigMsg addComm">
      <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
          <td width="126" align="center"><a target="_blank" href="" class="aImgComm">
		  <img height="70" title="'.$goods['goods_name'].'" alt="'.$goods['goods_name'].'" src="'.$goods['goods_thumb'].'"></a></td>
          <td width="240" align="left" title="'.$goods['goods_name'].'">
			<a target="_blank" href="" class="aBase">'.$goods['goods_name'].'</a></td>
          <td width="140" align="center">&nbsp;</td>
          <td align="left"><a class="button brighter_button" href="javascript:buy('.$id.')"><span>加入购物车</span></a></td>
        </tr>
      </tbody></table>
      </div> <div class="addCommBox">
		<form action="javascript:;" onsubmit="submitComment(this)" method="post" class="" style="width:100%;">		 
			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="commTb1">
			  <tbody>
			  <tr>
				<th width="95" valign="top" height="34">商品评分：</th>
				<td valign="top" height="34" class="radio_wrap label">
				<fieldset>
						<span class="star-rating-control"> <input type="radio" class="rank_star" name="comment_rank" value="1" title="很差"/>
                              <input type="radio" class="rank_star" name="comment_rank" value="2" title="不太满意"/>
                              <input type="radio" class="rank_star" name="comment_rank" value="3" title="一般"/>
                              <input type="radio" class="rank_star" name="comment_rank" value="4" title="还不错"/>
                              <input type="radio" checked="checked" class="rank_star" name="comment_rank" value="5"  title="很好"/>
							  </span> 
				     <em id="star_tip"></em>
				</fieldset>
				</td>
			  </tr>
			  <tr>
				<th valign="top">评价内容：</th>
				<td>
					<textarea name="content" rows="2" cols="20" tabindex="7" id="cf_content"></textarea>
					<p class="errTips"></p>
				</td>
			  </tr>
			  			  <tr>
				<th valign="top">验&nbsp;&nbsp;证&nbsp;&nbsp;码：</th>
				<td>
					<input type="text" name="captcha" size="6" tabindex="8" class="captcha" id="cf_captcha">
					<img src="captcha.php?" alt="验证码" class="captcha tip" title="点击刷新验证码" onclick="this.src=\'captcha.php?\'+Math.random()">
				</td>
			  </tr>
			  			  <tr>
				<th>&nbsp;</th>
				<td>
					<input type="submit" value="提交评论" tabindex="9" class="submit btn_s2_b">
					<input type="hidden" name="cmt_type" value="0">
					<input type="hidden" name="id" value="'.$id.'">				
				</td>
			  </tr>
			</tbody></table>
		  </form></div></div></div>
		  </td><td class="right"></td></tr><tr><td class="bottom-left"></td><td class="bottom"></td><td class="bottom-right"></td></tr></tbody>
		  ';
        $result['error'] = 0;
						
				 }   
            }  
	    }
   else{
	   $result['error']= 99;
	   $result['content']="只有登录用户才能够发表评论";
	 
	 }
	
}

elseif(!empty($_REQUEST['act'])&&$_REQUEST['act']=='comment_usefull')
 { $id = !empty($_GET['id'])   ? intval($_GET['id'])  : 0;
   $type = !empty($_GET['type'])   ? trim($_GET['type'])  : '';
    $browser  = get_user_browser();
    $ip       = real_ip();
	if($browser==$_SESSION['browser']&& $ip==$_SESSION['ip']){
		$result['error'] = 1; 
		$result['content'] = "您已经评论过了，请不要重复操作";
		}
   else{
	   $_SESSION['browser'] = $browser;
	   $_SESSION['ip'] = $ip;
	
     $sql = "UPDATE " .$GLOBALS['ecs']->table('comment'). " SET".
           " ".$type." = ".$type." + 1 ".
           " WHERE comment_id = '" .$id. "'";
    $GLOBALS['db']->query($sql);   
	$result['error'] = 0; 
	}
 }




elseif(!empty($_REQUEST['act'])&&$_REQUEST['act']=='gotopage')
{
    /*
     * act 参数不为空
     * 默认为评论内容列表
     * 根据 _GET 创建一个静态对象
     */
    $cmt = new stdClass();
    $cmt->id   = !empty($_GET['id'])   ? intval($_GET['id'])   : 0;
    $cmt->type = !empty($_GET['type']) ? intval($_GET['type']) : 0;
    $cmt->page = isset($_GET['page'])   && intval($_GET['page'])  > 0 ? intval($_GET['page'])  : 1;
	$cmt->kind = !empty($_GET['kind']) ? intval($_GET['kind']) : 0;
	
	$comments = assign_comment($cmt->id, $cmt->type, $cmt->page, $cmt->kind);
	if($comments['pager']['plall']!=0){
	   $percent['pl1'] =floor((100*$comments['pager']['pl1'])/$comments['pager']['plall']);
	   $percent['pl2'] =floor((100*$comments['pager']['pl2'])/$comments['pager']['plall']);
	   $percent['pl3'] =floor((100*$comments['pager']['pl3'])/$comments['pager']['plall']);
	}
	else{
	   $percent['pl1'] =0;
	   $percent['pl2'] =0;
	   $percent['pl3'] =0;
		}
    
    $smarty->assign('comment_type', $cmt->type);
    $smarty->assign('id',           $cmt->id);
	$smarty->assign('cur',          $cmt->kind);
	$smarty->assign('percent',      $percent);
    $smarty->assign('username',     $_SESSION['user_name']);
    $smarty->assign('email',        $_SESSION['email']);
    $smarty->assign('comments',     $comments['comments']);
    $smarty->assign('pager',        $comments['pager']);

    /* 验证码相关设置 */
    if ((intval($_CFG['captcha']) & CAPTCHA_COMMENT) && gd_version() > 0)
    {
        $smarty->assign('enabled_captcha', 1);
        $smarty->assign('rand', mt_rand());
    }
    $result['error'] = 0;
    $result['content'] = $smarty->fetch("library/comments_list.lbi");
	
	
}
else{
  $result['error'] = 0;
    }

echo $json->encode($result);

/*------------------------------------------------------ */
//-- PRIVATE FUNCTION
/*------------------------------------------------------ */

/**
 * 添加评论内容
 *
 * @access  public
 * @param   object  $cmt
 * @return  void
 */
function add_comment($cmt)
{
    /* 评论是否需要审核 */
    $status = 1 - $GLOBALS['_CFG']['comment_check'];

    $user_id = empty($_SESSION['user_id']) ? 0 : $_SESSION['user_id'];
    $email = empty($cmt->email) ? $_SESSION['email'] : trim($cmt->email);
    $user_name = empty($cmt->username) ? $_SESSION['user_name'] : trim($cmt->username);
    $email = htmlspecialchars($email);
    $user_name = htmlspecialchars($user_name);

    /* 保存评论内容 */
    $sql = "INSERT INTO " .$GLOBALS['ecs']->table('comment') .
           "(comment_type, id_value, email, user_name, content, comment_rank, add_time, ip_address, status, parent_id, user_id) VALUES " .
           "('" .$cmt->type. "', '" .$cmt->id. "', '$email', '$user_name', '" .$cmt->content."', '".$cmt->rank."', ".gmtime().", '".real_ip()."', '$status', '0', '$user_id')";

    $result = $GLOBALS['db']->query($sql);
    clear_cache_files('comments_list.lbi');
    /*if ($status > 0)
    {
        add_feed($GLOBALS['db']->insert_id(), COMMENT_GOODS);
    }*/
    return $result;
}

?>