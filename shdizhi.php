<form>   
   
			<table class="o_tb1 addressEditTable listTable" cellspacing="0" cellpadding="0" border="0" style="display: table;">
				<tbody>
					<tr class="odd">
						<th width="85">	<span class="redStar">*</span>配送区域：</th>
						<td class="">
			            <select autocomplete="off" class="select" name="country" id="selCountries_0" onchange="regionChanged(this, 1, 'selProvinces_0')">
						  <option value="0">请选择国家</option>
								<option value="1">中国</option>
							</select>
								<select class="select" name="province" id="selProvinces_0" onchange="regionChanged(this, 2, 'selCities_0')">
								<option value="0">请选择省</option>
								</select>
							<select class="select" name="city" id="selCities_0" onchange="regionChanged(this, 3, 'selDistricts_0')">
							<option value="0">请选择市</option>
							   </select>
							   <select class="select" name="district" id="selDistricts_0" style="display:none">
							  <option value="0">请选择区</option>
								</select>
							<span class="errInfo"></span>
						</td>
					</tr>
					<tr class="">
						<th><span class="redStar">*</span>详细地址：</th>
						<td><input class="input w258" type="text" name="address" value="{$con.address}" id="address_0">
						<span class="errInfo"></span></td>
					</tr>
					<tr>
						<th>邮政编码：</th>
						<td class="last"><input class="input w98" type="text" name="zipcode" value="{$con.zipcode}" id="zipcode_0"></td>
					</tr>
					<tr class="">
						<th><span class="redStar">*</span>收货人姓名：</th>
						<td><input class="input w98" type="text" name="consignee" value="{$con.consignee}" id="consignee_0">
						<span class="errInfo"></span></td>
					</tr>
					<tr class="">
						<th>手机：</th>
						<td class="last"><input class="input w98" name="mobile" type="text" value="{$con.mobile}" id="mobile_0">
						　<span class="c666">或者固定电话：</span>
						<input class="input w98" type="text" name="tel" value="" id="tel_0">
						<em>（二者至少填写一项）</em>
						<span class="errInfo"></span>
						</td>
					</tr>
					<tr>
						<th>电子邮件地址：</th>
						<td class="last"><input class="input w258" name="email" type="text" value="{$con.email}" id="email_0">
						<em>用来接收订单提醒邮件，便于您及时了解订单状态</em>
						<span class="errInfo"></span>
						</td>
					</tr>
					<input type="hidden" autocomplete="off" name="address_id" value="{$con.address_id}">
				</tbody>
			</table>
		</form>
