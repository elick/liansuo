<?php

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}
$ua = strtolower($_SERVER['HTTP_USER_AGENT']);

$uachar = "/(nokia|sony|ericsson|mot|samsung|sgh|lg|philips|panasonic|alcatel|lenovo|cldc|midp|mobile)/i";
/*------------------------------------------------------ */
//-- Shopex系统地址转换
/*------------------------------------------------------ */
if (!empty($_GET['gOo']))
{
    if (!empty($_GET['gcat']))
    {
        /* 商品分类。*/
        $Loaction = 'category.php?id=' . $_GET['gcat'];
    }
    elseif (!empty($_GET['acat']))
    {
        /* 文章分类。*/
        $Loaction = 'article_cat.php?id=' . $_GET['acat'];
    }
    elseif (!empty($_GET['goodsid']))
    {
        /* 商品详情。*/
        $Loaction = 'goods.php?id=' . $_GET['goodsid'];
    }
    elseif (!empty($_GET['articleid']))
    {
        /* 文章详情。*/
        $Loaction = 'article.php?id=' . $_GET['articleid'];
    }

    if (!empty($Loaction))
    {
        ecs_header("Location: $Loaction\n");

        exit;
    }
}


//判断是否有ajax请求
$act = !empty($_GET['act']) ? $_GET['act'] : '';
if ($act == 'cat_rec')
{
    $rec_array = array(1 => 'best', 2 => 'new', 3 => 'hot');
    $rec_type = !empty($_REQUEST['rec_type']) ? intval($_REQUEST['rec_type']) : '1';
    $cat_id = !empty($_REQUEST['cid']) ? intval($_REQUEST['cid']) : '0';
    include_once('includes/cls_json.php');
    $json = new JSON;
    $result   = array('error' => 0, 'content' => '', 'type' => $rec_type, 'cat_id' => $cat_id);

    $children = get_children($cat_id);
    $smarty->assign($rec_array[$rec_type] . '_goods',      get_category_recommend_goods($rec_array[$rec_type], $children));    // 推荐商品
    $smarty->assign('cat_rec_sign', 1);
    $result['content'] = $smarty->fetch('library/recommend_' . $rec_array[$rec_type] . '.lbi');
    die($json->encode($result));
}

/*------------------------------------------------------ */
//-- 判断是否存在缓存，如果存在则调用缓存，反之读取相应内容
/*------------------------------------------------------ */
/* 缓存编号 */
$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang']));

if (!$smarty->is_cached('mall.dwt', $cache_id))
{
    assign_template();
	
    $position = assign_ur_here();
    $smarty->assign('page_title',      $position['title']);    // 页面标题
    $smarty->assign('ur_here',         $position['ur_here']);  // 当前位置

    /* meta information */
    $smarty->assign('keywords',        htmlspecialchars($_CFG['shop_keywords']));
    $smarty->assign('description',     htmlspecialchars($_CFG['shop_desc']));
    $smarty->assign('flash_theme',     $_CFG['flash_theme']);  // Flash轮播图片模板

    $smarty->assign('feed_url',        ($_CFG['rewrite'] == 1) ? 'feed.xml' : 'feed.php'); // RSS URL

    $smarty->assign('categories',      get_categories_tree()); // 分类树
  
    $smarty->assign('hot_goods',      get_recommend_goods('hot'));    // 推荐商品
   
   
    // 首页广告
	 $sql = " SELECT * FROM " . $ecs->table("ad") ." WHERE position_id = 31 ORDER BY ad_id DESC LIMIT 1";
	 $ad = $db->getRow($sql);
     //$ad['ad_code']="data/afficheimg/".$ad['ad_code'];
	
     $smarty->assign('ad', $ad);
	
	 $sql = "SELECT * FROM " . $ecs->table('topic') .
        " WHERE   " . gmtime() . " >= start_time and " . gmtime() . "<= end_time order by topic_id DESC LIMIT 2 ";
     $topics = $db->getAll($sql);
	 
	 foreach($topics as $key => $item){
		   $topics[$key]['end_time'] = date('Y-m-d-H-i-s', $item['end_time']);
		  }
		  
     $smarty->assign('topics',      $topics);
	
	 // 热卖广告
	 $sql = " SELECT * FROM " . $ecs->table("ad") ." WHERE position_id=24 ORDER BY ad_id ASC";
	 $ad2 = $db->getAll($sql);
	 foreach($ad2 as $key => $item_ad){
		   $ad2[$key]['ad_code']="data/afficheimg/".$item_ad['ad_code'];
		  }
	 
     $smarty->assign('ad2', $ad2);
	 
	  // 热卖广告4xiao
	 $sql = " SELECT * FROM " . $ecs->table("ad") ." WHERE position_id=26 ORDER BY ad_id ASC";
	 $ad4 = $db->getAll($sql);
	 foreach($ad4 as $key => $item_ad){
		   $ad4[$key]['ad_code']="data/afficheimg/".$item_ad['ad_code'];
		   $ad4[$key]['end_time'] = date('Y-m-d-H-i-s', $item_ad['end_time']);
		  }
	 
     $smarty->assign('ad4', $ad4);
	 
	 // caIzhuang热卖广告
	 $sql = " SELECT * FROM " . $ecs->table("ad") ." WHERE position_id=25 ORDER BY ad_id ASC";
	 $ad3 = $db->getAll($sql);
	 foreach($ad3 as $key => $item_ad){
		   $ad3[$key]['ad_code']="data/afficheimg/".$item_ad['ad_code'];
		  }
	 
     $smarty->assign('ad3', $ad3);
	 
	  // 彩妆热卖广告4小
	 $sql = " SELECT * FROM " . $ecs->table("ad") ." WHERE position_id=27 ORDER BY ad_id ASC";
	 $cai4 = $db->getAll($sql);
	 foreach($cai4 as $key => $item_ad){
		   $cai4[$key]['ad_code']="data/afficheimg/".$item_ad['ad_code'];
		   $cai4[$key]['end_time'] = date('Y-m-d-H-i-s', $item_ad['end_time']);
		  }
	 
     $smarty->assign('cai4', $cai4);
	
	// 福音礼品热卖热卖广告
	 $sql = " SELECT * FROM " . $ecs->table("ad") ." WHERE position_id=28 ORDER BY ad_id ASC";
	 $fy5 = $db->getAll($sql);
	 foreach($fy5 as $key => $item_ad){
		   $fy5[$key]['ad_code']="data/afficheimg/".$item_ad['ad_code'];
		  }
	 
     $smarty->assign('fy5', $fy5);
	 
	  // 福音礼品热卖热卖广告4小
	 $sql = " SELECT * FROM " . $ecs->table("ad") ." WHERE position_id=29 ORDER BY ad_id ASC";
	 $fyxt = $db->getAll($sql);
	 foreach($fyxt as $key => $item_ad){
		   $fyxt[$key]['ad_code']="data/afficheimg/".$item_ad['ad_code'];
		   $fyxt[$key]['end_time'] = date('Y-m-d-H-i-s', $item_ad['end_time']);
		  }
	 
     $smarty->assign('fyxt', $fyxt);
    

    /* 页面中的动态内容 */
    assign_dynamic('index');
}

$smarty->assign('promotion_goods', get_promote_goods()); // 首页特价商品

$smarty->display('mall.dwt', $cache_id);



?>


